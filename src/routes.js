
//component
import App from './pages/coffee/App'
import AppNothing from './pages/coffee/App_nothing'
import Home from './pages/coffee/home'
import ManageCoffee from './pages/coffee/manage/manage_coffee'
import ManageState from './pages/coffee/manage/manage_state'
import ManageOption from './pages/coffee/manage/manage_option'
import ReportPerDay from './pages/coffee/report/report_per_day'
import ReportPerCoffee from './pages/coffee/report/report_per_coffee'
import ReportPerOption from './pages/coffee/report/report_per_option'
import Confirmorder from './pages/coffee/order/confirm_order'

import PageLineLiff from './pages/lineliff/login'
import PageLineLiffInto from './pages/lineliff/into'
import PageClintChat from './pages/io/chat'
import PageSaleOnLiff from './pages/coffee/order/liff_sale_page'

const Routes = [
    {
        path: '/',
        component: App,
        indexRoute:{ component: Home },
        childRoutes:[
            {path: 'main',component: Home},
            {path: 'manage_coffee',component: ManageCoffee,
                childRoutes:[{
                    path: ':idcode',component:  ManageCoffee,
                }]
            },
            {path: 'manage_state',component: ManageState,
                childRoutes:[{
                    path: ':idcode',component:  ManageState,
                }]
            },
            {path: 'manage_option',component: ManageOption,
                childRoutes:[{
                    path: ':idcode',component:  ManageOption,
                }]
            },
            {path: 'report_per_day',component: ReportPerDay},
            {path: 'report_per_coffee',component: ReportPerCoffee},
            {path: 'report_per_option',component: ReportPerOption},
        ]
    },
    {
        path: '/lineliff',
        component: AppNothing,
        indexRoute:{ component: PageLineLiff },
        childRoutes:[
            {path: 'into',component: PageLineLiffInto},
        ]
    },
    {
        path: '/chat',
        component: AppNothing,
        indexRoute:{ component: PageClintChat },
    },
    {
        path: '/sale_page',
        component: AppNothing,
        indexRoute:{ component: PageSaleOnLiff },
    },
    {
        path: '/confirmOrder',
        component: AppNothing,
        indexRoute:{ component: Confirmorder },
    }
]

const loginMiddleWare = (req, res, next) => {
    next();
}

export default Routes