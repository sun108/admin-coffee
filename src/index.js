import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import {Router , browserHistory} from 'react-router'
import Routes from './routes'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import Reducer from './reducer'
import './i18nextConf'
import {checkCookiePack_existAndNotExpire} from './services/authen/authen'
import {route_isNotAuthen} from './services/std_service/getString'

require("dotenv").config();

const store = createStore(Reducer)
store.subscribe(() => {
  // console.log('subscribe', store.getState())
});
ReactDOM.render(
  <Provider store={store}>
    <Router
      history={browserHistory}
      routes={Routes}
    />
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();