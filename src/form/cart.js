import React , { Component , useState, useEffect , Fragment } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { Link } from "react-router";
import {Router , browserHistory} from 'react-router'

import { connect } from 'react-redux'

//component
import {StdToastMz_error,StdToastMz_success,StdToastMz_info,StdToastMz_warning} from '../component/toast/std_toast.js'
import {StdIcon_yes} from '../component/icon/std_icon'
import {StdProgessBar_sizeBig001_loadLoop} from '../component/progressBar/std_progressBar'
import {StdBtn_hideCloseModal,StnBtn_close,StnBtn_done,StdBtnModal_delete,StdBtnIconOnly_Direct,StdBtn_add} from '../component/button/std_button'
import {StdOption_defaultFirstOption} from '../component/select/std_option';
import {StdImg_personImgTable} from '../component/image_profile/std_imageProfile'
import {item_order_cart} from '../component/product/item'

//service
import {add_arrItem} from '../services/mockup/arrItem'
import {convertIntToMoneyFormat} from '../services/std_service/getString'

//style
import style_cart from '../style/cart'

//icon
import UpdateIcon from '@material-ui/icons/Update';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import LocalShippingIcon from '@material-ui/icons/LocalShipping';

//mode : view , update , delete
class Class_cart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            globeStr: props.globe ,
            unitData: props.unitData,
            amount_inCart:null , arrayItem_inCart:null,
            title: props.title, load: true ,open: false ,
            totalProce: 0 ,
            dataSet: props.arrCart, arrIMastertem: null,
            mode: `view` , actionModeArrData:[],
            updateNewAmount:0,
            height: 0 ,wording: props.wording,
            winSize: props.winSize
        }
    }
    componentDidMount() {
        this.setDataPack(this.props)
    }
    componentWillReceiveProps(props){
        this.setDataPack(props)
    }
    setDataPack = async(props)=>{
        await this.setState({load : true})

        await this.setState({ 
            arrIMastertem: props.unitData,
            amount_inCart: props.arrOrder.amount_cart,
            arrayItem_inCart: props.arrOrder.array_cart,
            height: props.window.height
        })
        if(props.arrOrder.array_cart.length > 0){
            await this.calPriceTotal(props)
        }else{
            await this.setState({ totalProce : 0})
        }

        await this.setState({load : false})
    }
    setOpen =(val)=>{
        this.setState({ open: val})
    }
    onChangeInputAmount=(event, idcode ,oldAmount)=>{
        var amount = event.target.value
        var itemFind = this.state.arrIMastertem.filter(x=>x['idcode'] === idcode);
        if(itemFind.length>0 && idcode !== null){
            if(amount <= 100){
                if(amount === 0 || amount === '' || amount === null){
                    amount = 0
                }
            }else{
                amount = 100
            }
            this.setState({ updateNewAmount : amount })
        }
    }
    updateItemToCart =async(idcode)=>{
        var findItemState = this.state.arrIMastertem.filter(x=>x['idcode'] === idcode);
        if(findItemState.length > 0){
          this.props.actionReducer('updateItemInCart',{"idcode":this.state.actionModeArrData.idcode, "amount":this.state.updateNewAmount})
        }else{
          StdToastMz_error('กรุณาใส่จำนวนสินค้าด้วยค่ะ')
        }
    }
    calPriceTotal =async(props)=>{
        var totalPrice = 0;
        var id = ''
        var num = 0;
        props.arrOrder.array_cart.map(x=> {
            id = x.toString().replace(`${process.env.REACT_APP_PREFIX_ORDER}`,'')
            var findItem = props.unitData.filter(v=>v['idcode'] === id && v[`customer_view`] && !v[`Out_of_stock`]);
            if(findItem.length > 0){
                totalPrice = parseInt(totalPrice) + ( parseInt(props.arrOrder.amount_cart[num]) * parseInt(findItem[0].promotion))
            }
            num++;
        })
        await this.setState({ totalProce : totalPrice})
    }
    funcClickLink =(link)=>{
        // this.props.onClose.bind(this)
        browserHistory.push(link)
    }
    
    render(){
        const myTextfield_number =(theme)=>({
            textField: {
                borderBottom: `unset !important`,
                paddingLeft: `10px !important`,
                paddingRight: `10px !important`,
                textAlign: `end !important`
            }
        })
        if(!this.state.load && this.state.wording.th !== undefined){
            var wording = this.state.wording[this.props.lang]
            switch(this.state.mode){
                case 'view':
                    return (
                        <React.Fragment>
                            <style>{style_cart}</style>
                            <div style={{backgroundColor: `white` , position: `fixed` , width: `100%`}}><h4 className={`myfont_cart_modal_head`} >{`${wording.cart.title}`}</h4></div>
                            <Table style={{ backgroundColor: `white` ,marginTop: `60px` , paddingBottom: `100px` , tableLayout: `fixed` }}>
                                <TableHead >
                                    <TableRow>
                                        <TableCell align="center"><p className={`myfont_cart_table_head`}>{`${wording.wording.order}`}</p></TableCell>
                                        <TableCell align="center"><p className={`myfont_cart_table_head`}>{`${wording.wording.price}`}</p></TableCell>
                                        <TableCell align="center"><p className={`myfont_cart_table_head`}>{`${wording.wording.amount}`}</p></TableCell>
                                        <TableCell align="center"><p className={`myfont_cart_table_head`}>{`${wording.wording.manage}`}</p></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {query(
                                        this.props.lang ,
                                        this.state.amount_inCart ,
                                        this.state.arrayItem_inCart , 
                                        this.state.arrIMastertem , 
                                        this.func_click ,
                                        this.funcClickLink , 
                                        this.state.height ,
                                        wording ,
                                        this.props.onClose
                                    )}
                                </TableBody>
                            </Table>
                            <div >
                                <div style={{ backgroundColor: `white` , position: `fixed` , bottom: `0px` , display: `inline-block` , width: `${(this.state.winSize.w) -20}px` }}>
                                    <div style={{display: 'flex',alignItems: 'center',flexWrap: 'wrap' , paddingBottom: `0px`}}>
                                        <Grid container spacing={2}>
                                            <Grid item item xs={8}>
                                                <p className={`pCartWarningBottomTotalPrice`}>{`${wording.wording.total_price_all}`}</p>
                                            </Grid>
                                            <Grid item item xs={4}>
                                                <p className={`pCartWarningBottomTotalPrice`}>{`${convertIntToMoneyFormat(this.state.totalProce)} THB`}</p>
                                            </Grid>
                                        </Grid>
                                    </div>
                                    <div style={{marginBottom: `15px`}}>
                                        <Button className="modal-close" style={{backgroundColor: `green` , color: `white` , display: this.state.totalProce > 0 ? `unset` : `none`}} flat modal="Close" node="button" waves="green" button component={Link} to={`/order_summary`} onClick={this.props.onClose}>
                                            {`${wording.cart.order_summary}`}
                                        </Button>&nbsp;&nbsp;&nbsp;
                                        <Button className="modal-close" style={{backgroundColor: `red` , color: `white`}} flat modal="Close" node="button" waves="green" onClick={this.props.onClose}>Close</Button>
                                    </div>
                                </div> 
                                
                                {/* <div style={{ backgroundColor: `white` , position: `fixed` , width: `100%` , bottom: `80px` , display: `inline-block` }}>
                                    <div style={{display: 'flex',alignItems: 'center',flexWrap: 'wrap'}}><LocalShippingIcon fontSizeSmall />&nbsp;&nbsp;&nbsp;
                                        <p className={`pCartWarningBottomPromotion`}>จัดส่งฟรี! เมื่อซื้อครบ 500 บาทยกเว้นหมอน</p>
                                    </div>
                                </div> */}
                            </div>
                        </React.Fragment>
                    )
                break;
                case 'update':
                    return (
                        <React.Fragment>
                            <style>{style_cart}</style>
                            <p className={`myfont_cart_modal_head`}>{`${wording.cart.edit_your_cart}`}</p>
                            <h4>{this.state.actionModeArrData.title_name}</h4>
                            <center>
                                <TextField 
                                    className={`price`}
                                    type="number" placeholder="0"
                                    label={wording.wording.amount} InputLabelProps={{ shrink: true,}}
                                    variant="outlined" 
                                    defaultValue={this.state.actionModeArrData.amount}
                                    InputProps={{ style: {
                                        borderBottom: `0px !important`,
                                        paddingLeft: `10px !important`,
                                        paddingRight: `10px !important`,
                                        textAlign: `end !important`,
                                    } }}
                                    onChange={(e) =>this.onChangeInputAmount(e,this.state.actionModeArrData.idcode , this.state.actionModeArrData.amount)}
                                />
                            </center>
                            {component_button(this.state.actionModeArrData.idcode , this.func_click, this.func_action ,wording)}
                        </React.Fragment>
                    );
                break;
            }
        }else{
            return (<div> <StdProgessBar_sizeBig001_loadLoop /> </div>) 
        }
    }
    func_click=(mode,x)=>{
        this.setState({ mode : mode , actionModeArrData :x , updateNewAmount :x.amount })
    }
    func_action=(act,idcode)=>{
        var amount = 0
        amount = this.state.updateNewAmount
        if(amount === 0){
            amount = this.state.actionModeArrData.amount
            this.setState({ updateNewAmount :this.state.actionModeArrData.amount })
        }
        if(act === 'update'){
            if(amount !== null && amount !== ''){
                this.props.actionReducer('updateItemInCart',{"idcode":idcode , "amount":amount})
                this.func_click(`view`,``)
            }
        }else if(act === 'delete'){
            this.props.actionReducer('deleteItemInCart',{"idcode":idcode , "amount":amount})
            this.func_click(`view`,``)
        }
        
    }
}

function query(lang,cartAmount ,cartData , arrItem , func_click , funcLinkUnit , height ,wording ,onCloseDrawer){
    var element = [];
    var num = 0;
    var id = ''
    cartData.map(x=> {
        id = x.toString().replace(`${process.env.REACT_APP_PREFIX_ORDER}`,'')
        var findItem = arrItem.filter(v=>v['idcode'] === id);
        // console.log(findItem[0][lang][0])
        if(findItem.length > 0){
            var price = cartAmount[num] * findItem[0].promotion
            element.push(
            <TableRow>
                <TableCell align="start" colSpan={3} component="td" scope="row" style={{border: `0px`}}>
                    <p id={`title_name_${id}`} className={`myfont_cart_table_body modal-close`}
                        onClick={ ()=>{funcLinkUnit(`/unit_product/${findItem[0].url}`); onCloseDrawer();} }
                    >{findItem[0][lang][0]['title']}</p>{/* <Link to={`unit_product/${findItem[0].url}`}></Link> */}
                </TableCell>
                <TableCell align="center" colSpan={1} component="td" scope="row" style={{border: `0px`}}>
                    <img src={`${process.env.REACT_APP_URL_ASSETS}unit_product/${findItem[0].idcode}/${findItem[0].img}`}
                        onClick={ ()=>{funcLinkUnit(`/unit_product/${findItem[0].url}`); onCloseDrawer(); } } 
                        style={{width:`50%`}}
                    />
                </TableCell>
            </TableRow> )
            element.push( 
                <TableRow>
                    <TableCell align="center"><p className={`myfont_cart_table_body`}>{cartAmount[num]}</p></TableCell>
                    <TableCell align="center"><p className={`myfont_cart_table_body`}>{convertIntToMoneyFormat(findItem[0].promotion)}</p></TableCell>
                    <TableCell align="center"><p className={`myfont_cart_table_body`}>{convertIntToMoneyFormat(price)}</p></TableCell>
                    <TableCell align="center">
                        <Button 
                            onClick={func_click.bind(this,`update`,{
                                'title_name':findItem[0][lang][0]['title'] , 'amount':cartAmount[num] , 'idcode':id
                            })}
                            className={`myfont_cart_table_body`} style={{backgroundColor: `blue` , color: `white`}}
                        >
                            {`${wording.wording.edit}`}
                        </Button>
                        {/* <IconButton
                            onClick={func_click.bind(this,`update`,{
                                'title_name':findItem[0][lang][0]['title'] , 'amount':cartAmount[num] , 'idcode':id
                            })}
                            variant="contained"
                            color="primary"
                            className={`myfont_cart_table_body`}
                            style={{
                                backgroundColor: 'blue' ,
                                display: 'inline',
                                // borderRadius: `100%`,
                                alignItems: `center`,
                                justifyContent: `center`,
                                padding: `5px`,
                                color: `white`
                            }}
                        >
                            <UpdateIcon style={{color: `white`}}/>
                        </IconButton>*/}
                    </TableCell> 
                </TableRow>
            );
        }
        num++;
    })
    if(element.length <= 0){
        element.push(<p>no item</p>)
    }
    return element
}

function component_button(idcode,func_click , func_action ,wording){
    return (
        <div>
            <TableRow>
                <TableCell component="th">
                    <Button style={{color: `white` , backgroundColor: `blue`}} onClick={func_click.bind(this,`view`,``)}><KeyboardBackspaceIcon/>{`back`}</Button>
                </TableCell>
                <TableCell component="th">
                    <Button style={{color: `white` , backgroundColor: `green`}} onClick={func_action.bind(this,`update`,idcode)}><CheckCircleIcon/>{`ok`}</Button>
                </TableCell>
                <TableCell component="th">
                    <Button style={{color: `white` , backgroundColor: `red`}} onClick={func_action.bind(this,`delete`,idcode)}><DeleteForeverIcon/>{`${wording.wording.delete_unit}`}</Button>
                </TableCell>
            </TableRow>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        lang: state.page_getLangToState,
        arrOrder : state.func_actionCart,
        window: state.getWindows,
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actionReducer: (type,payload) => {
          dispatch({type: type,payload:payload})
        }
    }    
  }
const Form_cart = connect(mapStateToProps,mapDispatchToProps)(Class_cart)
export { Form_cart};