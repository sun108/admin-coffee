import React , { Component , useState, useEffect , Fragment } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';
import AvatarGroup from '@material-ui/lab/AvatarGroup';
import Chip from '@material-ui/core/Chip';
import FaceIcon from '@material-ui/icons/Face';
import DoneIcon from '@material-ui/icons/Done';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { connect } from 'react-redux'

//component
import {StdToastMz_error,StdToastMz_success,StdToastMz_info,StdToastMz_warning} from '../component/toast/std_toast'
import {StdIcon_yes} from '../component/icon/std_icon'
import {StdProgessBar_sizeBig001_loadLoop} from '../component/progressBar/std_progressBar'
import {StdBtn_hideCloseModal,StnBtn_close,StnBtn_done,StdBtnModal_delete,StdBtnIconOnly_Direct,StdFabBtn_done} from '../component/button/std_button'
import {StdOption_defaultFirstOption} from '../component/select/std_option';
import {StdImg_personImgTable} from '../component/image_profile/std_imageProfile'

//service
import {StdDate_myFormate} from '../services/std_service/getDate'
import {std_header_upload , std_header} from '../services/std_service/header'
import {getStringTitle , checkIsHasSpace_bool , checkString_EnglishAndNumberOnly_bool , checkStringInNotNullOrEmpty_bool} from '../services/std_service/getString'
import {service_insert,service_update,service_delete} from '../services/dayoff/dayoffPerson_service'

class Class_form_select_option extends Component {
    constructor(props) {
        super(props);
        this.state = {
            load: false,
            globeStr: props.globeStr ,
            title: props.title,
            mode: props.mode,
            nowSelectData: null,
            dataSource_person: props.dataSource_person,dataRow_selectInsert: [],
            dataSet_personChip: [] ,dataSet_personAutocomplete: [] ,dataSet_activeApploval: []
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    async componentWillReceiveProps(props) {
        await this.setState({
            nowSelectData: props.nowSelectData,
            load: true
        })
        await this.packDataToPersonChip()
        await this.packDataSetPersonAutoComplete()
        await this.packDataToOldApploval()
        this.setState({load: false})
    }
    packDataToOldApploval =()=>{
        var dataSet = []
        if(this.state.nowSelectData.applovalIdcode !== null){
            this.state.nowSelectData.applovalIdcode.map((x)=>{
                dataSet.push(x)
            })
            this.setState({dataSet_activeApploval : dataSet})
        }else{
            this.setState({dataSet_activeApploval : []})
        }
    }
    packDataToPersonChip =async()=>{
        var lang = this.props.lang ,dataSet = []
        this.state.dataSource_person.map((x)=>{
            dataSet.push( 
                {[x.idcode]:<Chip
                id={`chip_apploval_${x.idcode}`}
                avatar={<Avatar src={`${process.env.REACT_APP_URL_PICTURE_PERSON}${x.idcode}.jpg`} />}
                label={`${x[`firstname_${lang}`]} ${x[`lastname_${lang}`]}`}
                onDelete={evt=>this.handleDelete(x.idcode)}/>}
            )
        })
        this.setState({dataSet_personChip : dataSet})
    }
    packDataSetPersonAutoComplete =()=>{
        var dataArr = Array() ,fullName = null ,img = null ,idcode = null
        this.state.dataSource_person.map((row) => {
            img = <StdImg_personImgTable image={row.idcode}  />
            fullName = `\u00A0${row.firstname_th} ${row.lastname_th} (${row.nickname_th}) - ${row.firstname_en} ${row.lastname_en} (${row.nickname_en})`
            idcode = row.idcode
            dataArr.push({'img':img , 'fullName':fullName , 'idcode':idcode})
        })
        this.setState({dataSet_personAutocomplete : dataArr})
    }
    async handleSubmit(event) {
        event.preventDefault();
        this.update_approval()
    }
    update_approval = async(newData, oldData)=>{
        var result = null ,arrObj = []
        if(this.state.nowSelectData.canUpdate){
            //update
            var dataObj = this.setDataPackObj_forInsert('update')
            var arrObj = [
                {"idcode":this.state.nowSelectData.dayOffIdcode},
                this.setObj_data(dataObj)
            ]
            result = await service_update(JSON.stringify(arrObj))
            this.setAfterAction(result)
        }else{
            //insert
            var dataObj = await this.setDataPackObj_forInsert('create')
            arrObj = await [this.setObj_data(dataObj)]
            result = await service_insert(JSON.stringify(arrObj))
            this.setAfterAction(result)
        }
    }
    setObj_data =(dataPack)=>{
        return(
            {
                "idcode_person":dataPack.idcode_person , 
                "cut_round_date":dataPack.cut_round_date , 
                "data":dataPack.data,
                "approval":dataPack.approval
            }
        )
    }
    setDataPackObj_forInsert =(mode)=>{
        var dataset = []
        if(mode === 'create'){
            return({
                'idcode_person':this.state.nowSelectData.idcode , 
                'cut_round_date':'' ,
                'data':[] ,
                'approval':this.state.dataSet_activeApploval
            })
        }else if(mode === 'update'){
            return({
                'approval':this.state.dataSet_activeApploval
            })
        }
    }
    setAfterAction =(result)=>{
        var self = result;
        setTimeout(() =>{
            if(self){
                StdToastMz_success(
                    getStringTitle(this.state.globeStr,'success_manageData',this.props.lang)
                )
                document.getElementById("close_modal").click();
                this.props.refreshPage()
            }else{
                StdToastMz_error(
                    getStringTitle(this.state.globeStr,'fail_pleaseCheckYourData',this.props.lang)
                )
            }
        },500)
    }
    form_insertData = async()=>{
        this.setState({load:true})
        var arrObj = new Array()
        arrObj.push(this.state.idcode)
        var result = await service_delete(JSON.stringify(arrObj))
        setTimeout(() =>{
            if(result){
                StdToastMz_success(
                    getStringTitle(this.state.globeStr,'success_manageData',this.props.lang)
                )
                this.props.refreshPage()
            }else{
                StdToastMz_error(
                    getStringTitle(this.state.globeStr,'fail_pleaseCheckYourData',this.props.lang)
                )
                this.props.refreshPage()
            }
        },500)
    }
    handleDelete =(id)=>{
        var arr = this.state.dataSet_activeApploval
        arr = arr.filter(x => x !== id)
        this.setState({dataSet_activeApploval: arr})
    }
    handleAdd =(evt)=>{
        if(evt.currentTarget !== undefined && evt.currentTarget.id === 'btnAdd_personToPosition'){
            var dataRow = null
            this.state.dataRow_selectInsert.idcode !== null || this.state.dataRow_selectInsert.idcode !== undefined ? dataRow = this.state.dataRow_selectInsert.idcode : dataRow= null
            if(dataRow !== null && dataRow !== undefined){
                if(!this.state.dataSet_activeApploval.includes(dataRow)){
                    var arr = this.state.dataSet_activeApploval
                    arr.push(dataRow)
                    // console.log(`=============================${arr}`)
                    this.setState({dataSet_activeApploval: arr})
                }else{
                    //has this person in list approval
                }
            }
        }
    }
    onTagsChange = (event, values) => {
        console.log(values)
        this.setState({dataRow_selectInsert :values })
    }
    render(){
        var lang = this.props.lang
        if(this.state.load){
            return (<div> <StdProgessBar_sizeBig001_loadLoop /><StdBtn_hideCloseModal /> </div>) 
        }else{
            return (
                <div>
                    <StdBtn_hideCloseModal />
                    {/* <center>{getStringTitle(this.state.title , 'modal_delete_003' , lang)}</center> */}
                    {/* <br /><center><h4>{this.state.idcode}</h4></center> */}
                    <Divider /><br />
                    <h5>ผู้อนุมัติปัจจุบันที่เลือกไว้</h5>
                    {this.state.dataSet_activeApploval.map((x)=>
                        this.state.dataSet_personChip.find(y=>y[x])[x]
                    )}
                    <br />
                    <Divider /><br />
                    <Grid container spacing={3}>
                        <Grid item xs={10}>
                            <Autocomplete
                                options={this.state.dataSet_personAutocomplete}
                                getOptionLabel={(option) => option.fullName}
                                onChange={this.onTagsChange}
                                style={{ width: '100%' }}
                                renderOption={(option) => (
                                    <Fragment>
                                        {option.img}{option.fullName}
                                    </Fragment>
                                )}
                                renderInput={(params) => 
                                    <TextField
                                        {...params}
                                        label='เลือกรายชื่อเพื่อเพิ่มผู้อนุมัติ'
                                        variant="outlined"
                                        inputProps={{
                                            ...params.inputProps,
                                            autoComplete: 'new-password',
                                        }}
                                    />
                                }
                            />
                        </Grid>
                        <Grid item xs={2}>
                            <StdFabBtn_done 
                                id='btnAdd_personToPosition' 
                                onClick={evt => this.handleAdd(evt)} 
                            />
                        </Grid>
                    </Grid>
                    <form id='form_com_createUpdate' noValidate autoComplete="off" onSubmit={this.handleSubmit}>
                        <center >
                            <StnBtn_done form='form_com_createUpdate' type="submit" />&nbsp;
                            <StnBtn_close className='modal-close'/>
                        </center>
                    </form>
                </div>
            )
        }
    }
}
function mapStateToProps(state) {
    return {
        lang: state.page_getLangToState
    }
}
const Form_select_option  = connect(mapStateToProps)(Class_form_select_option)
export { Form_select_option};