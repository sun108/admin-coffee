import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import MenuItem from '@material-ui/core/MenuItem';
import { Link } from "react-router";
import { connect } from 'react-redux'
import LinearProgress from '@material-ui/core/LinearProgress';
import Grid from '@material-ui/core/Grid';
import Badge from '@material-ui/core/Badge';
import Typography from '@material-ui/core/Typography';

//component
import {StdBtn_hideCloseModal,StnBtn_close,StnBtn_done,StdBtnModal_delete} from '../component/button/std_button'

//style
import styleCode from '../style/globe_admin.js'
import useStyles  from '../style/app.js'
import breadcrumbStyle from '../style/breadcrumbs'

//icon
import MenuIcon from '@material-ui/icons/Menu';

//FontAwesomeIcon
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas, faChartLine, faFileContract, faPercentage, faVial, faTag, faLayerGroup, faShippingFast, faUserShield} from "@fortawesome/free-solid-svg-icons";
import { fab , faLine , faFacebookSquare } from "@fortawesome/free-brands-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";
import { faEnvelope , faPhone } from "@fortawesome/free-solid-svg-icons";

//materialize
import 'materialize-css';

//react-responsive-carousel
import "react-responsive-carousel/lib/styles/carousel.min.css";
import '../style/font.css'

//service
import getProfile from '../services/profile/getYourProfile'

//form
import {Form_cart} from '../form/cart'

class ResponsiveDrawer extends Component {
  constructor() {
    super();
    this.state = {
      mobileOpen: false,
      cartOpen:false,
      arrOrder: [], unitProduct:[] ,
      oldArrOrder: null ,load : true ,profile: [],
    }
  }
  componentDidMount() {
    // this.reloadData(true, null, this.props)
    this.start_page(this.props)
  }
  componentWillReceiveProps(props) {
    // this.reloadData(false, props)
    this.start_page(props)
  }
  handleDrawerToggle = () => {
    this.setState(state => ({ mobileOpen: !state.mobileOpen }));
  };
  handleCartDrawerToggle = () => {
    this.setState(state => ({ cartOpen: !state.cartOpen }));
  };
  handleChangeFotter =(event,value)=>{
    this.setState({selectFooter:value})
  }
  start_page = async(props)=>{
    var profile = await getProfile()
    // console.log(profile)
    this.setState({ load: false , profile : profile })
  }
  render(){
    const heightData = 50
    const load = <LinearProgress color="secondary" />
    const { classes, theme } = this.props;
    var childrenProps = this.props.children
    const drawer = (
      <div className={classes.toolbar}>
          <div>
            <style>{ [styleCode ,breadcrumbStyle ] }</style>
          </div>
          <div style={{backgroundColor: `#232323`}}>
            <img src={`/assets/img/logo_voodoo.png`} 
              style={{width: `100px`}} 
            />
          </div>
          <Divider />
          {componentMenu(`/mng`,this.handleDrawerToggle,<FontAwesomeIcon icon={faChartLine} />,`Dashboard`)}<Divider />
          {componentMenu(`/mng/orderquery`,this.handleDrawerToggle,<FontAwesomeIcon icon={faFileContract} />,`Query Order`)}<Divider />
          {componentMenu(`/mng/master_promotion`,this.handleDrawerToggle,<FontAwesomeIcon icon={faPercentage} />,`Promotion`)}<Divider />
          {componentMenu(`/mng/master_unit`,this.handleDrawerToggle,<FontAwesomeIcon icon={faVial} />,`Manage Unit Product`)}<Divider />
          {componentMenu(`/mng/master_category`,this.handleDrawerToggle,<FontAwesomeIcon icon={faTag} />,`Manage Category (tag)`)}<Divider />
          {componentMenu(`/mng/master_collection`,this.handleDrawerToggle,<FontAwesomeIcon icon={faLayerGroup} />,`Manage Collection`)}<Divider />
          {componentMenu(`/mng/master_delivery`,this.handleDrawerToggle,<FontAwesomeIcon icon={faShippingFast} />,`Manage Delivery Cost`)}<Divider />
          {componentMenu(`/mng/master_admin`,this.handleDrawerToggle,<FontAwesomeIcon icon={faUserShield} />,`Admin`)}<Divider />
          {/* <PriorityHighIcon fontSize="small" /> */}
      </div>
    );

    const StyledBadge = withStyles((theme) => ({
      badge: {
        right: -3,
        top: 13,
        border: `2px solid ${theme.palette.background.paper}`,
        padding: '0 4px',
      },
    }))(Badge);

    const head = (
      <React.Fragment>
        <AppBar position='fixed' style={{height: `${heightData}px` , backgroundColor: `black` , zIndex: 1}}>
          <Toolbar>
            <Hidden only={['xs']}>
              <div>
                  <IconButton edge="" color="inherit" aria-label="menu" style={{marginTop: `-35px` , [theme.breakpoints.up('md')]: {display: `none`}}}
                    onClick={this.handleDrawerToggle} >
                      <MenuIcon />
                  </IconButton>
                <img src={`/assets/img/logo_voodoo.png`} style={{width: `100px`}} />
              </div>
            </Hidden>
            <div className={classes.root}>
              <Grid container spacing={3}>
                <Grid container item sm={12} xs={12}>
                  <Grid item sm={1} xs={1}>
                    <IconButton edge="" className={classes.menuButton} color="inherit" aria-label="menu"
                      onClick={this.handleDrawerToggle}  >
                      <MenuIcon />
                    </IconButton>
                  </Grid>
                  <Grid item xs={10}>
                    <center>
                      <img className={classes.menuButton} src={`/assets/img/logo_voodoo.png`} style={{width: `100px`}} />
                    </center>
                  </Grid>
                  <Grid item xs={1}>
                    <center>
                      <img src={`${process.env.REACT_APP_URL_PICTURE_PERSON}${this.state.profile.idcode}.jpg`} style={{width: `100px` , borderRadius: `50%` , width: `50px` , height: `50px`}} />
                    </center>
                  </Grid>
                </Grid>
              </Grid>
            </div>
          </Toolbar>
        </AppBar>
      </React.Fragment>
    );

    if(this.state.load){
      return (
        <div>
          <div>{head}</div>
          <div style={{width:`100%`}}>{load}</div>
        </div>
      )
    }else{
      return (
        <div className={classes.root}>
          <CssBaseline />
          <StdBtn_hideCloseModal /> 
            {head}
            {/* <Hidden smUp implementation="css">
              <Drawer container={this.props.container} variant="temporary" style={{zIndex: '1'}}
                anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                open={this.state.mobileOpen} onClose={this.handleDrawerToggle}
                classes={{  paper: classes.drawerPaper, }}
                ModalProps={{ keepMounted: true,}}
              >
                {drawer}
              </Drawer>
            </Hidden> */}
            <Hidden xsDown implementation="css">
              <nav className={classes.drawer} aria-label="mailbox folders">
                <Drawer
                  classes={{
                    paper: classes.drawerPaper,
                  }}
                  variant="permanent"
                  open
                >
                  {drawer}
                </Drawer>
              </nav>
            </Hidden>
          <div style={{marginTop: `${heightData}px`,width: `100%`}}>{childrenProps}</div>
        </div>
        )
    }
  }
}

function componentMenu(tlink ,handleDrawerToggle ,icon ,string){
  return (
    <MenuItem 
      className={`myfont_content_std`} button 
      component={Link} to={tlink} onClick={handleDrawerToggle}
    >
      <ListItemIcon>
        {icon}
      </ListItemIcon>
      <Typography variant="inherit">{string}</Typography>
    </MenuItem>
  )
}

function queryDataCart(dataArray){
  var res = ``
  dataArray.array_cart.map(x=>{
    res = res + `${x.replace(`${process.env.REACT_APP_PREFIX_ORDER}`,``)},`
  })
  return res
}

ResponsiveDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
  window: PropTypes.func,
};
function mapStateToProps(state) {
  return {
    lang: state.page_getLangToState,
    arrOrder : state.func_actionCart,
    getProfile: state.getProfile
  }
}
function mapDispatchToProps(dispatch) {
  return {
    actionReducer: (type,payload) => {
      dispatch({type: type,payload:payload})
    }
  }    
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(useStyles, { withTheme: true })(ResponsiveDrawer))