import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Radium, {StyleRoot} from 'radium';
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid';
import Chip from '@material-ui/core/Chip';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import MaterialTable from 'material-table';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Select from '@material-ui/core/Select';
import Card from '@material-ui/core/Card';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Drawer from '@material-ui/core/Drawer';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';

//component
import {
  StdBtnIconOnly_upToTop,
  StdFabBtn_create,
  StdFabBtn_done,
  StdFabBtn_delete
} from '../../../component/button/std_button'

//style
import styleCode from '../../../style/menu'
import useStyles  from '../../../style/app.js'

//materialize
import 'materialize-css';
import { Collapsible, CollapsibleItem } from 'react-materialize';

//layout
import {column_query_unit} from '../../layouts/query_order/query_unit'

//form
import {Form_manageTag} from './form_manageTag'

//react-responsive-carousel
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

//icon
import AddCircleIcon from '@material-ui/icons/AddCircle';
import UpdateIcon from '@material-ui/icons/Update';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import LinkIcon from '@material-ui/icons/Link';
import DoneAllIcon from '@material-ui/icons/DoneAll';

//service
import {getStringTitle , getConsoleError ,generateString_string} from '../../../services/std_service/getString'
import FetchData from '../../../services/fetch_data/fetch'
import {funcClickLink} from '../../../services/link/service_link'
import {adminSendToServer_unit , adminSendToServer_unitDelete} from '../../../services/admin/unit'
import {admin_upload_unit_img , admin_upload_unit_img_multiple} from '../../../services/upload_picture/upload_picture'
import {adminSendToServer_tagDelete , adminSendToServer_tagAddition} from '../../../services/admin/tag'

const _ = require('lodash');

class MasterUnit extends Component {
  constructor() {
    super();
    this.state = {
      open: true,mobileOpen: false,lang: null,load: true,errString: null ,
      listArrCat: [],listArrCollection: [],listArrUnit: [],listArrRelative: [],listArrRibbon:[],
      startDate: null , toDate: null, listArrUnitTable: [] ,
      productIdCode: null ,showPromotion: false, openDrawerTag: false, openDrawerDelete: false,

      listArrRelative_forDelete: [], listArrRelative_forAddition: [],load_actionDrawerTag: false,

      fm_title_th: null,fm_title_en: null,fm_content_th: null,fm_content_en: null,
      fm_mainImg:null ,fm_subImg: [], fm_price: 0 , fm_promotion: 0, fm_notifyNumb: null ,fm_size: null ,
      fm_cv:null, fm_url: null, fm_collection: null,fm_Out_of_stock: false,

      load_uploadImg: false,
    }
    // this.addItemToCart = this.addItemToCart.bind(this);
    this.duration = 1;
    this.clickUploadImg_single = this.clickUploadImg_single.bind(this);
    this.clickUploadImg_multiple = this.clickUploadImg_multiple.bind(this);
    this.clickUploadImg_singleNewCreate = this.clickUploadImg_singleNewCreate.bind(this)
  }
  componentDidMount() {
    this.start_page(this.props);
  }
  componentWillReceiveProps(props) {
    this.start_page(props);
  }
  start_page= async(props)=>{
    this.setState({load:true})
    var url =null ; var data= []
    var idcode = props.params.productCode

    if(idcode !== null && idcode !== `` && idcode !== undefined && idcode !== `undefined`){
        await this.setState({ productIdCode: idcode })
        if(idcode !== `create`){
          url = `${process.env.REACT_APP_URL_API}query/collection/unit_product?idcode=${idcode}`
          try{ data.push ({"unit_product":await FetchData(url,'unit_product',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}
        }else{
          url = `${process.env.REACT_APP_URL_API}query?unit_product=all`
          try{ data.push ({"unit_product":await FetchData(url,'unit_product',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}
        }
    }else{
        await this.setState({ productIdCode: null })
        url = `${process.env.REACT_APP_URL_API}query?unit_product=all`
        try{ data.push ({"unit_product":await FetchData(url,'unit_product',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}
    }

    url = `${process.env.REACT_APP_URL_API}query?catagory_product=all&system_ribbon=all`
    try{ data.push ({"catagory_product":await FetchData(url,'catagory_product',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}

    url = `${process.env.REACT_APP_URL_API}query?relative_product=all`
    try{ data.push ({"relative_product":await FetchData(url,'relative_product',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}
    
    url = `${process.env.REACT_APP_URL_API}query?collection_type=all`
    try{ data.push ({"collection_type":await FetchData(url,'collection_type',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}
        
    try{ 
      await this.setState({
        listArrCat: data.find(x=>x['catagory_product']).catagory_product[0].catagory_product,
        listArrRibbon: data.find(x=>x['catagory_product']).catagory_product[1].system_ribbon,
        listArrUnit: data.find(x=>x['unit_product']).unit_product[0],
        listArrRelative: data.find(x=>x['relative_product']).relative_product[0],
        listArrCollection: data.find(x=>x['collection_type']).collection_type[0],
      })
      await this.packData();
    }catch(e){
      getConsoleError(e)
    }

    this.setState({load:false})
  }

  packData =async()=>{
    var listTable = [] , tPrice = 0 ,lang = this.props.lang ,tag = [] ,showPromotion= false
    
    this.state.listArrUnit.map(x=>{
      var findCollect = this.state.listArrCollection.filter(v=>v[`idcode`] === x.collection)
      var findTagCat = this.state.listArrRelative.filter(b=>b[`idcode_unit`] === x.idcode)
      if(findCollect.length > 0){
        x.columnCollection = (
          <Button
            startIcon={<LinkIcon />}
            color="default"
            style={{backgroundColor: `#b71c1c` ,color: `white`}}
            onClick={funcClickLink.bind(this,`/mng/master_collection/${x.collection}`)}
        >
            {findCollect[0][lang]}
        </Button>
        )
      }
      x.manage = (
        <Button
          startIcon={<UpdateIcon />}
          color="default"
          style={{backgroundColor: `blue` ,color: `white`}}
          onClick={funcClickLink.bind(this,`/mng/master_unit/${x.idcode}`)}
        >
            update
        </Button>
      )
      x.oos = (<Checkbox checked={x.Out_of_stock} /> )
      x.picture = (x.img !== null && x.img !== `` ? (<img src={`${process.env.REACT_APP_URL_ASSETS}unit_product/${x.idcode}/${x.img}`} style={{width:`70%`}} />) : (<div></div>))
      x.showPrice = x.promotion
      x.prodname = (`${x[lang][0].title}`)
      x.cv = ( <Checkbox checked={x.customer_view} /> )

      if(this.state.productIdCode !== null && this.state.productIdCode !== `create`){
        showPromotion = x.price !== x.promotion ? (true) : (false)
      }
      listTable.push(x)
    })
    if(this.state.productIdCode !== null && this.state.productIdCode !== undefined && this.state.productIdCode !== `` && this.state.productIdCode !== `create`){
      await this.setState({ 
        listArrUnitTable: listTable , showPromotion: showPromotion ,

        fm_title_th: listTable[0][`th`][0].title, fm_title_en: listTable[0][`en`][0].title,
        fm_content_th: listTable[0][`th`][0].content, fm_content_en: listTable[0][`en`][0].content,
        fm_mainImg: listTable[0].img, fm_subImg: listTable[0].subImg, fm_price: listTable[0].price, 
        fm_promotion: listTable[0].promotion, fm_notifyNumb: listTable[0].notificationNumber, fm_size: listTable[0].size,
        fm_cv: listTable[0].customer_view, fm_url: listTable[0].url, fm_collection: listTable[0].collection, 
        fm_Out_of_stock: listTable[0].Out_of_stock,
      })
    }else{
      await this.setState({ 
        listArrUnitTable: listTable , showPromotion: showPromotion ,

        fm_title_th: null, fm_title_en: null,
        fm_content_th: null, fm_content_en: null,
        fm_mainImg: null, fm_subImg: [], fm_price: 0, 
        fm_promotion: 0, fm_notifyNumb: null, fm_size: null,
        fm_cv: null, fm_url: null, fm_collection: null, fm_Out_of_stock: null,
      })
    }
  }

  async clickUploadImg_single(e){
    e.preventDefault();
    var file = this.refs.file.files[0];
    var reader = new FileReader();
    var idcode = await generateString_string(50)

    if(file !== [] && file !== undefined && file !== `undefined`){
      if(this.state.productIdCode !== `` && this.state.productIdCode !== null && this.state.productIdCode !== `create`){
        var url = reader.readAsDataURL(file);
        // const formData = new FormData();
        // await formData.append('attachFile',file);
        reader.onloadend = async function (e) {
          await this.setState({ load_uploadImg: true })
          var resUpload = await admin_upload_unit_img(file,idcode,this.state.productIdCode)
          if(resUpload.res){
            // console.log(`upload success ${resUpload.img}`)
            await this.addNewimageAfterUploadImage(resUpload.img)
          }else{
            // console.log(`upload fail`)
          }
          await this.setState({ load_uploadImg: false })
        }.bind(this);
      }
    }else{
      // this.setState({ canUseThisUpload: false, imgSrc: null, fileImage: null })
    }
  }
  
  async clickUploadImg_multiple(e){
    e.preventDefault();
    var arrDataImg = [];
    arrDataImg = this.state.fm_subImg
    var files = this.refs.file.files;
    
    for(var g=0; g< files.length ; g++){
      var idcode = await generateString_string(50)
      var resUpload = await admin_upload_unit_img(files[g],idcode,this.state.productIdCode)
      if(resUpload.res){
        arrDataImg.push(resUpload.img)
      }
      // reader.onloadend = async function (e) {
        
        // await this.setState({ load_uploadImg: true })
        
        // if(resUpload.res){
        //   await this.addNewimageAfterUploadImage(resUpload.img)
        // }else{
        //   console.log(`upload fail`)
        // }
        // await this.setState({ load_uploadImg: false })
      // }.bind(this);

      // reader.onloadend = async function (e) {
      //   arrDataImg.push(e.target.result)
      // }
      // var img = await reader.readAsDataURL(files[g]);

      if(g === (files.length-1)){
        await this.setState({ fm_subImg: arrDataImg })
      }
    }
  }

  async clickUploadImg_singleNewCreate(e){
    e.preventDefault();
    var file = this.refs.file.files[0];
    var arrDataImg = [];
    arrDataImg = this.state.fm_subImg
    var reader = new FileReader();

    if(file !== [] && file !== undefined && file !== `undefined`){
      var url = reader.readAsDataURL(file);
      reader.onloadend = async function (e) {
        await this.setState({ load_uploadImg: true })
        arrDataImg.push(e.target.result)
        await this.setState({ load_uploadImg: false })
      }.bind(this);
      await this.setState({ fm_subImg: arrDataImg })  
    }
  }
 
  addNewimageAfterUploadImage =async(img)=>{
    var res = [];
    res = this.state.fm_subImg
    res.push(img)
    await this.setState({ fm_subImg: res })
  }

  packObjAndSend=async()=>{
    var idcode = generateString_string(50) , promotion = 0 ,price = 0 ,modeCreate = true , cv = true
    if(this.state.productIdCode !== null && this.state.productIdCode !== `` && this.state.productIdCode !== `create`){
      idcode = this.state.productIdCode
      modeCreate = false
    }
    if(this.state.fm_mainImg === `` || this.state.fm_mainImg === null || this.state.fm_subImg.length <= 0 || modeCreate){
      cv = false
    }else{
      cv = this.state.fm_cv
    }
    price = parseInt(this.state.fm_price)
    if(this.state.showPromotion){
      promotion = parseInt(this.state.fm_promotion)
    }else{
      promotion = parseInt(price)
    }
    var res = {
      "idcode":`${idcode}`,
      "th":[
        {"title":this.state.fm_title_th,"content":this.state.fm_content_th}
      ],
      "en":[
        {"title":this.state.fm_title_en,"content":this.state.fm_content_en}
      ],
      "img":this.state.fm_mainImg,
      "subImg":this.state.fm_subImg,
      "price":parseInt(price),
      "promotion":parseInt(promotion),
      "notificationNumber" : this.state.fm_notifyNumb,
      "size" : this.state.fm_size,
      "wunit" : "58GYIZQpFVbKJYNE1yK4ceAhwdeYcKpKx7hDVYubwXWDCVk8wV",
      "customer_view" : modeCreate ? false : cv,
      "url" : this.state.fm_url,
      "collection" : this.state.fm_collection,
      "Out_of_stock": this.state.fm_Out_of_stock
    }
    adminSendToServer_unit(res , this.set_submitLoad , idcode, modeCreate)
  }
  set_submitLoad =(val)=>{
    this.setState({ load: val })
  }

  onChangeStatus =async(event , state)=>{
    await this.setState({ [state] :event.target.value})
  }

  onChangeOption= async(event,state)=>{
    await this.setState({ [state]:event.target.value})
    await this.packData()
  }

  onDeleteChip=()=>{
    // console.log(`test`)
  }

  onDeleteProductPanel =()=>{
    this.setState({ openDrawerDelete: true })
  }

  onDeleteProductClose =()=>{
    this.setState({ openDrawerDelete: false })
  }

  onSubmit =async()=>{
    this.packObjAndSend()
  }

  onChangeMainImage=async(img)=>{
    await this.setState({ fm_mainImg :img})
  }

  onDeleteImg=async(img)=>{
    var list = await this.state.fm_subImg.filter(e=>e !== img)
    if(this.state.fm_mainImg === img){
      await this.setState({ fm_subImg :list , fm_mainImg: null})
    }else{
      await this.setState({ fm_subImg :list})
    }
  }
  
  render(){
    const load = <LinearProgress color="secondary" />
    // const style = `.txt{ color: white ;  } .pic{ width: 30% }`
    const ex = `.carousel{height:unset} .rb{ left: 0px;}`
    if(this.state.load){
      return (<div>{load}</div>)
    }else{
        var lang = this.props.lang ,title_head = null ,dataRow=null ,modeCreate = false
        if(this.state.productIdCode !== null){
          if(this.state.productIdCode === `create`){
            modeCreate = true
            dataRow = []
            title_head = (
              <div>
                <center><h3 className={`myfont_orderall_head`}>{`สร้างรายการสินค้าใหม่`}</h3></center>
              </div>
            )
          }else{
            dataRow = this.state.listArrUnitTable
            title_head = (
            <div>
              <center><h3 className={`myfont_orderall_head`}>{`แสดงรายการสินค้า`}</h3></center>
              <center><h4 className={`myfont_orderall_under_head`}>{dataRow[0][lang][0].title}</h4></center>
            </div>
            )
          }
          // console.log(this.state.fm_subImg)
          return (
            <React.Fragment>
              <style>{ex}</style>
              <br/>
              {title_head}
              <br/>
              <Card boxShadow={3} className={`myCard_style`}>
                <h4 className={`myCard_style_title`}>Info. Product</h4>
                <Grid container space={2}>
                  <Grid item xs={6}>
                    {modeCreate ? 
                      myTextField(`product name (th)`,``,4,false,this.onChangeStatus,`fm_title_th`) : 
                      myTextField(`product name (th)`,this.state.fm_title_th,4,false,this.onChangeStatus,`fm_title_th`)}
                  </Grid>
                  <Grid item xs={6}>
                    {modeCreate ? 
                    myTextField(`product name (en)`,``,4,false,this.onChangeStatus,`fm_title_en`) : 
                    myTextField(`product name (en)`,this.state.fm_title_en,4,false,this.onChangeStatus,`fm_title_en`)}
                  </Grid>
                </Grid>
                <Grid container space={2}>
                  <Grid item xs={6}>
                    {modeCreate ? 
                      myTextField(`content (th)`,``,4,true,this.onChangeStatus,`fm_content_th`) : 
                      myTextField(`content (th)`,this.state.fm_content_th,4,true,this.onChangeStatus,`fm_content_th`)}
                  </Grid>
                  <Grid item xs={6}>
                    {modeCreate ? 
                      myTextField(`content (en)`,``,4,true,this.onChangeStatus,`fm_content_en`) : 
                      myTextField(`content (en)`,this.state.fm_content_en,4,true,this.onChangeStatus,`fm_content_en`)}
                  </Grid>
                </Grid>
              </Card>
              <hr/>
              <Card boxShadow={3} className={`myCard_style`}>
                <Grid container space={2}>
                  <Grid item xs={6}>
                    <h4 className={`myCard_style_title`}>Image</h4>
                    <Table style={{ backgroundColor: `white` ,marginTop: `10px` , paddingBottom: `100px` , width: `95%` }}>
                      <TableHead >
                        <TableRow>
                          <TableCell align="center"><p className={`myfont_cart_table_head`}>image</p></TableCell>
                          <TableCell align="center"><p className={`myfont_cart_table_head`}>main image</p></TableCell>
                          <TableCell align="center"><p className={`myfont_cart_table_head`}>manage</p></TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {!this.state.load_uploadImg ? 
                        modeCreate ? 
                          // queryImage(this.state.fm_subImg , null ,this.state.productIdCode ,this.onChangeMainImage, this.onDeleteImg,this.clickUploadImg_singleNewCreate ) 
                          (<div></div>): 
                          queryImage(this.state.fm_subImg , this.state.fm_mainImg ,this.state.productIdCode,this.onChangeMainImage, this.onDeleteImg,this.clickUploadImg_multiple )
                          // queryImage(this.state.fm_subImg , this.state.fm_mainImg ,this.state.productIdCode,this.onChangeMainImage, this.onDeleteImg,this.clickUploadImg_single )
                        :
                          <div>{load}</div>
                        }
                      </TableBody>
                    </Table>
                  </Grid>
                  <Grid container item xs={6} style={{alignContent: `baseline`}}>
                    <Grid item xs={12}>
                      <h4 className={`myCard_style_title`}>More info</h4>
                    </Grid>
                    <Grid item xs={6}>
                      <FormControl style={{ width:`100%`,padding:`10px`}}>
                        <InputLabel>{`แสดงราคาแบบโปรโมชั่น (Show Price Promotion)`}</InputLabel>
                        <Select
                          style={{ width:`100%` }}
                          defaultValue={this.state.showPromotion}
                          onChange={(e)=>this.onChangeStatus(e ,`showPromotion`)}
                        >
                          {createListChoice()}
                        </Select>
                      </FormControl>
                    </Grid>
                    <Grid item xs={6} >
                      <div style={{
                        display: modeCreate ? `none` : 
                        this.state.fm_mainImg !== `` && this.state.fm_mainImg !== null && this.state.fm_subImg.length > 0 ? `unset`: `none`}}
                      >
                      <FormControl style={{ width:`100%`,padding:`10px`}}>
                        <InputLabel>{`แสดงสินค้ารายการนี้ (Customer View)`}</InputLabel>
                        <Select
                          style={{ width:`100%` }}
                          defaultValue={this.state.fm_cv}
                          onChange={(e)=>this.onChangeStatus(e ,`fm_cv`)}
                        >
                          {createListChoice()}
                        </Select>
                      </FormControl>
                      </div>
                    </Grid>
                    <Grid item xs={6}>
                      {modeCreate ? 
                        myTextField(`price`,0,4,false,this.onChangeStatus,`fm_price`) : 
                        myTextField(`price`,this.state.fm_price,4,false,this.onChangeStatus,`fm_price`)}
                    </Grid>
                    <Grid item xs={6}>
                      <div style={{display: this.state.showPromotion ? `unset`: `none`}}>
                        {modeCreate ? 
                          myTextField(`price promotion`,0,4,false,this.onChangeStatus,`fm_promotion`) : 
                          myTextField(`price promotion`,this.state.fm_promotion,4,false,this.onChangeStatus,`fm_promotion`)}
                      </div>
                    </Grid>
                    <Grid item xs={6}>
                      {modeCreate ? 
                        myTextField(`notification number`,0,4,false,this.onChangeStatus,`fm_notifyNumb`) : 
                        myTextField(`notification number`,this.state.fm_notifyNumb,4,false,this.onChangeStatus,`fm_notifyNumb`)}
                    </Grid>
                    <Grid item xs={6}>
                      {modeCreate ? 
                        myTextField(`show size`,0,4,false,this.onChangeStatus,`fm_size`) : 
                        myTextField(`show size`,this.state.fm_size,4,false,this.onChangeStatus,`fm_size`)}
                    </Grid>
                    <Grid item xs={12}>
                      {modeCreate ? 
                        myTextField(`URL SEO`,0,4,false,this.onChangeStatus,`fm_url`) : 
                        myTextField(`URL SEO`,this.state.fm_url,4,false,this.onChangeStatus,`fm_url`)}
                    </Grid>
                    <Grid item xs={6}>
                      <FormControl variant="filled" style={{ width:`100%`,padding:`10px`}}>
                        <InputLabel>in collection</InputLabel>
                        <Select
                          style={{ width:`100%` }}
                          defaultValue={this.state.fm_collection}
                          onChange={(e)=>this.onChangeStatus(e ,`fm_collection`)}
                        >
                          {createListCollection(this.state.listArrCollection ,lang)}
                        </Select>
                      </FormControl>
                    </Grid>
                    <Grid item xs={6}>
                      <FormControl style={{ width:`100%`,padding:`10px`}}>
                        <InputLabel>out of stock</InputLabel>
                        <Select
                          style={{ width:`100%` }}
                          defaultValue={this.state.fm_Out_of_stock}
                          onChange={(e)=>this.onChangeStatus(e ,`fm_Out_of_stock`)}
                        >
                          {createListChoice_yesNo()}
                        </Select>
                      </FormControl>
                    </Grid>
                    <Grid container item xs={12} style={{display: modeCreate ? `none` : ``}}>
                      <Grid item xs={12}>
                        <h4 className={`myCard_style_title`}>Tag.</h4>
                      </Grid>
                      <Grid item xs={12}>
                        {queryTag(this.state.listArrRelative, this.state.listArrCat, this.state.productIdCode,lang,
                          this.onDeleteChip,this.tagOnOpenDrawer.bind(this) , this.state.openDrawerTag)}
                      </Grid>
                    </Grid>
                    <Grid container item xs={12} style={{display: modeCreate ? `` : `none`}}>
                      <Grid item xs={12}>
                        <h4 className={`myCard_style_title`}>Tag.</h4>
                      </Grid>
                      <Grid item xs={12}>
                        <h5 className={`myCard_style_title`}>ต้องสร้างรายการสินค้าก่อนจึงจะเข้ามา update tag. ได้</h5>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Card>
              {drawerTag(`right` , this.state.openDrawerTag,this.onCloseDrawer.bind(this) ,
                this.state.listArrRelative, this.state.listArrCat, this.state.productIdCode, lang ,
                this.onChangeCatInDrawer , this.state.load_actionDrawerTag
              )}
              {drawerDeleteProduct(`right` , this.state.openDrawerDelete,this.onDeleteProductClose.bind(this) , this.funcConfirm , this.onDeleteProductClose.bind(this) )}
            <StdBtnIconOnly_upToTop />
            <StdFabBtn_done funcClickSubmit={this.onSubmit.bind(this)}/>
            <StdFabBtn_delete funcClickSubmit={this.onDeleteProductPanel.bind(this)} />
          </React.Fragment>
          )
        }else{
          dataRow = this.state.listArrUnitTable
          return (
            <React.Fragment>
              <style>{ex}</style>
              <br/>
              <center><h3 className={`myfont_orderall_head`}>{`แสดงผลรายการสินค้าทั้งหมด`}</h3></center>
              <MaterialTable
                  tableRef={this.tableRef}
                  align="center" 
                  title=""
                  columns={column_query_unit()[lang]}
                  data={dataRow}
                  options={{
                      pageSize: dataRow.length,
                      pageSizeOptions: [dataRow.length, 100, 500]
                  }}
                  style={{zIndex: 0 , fontSize: `smaller` , width: `100%`}}
              />
              <StdBtnIconOnly_upToTop />
              <StdFabBtn_create funcClickLink={funcClickLink.bind(this,`/mng/master_unit/create`)} />
            </React.Fragment>
          )
        }
    }
  }
  funcConfirm= async()=>{
    var idcode = this.state.productIdCode
    await adminSendToServer_unitDelete([idcode] , this.set_submitLoad)
  }
  tagOnOpenDrawer =()=>{
    this.setState({ openDrawerTag: !this.state.openDrawerTag })
  }
  onCloseDrawer =()=>{
    this.setState({ openDrawerTag: !this.state.openDrawerTag })
  }
  onChangeCatInDrawer= async(e,idcode_cat,idcode)=>{
    var reListForUpdateInState = [];
    var dataArr = [];
    if(!e.target.checked){
      var idcode_delete = this.state.listArrRelative.filter(b=>b[`idcode_cat`] === idcode_cat && b[`idcode_unit`] === idcode)
      reListForUpdateInState = this.state.listArrRelative
      idcode_delete.map(h=>{
        dataArr.push(h.idcode)
        reListForUpdateInState = reListForUpdateInState.filter(g=>g[`idcode`] !== h.idcode)
      })
      await adminSendToServer_tagDelete( dataArr )
    }else{
      var genidcode = generateString_string(50)
      reListForUpdateInState = this.state.listArrRelative
      dataArr = {"idcode":genidcode , "idcode_cat":idcode_cat , "idcode_unit":idcode}
      await reListForUpdateInState.push(dataArr)
      await adminSendToServer_tagAddition( dataArr )
    }
    // console.log(this.state.listArrRelative)
    // console.log(reListForUpdateInState)
    await this.setState({ listArrRelative: reListForUpdateInState })
    // setTimeout(async function() {
    //   await this.setState({ load_actionDrawerTag: false })
    // }.bind(this), 700)
  }
}

function queryTag(listRelative, listCat, idcode ,lang, onDeleteChip ,tagOnOpenDrawer , stateOpenDrawer){
  var res = [] , list = []
  if(stateOpenDrawer){
    res.push(<LinearProgress color="secondary" />)
  }else{
    list = listRelative.filter(x=>x[`idcode_unit`] === idcode)
    if(list.length > 0){
      list.map(x=>{
        var result = listCat.find(f=>f[`idcode`] === x.idcode_cat)
        res.push(
          <Chip className={`myChip`}
          variant="outlined"
          label={result[lang]}
          // onDelete={onDeleteChip}
          color="primary"
        />
        )
      })
    }
    res.push(<div><hr/></div>)
    res.push( <Chip className={`myChip`} color="primary" onClick={tagOnOpenDrawer} 
    style={{backgroundColor: `green`}} icon={<AddCircleIcon />} label='add new tag' /> )
  }
  return res
}

function queryImage(subImg , mainImg, idcode, funcChangeMainImg , funcDeleteImg ,funcUpload){
  var res = []
  if(idcode !== null && idcode !== `create` && idcode !== undefined){
    subImg.map(x=>{
      res.push(
        <TableRow>
          <TableCell align="center">
            {<img src={`${process.env.REACT_APP_URL_ASSETS}unit_product/${idcode}/${x}`} style={{width:`100%`}} />}
          </TableCell>
          <TableCell align="center">
            <Checkbox checked={x === mainImg ? (true) : (false)} onClick={funcChangeMainImg.bind(this,x)} />
          </TableCell>
          <TableCell align="center">
            <IconButton style={{backgroundColor: `red`, color: `white`}} onClick={funcDeleteImg.bind(this,x)}>
              <DeleteForeverIcon />
            </IconButton>
          </TableCell>
        </TableRow>
      )
    })
    res.push(
      <TableRow>
        <TableCell align="center" colSpan={2}><p className={`myfont_orderall_head`}>Upload Image</p></TableCell>
          <TableCell align="center">
            {/* <IconButton style={{backgroundColor: `green`, color: `white`}}><AddCircleIcon /></IconButton> */}
            <form onSubmit={funcUpload}>
              <Button
                variant="contained"
                component="label"
              >
                <input type="file" ref="file" name="attachFile" multiple="true" onChange={funcUpload} />
              </Button>
            </form>
          </TableCell>
      </TableRow>
    )
  }else{
    if(subImg.length > 0){
      subImg.map(x=>{
        res.push(
          <TableRow>
            <TableCell align="center">
              {<img src={x} style={{width:`100%`}} />}
            </TableCell>
            <TableCell align="center">
              <Checkbox checked={x === mainImg ? (true) : (false)} onClick={funcChangeMainImg.bind(this,x)} />
            </TableCell>
            <TableCell align="center">
              <IconButton style={{backgroundColor: `red`, color: `white`}} onClick={funcDeleteImg.bind(this,x)}>
                <DeleteForeverIcon />
              </IconButton>
            </TableCell>
          </TableRow>
        )
      })
    }
    res.push(
      <TableRow>
        <TableCell align="center" colSpan={2}><p className={`myfont_orderall_head`}>Upload Image</p></TableCell>
        <TableCell align="center">
          <form onSubmit={funcUpload}>
            <Button
              variant="contained"
              component="label"
            >
              <input type="file" ref="file" name="attachFile" multiple="true" onChange={funcUpload} />
            </Button>
          </form>
        </TableCell>
      </TableRow>
    )
  }
  return res;
}

function drawerDeleteProduct(anchor,open,onClose , funcConfirm , funcCancel){
  var res = []
  res.push(
    <React.Fragment key={anchor}>
      <Drawer anchor={anchor} open={open} onClose={onClose}>
        <div style={{paddingLeft: `20px` , paddingRight: `20px`}}>
          <Grid container space={2}>
            <Grid item xs={12}>
              <center><h5>{`confirm for delete`}</h5></center>
              <center>
                <Button startIcon={<DoneAllIcon/>} 
                  style={{backgroundColor: `green` ,color: `white` ,marginRight: `5px`}}
                  onClick={funcConfirm}
                >
                  {`confirm`}
                </Button>
                <Button startIcon={<DeleteForeverIcon/>} 
                  style={{backgroundColor: `red` ,color: `white`}}
                  onClick={funcCancel}
                >
                  {`cancel`}
                </Button>
              </center>
            </Grid>
          </Grid>
        </div>
      </Drawer>
    </React.Fragment>
  )
  return res;
}

function drawerTag(anchor,open,onClose,listRelative, listCat, idcode ,lang ,onChnageTag ,loadDrawerTag){
  var res = []
  res.push(
    <React.Fragment key={anchor}>
      <Drawer anchor={anchor} open={open} onClose={onClose}>
        {loadDrawerTag ? (<LinearProgress color="secondary" />) : queryListTagInDrawer(listRelative, listCat, idcode ,lang ,onChnageTag)}
      </Drawer>
    </React.Fragment>
  )
  return res;
}

function queryListTagInDrawer(listRelative, listCat, idcode ,lang ,onChnageTag){
  var res = []
  listCat.map(x=>{
    res.push(
      <FormControlLabel style={{paddingLeft: `20px` , paddingRight: `20px`}}
        control={<Checkbox 
          checked={listRelative.filter(v=>v[`idcode_cat`] === x.idcode && v[`idcode_unit`] === idcode).length > 0 ? true : false} 
          onChange={(e)=>{onChnageTag(e,x.idcode,idcode)}}
        />}
        label={x[lang]}
      />
    )
  })
  return res
}

function createListChoice(){
  var res= []
  res.push(<MenuItem value={true}>{`show`}</MenuItem>)
  res.push(<MenuItem value={false}>{`hide`}</MenuItem>)
  return res
}

function createListChoice_yesNo(){
  var res= []
  res.push(<MenuItem value={true}>{`yes`}</MenuItem>)
  res.push(<MenuItem value={false}>{`no`}</MenuItem>)
  return res
}

function createListCollection(listCollection,lang){
  var res= []
  listCollection.map(x=>{
    res.push(<MenuItem value={x.idcode}>{x[lang]}</MenuItem>)
  })
  return res
}

function myTextField(label,defaultValue,line,multiline,onChange,statename){
  return (
    <TextField
      className={`myfont_orderall_textfield`}
      label={label}
      defaultValue={defaultValue}
      InputProps={{
        readOnly: false,
      }}
      rows={line}
      multiline={multiline}
      onChange={(e)=>onChange(e,statename)}
      variant="outlined"
      style={{width: `95%` , marginTop: `10px`}}
    />
  )
}

MasterUnit.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
  window: PropTypes.func,
};
function mapStateToProps(state) {
  return {
    lang: state.page_getLangToState,
    arrCart: state.func_actionCart
  }
}
function mapDispatchToProps(dispatch) {
  return {
      actionReducer: (type,payload) => {
        dispatch({type: type,payload:payload})
      }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(useStyles, { withTheme: true })(MasterUnit))