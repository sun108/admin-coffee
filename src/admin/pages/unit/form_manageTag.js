import React , { Component , useState, useEffect , Fragment } from 'react';
import { connect } from 'react-redux'

//component
import {StdProgessBar_sizeBig001_loadLoop} from '../../../component/progressBar/std_progressBar'

class Class_form_tag extends Component {
    constructor(props) {
        super(props);
        this.state = {
            load: true
        }
    }
    componentDidMount() {
        this.setDataPack(this.props)
    }
    componentWillReceiveProps(props){
        this.setDataPack(props)
    }
    setDataPack = async(props)=>{
        // this.setState({load : true})

        this.setState({load : false})
    }
    
    render(){
        if(!this.state.load){
            
        }else{
            return (<div> <StdProgessBar_sizeBig001_loadLoop /></div>) 
        }
    }
}

function mapStateToProps(state) {
    return {
        lang: state.page_getLangToState,
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actionReducer: (type,payload) => {
          dispatch({type: type,payload:payload})
        }
    }    
  }
const Form_manageTag = connect(mapStateToProps,mapDispatchToProps)(Class_form_tag)
export { Form_manageTag};