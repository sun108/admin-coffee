import React,{Component} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { browserHistory , withRouter , Redirect ,Router } from 'react-router'

//component
import {StdLoad_standard} from '../../../component/load/std_load'
import {StdCopyright} from '../../../component/copyright/stdCopyright_string'
import {StdToastMz_error,StdToastMz_success,StdToastMz_info,StdToastMz_warning} from '../../../component/toast/std_toast.js'

//service
import {getStringTitle ,checkStringInNotNullOrEmpty_bool , get_comLang , generateString_string , getConsoleError} from '../../../services/std_service/getString'
import FetchData from '../../../services/fetch_data/fetch'
import FetchData_fhr from '../../../services/fetch_data/fetch_fhr'
import {service_login} from '../../../services/login/login_service'
import {encrypt_passw} from '../../../services/encryption/passw'
import {jwt_encode ,jwt_setCookie ,delete_oldCookie} from '../../../services/authen/authen'
import {funcClickLink} from '../../../services/link/service_link'

//style
import styleCode from '../../../style/globe_admin.js'

class AppLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      load: true ,title: [] ,globeStr: [] ,idcode_employee: null ,password: null,load_login: false ,
      dataSource_person: [],dataSource_admin: [],
    }
    this.loginSubmit = this.loginSubmit.bind(this);
  }
  componentDidMount() {
    this.start_page(this.props);
  }
  start_page= async(props)=>{
    this.setState({load:true})
    


    this.setState({load:false})
  }

  async loginSubmit(event) {
    event.preventDefault();

    await delete_oldCookie()
    
    this.setState({load_login:true})
    if(this.checkFieldDataBeforeSubmit()){
      this.setAfterAction([false,false])
    }else{
      var arrObj = [{
        "login":this.state.idcode_employee , 
        "password": await encrypt_passw(this.state.password)
      }]
      // console.log(arrObj)
      var result = await service_login(JSON.stringify(arrObj))
      // console.log(result)
      this.setAfterAction(result)
    }
  }
  checkFieldDataBeforeSubmit =()=>{
    var result = checkStringInNotNullOrEmpty_bool(this.state.idcode_employee) || checkStringInNotNullOrEmpty_bool(this.state.password) ? false : true
    return result
  }
  setAfterAction = async(result)=>{
    var self = result;
    setTimeout( async()=>{
      if(self === false){
          StdToastMz_error(
            `login fail`
          )
      }else{
        if(self[0]){
          await this.load_infoPerson()
          var idcode = this.state.dataSource_person.find(x=>x['idcode']).idcode
          var data = {
            idcode_employee: idcode,
            emp_code:this.state.dataSource_person.find(x=>x['idcode_employee']).idcode_employee,
            fullname_th:`${this.state.dataSource_person.find(x=>x['firstname_th']).firstname_th} ${this.state.dataSource_person.find(x=>x['lastname_th']).lastname_th}`,
            fullname_en:`${this.state.dataSource_person.find(x=>x['firstname_en']).firstname_en} ${this.state.dataSource_person.find(x=>x['lastname_en']).lastname_en}`,
            nickname_th:`${this.state.dataSource_person.find(x=>x['nickname_th']).nickname_th}`,
            nickname_en:`${this.state.dataSource_person.find(x=>x['nickname_en']).nickname_en}`,
          }
          var jwt = jwt_encode(data)
          jwt_setCookie(jwt)
          StdToastMz_success(
            `success`
          )

          // if(self[1]){
          //   console.log('true')
          //   browserHistory.push(`/changepass/${idcode}`)
          // }else{
            // console.log('false')
            // browserHistory.push('/')
          funcClickLink(`/mng`)
          // }
        }else{
          StdToastMz_error( //fail authen
            `login fail`
          )
        }
      }
      this.setState({load_login:false})
    },500)
  }
  load_infoPerson = async()=>{
    var url =null ; var data= []

    url = `${process.env.REACT_APP_URL_API_PERSON}query/collection/person?idcode_employee=${this.state.idcode_employee}`
    try{ await data.push ({"dataSource_person":await FetchData_fhr(url,'dataSource_person',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}

    url = `${process.env.REACT_APP_URL_API}query/collection/data_admin?idcode_employee=${this.state.idcode_employee}`
    try{ await data.push ({"dataSource_admin":await FetchData(url,'dataSource_admin',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}
    
    this.setState({ 
      dataSource_person: data.find(x=>x['dataSource_person']).dataSource_person[0],
      dataSource_admin: data.find(x=>x['dataSource_admin']).dataSource_admin[0],
    })
  }
  updateState =(evt,inpState)=>{
    this.setState({ [inpState]:evt.target.value })
  }
  
  render(){
    if(this.state.load){
      return (<div>{<StdLoad_standard />}</div>)
    }else{
      // var lang = this.props.lang
      const darkTheme = createMuiTheme({ palette: {type: 'dark'}})
      if(this.state.load_login){
        return (
          <div className='colorDefault_login'>
            <ThemeProvider theme={darkTheme}>
              <Container component="main" maxWidth="xs" >
                <CssBaseline />
                <div style={{paddingTop: `60px`}}>
                  <StdLoad_standard />
                </div>
              </Container>
            </ThemeProvider>
          </div>
        )
      }else{
        var lang = this.props.lang
        const darkTheme = createMuiTheme({ palette: {type: 'dark'}})
  
        return (
          <div className='colorDefault_login'>
            <style>{ [styleCode] }</style>
            <ThemeProvider theme={darkTheme}>
              <Container component="main" maxWidth="xs" >
                <CssBaseline />
                <div style={{paddingTop: `60px`}}>
                  <center>
                    <Avatar>
                      <LockOutlinedIcon />
                    </Avatar>
                  </center>
                  <form id='form_login' noValidate autoComplete="off" 
                    onSubmit={this.loginSubmit}
                  >
                    <TextField
                      variant="outlined" margin="normal" required fullWidth
                      onChange={evt => this.updateState(evt,'idcode_employee')}
                      label={`username`}
                      // defaultValue={this.state.idcode_employee}
                      autoFocus
                    />
                    <TextField
                      variant="outlined" margin="normal" required fullWidth
                      onChange={evt => this.updateState(evt,'password')}
                      label={`password`}
                      // defaultValue={this.state.password}
                      type="password"
                    />
                    <Button
                      type="submit" fullWidth variant="contained" color="primary" form='form_login'
                    >
                      Sign In
                    </Button>
                    <Grid container>
                    </Grid>
                  </form>
                </div>
                <Box mt={8}>
                  {/* {StdCopyright()} */}
                </Box>
              </Container>
            </ThemeProvider>
          </div>
        );
      }
    }
  }
}

export default (AppLogin);