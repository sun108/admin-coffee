import React,{Component} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { connect } from 'react-redux'
import { browserHistory , withRouter , Redirect ,Router } from 'react-router'

//component
import {StdIcon_yes} from '../../component/icon/std_icon'
import {StdBtnIconOnly_customDirect} from '../../component/button/std_button'
import {StdImg_personImgMedium} from '../../component/image_profile/std_imageProfile'
import {StdLoad_standard} from '../../component/load/std_load'
import {StdCopyright} from '../../component/copyright/stdCopyright_string'
import {StdToastMz_error,StdToastMz_success,StdToastMz_info,StdToastMz_warning} from '../../component/toast/std_toast.js'

//service
import {getStringTitle  , checkChangePassword  , getConsoleError} from '../../services/std_service/getString'
import FetchData from '../../services/fetch_data/fetch'
import {service_login,service_changePassword} from '../../services/login/login_service'
import {encrypt_passw} from '../../services/encryption/passw'

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.loginSubmit = this.loginSubmit.bind(this);
    this.state = {
      load: true ,title: [] ,globeStr: [] ,idcode_employee: null ,password: null,
      load_login: false ,oldpassword: null ,newpassword: null ,newpasswordagain: null ,
      dataSource_person: [] ,load_uiAfterChangePass: false
    }
  }
  componentDidMount() {
    this.start_page();
  }
  start_page= async()=>{
    this.setState({load:true , idcode: this.props.children.props.params.personIdCode})
    var url =null ; var data= []

    url = `${process.env.REACT_APP_URL_API}query?page_login=all&page_globalString=all`
    try{ data.push ({"page_login":await FetchData(url,'page_login',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}

    url = `${process.env.REACT_APP_URL_API_PERSON}query/collection/person?idcode=${this.state.idcode}`
    try{ data.push ({"dataSource_person":await FetchData(url,'dataSource_person',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}

    this.setState({
        title : data.find(x=>x['page_login']).page_login[0],
        globeStr : data.find(x=>x['page_login']).page_login[1],
        dataSource_person: data.find(x=>x['dataSource_person']).dataSource_person[0],
        load:false
    })
  }
  updateState =(evt,inpState)=>{
    this.setState({ [inpState]:evt.target.value })
  }
  cancel_toMainWeb =()=>{
    browserHistory.push('/dashboard')
  }
  async loginSubmit(event) {
    event.preventDefault();
    if(checkChangePassword(this.state.globeStr , this.props.lang , this.state.oldpassword , this.state.newpassword , this.state.newpasswordagain)){
      this.setState({load_login:true})
      setTimeout( async() =>{
        var arrObj = [{
          "idcode_employee":this.state.dataSource_person.find(x=>x['idcode_employee']).idcode_employee , 
          "password": await encrypt_passw(this.state.oldpassword) ,
          "newpassword": await encrypt_passw(this.state.newpassword) ,
          "newpasswordagain": await encrypt_passw(this.state.newpasswordagain)
        }]
        var result = await service_changePassword(JSON.stringify(arrObj))
        this.setAfterAction(result)
      },500)
    }
  }
  setAfterAction =(result)=>{
    var self = result;
    setTimeout(() =>{
        var resultChangePassword = false
        if(!self){
            StdToastMz_error(
              getStringTitle(this.state.globeStr,'fail_cannotConnectServer',this.props.lang)
            )
        }else{
          if(self[0]){
            if(self[1]){
              StdToastMz_success(
                getStringTitle(this.state.globeStr,'success_manageData',this.props.lang)
              )
              resultChangePassword = true
            }else{
              StdToastMz_error(
                getStringTitle(this.state.globeStr,'fail_cannotConnectServer',this.props.lang)
              )
            }
          }else{
            StdToastMz_error(
              getStringTitle(this.state.globeStr,'fail_oldPasswordNotRight',this.props.lang)
            )
          }
        }
        this.setState({load_login:false ,load_uiAfterChangePass: resultChangePassword})
    },500)
  }
  render(){
    if(this.state.load){
      return (<div>{<StdLoad_standard />}</div>)
    }else{
      var lang = this.props.lang
      const darkTheme = createMuiTheme({ palette: {type: 'dark'}})
      if(this.state.load_login){
        return (
          <div className='colorDefault_login'>
            <ThemeProvider theme={darkTheme}>
              <Container component="main" maxWidth="xs" >
                <CssBaseline />
                <div style={{paddingTop: `60px`}}>
                  <StdLoad_standard />
                </div>
              </Container>
            </ThemeProvider>
          </div>
        )
      }else{
        var lang = this.props.lang
        const darkTheme = createMuiTheme({ palette: {type: 'dark'}})
        var ui_form = null

        if(!this.state.load_uiAfterChangePass){
          ui_form = (<form id='form_login' noValidate autoComplete="off" onSubmit={this.loginSubmit}>
          <TextField
            variant="outlined" margin="normal" required fullWidth
            onChange={evt => this.updateState(evt,'oldpassword')}
            label={this.state.title.find(x => x['idcode'] === 'text_004')[lang]}
            defaultValue={this.state.oldpassword}
            type="password"
          />
          <TextField
            variant="outlined" margin="normal" required fullWidth
            onChange={evt => this.updateState(evt,'newpassword')}
            label={this.state.title.find(x => x['idcode'] === 'text_005')[lang]}
            defaultValue={this.state.newpassword}
            type="password"
          />
          <TextField
            variant="outlined" margin="normal" required fullWidth
            onChange={evt => this.updateState(evt,'newpasswordagain')}
            label={this.state.title.find(x => x['idcode'] === 'text_006')[lang]}
            defaultValue={this.state.newpasswordagain}
            type="password"
          />
          <Grid container>
            <Grid item xs>
              <Button type="submit" variant="contained" color="primary" form='form_login'>
                Change Password
              </Button>
            </Grid>
            <Grid item>
              <Button variant="contained" color="Secondary" onClick={this.cancel_toMainWeb}>
                Cancel (goto home page)
              </Button>
            </Grid>
          </Grid>
        </form>)
        }else{
          ui_form = (<div>
            <center>
              <h5>{this.state.title.find(x => x['idcode'] === 'text_008')[lang]}</h5>
              <StdBtnIconOnly_customDirect
                link={'/dashboard'}
                style={{backgroundColor: 'green',color:'white'}}
                icon={<StdIcon_yes />}
              />
            </center>
          </div>)
        }
        
        return (
          <div className='colorDefault_login'>
            <ThemeProvider theme={darkTheme}>
              <Container component="main" maxWidth="xs" >
                <CssBaseline />
                <div style={{paddingTop: `60px`}}>
                  <center>
                    <StdImg_personImgMedium image={this.state.idcode} />
                    <h5>{this.state.title.find(x => x['idcode'] === 'text_007')[lang]}</h5>
                  </center>
                  {ui_form}
                </div>
                <Box mt={8}>
                  {StdCopyright()}
                </Box>
              </Container>
            </ThemeProvider>
          </div>
        );
      }
    }
  }
}
function mapStateToProps(state) {
  return {
      lang: state.page_getLangToState
  }
}
export default connect(mapStateToProps)(ChangePassword);