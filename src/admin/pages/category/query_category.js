import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Radium, {StyleRoot} from 'radium';
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid';
import Chip from '@material-ui/core/Chip';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Hidden from '@material-ui/core/Hidden';
import { Link ,Router , browserHistory} from "react-router";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import MaterialTable from 'material-table';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Select from '@material-ui/core/Select';
import Card from '@material-ui/core/Card';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Drawer from '@material-ui/core/Drawer';

//component
import {
  StdBtnIconOnly_upToTop,
  StdFabBtn_create,
  StdFabBtn_done,
  StdFabBtn_delete
} from '../../../component/button/std_button'
import {StdSnackBar_control , StdToastMz_error,StdToastMz_success,StdToastMz_info,StdToastMz_warning } from '../../../component/toast/std_toast'

//style
import styleCode from '../../../style/menu'
import useStyles  from '../../../style/app.js'

//materialize
import 'materialize-css';
import { Collapsible, CollapsibleItem } from 'react-materialize';

//layout
import {column_query_category} from '../../layouts/query_order/query_category'
import {column_query_unit} from '../../layouts/query_order/query_unit'

//react-responsive-carousel
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

//icon
import CreateIcon from '@material-ui/icons/Create';
import UpdateIcon from '@material-ui/icons/Update';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import DoneAllIcon from '@material-ui/icons/DoneAll';

//service
import {getStringTitle , getConsoleError ,generateString_string} from '../../../services/std_service/getString'
import {myFormatDate , myFormatTime ,mySubtractFormatDate} from '../../../services/std_service/getDate'
import {add_arrItem} from '../../../services/mockup/arrItem'
import FetchData from '../../../services/fetch_data/fetch'
import FetchData_menu from '../../../services/fetch_data/fetch_menu'
import {funcClickLink} from '../../../services/link/service_link'
import {adminSendToServer_category ,adminSendToServer_categoryDelete} from '../../../services/admin/category'

const _ = require('lodash');

class MasterCategory extends Component {
  constructor() {
    super();
    this.state = {
      open: false,openDrawerDelete: false,lang: null,load: true,
      listArrCat: [],listArrCollection: [],listArrUnit: [],listArrRelative: [],listArrRibbon:[],
      listArrCatTable: [] ,listTableProd: [],
      name_th:null , name_en:null, url_seo:null, modeIdCode: false, visible: false
    }
    // this.addItemToCart = this.addItemToCart.bind(this);
    this.duration = 1;
  }
  componentDidMount() {
    this.start_page(this.props);
  }
  componentWillReceiveProps(props) {
    this.start_page(props);
  }
  
  start_page= async(props)=>{
    this.setState({load:true})
    var url =null ; var data= []
    var idcode = props.params.productCode

    if(idcode !== null && idcode !== `` && idcode !== undefined && idcode !== `undefined`){

      await this.setState({ productIdCode: idcode, modeIdCode: true ,name_th:null , name_en:null, url_seo:null, visible: false})
      url = `${process.env.REACT_APP_URL_API}query/collection/catagory_product?idcode=${idcode}`
      try{ data.push ({"catagory_product_only":await FetchData(url,'catagory_product_only',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}
    
    }else{

      await this.setState({ productIdCode: null, modeIdCode: false ,name_th:null , name_en:null, url_seo:null, visible: false})
      url = `${process.env.REACT_APP_URL_API}query?catagory_product=all&system_ribbon=all`
      try{ data.push ({"catagory_product":await FetchData(url,'catagory_product',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}
    }
    url = `${process.env.REACT_APP_URL_API}query?relative_product=all`
    try{ data.push ({"relative_product":await FetchData(url,'relative_product',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}
        
    url = `${process.env.REACT_APP_URL_API}query?unit_product=all`
    try{ data.push ({"unit_product":await FetchData(url,'unit_product',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}

    // console.log(data.find(x=>x['catagory_product_only']).catagory_product_only[0])
    if(this.state.modeIdCode){
      try{ 
        await this.setState({
            listArrCat: data.find(x=>x['catagory_product_only']).catagory_product_only[0],
            listArrRelative: data.find(x=>x['relative_product']).relative_product[0],
            listArrUnit: data.find(x=>x['unit_product']).unit_product[0],
        })
        await this.packData();
      }catch(e){
          getConsoleError(e)
      }
    }else{
      try{ 
        await this.setState({
            listArrCat: data.find(x=>x['catagory_product']).catagory_product[0].catagory_product,
            listArrRelative: data.find(x=>x['relative_product']).relative_product[0],
        })
        await this.packData();
      }catch(e){
          getConsoleError(e)
      }
    }
    this.setState({load:false})
  }

  packData =()=>{
    var listTable = [] , tPrice = 0 ,lang = this.props.lang ,tag = [] ,listTableProd=[]
    this.state.listArrCat.map(x=>{
        x.catname = x[lang]
        x.unitCount = this.state.listArrRelative.filter(v=>v[`idcode_cat`] === x.idcode).length
        x.position = x.order
        x.manage = (
          <Button
              startIcon={<UpdateIcon />}
              color="default"
              style={{backgroundColor: `blue` ,color: `white`}}
              onClick={funcClickLink.bind(this,`/mng/master_category/${x.idcode}`)}
          >
              update
          </Button>
        )
        x.cv = ( <Checkbox checked={x.customer_view} /> )
        listTable.push(x)
    })
    if(!this.state.modeIdCode){
      this.setState({ listArrCatTable: listTable })
    }else{
      this.state.listArrUnit.map(x=>{
        var findInThisCat = this.state.listArrRelative.filter(v=>v[`idcode_cat`] === this.state.productIdCode && v[`idcode_unit`] === x.idcode).length
        if(findInThisCat > 0){
          x.columnCollection = (``)
          x.manage = (
            <Button
              startIcon={<UpdateIcon />}
              color="default"
              style={{backgroundColor: `blue` ,color: `white`}}
              onClick={funcClickLink.bind(this,`/mng/master_unit/${x.idcode}`)}
            >
                update
            </Button>
          )
          x.picture = (x.img !== null && x.img !== `` ? (<img src={`${process.env.REACT_APP_URL_ASSETS}unit_product/${x.idcode}/${x.img}`} style={{width:`70%`}} />) : (<div></div>))
          x.showPrice = x.promotion
          x.prodname = (`${x[lang][0].title}`)
          x.cv = ( <Checkbox checked={x.customer_view} /> )
          listTableProd.push(x)
        }
      })

      this.setState({ 
        listTableProd: listTableProd,
        listArrCatTable: listTable ,
        name_th: listTable[0][`th`] , 
        name_en: listTable[0][`en`] , 
        url_seo: listTable[0][`url`] , 
        visible: listTable[0][`customer_view`]
      })
    }
  }

  onChangeStatus =async(event , state)=>{
    await this.setState({ [state] :event.target.value})
  }
  set_submitLoad =()=>{}

  onChangeOption= async(event,state)=>{
    await this.setState({ [state]:event.target.value})
    await this.packData()
  }

  onDeleteChip=()=>{
    // console.log(`test`)
  }
  funcConfirmDelete= async()=>{
    var idcode = this.state.productIdCode
    var result = this.state.listArrRelative.filter(v=>v[`idcode_cat`] === idcode).length
    if(result > 0){
      StdToastMz_error(`Can not be deleted You must delete all items from this section first.`)
    }else{
      await adminSendToServer_categoryDelete([idcode] , this.set_submitLoad)
    }
  }

  onPanelManage=async(val)=>{
    await this.setState({ open: val })
  }

  onSubmit= async()=>{
    await this.onPanelManage(false)
    await this.packObjAndSend()
  }

  packObjAndSend=async()=>{
    var idcode = this.state.productIdCode
    var res = {
      "th":this.state.name_th,
      "en":this.state.name_en,
      "customer_view" : this.state.visible,
      "url" : this.state.url_seo
    }
    await adminSendToServer_category(res , this.set_submitLoad , idcode, this.state.modeIdCode)
  }
  onDeletePanel=async(val)=>{
    await this.setState({ openDrawerDelete: val })
  }
  
  render(){
    const load = <LinearProgress color="secondary" />
    // const style = `.txt{ color: white ;  } .pic{ width: 30% }`
    const ex = `
    .carousel{height:unset}
    .rb{
      left: 0px;
    }`
    if(this.state.load){
      return (<div>{load}</div>)
    }else{
      var lang = this.props.lang
      var dataRow = this.state.listArrCatTable
      var dataprod = this.state.listTableProd
      // console.log(dataRow)
      if(this.state.modeIdCode){
        return (
          <React.Fragment>
              <style>{ex}</style>
              <br/>
              <center><h3 className={`myfont_orderall_head`}>{`แสดงรายการหมวดสินค้า`}</h3></center>
              <center><h4 className={`myfont_orderall_under_head`}>{this.state.productIdCode}</h4></center>
              <br/>
              <Grid container space={2}>
              <Grid item xs={6}>
                {myTextField(`category (TH)`,this.state.name_th,4,false,this.onChangeStatus,`name_th`)}
              </Grid>
              <Grid item xs={6}>
                {myTextField(`category (EN)`,this.state.name_en,4,false,this.onChangeStatus,`name_en`)}
              </Grid>
              <Grid item xs={6}>
                {myTextField(`URL SEO`,this.state.url_seo,4,false,this.onChangeStatus,`url_seo`)}
              </Grid>
              <Grid item xs={6}>
                <FormControl style={{ width:`100%`,padding:`10px`}}>
                  <InputLabel>{`visible`}</InputLabel>
                  <Select
                    style={{ width:`100%` }}
                    defaultValue={this.state.visible}
                    onChange={(e)=>this.onChangeStatus(e ,`visible`)}
                  >
                    {createListChoice()}
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
            <Grid container space={2}>
            <center><h4 className={`myfont_orderall_under_head`}>{`all product in category`}</h4></center>
              <MaterialTable
                  tableRef={this.tableRef}
                  align="center" 
                  title=""
                  columns={column_query_unit()[lang]}
                  data={dataprod}
                  options={{
                      pageSize: dataprod.length,
                      pageSizeOptions: [dataprod.length, 100, 500]
                  }}
                  style={{zIndex: 0 , fontSize: `smaller` , width: `100%`}}
              />
            </Grid>
            <StdBtnIconOnly_upToTop />
            <StdFabBtn_done funcClickSubmit={this.onSubmit.bind(this)}/>
            <StdFabBtn_delete funcClickSubmit={this.onDeletePanel.bind(this,true)} />
            {drawerDeleteProduct(`right` , 
              this.state.openDrawerDelete,
              this.onDeletePanel.bind(this,true), 
              this.funcConfirmDelete , 
              this.onDeletePanel.bind(this,false) )
            }
          </React.Fragment>
        )
      }else{
        return (
          <React.Fragment>
            <style>{ex}</style>
            <br/>
            <center><h3 className={`myfont_orderall_head`}>{`แสดงรายการหมวดสินค้าทั้งหมด`}</h3></center>
            <MaterialTable
              tableRef={this.tableRef}
              align="center" 
              title=""
              columns={column_query_category()[lang]}
              style={{zIndex: 0 , fontSize: `medium` , width: `100%`}}
              data={dataRow}
              options={{
                pageSize: dataRow.length,
                pageSizeOptions: [dataRow.length, 100, 500]
              }}
            />
            <StdBtnIconOnly_upToTop />
            <StdFabBtn_create funcClickLink={this.onPanelManage.bind(this,true)} />
            {drawerManage(
              `right`,
              this.state.open,
              this.onPanelManage.bind(this,false),
              this.onChangeStatus,
              this.onSubmit.bind(this)
            )}
          </React.Fragment>
        )
      }
    }
  }
  
}

function drawerManage(anchor,openStatus,onClose,onChangeStatus,onSubmitCreate){
  var res = []
  res.push(
    <React.Fragment key={anchor}>
      <Drawer anchor={anchor} open={openStatus} onClose={onClose}>
        <div style={{padding: `20px`}}>
          <Grid container space={2}>
            <center><h5>{`เพิ่มรายชื่อ collection`}</h5></center>
          </Grid>
          <Grid container space={2}>
            <Grid item xs={6}>
              {myTextField(`category (TH)`,``,4,false,onChangeStatus,`name_th`)}
            </Grid>
            <Grid item xs={6}>
              {myTextField(`category (EN)`,``,4,false,onChangeStatus,`name_en`)}
            </Grid>
            <Grid item xs={12}>
              {myTextField(`URL SEO`,``,4,false,onChangeStatus,`url_seo`)}
            </Grid>
            <Grid item xs={12}>
              <hr/>
              <center>
              <Button
                startIcon={<CreateIcon />}
                color="default"
                style={{backgroundColor: `orange` ,color: `white`}}
                onClick={onSubmitCreate.bind(this)}
              >
                  Create
              </Button>
              </center>
            </Grid>
          </Grid>
        </div>
      </Drawer>
    </React.Fragment>
  )
  return res;
}

function drawerDeleteProduct(anchor,open,onClose , funcConfirm , funcCancel){
  var res = []
  res.push(
    <React.Fragment key={anchor}>
      <Drawer anchor={anchor} open={open} onClose={onClose}>
        <div style={{paddingLeft: `20px` , paddingRight: `20px`}}>
          <Grid container space={2}>
            <Grid item xs={12}>
              <center><h5>{`confirm for delete`}</h5></center>
              <center>
                <Button startIcon={<DoneAllIcon/>} 
                  style={{backgroundColor: `green` ,color: `white` ,marginRight: `5px`}}
                  onClick={funcConfirm}
                >
                  {`confirm`}
                </Button>
                <Button startIcon={<DeleteForeverIcon/>} 
                  style={{backgroundColor: `red` ,color: `white`}}
                  onClick={funcCancel}
                >
                  {`cancel`}
                </Button>
              </center>
            </Grid>
          </Grid>
        </div>
      </Drawer>
    </React.Fragment>
  )
  return res;
}

function createListChoice(){
  var res= []
  res.push(<MenuItem value={true}>{`show`}</MenuItem>)
  res.push(<MenuItem value={false}>{`hide`}</MenuItem>)
  return res
}

function myTextField(label,defaultValue,line,multiline,onChangeStatus,statename){
  return (
    <TextField
      className={`myfont_orderall_textfield`}
      label={label}
      defaultValue={defaultValue}
      InputProps={{
        readOnly: false,
      }}
      onChange={(e)=>onChangeStatus(e,statename)}
      rows={line}
      multiline={multiline}
      variant="outlined"
      style={{width: `95%` , marginTop: `10px`}}
    />
  )
}

MasterCategory.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
  window: PropTypes.func,
};
function mapStateToProps(state) {
  return {
    lang: state.page_getLangToState,
    arrCart: state.func_actionCart
  }
}
function mapDispatchToProps(dispatch) {
  return {
      actionReducer: (type,payload) => {
        dispatch({type: type,payload:payload})
      }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(useStyles, { withTheme: true })(MasterCategory))