import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Radium, {StyleRoot} from 'radium';
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid';
import Chip from '@material-ui/core/Chip';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import MaterialTable from 'material-table';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Select from '@material-ui/core/Select';
import Card from '@material-ui/core/Card';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Drawer from '@material-ui/core/Drawer';
import InputAdornment from '@material-ui/core/InputAdornment';
import FilledInput from '@material-ui/core/FilledInput';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Avatar from '@material-ui/core/Avatar';

//component
import {
  StdBtnIconOnly_upToTop,
  StdFabBtn_create,
  StdFabBtn_done,
  StdFabBtn_delete
} from '../../../component/button/std_button'
import {StdSnackBar_control , StdToastMz_error,StdToastMz_success,StdToastMz_info,StdToastMz_warning } from '../../../component/toast/std_toast'

//style
import useStyles  from '../../../style/app.js'

//materialize
import 'materialize-css';

//layout
import {column_query_collection} from '../../layouts/query_order/query_collection'
import {column_query_delivery} from '../../layouts/query_order/query_delivery'

//react-responsive-carousel
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

//icon
import CreateIcon from '@material-ui/icons/Create';
import UpdateIcon from '@material-ui/icons/Update';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import LinkIcon from '@material-ui/icons/Link';

//service
import {getStringTitle , getConsoleError ,generateString_string} from '../../../services/std_service/getString'
import FetchData from '../../../services/fetch_data/fetch'
import FetchData_menu from '../../../services/fetch_data/fetch_menu'
import {funcClickLink} from '../../../services/link/service_link'
import {
  adminSendToServer_delivery ,
  adminSendToServer_deliveryDelete ,
  adminSendToServer_selectUnitDelete ,
  adminSendToServer_selectUnitAdd
} from '../../../services/admin/delivery'

const _ = require('lodash');

class MasterDelivery extends Component {
  constructor() {
    super();
    this.state = {
      openCreate: false, openUpdate: false,openUnitSelect: false, openDrawerDelete: false,load: true, listArrDeliveryTable: [] ,productIdCode: null ,showPromotion: false,
      listArrCat: [],listArrCollection: [],listArrUnit: [],listArrRelative: [],listArrRibbon:[], listTableProd:[],
      listArrSysDelivery: [],listArrRelativeDelivery: [], listArrSelectUnit: [],selectIdcodeDelivery: null,
      name_th:null , name_en:null, price_start:0, unit_min_price:1, price_per_unit: 0, modeIdCode: false, visible: false
    }
    this.duration = 1;
  }
  componentDidMount() {
    this.start_page(this.props);
  }
  componentWillReceiveProps(props) {
    this.start_page(props);
  }
  start_page= async(props)=>{
    this.setState({load:true})
    var url =null ; var data= []
    var idcode = props.params.productCode

    if(idcode !== null && idcode !== `` && idcode !== undefined && idcode !== `undefined`){

      await this.setState({ productIdCode: idcode, modeIdCode: true})
      url = `${process.env.REACT_APP_URL_API}query/collection/collection_type?idcode=${idcode}`
      try{ data.push ({"collection_type":await FetchData(url,'collection_type',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}
    
      url = `${process.env.REACT_APP_URL_API}query/collection/unit_product?collection=${idcode}`
      try{ data.push ({"unit_product":await FetchData(url,'unit_product',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}  
    }else{

      await this.setState({ productIdCode: null, modeIdCode: false})
      url = `${process.env.REACT_APP_URL_API}query?system_delivery=all`
      try{ data.push ({"system_delivery":await FetchData(url,'system_delivery',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}
    
      url = `${process.env.REACT_APP_URL_API}query?relative_delivery=all`
      try{ data.push ({"relative_delivery":await FetchData(url,'relative_delivery',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}
      
      url = `${process.env.REACT_APP_URL_API}query?unit_product=all`
      try{ data.push ({"unit_product":await FetchData(url,'unit_product',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}
    }
    
    try{ 

      await this.setState({
        listArrSysDelivery: data.find(x=>x['system_delivery']).system_delivery[0],
        listArrRelativeDelivery: data.find(x=>x['relative_delivery']).relative_delivery[0],
        listArrUnit: data.find(x=>x['unit_product']).unit_product[0],
      })
      await this.packData();
    }catch(e){
        getConsoleError(e)
    }

    this.setState({load:false})
  }

  packData =()=>{
    var listTable = [] , lang = this.props.lang ,tag = [] ,listTableProd = []
    this.state.listArrSysDelivery.map(x=>{
      x.prodname = x[lang]
      x.price_min = x.price_start
      x.unit_min = x.unit_min
      x.price_per_unit = x.price_per_unit
      x.unit_use_total = this.state.listArrRelativeDelivery.filter(v=>v[`idcode_delivery`] === x.idcode).length
      x.manage = (
          <div>
            <Button
                startIcon={<UpdateIcon />}
                color="default"
                style={{backgroundColor: `pink` ,color: `black`}}
                onClick={this.onOpenDrawerSelectUnit.bind(this,x.idcode)}
            >
              select unit
            </Button>
            <Button
                startIcon={<UpdateIcon />}
                color="default"
                style={{backgroundColor: `blue` ,color: `white` , marginTop: `5px`}}
                onClick={this.onOpenDrawerUpdate.bind(this,x.idcode)}
            >
              update
            </Button>
          </div>
        )
      listTable.push(x)
    })
    this.setState({ listArrDeliveryTable: listTable })
  }

  onChangeState =async(event , state)=>{
    await this.setState({ [state] :event.target.value})
  }

  onOpenDrawerSelectUnit=async(idcode)=>{
    var findIdCodeDelivery = this.state.listArrRelativeDelivery.filter(v=>v[`idcode_delivery`] === idcode)
    await this.setState({
        listArrSelectUnit: this.state.listArrUnit , openUnitSelect: true ,selectIdcodeDelivery: idcode
    })
  }

  onOpenDrawerUpdate=async(idcode)=>{
    var find = this.state.listArrSysDelivery.filter(x=>x[`idcode`] === idcode)
    if(find.length > 0){
        await this.setState({
            name_th: find[0].th, name_en: find[0].en, price_start: find[0].price_start,
            price_per_unit: find[0].price_per_unit, unit_min_price: find[0].unit_min, 
            openUpdate: true ,productIdCode: idcode
        })
    }
  }

  onOpenDrawer =async(val , state)=>{
    await this.setState({ [state] :val})
  }
  set_submitLoad=()=>{}

  onSubmit= async(update)=>{
    await this.onOpenDrawer(false,`openCreate`)
    await this.onOpenDrawer(false,`openUpdate`)
    await this.packObjAndSend(update)
  }
  onSubmitUpdate= async()=>{
    await this.onOpenDrawer(false,`openUpdate`)
    await this.packObjAndSend(true)
  }
  onSubmitSelectunit= async()=>{
    await this.onOpenDrawer(false,`openUnitSelect`)
    // await this.packObjAndSend()
  }
  packObjAndSend=async(update)=>{
    var idcode = this.state.productIdCode
    var res = {
        "price_start" : this.state.price_start,
        "unit_min" : this.state.unit_min_price,
        "price_per_unit" : this.state.price_per_unit,
        "th" : this.state.name_th,
        "en" : this.state.name_en
    }
    await adminSendToServer_delivery(res , this.set_submitLoad , idcode, update)
  }
  funcConfirmDelete= async()=>{
    var idcode = this.state.productIdCode
    var result = this.state.listArrRelativeDelivery.filter(v=>v[`idcode_delivery`] === idcode).length
    if(result > 0){
      StdToastMz_error(`Can not be deleted You must delete all items from this section first.`)
    }else{
        await this.onOpenDrawer(false,`openUpdate`)
        await adminSendToServer_deliveryDelete([idcode] , this.set_submitLoad)
    }
  }
  funcOnClickCheckBoxunit = async(e,idcode_unit,idcode_delivery)=>{
    var reListForUpdateInState = [];
    var dataArr = [];
    if(!e.target.checked){
      var idcode_delete = this.state.listArrRelativeDelivery.filter(b=>b[`idcode_unit`] === idcode_unit)
      reListForUpdateInState = this.state.listArrRelativeDelivery
      idcode_delete.map(h=>{
        dataArr.push(h.idcode)
        reListForUpdateInState = reListForUpdateInState.filter(g=>g[`idcode`] !== h.idcode)
      })
      await adminSendToServer_selectUnitDelete( dataArr )
    }else{
      var genidcode = generateString_string(50)
      reListForUpdateInState = this.state.listArrRelativeDelivery
      dataArr = {"idcode":genidcode , "idcode_delivery":idcode_delivery , "idcode_unit":idcode_unit}
      await reListForUpdateInState.push(dataArr)
      await adminSendToServer_selectUnitAdd( dataArr )
    }
    await this.setState({ listArrRelativeDelivery: reListForUpdateInState })
    await this.packData();
  }
  
  render(){
    const load = <LinearProgress color="secondary" />
    // const style = `.txt{ color: white ;  } .pic{ width: 30% }`
    const ex = `.carousel{height:unset}
    .rb{
      left: 0px;
    }`
    if(this.state.load){
      return (<div>{load}</div>)
    }else{
        var lang = this.props.lang
        var dataRow = this.state.listArrDeliveryTable
        var width = this.props.windows.width
        return (
          <React.Fragment>
            <style>{ex}</style>
            <br/>
            <center><h3 className={`myfont_orderall_head`}>{`แสดงรายการ Delivery ทั้งหมด`}</h3></center>
            <MaterialTable
              tableRef={this.tableRef}
              align="center" 
              title=""
              columns={column_query_delivery()[lang]}
              style={{zIndex: 0 , fontSize: `medium` , width: `100%`}}
              data={dataRow}
              options={{
                pageSize: dataRow.length,
                pageSizeOptions: [dataRow.length, 100, 500]
              }}
            />
            <StdBtnIconOnly_upToTop />
            <StdFabBtn_create funcClickLink={this.onOpenDrawer.bind(this,true,`openCreate`)} />
            {drawerCreate(
              `bottom`,
              this.state.openCreate,
              this.onOpenDrawer.bind(this,false,`openCreate`),
              this.onChangeState,
              this.onSubmit.bind(this,false)
            )}
            {drawerUnitSelect(
              `right`,
              this.state.openUnitSelect,
              this.state.listArrUnit,
              width,lang,
              this.state.listArrRelativeDelivery,
              this.state.selectIdcodeDelivery,
              this.funcOnClickCheckBoxunit,
              this.onOpenDrawer.bind(this,false,`openUnitSelect`),
              this.onChangeState,
              this.onSubmitSelectunit.bind(this),
            )}
            {drawerUpdate(
              `right`,
              this.state.openUpdate,
              this.onOpenDrawer.bind(this,false,`openUpdate`),
              this.onChangeState,
              this.onSubmit.bind(this,true),
                [this.state.name_th , this.state.name_en , this.state.price_start , this.state.unit_min_price , this.state.price_per_unit],
                this.funcConfirmDelete.bind(this)
            )}
          </React.Fragment>
        )
    }
  }
}

function drawerUnitSelect(anchor,openStatus,listData,width,lang,relativeDelivery,idcodeDelivery,onClickCheckBox,onClose){
  var res = []
  res.push(
    <React.Fragment key={anchor}>
      <Drawer anchor={anchor} open={openStatus} onClose={onClose}>
          <div style={{padding: `20px`, width: `${width/2}px`}}>
            <Grid container space={2}>
              <center><h5 className={`myfont_orderall_under_head`}>{`เลือกสินค้าที่ใช้การขนส่งนี้`}</h5></center>
            </Grid>
            <Grid container space={2}>
              <MenuList>
                {queryListTagInDrawer(listData,lang,relativeDelivery,idcodeDelivery,onClickCheckBox)}
              </MenuList>
            </Grid>
          </div>
      </Drawer>
    </React.Fragment>
  )
  return res;
}
function queryListTagInDrawer(listData,lang,relativeDelivery,idcodeDelivery,onClickCheckBox){
  var res = []
  listData.map(x=>{
    if(relativeDelivery.filter(v=>v[`idcode_unit`] === x.idcode).length <= 0){
      res.push(createCheckBoxSelectUnit(false,x,lang,onClickCheckBox,idcodeDelivery))
    }else{
      if(relativeDelivery.filter(v=>v[`idcode_unit`] === x.idcode && v[`idcode_delivery`] === idcodeDelivery).length > 0){
        res.push(createCheckBoxSelectUnit(true,x,lang,onClickCheckBox,idcodeDelivery))
      }else{
        //
      }
    }
  })
  return res
}
function createCheckBoxSelectUnit(checked,x,lang,onClickCheckBox,idcodeDelivery){
  return (
    <div>
      <FormControlLabel style={{paddingLeft: `10px` , paddingRight: `10px`}}
        control={
          <Checkbox checked={checked} onChange={(e)=>{onClickCheckBox(e,x.idcode,idcodeDelivery)}}/>
        }label={
          <Chip avatar={
            <Avatar src={`${process.env.REACT_APP_URL_ASSETS}unit_product/${x.idcode}/${x.img}`} />
          } label={`${x[lang][0][`title`]}`} />
        }
      />
      <br/>
    </div>
  )
}
function drawerCreate(anchor,openStatus,onClose,onChangeState,onSubmitCreate){
    var res = []
    res.push(
        <React.Fragment key={anchor}>
        <Drawer anchor={anchor} open={openStatus} onClose={onClose}>
            <div style={{padding: `20px`}}>
            <Grid container space={2}>
                <center><h5 className={`myfont_orderall_under_head`}>{`เพิ่มรูปแบบการจัดส่ง`}</h5></center>
            </Grid>
            <Grid container space={2}>
                <Grid item xs={3}>
                    {myTextField(`delivery name (TH)`,``,4,false,onChangeState,`name_th`)}
                </Grid>
                <Grid item xs={3}>
                    {myTextField(`delivery name (EN)`,``,4,false,onChangeState,`name_en`)}
                </Grid>
                <Grid item xs={2}>
                    {myTextField_money_baht(`ราคาเริ่มต้น`,``,4,false,onChangeState,`price_start`)}
                </Grid>
                <Grid item xs={2}>
                    <FormControl style={{ width:`100%`,padding:`10px`}}>
                        <InputLabel>{`จำนวนต่ำสุดต่อราคาเริ่ม`}</InputLabel>
                        <Select
                          style={{ width:`100%` }}
                          defaultValue={1}
                          onChange={(e)=>onChangeState(e ,`unit_min_price`)}
                        >
                          {createOptionNumber(1,101)}
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={2}>
                    {myTextField_money_baht(`ราคาต่อชิ้นเมื่อเลยขั้นต่ำ`,``,4,false,onChangeState,`price_per_unit`)}
                </Grid>
                <Grid item xs={12}>
                    <hr/>
                    <center>
                        <Button
                            startIcon={<CreateIcon />}
                            color="default"
                            style={{backgroundColor: `orange` ,color: `white`}}
                            onClick={onSubmitCreate.bind(this)}
                        >
                            Create
                        </Button>
                    </center>
                </Grid>
            </Grid>
            </div>
        </Drawer>
        </React.Fragment>
    )
    return res;
}

function drawerUpdate(anchor,openStatus,onClose,onChangeState,onSubmitUpdate,dataPack,onDelete){
    var res = []
    res.push(
        <React.Fragment key={anchor}>
        <Drawer anchor={anchor} open={openStatus} onClose={onClose}>
            <div style={{padding: `20px`}}>
            <Grid container space={2}>
                <center><h5 className={`myfont_orderall_under_head`}>{`แก้ไขรูปแบบการจัดส่ง`}</h5></center>
            </Grid>
            <Grid container space={2}>
                <Grid item xs={3}>
                    {myTextField(`delivery name (TH)`,dataPack[0],4,false,onChangeState,`name_th`)}
                </Grid>
                <Grid item xs={3}>
                    {myTextField(`delivery name (EN)`,dataPack[1],4,false,onChangeState,`name_en`)}
                </Grid>
                <Grid item xs={2}>
                    {myTextField_money_baht(`ราคาเริ่มต้น`,dataPack[2],4,false,onChangeState,`price_start`)}
                </Grid>
                <Grid item xs={2}>
                    <FormControl style={{ width:`100%`,padding:`10px`}}>
                        <InputLabel>{`จำนวนต่ำสุดต่อราคาเริ่ม`}</InputLabel>
                        <Select
                          style={{ width:`100%` }}
                          defaultValue={dataPack[3]}
                          onChange={(e)=>onChangeState(e ,`unit_min_price`)}
                        >
                          {createOptionNumber(1,101)}
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={2}>
                    {myTextField_money_baht(`ราคาต่อชิ้นเมื่อเลยขั้นต่ำ`,dataPack[4],4,false,onChangeState,`price_per_unit`)}
                </Grid>
                <Grid item xs={12}>
                    <hr/>
                    <center>
                    <Button
                        startIcon={<CreateIcon />}
                        color="default"
                        style={{backgroundColor: `blue` ,color: `white`}}
                        onClick={onSubmitUpdate.bind(this)}
                    >
                        Update
                    </Button>
                    
                    <Button
                        startIcon={<DeleteForeverIcon />}
                        color="default"
                        style={{backgroundColor: `red` ,color: `white` , marginLeft: `5px`}}
                        onClick={onDelete.bind(this)}
                    >
                        Delete
                    </Button>
                    </center>
                </Grid>
            </Grid>
            </div>
        </Drawer>
        </React.Fragment>
    )
    return res;
}

function myTextField(label,defaultValue,line,multiline,onChangeStatus,statename){
  return (
    <TextField
      className={`myfont_orderall_textfield`}
      label={label}
      defaultValue={defaultValue}
      InputProps={{
        readOnly: false,
      }}
      rows={line}
      multiline={multiline}
      onChange={(e)=>onChangeStatus(e,statename)}
      variant="outlined"
      style={{width: `95%` , marginTop: `10px`}}
    />
  )
}
function myTextField_money_baht(label,defaultValue,line,multiline,onChangeStatus,statename){
    return (
        <div>
        <InputLabel>{label}</InputLabel>
        <FilledInput
            className={`myfont_orderall_textfield`}
            label={label}
            defaultValue={defaultValue}
            InputProps={{
            readOnly: false,
            }}
            rows={line}
            multiline={multiline}
            startAdornment={<InputAdornment position="start">฿</InputAdornment>}
            onChange={(e)=>onChangeStatus(e,statename)}
            variant="outlined"
            style={{width: `95%` , marginTop: `10px`}}
        />
      </div>
    )
}
function createOptionNumber(start , end){
    var res= []
    for(var i = start ; i < end ; i ++){
        res.push(<MenuItem value={i}>{i}</MenuItem>)
    }
    return res
  }
function createListChoice(){
  var res= []
  res.push(<MenuItem value={true}>{`show`}</MenuItem>)
  res.push(<MenuItem value={false}>{`hide`}</MenuItem>)
  return res
}
MasterDelivery.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
  window: PropTypes.func,
};
function mapStateToProps(state) {
  return {
    lang: state.page_getLangToState,
    arrCart: state.func_actionCart,
    windows: state.getWindows
  }
}
function mapDispatchToProps(dispatch) {
  return {
      actionReducer: (type,payload) => {
        dispatch({type: type,payload:payload})
      }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(useStyles, { withTheme: true })(MasterDelivery))