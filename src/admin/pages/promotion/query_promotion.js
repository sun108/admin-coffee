import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Radium, {StyleRoot} from 'radium';
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid';
import Chip from '@material-ui/core/Chip';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import MaterialTable from 'material-table';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Select from '@material-ui/core/Select';
import Card from '@material-ui/core/Card';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Drawer from '@material-ui/core/Drawer';
import InputAdornment from '@material-ui/core/InputAdornment';
import FilledInput from '@material-ui/core/FilledInput';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Avatar from '@material-ui/core/Avatar';
import ListItemIcon from '@material-ui/core/ListItemIcon';

//component
import {
  StdBtnIconOnly_upToTop,
  StdFabBtn_create,
  StdFabBtn_done,
  StdFabBtn_delete
} from '../../../component/button/std_button'
import {StdSnackBar_control , StdToastMz_error,StdToastMz_success,StdToastMz_info,StdToastMz_warning } from '../../../component/toast/std_toast'

//style
import useStyles  from '../../../style/app.js'

//materialize
import 'materialize-css';

//layout
import {column_query_promotion} from '../../layouts/query_order/query_promotion'

//react-responsive-carousel
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

//icon
import CreateIcon from '@material-ui/icons/Create';
import UpdateIcon from '@material-ui/icons/Update';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import LinkIcon from '@material-ui/icons/Link';

//service
import {getStringTitle , getConsoleError ,generateString_string} from '../../../services/std_service/getString'
import FetchData from '../../../services/fetch_data/fetch'
import FetchData_menu from '../../../services/fetch_data/fetch_menu'
import {funcClickLink} from '../../../services/link/service_link'
import {
  adminSendToServer_promotion ,adminSendToServer_promotionDelete
} from '../../../services/admin/promotion'

const _ = require('lodash');

class MasterPromotion extends Component {
  constructor() {
    super();
    this.state = {
      openCreate: false, openUpdate: false,openUnitSelect: false, openDrawerDelete: false, 
      productIdCode: null ,load: true, 
      listArrPromotionTable: [] , listArrCat: [],listArrCollection: [],listArrUnit: [],listArrRelative: [], 
      listArrSysDelivery: [],listArrRelativeDelivery: [], listArrSelectUnit: [],selectIdcodeDelivery: null,
      listArrPromotionType:[] ,listArrRelativePrmotion:[] ,listTableProd:[],
      
      title_th:null,title_en:null,content_th:null,content_en:null,idcode_promotionType:null,condition: null, active: false,

      idcode_delivery:null, amount: 0, idcode_unit: null, idcode_unit_add: null,
    }
    this.duration = 1;
  }
  componentDidMount() {
    this.start_page(this.props);
  }
  componentWillReceiveProps(props) {
    this.start_page(props);
  }
  start_page= async(props)=>{
    this.setState({load:true})
    var url =null ; var data= []
    var idcode = props.params.productCode

    if(idcode !== null && idcode !== `` && idcode !== undefined && idcode !== `undefined`){
      await this.setState({ productIdCode: idcode, modeIdCode: true ,listArrRelativePrmotion: [] ,active: false})
      url = `${process.env.REACT_APP_URL_API}query/collection/relative_promotion?idcode=${idcode}`
      try{ data.push ({"relative_promotion":await FetchData(url,'relative_promotion',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}
    }else{
      await this.setState({ productIdCode: null, modeIdCode: false,listArrRelativePrmotion: [] ,active: false})
      url = `${process.env.REACT_APP_URL_API}query?relative_promotion=all`
      try{ data.push ({"relative_promotion":await FetchData(url,'relative_promotion',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}
    }

    url = `${process.env.REACT_APP_URL_API}query?promotion_type=all`
    try{ data.push ({"promotion_type":await FetchData(url,'promotion_type',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}
  
    url = `${process.env.REACT_APP_URL_API}query?relative_delivery=all`
    try{ data.push ({"relative_delivery":await FetchData(url,'relative_delivery',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}
    
    url = `${process.env.REACT_APP_URL_API}query?unit_product=all`
    try{ data.push ({"unit_product":await FetchData(url,'unit_product',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}
  
    url = `${process.env.REACT_APP_URL_API}query?system_delivery=all`
    try{ data.push ({"system_delivery":await FetchData(url,'system_delivery',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}

    try{ 
      await this.setState({
        listArrPromotionType: data.find(x=>x['promotion_type']).promotion_type[0],
        listArrRelativeDelivery: data.find(x=>x['relative_delivery']).relative_delivery[0],
        listArrRelativePrmotion: data.find(x=>x['relative_promotion']).relative_promotion[0],
        listArrUnit: data.find(x=>x['unit_product']).unit_product[0],
        listArrSysDelivery: data.find(x=>x['system_delivery']).system_delivery[0],
      })
      await this.packData();
    }catch(e){
        getConsoleError(e)
    }

    this.setState({load:false})
  }

  packData =()=>{
    var listTable = [] , lang = this.props.lang ,tag = [] ,listTableProd = []
    this.state.listArrRelativePrmotion.map(x=>{
      var find = this.state.listArrPromotionType.filter(c=>c[`idcode`] === x.idcode_promotionType);
      // console.log(find)
      if(find.length > 0){
        x.promotionname = x[lang][0].title
        x.tcondition = find[0][lang][0][`condition`]
        x.benefit = find[0][lang][0][`benefit`]
        x.total_use = this.state.listArrRelativePrmotion.filter(v=>v[`idcode_promotion`] === x.idcode).length
        x.activec = (<Checkbox checked={x.active}/>)
        x.manage = (
          <div>
            <Button
              startIcon={<UpdateIcon />}
              color="default"
              style={{backgroundColor: `blue` ,color: `white` , marginTop: `5px`}}
              onClick={funcClickLink.bind(this,`/mng/master_promotion/${x.idcode}`)}
            >
              update
            </Button>
          </div>
        )
        listTable.push(x)
      }
    })
    if(this.state.modeIdCode){
      this.setState({ 
        listArrPromotionTable: listTable ,
        title_th: listTable[0][`th`][0].title, title_en: listTable[0][`en`][0].title, 
        content_th: listTable[0][`th`][0].content, content_en: listTable[0][`en`][0].content, 
        idcode_promotionType: listTable[0].idcode_promotionType, active: listTable[0].active,
        condition: listTable[0].condition,

        idcode_delivery: listTable[0][`idcode_delivery`], amount: listTable[0][`amount`], 
        idcode_unit: listTable[0][`idcode_unit`], idcode_unit_add: listTable[0][`idcode_unit_add`], 
      })
    }else{
      this.setState({ listArrPromotionTable: listTable })
    }
  }
  onChangeState =async(event , state)=>{
    await this.setState({ [state] :event.target.value})
  }
  onOpenDrawer =async(val , state)=>{
    await this.setState({ [state] :val})
  }
  set_submitLoad=()=>{}

  onPanelManage=async(val)=>{
    await this.setState({ open: val })
  }
  onSubmit=async()=>{
    await this.onOpenDrawer(false,`openCreate`)
    await this.onOpenDrawer(false,`openUpdate`)
    await this.packObjAndSend()
  }
  funcConfirmDelete= async()=>{
    var idcode = this.state.productIdCode
    await this.onOpenDrawer(false,`openDrawerDelete`)
    await adminSendToServer_promotionDelete([idcode] , this.set_submitLoad)
  }

  packObjAndSend=async()=>{
    var idcode = this.state.productIdCode
    var active = false
    if(this.state.modeIdCode){
      active = this.state.active
    }
    var res = {
      "th":[
        {"title":this.state.title_th,"content":this.state.content_th}
      ],
      "en":[
        {"title":this.state.title_en,"content":this.state.content_en}
      ],
      "condition" : this.state.condition,
      "active" : active,
      "amount" : this.state.amount,
      "idcode_delivery" : this.state.idcode_delivery,
      "idcode_unit" : this.state.idcode_unit,
      "idcode_unit_add" : this.state.idcode_unit_add,
      "idcode_promotionType": this.state.idcode_promotionType,
    }
    if(this.state.idcode_promotionType !== `` && this.state.idcode_promotionType !== null && this.state.idcode_promotionType !== undefined){
      await adminSendToServer_promotion(res , this.set_submitLoad , idcode, this.state.modeIdCode)
    }else{
      StdToastMz_error(`please select promotion type`)
    }
  }
  
  render(){
    const load = <LinearProgress color="secondary" />
    // const style = `.txt{ color: white ;  } .pic{ width: 30% }`
    const ex = `.carousel{height:unset}
    .rb{left: 0px;}`
    if(this.state.load){
      return (<div>{load}</div>)
    }else{
        var lang = this.props.lang
        var dataRow = this.state.listArrPromotionTable
        var width = this.props.windows.width
        if(this.state.modeIdCode){
          return (
            <React.Fragment>
              <div style={{paddingBottom: `100px`}}>
                <style>{ex}</style>
                <br/>
                <center><h3 className={`myfont_orderall_head`}>{`แสดงรายการ Promotion`}</h3></center>
                <center><h4 className={`myfont_orderall_under_head`}>{this.state.productIdCode}</h4></center>
                <br/>
                <Grid container space={2}>
                  <Grid item xs={9}>
                    <FormControl style={{ width:`100%`,padding:`10px`}}>
                      <InputLabel>{`ประเภท Promotion`}</InputLabel>
                      <Select
                        style={{ width:`100%` }}
                        defaultValue={this.state.idcode_promotionType}
                        onChange={(e)=>this.onChangeState(e ,`idcode_promotionType`)}
                      >
                        {createOptionPromotion(this.state.listArrPromotionType,lang)}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={3}>
                    <FormControl style={{ width:`100%`,padding:`10px`}}>
                      <InputLabel>{`active`}</InputLabel>
                      <Select
                        style={{ width:`100%` }}
                        defaultValue={this.state.active}
                        onChange={(e)=>this.onChangeState(e ,`active`)}
                      >
                        {createOptionActive()}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid container space={2}>
                    <Grid item xs={6}>
                      {myTextField(`ชื่อเงื่อนไข (th)`,this.state.title_th,4,false,this.onChangeState,`title_th`)}
                    </Grid>
                    <Grid item xs={6}>
                      {myTextField(`ชื่อเงื่อนไข (en)`,this.state.title_en,4,false,this.onChangeState,`title_en`)}
                    </Grid>
                    <Grid item xs={6}>
                      {myTextField(`รายละเอียดเงื่อนไข (th)`,this.state.content_th,4,true,this.onChangeState,`content_th`)}
                    </Grid>
                    <Grid item xs={6}>
                      {myTextField(`รายละเอียดเงื่อนไข (en)`,this.state.content_en,4,true,this.onChangeState,`content_en`)}
                    </Grid>
                  </Grid>
                </Grid>
                <hr/>
                {createPanelCondition(
                  this.state.idcode_promotionType ,lang, 
                  this.onChangeState, this.state.condition ,
                  this.state.listArrSysDelivery ,
                  this.state.listArrUnit ,
                  this.state.idcode_delivery ,
                  this.state.idcode_unit ,
                  this.state.idcode_unit_add ,
                  this.state.amount ,
                )}
                {drawerDeleteProduct(`right` , 
                  this.state.openDrawerDelete,
                  this.onOpenDrawer.bind(this,true,`openDrawerDelete`), 
                  this.funcConfirmDelete , 
                  this.onOpenDrawer.bind(this,false,`openDrawerDelete`) 
                )}
                <StdBtnIconOnly_upToTop />
                <StdFabBtn_done funcClickSubmit={this.onSubmit.bind(this)}/>
                <StdFabBtn_delete funcClickSubmit={this.onOpenDrawer.bind(this,true,`openDrawerDelete`)} />
              </div>
            </React.Fragment>
          )
        }
        return (
          <React.Fragment>
            <style>{ex}</style>
            <br/>
            <center><h3 className={`myfont_orderall_head`}>{`แสดงรายการ Promotion ทั้งหมด`}</h3></center>
            <MaterialTable
              tableRef={this.tableRef}
              align="center" 
              title=""
              columns={column_query_promotion()[lang]}
              style={{zIndex: 0 , fontSize: `medium` , width: `100%`}}
              data={dataRow}
              options={{
                pageSize: dataRow.length,
                pageSizeOptions: [dataRow.length, 100, 500]
              }}
            />
              {drawerCreate(
                `bottom`,
                this.state.openCreate,
                this.onOpenDrawer.bind(this,false,`openCreate`),
                this.onChangeState,
                this.onSubmit.bind(this,false),
                this.state.listArrPromotionType,lang
              )}
            <StdBtnIconOnly_upToTop />
            <StdFabBtn_create funcClickLink={this.onOpenDrawer.bind(this,true,`openCreate`)} />
          </React.Fragment>
        )
    }
  }
}
function createOptionActive(){
  var res = []
  res.push(<MenuItem value={true}>{`✨ active`}</MenuItem>)
  res.push(<MenuItem value={false}>{`❌ deactivate`}</MenuItem>)
  return res
}
function createOptionPromotion(listPromotionType,lang){
  var res = []
  listPromotionType.map(x=>{
    res.push(<MenuItem value={x.idcode}>{x[lang][0][`title`]}</MenuItem>)
  })
  return res
}
function createPanelCondition(idcode ,lang ,onChangeState ,condition ,listDelivery ,listUnit ,idcode_delivery, idcode_unit , idcode_unit_add ,amount){
  var res = []
  if(idcode === `promotion_001`){
    res.push(
      <Grid container space={2}>
        <Grid item xs={6}>
          {myTextField(`ราคาที่เข้าเงื่อนไข`,condition,4,false,onChangeState,`condition`)}
        </Grid>
        <Grid item xs={6}>
          <FormControl style={{ width:`100%`,padding:`10px`}}>
            <InputLabel>{`รายการขนส่ง`}</InputLabel>
            <Select
              style={{ width:`100%` }}
              defaultValue={idcode_delivery}
              onChange={(e)=>onChangeState(e ,`idcode_delivery`)}
            >
              {listDelivery.map(i=>
                (<MenuItem value={i.idcode}>{i[lang]}</MenuItem>)
              )}
            </Select>
          </FormControl>
        </Grid>
      </Grid>
    )
  }else 
  if(idcode === `promotion_002`){
    res.push(
      <Grid container space={2}>
        <Grid item xs={4}>
          <FormControl style={{ width:`100%`,padding:`10px`}}>
            <InputLabel>{`รายการสินค้าที่จะทำโปร`}</InputLabel>
            <Select
              style={{ width:`100%` }}
              defaultValue={idcode_unit}
              onChange={(e)=>onChangeState(e ,`idcode_unit`)}
            >
              {listUnit.map(i=>
                (<MenuItem value={i.idcode}>
                  <ListItemIcon>
                    {<Avatar src={`${process.env.REACT_APP_URL_ASSETS}unit_product/${i.idcode}/${i.img}`} />}
                  </ListItemIcon>
                  {i[lang][0][`title`]}
                </MenuItem>)
              )}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={2}>
          <FormControl style={{ width:`100%`,padding:`10px`}}>
            <InputLabel>{`จำนวนสินค้าที่จะเข้าเงื่อนไข`}</InputLabel>
            <Select
              style={{ width:`100%` }}
              defaultValue={condition}
              onChange={(e)=>onChangeState(e ,`condition`)}
            >
              {createOptionNumber(1,10)}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={2}>
          <FormControl style={{ width:`100%`,padding:`10px`}}>
            <InputLabel>{`จำนวนสินค้าที่จะแถม`}</InputLabel>
            <Select
              style={{ width:`100%` }}
              defaultValue={amount}
              onChange={(e)=>onChangeState(e ,`amount`)}
            >
              {createOptionNumber(1,10)}
            </Select>
          </FormControl>
        </Grid>
      </Grid>
    )
  }else
  if(idcode === `promotion_003`){
    res.push(
      <Grid container space={2}>
        <Grid item xs={4}>
          <FormControl style={{ width:`100%`,padding:`10px`}}>
            <InputLabel>{`รายการสินค้าที่จะทำโปร`}</InputLabel>
            <Select
              style={{ width:`100%` }}
              defaultValue={idcode_unit}
              onChange={(e)=>onChangeState(e ,`idcode_unit`)}
            >
              {listUnit.map(i=>
                (<MenuItem value={i.idcode}>
                  <ListItemIcon>
                    {<Avatar src={`${process.env.REACT_APP_URL_ASSETS}unit_product/${i.idcode}/${i.img}`} />}
                  </ListItemIcon>
                  {i[lang][0][`title`]}
                </MenuItem>)
              )}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={4}>
          <FormControl style={{ width:`100%`,padding:`10px`}}>
            <InputLabel>{`รายการสินค้าที่จะแถม`}</InputLabel>
            <Select
              style={{ width:`100%` }}
              defaultValue={idcode_unit_add}
              onChange={(e)=>onChangeState(e ,`idcode_unit_add`)}
            >
              {listUnit.map(i=>
                (<MenuItem value={i.idcode}>
                  <ListItemIcon>
                    {<Avatar src={`${process.env.REACT_APP_URL_ASSETS}unit_product/${i.idcode}/${i.img}`} />}
                  </ListItemIcon>
                  {i[lang][0][`title`]}
                </MenuItem>)
              )}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={2}>
          <FormControl style={{ width:`100%`,padding:`10px`}}>
            <InputLabel>{`จำนวนสินค้าที่จะเข้าเงื่อนไข`}</InputLabel>
            <Select
              style={{ width:`100%` }}
              defaultValue={condition}
              onChange={(e)=>onChangeState(e ,`condition`)}
            >
              {createOptionNumber(1,10)}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={2}>
          <FormControl style={{ width:`100%`,padding:`10px`}}>
            <InputLabel>{`จำนวนสินค้าที่จะแถม`}</InputLabel>
            <Select
              style={{ width:`100%` }}
              defaultValue={amount}
              onChange={(e)=>onChangeState(e ,`amount`)}
            >
              {createOptionNumber(1,10)}
            </Select>
          </FormControl>
        </Grid>
      </Grid>
    )
  }
  return res
}
function drawerDeleteProduct(anchor, open, onClose ,funcConfirm ,funcCancel){
  var res = []
  res.push(
    <React.Fragment key={anchor}>
      <Drawer anchor={anchor} open={open} onClose={onClose}>
        <div style={{paddingLeft: `20px` , paddingRight: `20px`}}>
          <Grid container space={2}>
            <Grid item xs={12}>
              <center><h5>{`confirm for delete`}</h5></center>
              <center>
                <Button startIcon={<DoneAllIcon/>} 
                  style={{backgroundColor: `green` ,color: `white` ,marginRight: `5px`}}
                  onClick={funcConfirm}
                >
                  {`confirm`}
                </Button>
                <Button startIcon={<DeleteForeverIcon/>} 
                  style={{backgroundColor: `red` ,color: `white`}}
                  onClick={funcCancel}
                >
                  {`cancel`}
                </Button>
              </center>
            </Grid>
          </Grid>
        </div>
      </Drawer>
    </React.Fragment>
  )
  return res;
}
function drawerCreate(anchor,openStatus,onClose,onChangeState,onSubmitCreate,listPromotionType,lang){
  var res = []
  res.push(
    <React.Fragment key={anchor}>
      <Drawer anchor={anchor} open={openStatus} onClose={onClose}>
        <div style={{padding: `20px`}}>
          <Grid container space={2}>
            <center><h5 className={`myfont_orderall_under_head`}>{`เพิ่มรูปแบบโปรโมชั่น`}</h5></center>
          </Grid>
          <Grid container space={2}>
            <Grid item xs={6}>
              {myTextField(`promotion name (TH)`,``,4,false,onChangeState,`title_th`)}
            </Grid>
            <Grid item xs={6}>
              {myTextField(`promotion name (EN)`,``,4,false,onChangeState,`title_en`)}
            </Grid>
            <Grid item xs={6}>
              {myTextField(`promotion content (TH)`,``,4,true,onChangeState,`content_th`)}
            </Grid>
            <Grid item xs={6}>
              {myTextField(`promotion content (EN)`,``,4,true,onChangeState,`content_en`)}
            </Grid>
            <Grid item xs={12}>
              <FormControl style={{ width:`100%`,padding:`10px`}}>
                <InputLabel>{`ประเภท Promotion`}</InputLabel>
                <Select
                  style={{ width:`100%` }}
                  onChange={(e)=>onChangeState(e ,`idcode_promotionType`)}
                >
                  {createOptionPromotion(listPromotionType,lang)}
                </Select>
              </FormControl>
            </Grid>
            
            <Grid item xs={12}>
              <hr/>
              <center>
                <Button
                  startIcon={<CreateIcon />}
                  color="default"
                  style={{backgroundColor: `orange` ,color: `white`}}
                  onClick={onSubmitCreate.bind(this)}
                >
                  Create
                </Button>
              </center>
            </Grid>
          </Grid>
        </div>
      </Drawer>
    </React.Fragment>
  )
  return res;
}
function myTextField(label,defaultValue,line,multiline,onChangeStatus,statename){
  return (
    <TextField
      className={`myfont_orderall_textfield`}
      label={label}
      defaultValue={defaultValue}
      InputProps={{
        readOnly: false,
      }}
      rows={line}
      multiline={multiline}
      onChange={(e)=>onChangeStatus(e,statename)}
      variant="outlined"
      style={{width: `95%` , marginTop: `10px`}}
    />
  )
}
function myTextField_money_baht(label,defaultValue,line,multiline,onChangeStatus,statename){
    return (
        <div>
        <InputLabel>{label}</InputLabel>
        <FilledInput
            className={`myfont_orderall_textfield`}
            label={label}
            defaultValue={defaultValue}
            InputProps={{
            readOnly: false,
            }}
            rows={line}
            multiline={multiline}
            startAdornment={<InputAdornment position="start">฿</InputAdornment>}
            onChange={(e)=>onChangeStatus(e,statename)}
            variant="outlined"
            style={{width: `95%` , marginTop: `10px`}}
        />
      </div>
    )
}
function createOptionNumber(start , end){
    var res= []
    for(var i = start ; i < end ; i ++){
        res.push(<MenuItem value={i}>{i}</MenuItem>)
    }
    return res
  }
function createListChoice(){
  var res= []
  res.push(<MenuItem value={true}>{`show`}</MenuItem>)
  res.push(<MenuItem value={false}>{`hide`}</MenuItem>)
  return res
}
MasterPromotion.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
  window: PropTypes.func,
};
function mapStateToProps(state) {
  return {
    lang: state.page_getLangToState,
    arrCart: state.func_actionCart,
    windows: state.getWindows
  }
}
function mapDispatchToProps(dispatch) {
  return {
      actionReducer: (type,payload) => {
        dispatch({type: type,payload:payload})
      }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(useStyles, { withTheme: true })(MasterPromotion))