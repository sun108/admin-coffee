import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Radium, {StyleRoot} from 'radium';
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid';
import Chip from '@material-ui/core/Chip';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import MaterialTable from 'material-table';
import Drawer from '@material-ui/core/Drawer';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Avatar from '@material-ui/core/Avatar';

//component
import {
  StdBtnIconOnly_upToTop,
  StdFabBtn_create,
  StdFabBtn_done,
  StdFabBtn_delete
} from '../../../component/button/std_button'

//style
import styleCode from '../../../style/menu'
import useStyles  from '../../../style/app.js'

//materialize
import 'materialize-css';
import { Collapsible, CollapsibleItem } from 'react-materialize';

//layout
import {column_query_admin} from '../../layouts/admin/admin'

//react-responsive-carousel
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

//icon
import AddCircleIcon from '@material-ui/icons/AddCircle';
import UpdateIcon from '@material-ui/icons/Update';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import LinkIcon from '@material-ui/icons/Link';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import DoneAllIcon from '@material-ui/icons/DoneAll';

//service
import {getStringTitle , getConsoleError ,generateString_string} from '../../../services/std_service/getString'
import FetchData from '../../../services/fetch_data/fetch'
import FetchData_fhr from '../../../services/fetch_data/fetch_fhr'
import {funcClickLink} from '../../../services/link/service_link'
import {adminSendToServer_deleteAdmin , adminSendToServer_additionAdmin} from '../../../services/admin/admin'

class masterAdmin extends Component {
  constructor() {
    super();
    this.state = {
      open: false,lang: null,load: true, load_adminPanel: false,
      listArrAdmin: [], dataSource_person: [], dataSource_admin:[], listTableAdmin:[],
    }
  }
  componentDidMount() {
    this.start_page(this.props);
  }
  componentWillReceiveProps(props) {
    this.start_page(props);
  }
  start_page= async(props)=>{
    this.setState({load:true})
    var url =null ; var data= []
    var idcode = props.params.productCode

    if(idcode !== null && idcode !== `` && idcode !== undefined && idcode !== `undefined`){
        await this.setState({ productIdCode: idcode })
        if(idcode !== `create`){
          url = `${process.env.REACT_APP_URL_API}query/collection/unit_product?idcode=${idcode}`
          try{ data.push ({"unit_product":await FetchData(url,'unit_product',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}
        }else{
          url = `${process.env.REACT_APP_URL_API}query?unit_product=all`
          try{ data.push ({"unit_product":await FetchData(url,'unit_product',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}
        }
    }else{
        await this.setState({ productIdCode: null })

        url = `${process.env.REACT_APP_URL_API}query?data_admin=all`
        try{ data.push ({"dataSource_admin":await FetchData(url,'dataSource_admin',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}
        
        url = `${process.env.REACT_APP_URL_API_PERSON}query/collection/person?idcode_employee=all`
        try{ await data.push ({"dataSource_person":await FetchData_fhr(url,'dataSource_person',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}
        }
        
    try{ 
      await this.setState({
        listArrAdmin: data.find(x=>x['dataSource_admin']).dataSource_admin[0],
        dataSource_person: data.find(x=>x['dataSource_person']).dataSource_person[0],
      })
      await this.packData();
    }catch(e){
      getConsoleError(e)
    }

    this.setState({load:false})
  }

  packData =async()=>{
    var listTable = [] ,lang = this.props.lang 
    this.state.listArrAdmin.map(x=>{
        var personSource = this.state.dataSource_person.filter(v=>v[`idcode_employee`] === x.idcode_employee)
        if(personSource.length > 0){
            console.log(personSource)
            x.emp_idcode = x.idcode_employee
            x.manage = (
                <Button
                    startIcon={<UpdateIcon />}
                    color="default"
                    style={{backgroundColor: `blue` ,color: `white`}}
                    onClick={funcClickLink.bind(this,`/mng/master_unit/${x.idcode}`)}
                >
                    update
                </Button>
            )
            x.nickname = `${personSource[0].nickname_en} (${personSource[0].firstname_en} ${personSource[0].lastname_en})`
            x.picture = (<img src={`${process.env.REACT_APP_URL_PICTURE_PERSON}${personSource[0].idcode}.jpg`} style={{width:`50%`}} />)
            listTable.push(x)
        }
    })
    await this.setState({ listTableAdmin: listTable })
  }
  
  render(){
    const load = <LinearProgress color="secondary" />
    // const style = `.txt{ color: white ;  } .pic{ width: 30% }`
    const ex = `.carousel{height:unset}
    .rb{ left: 0px;}`
    var width = this.props.windows.width
    if(this.state.load){
      return (<div>{load}</div>)
    }else{
        var lang = this.props.lang ,title_head = null ,dataRow=null ,modeCreate = false
        if(this.state.productIdCode !== null){
          if(this.state.productIdCode === `create`){
            modeCreate = true
            dataRow = []
            title_head = (
              <div>
                <center><h3 className={`myfont_orderall_head`}>{`เพิ่มรายชื่อผู้ดูแลระบบ`}</h3></center>
              </div>
            )
          }else{
            dataRow = this.state.listArrUnitTable
            title_head = (
            <div>
              <center><h3 className={`myfont_orderall_head`}>{`รายชื่อผู้ดูแลระบบ`}</h3></center>
              <center><h4 className={`myfont_orderall_under_head`}>{dataRow[0][lang][0].title}</h4></center>
            </div>
            )
          }
          return (
            <React.Fragment>
              <style>{ex}</style>
              <br/>
          </React.Fragment>
          )
        }else{
          dataRow = this.state.listArrAdmin
          return (
            <React.Fragment>
              <style>{ex}</style>
              <br/>
              <center><h3 className={`myfont_orderall_head`}>{`รายชื่อผู้ดูแลระบบ`}</h3></center>
              {
                this.state.open ?
                (<div>{load}</div>) :
                <MaterialTable
                  tableRef={this.tableRef}
                  align="center" 
                  title=""
                  columns={column_query_admin()[lang]}
                  data={dataRow}
                  options={{
                      pageSize: dataRow.length,
                      pageSizeOptions: [dataRow.length, 100, 500]
                  }}
                  style={{zIndex: 0 , fontSize: `smaller` , width: `100%`}}
                />
              }
              {drawerManageAdmin(
                `right`,
                this.state.open,
                this.onPanelAdmin.bind(this,false) , 
                this.state.dataSource_person , 
                width ,
                this.state.listArrAdmin,
                this.onChangeAdminInDrawer
              )}
              <StdBtnIconOnly_upToTop />
              <StdFabBtn_create funcClickLink={this.onPanelAdmin.bind(this,true)} />
            </React.Fragment>
          )
        }
    }
  }
  onPanelAdmin =(val)=>{
    this.setState({ open:val})
  }
  onChangeAdminInDrawer= async(e,idcode_emp)=>{
    var reListForUpdateInState = [];
    var dataArr = [];
    if(!e.target.checked){
      var idcode_delete = this.state.listArrAdmin.filter(b=>b[`idcode_employee`] === idcode_emp)
      reListForUpdateInState = this.state.listArrAdmin
      idcode_delete.map(h=>{
        dataArr.push(h.idcode)
        reListForUpdateInState = reListForUpdateInState.filter(g=>g[`idcode`] !== h.idcode)
      })
      await adminSendToServer_deleteAdmin( dataArr )
    }else{
      var genidcode = generateString_string(50)
      reListForUpdateInState = this.state.listArrAdmin
      dataArr = {"idcode":genidcode , "idcode_employee":idcode_emp , "type":"fhr"}
      await reListForUpdateInState.push(dataArr)
      await adminSendToServer_additionAdmin( dataArr )
    }
    await this.setState({ listArrAdmin: reListForUpdateInState })
    await this.packData();
  }
}

function drawerManageAdmin(anchor,openStatus,onClose,dataSource_person,width,listadmin,onChangeAdminInDrawer){
    var res = []
    res.push(
      <React.Fragment key={anchor}>
        <Drawer anchor={anchor} open={openStatus} onClose={onClose}>
            <div style={{padding: `20px` , width: `${width/3.5}px`}}>
              <MenuList>
                {queryListTagInDrawer(dataSource_person,listadmin,onChangeAdminInDrawer)}
              </MenuList>
            </div>
        </Drawer>
      </React.Fragment>
    )
    return res;
}

function queryListTagInDrawer(dataSource_person,listadmin,onChangeAdminInDrawer){
    var res = []
    dataSource_person.map(x=>{
      res.push(
        <FormControlLabel style={{paddingLeft: `10px` , paddingRight: `10px`}}
          control={<Checkbox 
            checked={listadmin.filter(v=>v[`idcode_employee`] === x.idcode_employee).length > 0 ? true : false} 
            onChange={(e)=>{onChangeAdminInDrawer(e,x.idcode_employee)}}
          />}
          label={
            <Chip avatar={<Avatar src={`${process.env.REACT_APP_URL_PICTURE_PERSON}${x.idcode}.jpg`} />} label={`(${x.nickname_en}) ${x.firstname_en}`} />
          }
        />
      )
    })
    return res
  }

masterAdmin.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
  window: PropTypes.func,
};
function mapStateToProps(state) {
  return {
    lang: state.page_getLangToState,
    arrCart: state.func_actionCart,
    windows: state.getWindows
  }
}
function mapDispatchToProps(dispatch) {
  return {
      actionReducer: (type,payload) => {
        dispatch({type: type,payload:payload})
      }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(useStyles, { withTheme: true })(masterAdmin))