import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Radium, {StyleRoot} from 'radium';
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid';
import Chip from '@material-ui/core/Chip';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Hidden from '@material-ui/core/Hidden';
import { Link ,Router , browserHistory} from "react-router";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import MaterialTable from 'material-table';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Select from '@material-ui/core/Select';
import Card from '@material-ui/core/Card';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';

//component
import {StdBtnIconOnly_upToTop,StdBtnIconOnly_customDirect} from '../../../component/button/std_button'

//style
import styleCode from '../../../style/menu'
import useStyles  from '../../../style/app.js'

//materialize
import 'materialize-css';
import { Collapsible, CollapsibleItem } from 'react-materialize';

//layout
import {column_query_collection} from '../../layouts/query_order/query_collection'

//react-responsive-carousel
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

//icon
import CreateIcon from '@material-ui/icons/Create';
import UpdateIcon from '@material-ui/icons/Update';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';

//service
import {getStringTitle , getConsoleError ,generateString_string} from '../../../services/std_service/getString'
import {myFormatDate , myFormatTime ,mySubtractFormatDate} from '../../../services/std_service/getDate'
import {add_arrItem} from '../../../services/mockup/arrItem'
import FetchData from '../../../services/fetch_data/fetch'
import FetchData_menu from '../../../services/fetch_data/fetch_menu'
import {funcClickLink} from '../../../services/link/service_link'

const _ = require('lodash');

class MasterCollection extends Component {
  constructor() {
    super();
    this.state = {
      open: true,mobileOpen: false,lang: null,load: true,errString: null ,
      listArrCat: [],listArrCollection: [],listArrUnit: [],listArrRelative: [],listArrRibbon:[],
      startDate: null , toDate: null, listArrCollectionTable: [] ,
      productIdCode: null ,showPromotion: false
    }
    // this.addItemToCart = this.addItemToCart.bind(this);
    this.duration = 1;
  }
  componentDidMount() {
    this.start_page(this.props);
  }
  componentWillReceiveProps(props) {
    this.start_page(props);
  }
  start_page= async(props)=>{
    this.setState({load:true})
    var url =null ; var data= []
    var idcode = props.params.productCode

    url = `${process.env.REACT_APP_URL_API}query?collection_type=all`
    try{ data.push ({"collection_type":await FetchData(url,'collection_type',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}

    url = `${process.env.REACT_APP_URL_API}query?unit_product=all`
    try{ data.push ({"unit_product":await FetchData(url,'unit_product',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}
        
    try{ 
      await this.setState({
        listArrCollection: data.find(x=>x['collection_type']).collection_type[0],
        listArrUnit: data.find(x=>x['unit_product']).unit_product[0],
      })
      await this.packData();
    }catch(e){
        getConsoleError(e)
    }

    this.setState({load:false})
  }

  packData =()=>{
    var listTable = [] , lang = this.props.lang ,tag = []
    this.state.listArrCollection.map(x=>{
      x.collectname = x[lang]
      x.unitCount = this.state.listArrUnit.filter(v=>v[`collection`] === x.idcode).length
      x.manage = (
          <Button
              startIcon={<UpdateIcon />}
              color="default"
              style={{backgroundColor: `blue` ,color: `white`}}
              onClick={funcClickLink.bind(this,`/mng/master_unit/${x.idcode}`)}
          >
              update
          </Button>
      )
      listTable.push(x)
    })
    this.setState({ listArrCollectionTable: listTable })
  }

  onChangeStatus =async(event , state)=>{
    await this.setState({ [state] :event.target.value})
  }

  onChangeOption= async(event,state)=>{
    await this.setState({ [state]:event.target.value})
    await this.packData()
  }

  onDeleteChip=()=>{
    console.log(`test`)
  }
  
  render(){
    const load = <LinearProgress color="secondary" />
    // const style = `.txt{ color: white ;  } .pic{ width: 30% }`
    const ex = `
    .carousel{height:unset}
    .rb{
      left: 0px;
    }`
    if(this.state.load){
      return (<div>{load}</div>)
    }else{
        var lang = this.props.lang
        var dataRow = this.state.listArrCollectionTable
        if(this.state.productIdCode !== null){
            var def_showPromotion = dataRow[0].price !== dataRow[0].promotion ? (true) : (false)
            return (
                <React.Fragment>
                    <style>{ex}</style>
                    <br/>
                    <center><h3 className={`myfont_orderall_head`}>{`แสดงรายการ Collection`}</h3></center>
                    <center><h4 className={`myfont_orderall_under_head`}>{dataRow[0][lang][0].title}</h4></center>
                    <br/>
                  <StdBtnIconOnly_upToTop />
                </React.Fragment>
            )
        }else{
            return (
                <React.Fragment>
                    <style>{ex}</style>
                    <br/>
                    <center><h3 className={`myfont_orderall_head`}>{`แสดงรายการ Collection ทั้งหมด`}</h3></center>
                    <MaterialTable
                      tableRef={this.tableRef}
                      align="center" 
                      title=""
                      columns={column_query_collection()[lang]}
                      style={{zIndex: 0 , fontSize: `medium` , width: `100%`}}
                      data={dataRow}
                      options={{
                        pageSize: dataRow.length,
                        pageSizeOptions: [dataRow.length, 100, 500]
                      }}
                    />
                    <StdBtnIconOnly_upToTop />
                    <StdBtnIconOnly_customDirect/>
                </React.Fragment>
            )
        }
    }
  }
}

function createListChoice(){
  var res= []
  res.push(<MenuItem value={true}>{`show`}</MenuItem>)
  res.push(<MenuItem value={false}>{`hide`}</MenuItem>)
  return res
}

function myTextField(label,defaultValue,line,multiline){
  return (
    <TextField
      className={`myfont_orderall_textfield`}
      label={label}
      defaultValue={defaultValue}
      InputProps={{
        readOnly: false,
      }}
      rows={line}
      multiline={multiline}
      variant="outlined"
      style={{width: `95%` , marginTop: `10px`}}
    />
  )
}

MasterCollection.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
  window: PropTypes.func,
};
function mapStateToProps(state) {
  return {
    lang: state.page_getLangToState,
    arrCart: state.func_actionCart
  }
}
function mapDispatchToProps(dispatch) {
  return {
      actionReducer: (type,payload) => {
        dispatch({type: type,payload:payload})
      }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(useStyles, { withTheme: true })(MasterCollection))