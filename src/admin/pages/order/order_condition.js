import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Radium, {StyleRoot} from 'radium';
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Hidden from '@material-ui/core/Hidden';
import { Link ,Router , browserHistory} from "react-router";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import MaterialTable from 'material-table';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import Card from '@material-ui/core/Card';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';

//component
import {StdBtnIconOnly_upToTop} from '../../../component/button/std_button'

//style
import styleCode from '../../../style/menu'
import useStyles  from '../../../style/app.js'

//materialize
import 'materialize-css';
import { Collapsible, CollapsibleItem } from 'react-materialize';

//layout
import {column_query_order} from '../../layouts/query_order/query_order'

//react-responsive-carousel
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

//icon
import CreateIcon from '@material-ui/icons/Create';
import LinkIcon from '@material-ui/icons/Link';

//service
import {getStringTitle , getConsoleError ,generateString_string} from '../../../services/std_service/getString'
import {myFormatDate , myFormatTime ,mySubtractFormatDate} from '../../../services/std_service/getDate'
import {add_arrItem} from '../../../services/mockup/arrItem'
import FetchData from '../../../services/fetch_data/fetch'
import FetchData_menu from '../../../services/fetch_data/fetch_menu'
import {funcClickLink} from '../../../services/link/service_link'

const _ = require('lodash');

class OrderQueryCondition extends Component {
  constructor() {
    super();
    this.state = {
      open: true,mobileOpen: false,lang: null,load: true,errString: null ,
      listArrOrderStatus: [],listArrCustomerOrder: [], listdataTable: [], listArrPayment: [],
      showSuccessOrder: true, showCancelOrder: true ,
      startDate: null , toDate: null
    }
    // this.addItemToCart = this.addItemToCart.bind(this);
    this.duration = 1;
  }
  onChangeNumber(e){
    const re = /^[0-9\b]+$/;
    if (e.target.value === '' || re.test(e.target.value)) {
       this.setState({value: e.target.value})
    }
  }
  componentDidMount() {
    this.start_page(this.props);
  }
  componentWillReceiveProps(props) {
    this.start_page(props);
  }
  start_page= async()=>{
    this.setState({startDate: mySubtractFormatDate(14) , toDate: myFormatDate() ,load:false})
  }

  onStartFetch=async()=>{
    if(this.state.startDate !== null && this.state.startDate !== `` && this.state.toDate !== null && this.state.toDate !== ``){
      //this.state.startDate , this.state.toDate
      this.setState({load:true})
      var url =null ; var data= []
  
      url = `${process.env.REACT_APP_URL_API}admin/menu/dashboard`
      try{ data.push ({"menu_dashboard":await FetchData_menu(url,'menu_dashboard',process.env.REACT_APP_TIME_Fetch_primary ,bodyReq(this.state.startDate , this.state.toDate))})  }catch(e){getConsoleError(e)}
  
      url = `${process.env.REACT_APP_URL_API}query?pament_system=all`
      try{ data.push ({"pament_system":await FetchData(url,'pament_system',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}
  
      try{ 
        await this.setState({
          listArrOrderStatus: data.find(x=>x['menu_dashboard']).menu_dashboard.find(x=>x['order_status']).order_status,
          listArrCustomerOrder: data.find(x=>x['menu_dashboard']).menu_dashboard.find(x=>x['customer_order']).customer_order,
          listArrPayment: data.find(x=>x['pament_system']).pament_system[0],
        })
        await this.packData();
      }catch(e){
        getConsoleError(e)
      }
  
      this.setState({load:false})
    }
  }

  packData =()=>{
    var listdataTable = [] , index = 0, nowStatus = 1 ,add = true;
    if(this.state.listArrCustomerOrder.length > 0){
      var list = this.state.listArrCustomerOrder.sort(function(a,b){ 
        return new Date(a.datePure) < new Date(b.datePure)
      });
      list.map(x=>{
        add = true
        nowStatus = 1
        x.cpv = (
          <a href={`${process.env.REACT_APP_URL_THIS_PROD}tracking/${x.orderid}`} target='_blank'>
            <Button startIcon={<LinkIcon />} color="default" style={{backgroundColor: `#b71c1c` ,color: `white`}} >
              {`preview`}
            </Button>
          </a>
        )
        x.manage = (
        <Button style={{backgroundColor: `blue` , color: `white`}} 
          onClick={funcClickLink.bind(this,`/mng/orderidcode/${x.orderid}`)}
        >
          <CreateIcon/>
        </Button>)
        var findItem = this.state.listArrOrderStatus.filter(v=>v[`idcode_order`] === x.orderid)
        if(findItem.length > 0){
          index = findItem.length -1
          nowStatus = ((findItem[index].now)+1)
        }
        x.status = nowStatus
        if(!this.state.showSuccessOrder && nowStatus === 3){
          add = false
        }
        if(!this.state.showCancelOrder && nowStatus === 1100){
          add = false
        }
        // console.log(`${add} ${this.state.showCancelOrder} ${this.state.showSuccessOrder} ${nowStatus}`)
        if(add){
          listdataTable.push(x)
        }
      })
      this.setState({ listdataTable: listdataTable })
    }
  }

  onChangeStatus =async(event , state)=>{
    console.log(event.target.value)
    await this.setState({ [state] :event.target.value})
  }

  onChangeOption= async(event,state)=>{
    await this.setState({ [state]:event.target.value})
    await this.packData()
  }
  
  render(){
    const load = <LinearProgress color="secondary" />
    // const style = `.txt{ color: white ;  } .pic{ width: 30% }`
    const ex = `
    .carousel{height:unset}
    .rb{
      left: 0px;
    }`
    if(this.state.load){
      return (<div>{load}</div>)
    }else{
        var lang = this.props.lang
        var dataRow = this.state.listdataTable
        return (
            <React.Fragment>
              <style>{ex}</style>
              <br/>
              <center><h3 className={`myfont_orderall_head`}>{`การแสดงผลแบบตั้งค่าได้`}</h3></center>
              <Card boxShadow={3} className={`myCard_style`}>
                <h4 className={`myCard_style_title`}>Setting For Query Data</h4>
                    <Grid container space={2}>
                        <Grid item xs={5}>
                            <FormControl style={{ width:`100%`,padding:`10px`}}>
                                <TextField
                                    id="date"
                                    label="วันที่เริ่มต้นการแสดงผล"
                                    type="date"
                                    format={'DD/MM/YYYY'}
                                    onChange={(e)=>this.onChangeStatus(e,`startDate`)}
                                    defaultValue={this.state.startDate}
                                    InputLabelProps={{
                                      shrink: true,
                                    }}
                                />
                            </FormControl>
                        </Grid>
                        <Grid item xs={5}>
                            <FormControl style={{ width:`100%`,padding:`10px`}}>
                                <TextField
                                    id="date"
                                    label="ถึงวันที่"
                                    type="date"
                                    format={'DD/MM/YYYY'}
                                    onChange={(e)=>this.onChangeStatus(e,`toDate`)}
                                    defaultValue={this.state.toDate}
                                    InputLabelProps={{
                                      shrink: true,
                                    }}
                                />
                            </FormControl>
                        </Grid>
                        <Grid item xs={2}>
                            <Button style={{backgroundColor: `green`,color:`white`,width:`100%`}} onClick={this.onStartFetch.bind(this)}>QUERY</Button>
                        </Grid>
                    </Grid>
                </Card>
                <br/>
                <Card boxShadow={3} className={`myCard_style`} style={{display: this.state.listArrCustomerOrder.length > 0 ? `block` : `none`}}>
                  <h4 className={`myCard_style_title`}>Result Query Data</h4>
                    <Grid container space={2}>
                      <Grid item xs={2}>
                          <FormControl style={{ width:`100%`,padding:`10px`}}>
                              <InputLabel>ตั้งค่าแสดงสถานะที่ 3</InputLabel>
                              <Select
                                  style={{ width:`100%` }}
                                  defaultValue={this.state.showSuccessOrder}
                                  onChange={(e)=>this.onChangeOption(e ,`showSuccessOrder`)}
                              >
                                  {createListChoice()}
                              </Select>
                          </FormControl>
                      </Grid>
                      <Grid item xs={2}>
                          <FormControl style={{ width:`100%`,padding:`10px`}}>
                              <InputLabel>ตั้งค่าสถานะที่ยกเลิก</InputLabel>
                              <Select
                                  style={{ width:`100%` }}
                                  defaultValue={this.state.showCancelOrder}
                                  onChange={(e)=>this.onChangeOption(e ,`showCancelOrder`)}
                              >
                                  {createListChoice()}
                              </Select>
                          </FormControl>
                      </Grid>
                    </Grid>
                    <Grid>
                    <MaterialTable
                      tableRef={this.tableRef}
                      align="center" 
                      title=""
                      columns={column_query_order()[lang]}
                      data={dataRow}
                      options={{
                        pageSize: this.state.listArrCustomerOrder.length,
                        pageSizeOptions: [this.state.listArrCustomerOrder.length, 100, 500]
                      }}
                      style={{zIndex: 0 , fontSize: `medium` , width: `100%`}}
                      detailPanel={[
                        {
                          tooltip: 'Show Name',
                          render: rowData => {
                            return (
                              <Container>
                              <div style={{padding: `10px` , backgroundColor: `rgb(217, 222, 255)`}}>
                                <Grid container spacing={2}>
                                  <Grid item xs={6} sm={2}>
                                    {textFieldReadOnly(`ผู้รับสินค้า`,rowData.cust_name)}
                                  </Grid>
                                  <Grid item xs={6} sm={2}>
                                    {textFieldReadOnly(`เบอร์ติดต่อ`,rowData.cust_tel)}
                                  </Grid>
                                  <Grid item xs={6} sm={2}>
                                    {textFieldReadOnly(`e-mail`,rowData.cust_email)}
                                  </Grid>
                                  <Grid item xs={6} sm={2}>
                                    {textFieldReadOnly(`line ID`,rowData.cust_line)}
                                  </Grid>
                                  <Grid item xs={6} sm={2}>
                                    {textFieldReadOnly(`จ่ายเงินผ่านทาง`,this.state.listArrPayment.find(f=>f[`idcode`] === rowData.payment_idcode)[lang])}
                                  </Grid>
                                  {rowData.payment_idcode === `tranfer` ?
                                    <Grid item xs={6} sm={2}>
                                      <a href={rowData.payment_attact} target='_blank'>
                                        <Button style={{backgroundColor: `green` , color: `white`}}>
                                          ดูหลักฐานการโอนเงิน
                                        </Button>
                                      </a>
                                    </Grid> : null
                                  }
                                  <Grid item xs={12}>
                                    {textFieldReadOnly(`จัดส่งสินค้าที่`,rowData.cust_sendToAddress)}
                                  </Grid>
                                </Grid>
                                <Table style={{ backgroundColor: `white` ,marginTop: `10px` , paddingBottom: `100px` }}>
                                  <TableHead >
                                      <TableRow>
                                        <TableCell align="center"><p className={`myfont_cart_table_head`}>สินค้า</p></TableCell>
                                        <TableCell align="center"><p className={`myfont_cart_table_head`}>จำนวน</p></TableCell>
                                        <TableCell align="center"><p className={`myfont_cart_table_head`}>ราคา</p></TableCell>
                                        <TableCell align="center"><p className={`myfont_cart_table_head`}>ราคารวม</p></TableCell>
                                      </TableRow>
                                  </TableHead>
                                  <TableBody>
                                      {query(rowData.order_list,lang)}
                                  </TableBody>
                                </Table>
                              </div>
                              </Container>
                            )
                          }
                        }
                      ]}
                    />
                  </Grid>
                </Card>
              <StdBtnIconOnly_upToTop />
            </React.Fragment>
        )
    }
  }
}

function createListChoice(){
    var res= []
    res.push(<MenuItem value={true}>{`show`}</MenuItem>)
    res.push(<MenuItem value={false}>{`hide`}</MenuItem>)
    return res
}

function textFieldReadOnly(label , defaultValue){
  return (
    <TextField
      className={`myfont_orderall_textfield`}
      label={label}
      defaultValue={defaultValue}
      InputProps={{
        readOnly: true,
      }}
      variant="outlined"
    />
  )
}

function bodyReq(start ,toDate){
  return (
    {"onDate":
        {$gte: `${start} 00:00:00`,$lt: `${toDate} 23:59:59`}
    }
  )
}

function query(listCustomerOrder , lang){
    var res = [];
    listCustomerOrder.map(x=>{
      var price = x.price * x.amount
        res.push(
            <TableRow>
                <TableCell align="center"><p className={`myfont_orderall_body`}>{x.unitname[0][lang]}</p></TableCell>
                <TableCell align="center"><p className={`myfont_orderall_body`}>{x.amount}</p></TableCell>
                <TableCell align="center"><p className={`myfont_orderall_body`}>{x.price}</p></TableCell>
                <TableCell align="center"><p className={`myfont_orderall_body`}>{price}</p></TableCell>
            </TableRow>
        )
    })
    return res
}

OrderQueryCondition.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
  window: PropTypes.func,
};
function mapStateToProps(state) {
  return {
    lang: state.page_getLangToState,
    arrCart: state.func_actionCart
  }
}
function mapDispatchToProps(dispatch) {
  return {
      actionReducer: (type,payload) => {
        dispatch({type: type,payload:payload})
      }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(useStyles, { withTheme: true })(OrderQueryCondition))