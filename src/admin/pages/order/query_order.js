import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Radium, {StyleRoot} from 'radium';
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Hidden from '@material-ui/core/Hidden';
import { Link ,Router , browserHistory} from "react-router";
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import MaterialTable from 'material-table';
import TextField from '@material-ui/core/TextField';
import Chip from '@material-ui/core/Chip';
import Card from '@material-ui/core/Card';
import Divider from '@material-ui/core/Divider';

//component
import {StdBtnIconOnly_upToTop} from '../../../component/button/std_button'
import {card_example_order_status} from '../../../component/card/card_example_order_status'
import ApexChart from '../../../component/graph/line_month'
import PieChart from '../../../component/graph/pie'

//style
import styleCode from '../../../style/menu'
import useStyles  from '../../../style/app.js'

//materialize
import 'materialize-css';

//layout
import {column_query_order} from '../../layouts/query_order/query_order'

//react-responsive-carousel
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

//icon
import CreateIcon from '@material-ui/icons/Create';
import LinkIcon from '@material-ui/icons/Link';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CancelIcon from '@material-ui/icons/Cancel';
import ColorizeIcon from '@material-ui/icons/Colorize';

//FontAwesomeIcon
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas , faFire ,faDonate} from "@fortawesome/free-solid-svg-icons";
import { fab , faLine , faSellcast , faInstalod } from "@fortawesome/free-brands-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";
import { faEnvelope , faPhone } from "@fortawesome/free-solid-svg-icons";

//service
import {getStringTitle , getConsoleError ,generateString_string ,convertIntToMoneyFormat} from '../../../services/std_service/getString'
import {myFormatDate , myFormatTime ,mySubtractFormatDate ,myAddFormatDate} from '../../../services/std_service/getDate'
import {add_arrItem} from '../../../services/mockup/arrItem'
import FetchData from '../../../services/fetch_data/fetch'
import FetchData_menu from '../../../services/fetch_data/fetch_menu'
import {funcClickLink} from '../../../services/link/service_link'

const _ = require('lodash');

class OrderAll extends Component {
  constructor() {
    super();
    this.state = {
      open: true,mobileOpen: false,lang: null,load: true,errString: null ,
      listArrOrderStatus: [],listArrCustomerOrder: [], listdataTable: [], listArrPayment: [],
      listAllCustomerOrder: [], listAllOrderStatus: [],

      db_loading: true, db_tt_price: 0 , db_tt_cancel: 0 , db_tt_order: 0 , db_tt_discount: 0,
      dataSourceInYear: [0,0,0,0,0,0,0,0,0,0,0,0],
    }
    // this.addItemToCart = this.addItemToCart.bind(this);
    this.duration = 1;
  }
  onChangeNumber(e){
    const re = /^[0-9\b]+$/;
    if (e.target.value === '' || re.test(e.target.value)) {
       this.setState({value: e.target.value})
    }
  }
  componentDidMount() {
    this.start_page(this.props);
  }
  componentWillReceiveProps(props) {
    this.start_page(props);
  }
  start_page= async()=>{
    this.setState({load:true , db_loading: true})
    var url =null ; var data= []

    url = `${process.env.REACT_APP_URL_API}admin/menu/dashboard`
    try{ data.push ({"menu_dashboard":await FetchData_menu(url,'menu_dashboard',process.env.REACT_APP_TIME_Fetch_primary ,bodyReq())})  }catch(e){getConsoleError(e)}
        
    url = `${process.env.REACT_APP_URL_API}query?order_status=all`
    try{ data.push ({"order_status":await FetchData(url,'order_status',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}

    url = `${process.env.REACT_APP_URL_API}query?customer_order=all`
    try{ data.push ({"customer_order":await FetchData(url,'customer_order',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}

    url = `${process.env.REACT_APP_URL_API}query?pament_system=all`
    try{ data.push ({"pament_system":await FetchData(url,'pament_system',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}

    try{ 
      await this.setState({
        listArrOrderStatus: data.find(x=>x['menu_dashboard']).menu_dashboard.find(x=>x['order_status']).order_status,
        listArrCustomerOrder: data.find(x=>x['menu_dashboard']).menu_dashboard.find(x=>x['customer_order']).customer_order,
        listArrPayment: data.find(x=>x['pament_system']).pament_system[0],
      })
      await this.packData();
    }catch(e){
      getConsoleError(e)
      this.setState({ errString: e.toString() })
    }

    this.setState({load:false})

    await this.loadDataSource();
  }

  packData =()=>{
    var listdataTable = [] , index = 0
    // var list = _.sortBy(this.state.listArrCustomerOrder,new Date(['date']), ['asc'])
    var list = this.state.listArrCustomerOrder.sort(function(a,b){ 
      return new Date(a.datePure) < new Date(b.datePure)
    });
    list.map(x=>{
      x.status = 1
      x.cpv = (
        <a href={`${process.env.REACT_APP_URL_THIS_PROD}tracking/${x.orderid}`} target='_blank'>
          <Button startIcon={<LinkIcon />} color="default" style={{backgroundColor: `#b71c1c` ,color: `white`}} >
            {`preview`}
          </Button>
        </a>
      )
      x.manage = <Button style={{backgroundColor: `blue` , color: `white`}} 
        onClick={funcClickLink.bind(this,`/mng/orderidcode/${x.orderid}`)}
      >
        <CreateIcon/>
      </Button>
      var findItem = this.state.listArrOrderStatus.filter(v=>v[`idcode_order`] === x.orderid)
      if(findItem.length > 0){
        index = findItem.length -1
        x.status = ((findItem[index].now)+1)
      }
      listdataTable.push(x)
    })
    this.setState({ 
      listdataTable: listdataTable 
    })
  }

  loadDataSource=async()=>{
    var url =null ; var data= []

    url = `${process.env.REACT_APP_URL_API}query?customer_order=all`
    try{ data.push ({"customer_order":await FetchData(url,'customer_order',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}
    
    url = `${process.env.REACT_APP_URL_API}query?order_status=all`
    try{ data.push ({"order_status":await FetchData(url,'order_status',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}

    try{ 
      await this.setState({
        listAllCustomerOrder: data.find(x=>x['customer_order']).customer_order[0],
        listAllOrderStatus: data.find(x=>x['order_status']).order_status[0],
      })
      await this.packDataDashboard()
    }catch(e){
      getConsoleError(e)
      this.setState({ errString: e.toString() })
    }
  }

  packDataDashboard=async()=>{
    var total_price = 0 ,total_discount = 0
    var countOrderAll = this.state.listAllCustomerOrder.length
    var countCancel = this.state.listAllOrderStatus.filter(v=>v[`now`] === `1001`).length
    var countSuccess = this.state.listAllOrderStatus.filter(v=>v[`now`] === 2)
    countSuccess = _.uniqBy(countSuccess , `idcode_order`)
    countSuccess.map(x=>{
      var findOrder = this.state.listAllCustomerOrder.filter(b=>b[`orderid`])
      if(findOrder.length > 0){
        total_price = parseInt(total_price) + parseInt(findOrder[0].total_price)
      }
    })
    this.state.listAllCustomerOrder.map(k=>{
      k.discount_list.map(g=>{
        total_discount = total_discount + parseInt(g.total)
      })
    })
    await this.setState({ 
      db_tt_price: total_price ,
      db_tt_cancel: countCancel ,
      db_tt_order: countOrderAll ,
      db_tt_discount: total_discount ,
    })

    var dataSourceInYear = [0,0,0,0,0,0,0,0,0,0,0,0]
    this.state.listArrCustomerOrder.map(x=>{
      var getY = new Date(x.datePure).getFullYear()
      if(getY === 2021){
        var getData = new Date(x.datePure)
        dataSourceInYear[getData.getMonth()] = parseInt(dataSourceInYear[getData.getMonth()]) + parseInt(x.total_price)
        // console.log(getData.getMonth())
      }
    })
    await this.setState({ dataSourceInYear: dataSourceInYear ,db_loading: false })
  }
  
  render(){
    const load = <LinearProgress color="secondary" />
    // const style = `.txt{ color: white ;  } .pic{ width: 30% }`
    const ex = `.carousel{ height:unset } .rb{ left: 0px; } .radiusCard{ border-radius: 10px; padding: 10px; }`
    if(this.state.load){
      return (<div>{load}</div>)
    }else{
        var lang = this.props.lang
        var dataRow = this.state.listdataTable
        return (
            <React.Fragment>
              <style>{ex}</style>
              <br/>
              {
                this.state.db_loading ? 
                <div>{load}<br/></div> : queryGraph( this.state.db_tt_price , this.state.db_tt_cancel , this.state.db_tt_order , this.state.db_tt_discount , this.state.dataSourceInYear )
              }
              {/* <div style={{ display: this.state.db_loading ? `none` : `unset`}}>
                <Grid container space={2} >
                  {boxInDashboard( `green` , faSellcast , `Total sell price: ` , `${convertIntToMoneyFormat(this.state.db_tt_price)} THB` )}
                  {boxInDashboard( `red` , faFire , `Total cancal price: ` , `${convertIntToMoneyFormat(this.state.db_tt_cancel)} Time` )}
                  {boxInDashboard( `blue` , faInstalod , `Total order: ` , `${convertIntToMoneyFormat(this.state.db_tt_order)} Order` )}
                  {boxInDashboard( `#937900` , faDonate , `Total discount customer: ` , `${convertIntToMoneyFormat(this.state.db_tt_discount)} THB` )}
                </Grid>
                {this.state.db_loading ? null : <ApexChart data={this.state.dataSourceInYear}/>}
              </div> */}
              <center><h3 className={`myfont_orderall_head`}>{`รายการลูกคำสั่งซื้อล่าสุด (ย้อนหลัง 14 วัน)`}</h3></center>
              <center><p>{`from ${mySubtractFormatDate(14)} to ${myFormatDate()}`}</p></center>
              {card_example_order_status()}
              <br/>
              <MaterialTable
                tableRef={this.tableRef}
                align="center" 
                title=""
                columns={column_query_order()[lang]}
                data={dataRow}
                options={{
                  pageSize: dataRow.length,
                  pageSizeOptions: [dataRow.length, 100, 500]
                }}
                style={{zIndex: 0 , fontSize: `smaller` , width: `100%`}}
                detailPanel={[
                  {
                    tooltip: 'Show Name',
                    render: rowData => {
                      return (
                        <Container>
                        <div style={{padding: `10px` , backgroundColor: `rgb(217, 222, 255)`}}>
                          <Grid container spacing={2}>
                            <Grid item xs={6} sm={2}>
                              {textFieldReadOnly(`ผู้รับสินค้า`,rowData.cust_name)}
                            </Grid>
                            <Grid item xs={6} sm={2}>
                              {textFieldReadOnly(`เบอร์ติดต่อ`,rowData.cust_tel)}
                            </Grid>
                            <Grid item xs={6} sm={2}>
                              {textFieldReadOnly(`e-mail`,rowData.cust_email)}
                            </Grid>
                            <Grid item xs={6} sm={2}>
                              {textFieldReadOnly(`line ID`,rowData.cust_line)}
                            </Grid>
                            <Grid item xs={6} sm={2}>
                              {textFieldReadOnly(`จ่ายเงินผ่านทาง`,this.state.listArrPayment.find(f=>f[`idcode`] === rowData.payment_idcode)[lang])}
                            </Grid>
                            {rowData.payment_idcode === `tranfer` ?
                              <Grid item xs={6} sm={2}>
                                <a href={rowData.payment_attact} target='_blank'>
                                  <Button style={{backgroundColor: `green` , color: `white`}}>
                                    ดูหลักฐานการโอนเงิน
                                  </Button>
                                </a>
                              </Grid> : null
                            }
                            <Grid item xs={12}>
                              {textFieldReadOnly(`จัดส่งสินค้าที่`,rowData.cust_sendToAddress)}
                            </Grid>
                          </Grid>
                          <Table style={{ backgroundColor: `white` ,marginTop: `10px` , paddingBottom: `100px` }}>
                            <TableHead >
                                <TableRow>
                                  <TableCell align="center"><p className={`myfont_cart_table_head`}>สินค้า</p></TableCell>
                                  <TableCell align="center"><p className={`myfont_cart_table_head`}>จำนวน</p></TableCell>
                                  <TableCell align="center"><p className={`myfont_cart_table_head`}>ราคา</p></TableCell>
                                  <TableCell align="center"><p className={`myfont_cart_table_head`}>ราคารวม</p></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {query(rowData.order_list,lang)}
                            </TableBody>
                          </Table>
                        </div>
                        </Container>
                      )
                    }
                  }
                ]}
              />
              <StdBtnIconOnly_upToTop />
            </React.Fragment>
        )
    }
  }
}
function queryGraph(db_tt_price , db_tt_cancel , db_tt_order , db_tt_discount , dataSourceInYear){
  return (
    <div>
      <Grid container space={2} >
        {boxInDashboard( `green` , faSellcast , `Total sell price: ` , `${convertIntToMoneyFormat(db_tt_price)} THB` )}
        {boxInDashboard( `red` , faFire , `Total cancal price: ` , `${convertIntToMoneyFormat(db_tt_cancel)} Time` )}
        {boxInDashboard( `blue` , faInstalod , `Total order: ` , `${convertIntToMoneyFormat(db_tt_order)} Order` )}
        {boxInDashboard( `#937900` , faDonate , `Total discount customer: ` , `${convertIntToMoneyFormat(db_tt_discount)} THB` )}
      </Grid>
      <ApexChart data={dataSourceInYear}/>
    </div>
  )
}
function textFieldReadOnly(label , defaultValue){
  return (
    <TextField
      className={`myfont_orderall_textfield`}
      label={label}
      defaultValue={defaultValue}
      InputProps={{
        readOnly: true,
      }}
      variant="outlined"
    />
  )
}

function bodyReq(){
  // console.log(mySubtractFormatDate())
  return (
    {"onDate":
        {$gte: `${mySubtractFormatDate(14)} 00:00:00`,$lt: `${myFormatDate()} 23:59:59`}
    }
  )
}

function query(listCustomerOrder , lang){
  var res = [];
  listCustomerOrder.map(x=>{
      res.push(
          <TableRow>
              <TableCell align="center"><p className={`myfont_orderall_body`}>{x.inItem[lang][0][`title`]}</p></TableCell>
              <TableCell align="center"><p className={`myfont_orderall_body`}>{x.amount}</p></TableCell>
              <TableCell align="center"><p className={`myfont_orderall_body`}>{x.priceUse}</p></TableCell>
              <TableCell align="center"><p className={`myfont_orderall_body`}>{x.total}</p></TableCell>
          </TableRow>
      )
  })
  return res
}

function boxInDashboard(color , icon , title , value){
  return (
    <Grid item xs={3} style={{padding: `10px`}}>
      <div className={`radiusCard`} style={{ border: `0.5px solid ${color}` ,height: `100px` , width: `100%` , boxShadow: `1px 1px 1px`}}>
        <Grid container>
          <Grid item xs={4}>
            <center>
              <FontAwesomeIcon icon={icon} style={{color: color , fontSize: `xxx-large`}} /> 
              {/* <CancelIcon style={{color: `green` , fontSize: `xxx-large`}} /> */}
            </center>
          </Grid>
          <Grid item xs={8}>
            <p className={`myFontDashBoard`} style={{fontSize: `larger` , textAlign: `end` , margin: `0 0 0px`}}>{`${title}`}</p>
            <p className={`myFontDashBoard`} style={{fontSize: `x-large`, textAlign: `end`, margin: `0 0 0px` , fontWeight: `bold` , color: color}}>{`${value}`}</p>
          </Grid>
        </Grid>
        <Divider/>
      </div>
    </Grid>
  )
}

OrderAll.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
  window: PropTypes.func,
};
function mapStateToProps(state) {
  return {
    lang: state.page_getLangToState,
    arrCart: state.func_actionCart
  }
}
function mapDispatchToProps(dispatch) {
  return {
      actionReducer: (type,payload) => {
        dispatch({type: type,payload:payload})
      }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(useStyles, { withTheme: true })(OrderAll))