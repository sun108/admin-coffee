import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Radium, {StyleRoot} from 'radium';
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import Box from '@material-ui/core/Box';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Hidden from '@material-ui/core/Hidden';
import { Link ,Router , browserHistory} from "react-router";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import MaterialTable from 'material-table';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import CircularProgress from '@material-ui/core/CircularProgress';
import Chip from '@material-ui/core/Chip';

//component
import {StdBtnIconOnly_upToTop} from '../../../component/button/std_button'
import {StdToastMz_error ,StdToastMz_success} from '../../../component/toast/std_toast'
import {
  query_tableInCard_unitCustomerDiscount,
  query_tableInCard_unitCustomerShipping,
  query_tableInCard_unitCustomerGiftFree,
  query_tableInCard_unitCustomerBuy ,
  query_tableInCard_unitCustomerTotalPrice
} from '../../../component/table/order_summary'

//style
// import styleCode from '../../../style/menu'
import {styleCode} from '../../../style/order_summery'
import useStyles  from '../../../style/app.js'

//materialize
import 'materialize-css';
import { Collapsible, CollapsibleItem } from 'react-materialize';

//layout
import {column_query_order} from '../../layouts/query_order/query_order'

//react-responsive-carousel
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

//service
import {getStringTitle , getConsoleError ,generateString_string , convertIntToMoneyFormat} from '../../../services/std_service/getString'
import {add_arrItem} from '../../../services/mockup/arrItem'
import FetchData from '../../../services/fetch_data/fetch'
import {funcClickLink} from '../../../services/link/service_link'
import {myFormatDate , myFormatTime} from '../../../services/std_service/getDate'
import getProfile from '../../../services/profile/getYourProfile'
import {adminSendToServer} from '../../../services/admin/setting_status'

const _ = require('lodash');

class OrderIDCode extends Component {
  constructor() {
    super();
    this.state = {
      lang: null,load: true,submit_load: false,adminInfo: [],
      listArrOrderStatus: [],listArrCustomerOrder: [], listdataTable: [], listArrPayment: [], listArrStatus: [],
      idcodeOrder: null ,wording:[],
      nowStatus: 0, cancel: false, cancel_comment: null , ServiceProvider: null , trackcode: null
    }
    // this.addItemToCart = this.addItemToCart.bind(this);
    this.duration = 1;
  }
  onChangeNumber(e){
    const re = /^[0-9\b]+$/;
    if (e.target.value === '' || re.test(e.target.value)) {
       this.setState({value: e.target.value})
    }
  }
  componentDidMount() {
    this.start_page(this.props);
  }
  componentWillReceiveProps(props) {
    this.start_page(props);
  }
  start_page= async(props)=>{
    this.setState({load:true})
    var url =null ; var data= []
    var idcode = props.params.idcodeOrder
    var wording = await this.props.wording
    if(idcode !== null && idcode !== `undefined` && idcode !== undefined){
        this.setState({idcodeOrder: idcode})

        url = `${process.env.REACT_APP_URL_API}query/collection/customer_order?orderid=${idcode}`
        try{ data.push ({"customer_order":await FetchData(url,'customer_order',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}
        
        url = `${process.env.REACT_APP_URL_API}query/collection/order_status?idcode_order=${idcode}`
        try{ data.push ({"order_status":await FetchData(url,'order_status',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}
    
        url = `${process.env.REACT_APP_URL_API}query?pament_system=all`
        try{ data.push ({"pament_system":await FetchData(url,'pament_system',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}
        
        url = `${process.env.REACT_APP_URL_API}query?system_status=all`
        try{ data.push ({"system_status":await FetchData(url,'system_status',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}

        try{ 
          var adminInfo = await getProfile()
          await this.setState({
              // listArrCatProduct: _.orderBy(data.find(x=>x['catagory_product']).catagory_product[0].catagory_product,['order'] , ['asc']),
              listArrOrderStatus: data.find(x=>x['order_status']).order_status[0],
              listArrCustomerOrder: data.find(x=>x['customer_order']).customer_order[0],
              listArrPayment: data.find(x=>x['pament_system']).pament_system[0],
              listArrStatus: data.find(x=>x['system_status']).system_status[0],
              adminInfo: adminInfo ,
              wording: wording
          })
          await this.packData();
        }catch(e){
            getConsoleError(e)
        }
    }else{
        // StdToastMz_error('ไม่พบรหัส order id ดังกล่าว')
        funcClickLink(`/mng`)
    }
    this.setState({load:false})
  }

  packData =()=>{
    var findItem = this.state.listArrOrderStatus.filter(v=>v[`idcode_order`] === this.state.idcodeOrder)
    var sortedJsObjects = findItem.sort(function(a,b){ 
      return new Date(a.datePure) < new Date(b.datePure)
    });
    if(sortedJsObjects.length > 0){
      this.setState({
        nowStatus: sortedJsObjects[0].now,
        cancel: sortedJsObjects[0].cancel,
        cancel_comment: sortedJsObjects[0].cancel_comment,
        ServiceProvider: sortedJsObjects[0].service_provider,
        trackcode: sortedJsObjects[0].trackcode,
      })
    }
    this.setState({ listdataTable: sortedJsObjects })
  }

  onChangeStatus =(event , state)=>{
    if(state === `nowStatus`){
      if(event.target.value === 1099){
        this.setState({ [state] :event.target.value , cancel: true})
       }else{
        this.setState({ [state] :event.target.value , cancel: false})
      }
    }else{
      this.setState({ [state] :event.target.value})
    }
  }
  packObjAndSend=async()=>{
    let date = myFormatDate() ; let time = myFormatTime()
    var res = {
      "idcode_order":`${this.state.idcodeOrder}`,
      "now":this.state.nowStatus,
      "cancel":this.state.cancel,
      "cancel_comment":this.state.cancel_comment,
      "service_provider":this.state.ServiceProvider,
      "trackcode":this.state.trackcode,
      "onDate":`${date} ${time}`,
      "datePure":new Date(),
      "dateWithZone":new Date().toString(),
      "admin_name":this.state.adminInfo.nickname_en.toString(),
      "idcode_person":this.state.adminInfo.idcode,
    }
    console.log(res)
    adminSendToServer(res , this.set_submitLoad , this.state.idcodeOrder)
    // customer_start_order(res , this.changeButtonSubmitToLoadShow , this.clearItemInCart)
    // return res
  }

  set_submitLoad=(value)=>{
    this.setState({ submit_load : value })
  }
  
  render(){
    const load = <LinearProgress color="secondary" />
    // const style = `.txt{ color: white ;  } .pic{ width: 30% }`
    const ex = `
    .carousel{height:unset}
    .rb{ left: 0px;}`
    if(!this.state.load && this.state.wording.th !== undefined){
        var lang = this.props.lang
        var wording = this.state.wording[lang]
        var rowData = this.state.listArrCustomerOrder[0]
        var def_statusNow = 0 , def_ServiceProvider ,def_TrackCode ,def_Cancel = false,def_CancelComment
        if(this.state.listArrOrderStatus.length > 0){
          def_statusNow = this.state.listArrOrderStatus.now;
        }
        return (
            <React.Fragment>
                <style>{ex}</style>
                <style>{styleCode}</style>
                <br/>
                <Container style={{paddingBottom: `150px`}}>
                    <center><h3 style={{fontWeight: `bold`}}>{`SETTING STATUS: ${this.state.idcodeOrder}`}</h3></center>
                    <Card boxShadow={3} className={`myCard_style`}>
                      <h4 className={`myCard_style_title`}>Card Admin</h4>
                      <Grid container spacing={2}>
                        <Grid item xs={6}>
                          <FormControl>
                            <InputLabel>ตั้งค่าให้รายการขอซื้อนี้</InputLabel>
                            <Select
                              style={{ width:`100%` }}
                              defaultValue={this.state.nowStatus}
                              onChange={(e)=>this.onChangeStatus(e ,`nowStatus`)}
                            >
                              {createListChoice(this.state.listArrStatus, lang)}
                            </Select>
                          </FormControl>
                        </Grid>
                        <Grid item xs={2}>
                          { !this.state.submit_load ?
                            <Button style={{width: `100%` , backgroundColor: `green` , color: `white`}} onClick={this.packObjAndSend.bind(this)}>ยืนยันการตั้งค่า</Button> :
                            <CircularProgress color="secondary" />
                          }
                        </Grid>
                      </Grid>
                      <Card boxShadow={3} className={`myCard_style`} style={{display: this.state.nowStatus === 2 ? `block` : `none`}}>
                        <h4 className={`myCard_style_title`}>ข้อมูลการขนส่ง</h4>
                        <Grid container spacing={2}>
                          <Grid item xs={6}>
                            <TextField label="ผู้ให้บริการ ขนส่งสินค้า" 
                              variant="outlined" style={{width: `100%`}}
                              defaultValue={this.state.ServiceProvider}
                              onChange={(e)=>this.onChangeStatus(e ,`ServiceProvider`)}
                            />
                          </Grid>
                          <Grid item xs={6}>
                            <TextField label="Track code" variant="outlined" 
                              style={{width: `100%`}}
                              defaultValue={this.state.trackcode}
                              onChange={(e)=>this.onChangeStatus(e ,`trackcode`)}
                            />
                          </Grid>
                        </Grid>
                      </Card>
                      <Card boxShadow={3} className={`myCard_style`} style={{display: this.state.nowStatus === 1099 ? `block` : `none`}}>
                        <h4 className={`myCard_style_title`}>ข้อมูลการยกเลิก</h4>
                        <Grid container spacing={2}>
                          <Grid item xs={12}>
                            <TextField 
                              label="แจ้งให้กับลูกค้าทราบว่า..." 
                              variant="outlined" 
                              style={{width: `100%`}} 
                              defaultValue={this.state.cancel_comment}
                              onChange={(e)=>this.onChangeStatus(e ,`cancel_comment`)}
                            />
                          </Grid>
                        </Grid>
                      </Card>
                      <hr/>
                      <h4 className={`myCard_style_title`}>History Setting By Admin</h4>
                      <Grid container spacing={2}>
                        <Table style={{ backgroundColor: `white` ,marginTop: `10px` , paddingBottom: `100px` }}>
                          <TableHead >
                            <TableRow>
                                <TableCell align="center"><p className={`myfont_cart_table_head`}>set status</p></TableCell>
                                <TableCell align="center"><p className={`myfont_cart_table_head`}>nick name</p></TableCell>
                                <TableCell align="center"><p className={`myfont_cart_table_head`}>picture</p></TableCell>
                                <TableCell align="center"><p className={`myfont_cart_table_head`}>datetime</p></TableCell>
                                <TableCell align="center"><p className={`myfont_cart_table_head`}>description</p></TableCell>
                            </TableRow>
                          </TableHead>
                          <TableBody>
                              {queryAdminLog(this.state.listdataTable)}
                          </TableBody>
                        </Table>
                      </Grid>
                    </Card>
                    <Card boxShadow={3} style={{padding: `10px`}} className={`myCard_style`}>
                      <h4 className={`myCard_style_title`}>Card Description This Order</h4>
                      <Grid container spacing={2}>
                          <Grid item xs={6} sm={2}>
                              {textFieldReadOnly(`ผู้รับสินค้า`,rowData.cust_name)}
                          </Grid>
                          <Grid item xs={6} sm={2}>
                              {textFieldReadOnly(`เบอร์ติดต่อ`,rowData.cust_tel)}
                          </Grid>
                          <Grid item xs={6} sm={2}>
                              {textFieldReadOnly(`e-mail`,rowData.cust_email)}
                          </Grid>
                          <Grid item xs={6} sm={2}>
                              {textFieldReadOnly(`line ID`,rowData.cust_line)}
                          </Grid>
                          <Grid item xs={6} sm={2}>
                              {textFieldReadOnly(`จ่ายเงินผ่านทาง`,this.state.listArrPayment.find(f=>f[`idcode`] === rowData.payment_idcode)[lang])}
                          </Grid>
                              {rowData.payment_idcode === `tranfer` ?
                                  <Grid item xs={6} sm={2}>
                                      <a href={rowData.payment_attact} target='_blank'>
                                      <Button style={{backgroundColor: `green` , color: `white`}}>
                                          ดูหลักฐานการโอนเงิน
                                      </Button>
                                      </a>
                                  </Grid> : null
                              }
                          <Grid item xs={12}>
                              {textFieldReadOnly(`จัดส่งสินค้าที่`,rowData.cust_sendToAddress)}
                          </Grid>
                          <Grid item xs={6} sm={6}>
                            {textFieldReadOnly(`full datetime`,rowData.dateWithZone)}
                          </Grid>
                          <Grid item xs={6} sm={6}>
                            {textFieldReadOnly(`by browser`,rowData.browser)}
                          </Grid>
                          <Grid item xs={6} sm={6}>
                            {textFieldReadOnly(`IP address`,rowData.ip)}
                          </Grid>
                          <Grid item xs={6} sm={6}>
                            {textFieldReadOnly(`colo`,rowData.colo)}
                          </Grid>
                      </Grid>
                      <div>
                      <br/>
                      <hr/>
                      <center>
                        <Box className='example1' boxShadow={4} style={{border: `black solid 0.5px`}}>
                          <center><h4><p className={`myfont_cart_modal_head`}>รายการสั่งซื้อ</p></h4></center>
                          <hr/>
                          {query_tableInCard_unitCustomerBuy(lang , rowData.order_list , wording)}
                          {query_tableInCard_unitCustomerGiftFree(lang , rowData.giftFree_list, wording)}
                          {query_tableInCard_unitCustomerShipping(lang , rowData.shipping_list, wording)}
                          {query_tableInCard_unitCustomerDiscount(lang , rowData.discount_list, wording)}
                          {query_tableInCard_unitCustomerTotalPrice(lang , rowData.total_price, wording)}
                        </Box>
                      </center>
                    </div>
                    </Card>
                </Container>
                <StdBtnIconOnly_upToTop />
            </React.Fragment>
        )
    }else{
      return (<div>{load}</div>)
    }
  }
}

function textFieldReadOnly(label , defaultValue){
  return (
    <TextField
      className={`myfont_orderall_textfield`}
      label={label}
      defaultValue={defaultValue}
      InputProps={{
        readOnly: true,
      }}
      variant="outlined"
    />
  )
}

function queryAdminLog(listOrderStatus){
  var res = [] , numb = 0
  listOrderStatus.map(x=>{
    res.push(
      <TableRow style={{backgroundColor: numb === 0 ? `#fff9c4`: null}}>
        <TableCell align="center"><p className={`myfont_cart_table_head`}>{(parseInt(x.now) +1)}</p></TableCell>
        <TableCell align="center"><p className={`myfont_cart_table_head`}>{x.admin_name}</p></TableCell>
        <TableCell align="center">
        <img src={`${process.env.REACT_APP_URL_PICTURE_PERSON}${x.idcode_person}.jpg`} style={{width: `100px` , borderRadius: `50%` , width: `50px` , height: `50px`}} />
        </TableCell>
        <TableCell align="center"><p className={`myfont_cart_table_head`}>{`${x.onDate} ${numb === 0 ? `(last time)`: null}`}</p></TableCell>
        <TableCell align="center"><p className={`myfont_cart_table_head`}>description</p></TableCell>
    </TableRow>
    )
    numb++
  })
  return res
}

function query(listCustomerOrder , lang){
    var res = [];
    var total_price = 0
    listCustomerOrder.map(x=>{
        res.push(
          <TableRow>
            <TableCell align="center"><p className={`myfont_orderall_body`}>{x.inItem[lang][0][`title`]}</p></TableCell>
            <TableCell align="center"><p className={`myfont_orderall_body`}>{x.amount}</p></TableCell>
            <TableCell align="center"><p className={`myfont_orderall_body`}>{convertIntToMoneyFormat(x.priceUse)}</p></TableCell>
            <TableCell align="center"><p className={`myfont_orderall_body`}>{convertIntToMoneyFormat(x.total)}</p></TableCell>
          </TableRow>
        )
    })
    res.push(
      <TableRow>
          <TableCell align="center" colspan={3}><p className={`myfont_orderall_body`}>{`total price`}</p></TableCell>
          <TableCell align="center"><p className={`myfont_orderall_body`}>{convertIntToMoneyFormat(total_price)}</p></TableCell>
      </TableRow>
    )
    return res
}
function createListChoice(listStatus,lang){
    var res = [];
    var list = _.sortBy(listStatus, function (x) {return parseInt(x.numb, 10);})
    list.map(x=>{
        res.push(<MenuItem value={x.numb}>{`${x[lang]} (${x.numb + 1})`}</MenuItem>)
    })
    return res;
  }

OrderIDCode.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
  window: PropTypes.func,
};
function mapStateToProps(state) {
  return {
    lang: state.page_getLangToState,
    arrCart: state.func_actionCart,
    wording: state.getWordingStringThis,
  }
}
function mapDispatchToProps(dispatch) {
  return {
      actionReducer: (type,payload) => {
        dispatch({type: type,payload:payload})
      }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(useStyles, { withTheme: true })(OrderIDCode))