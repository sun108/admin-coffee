function column_query_order(){
    return {
        "th" : [ 
            {
                "title" : "order id",
                "field" : "orderid",
                "align" : "center",
            },
            {
                "title" : "วันที่",
                "field" : "onDate",
                "align" : "center",
            },
            {
                "title" : "สถานะ",
                "field" : "status",
                "align" : "center",
            },
            {
                "title" : "สิ่งที่ลูกค้าเห็น",
                "field" : "cpv",
                "align" : "center",
            },
            {
                "title" : "ตั้งค่า",
                "field" : "manage",
                "align" : "center",
            }
        ],
        "en" : [ 
            {
                "title" : "order id",
                "field" : "orderid",
                "align" : "center",
            },
            {
                "title" : "onDate",
                "field" : "onDate",
                "align" : "center",
            }, 
            {
                "title" : "status",
                "field" : "status",
                "align" : "center",
            },
            {
                "title" : "what to see",
                "field" : "cpv",
                "align" : "center",
            }, 
            {
                "title" : "manage",
                "field" : "manage",
                "align" : "center",
            }
        ]
    }
}

export { 
    column_query_order,
 }