function column_query_category(){
    return {
        "th" : [ 
            {
                "title" : "ชื่อหมวดสินค้า",
                "field" : "catname",
                "align" : "center",
            },
            {
                "title" : "สินค้ารวม",
                "field" : "unitCount",
                "align" : "center",
            },
            {
                "title" : "ตำแหน่ง",
                "field" : "position",
                "align" : "center",
            },
            {
                "title" : "URL",
                "field" : "url",
                "align" : "center",
            },
            {
                "title" : "แสดงผล",
                "field" : "cv",
                "align" : "center",
            },
            {
                "title" : "ตั้งค่า",
                "field" : "manage",
                "align" : "center",
            }
        ],
        "en" : [ 
            {
                "title" : "Category name",
                "field" : "catname",
                "align" : "center",
            },
            {
                "title" : "สินค้ารวม",
                "field" : "unitCount",
                "align" : "center",
            },
            {
                "title" : "ตำแหน่ง",
                "field" : "position",
                "align" : "center",
            },
            {
                "title" : "URL",
                "field" : "url",
                "align" : "center",
            },
            {
                "title" : "visible",
                "field" : "cv",
                "align" : "center",
            },
            {
                "title" : "ตั้งค่า",
                "field" : "manage",
                "align" : "center",
            }
        ]
    }
}

export { 
    column_query_category,
 }