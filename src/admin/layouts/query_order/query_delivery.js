function column_query_delivery(){
    return {
        "th" : [ 
            {
                "title" : "ชื่อการส่งสินค้า",
                "field" : "prodname",
                "align" : "center",
            },
            {
                "title" : "ราคาขั้นต่ำ",
                "field" : "price_min",
                "align" : "center",
            },
            {
                "title" : "จำนวนต่ำสุด",
                "field" : "unit_min",
                "align" : "center",
            },
            {
                "title" : "ราคาต่อชิ้น",
                "field" : "price_per_unit",
                "align" : "center",
            },
            {
                "title" : "สินค้าที่ใช้การส่งนี้",
                "field" : "unit_use_total",
                "align" : "center",
            },
            {
                "title" : "ตั้งค่า",
                "field" : "manage",
                "align" : "center",
            }
        ],
        "en" : [ 
            {
                "title" : "delivery name",
                "field" : "prodname",
                "align" : "center",
            },
            {
                "title" : "price min",
                "field" : "price_min",
                "align" : "center",
            },
            {
                "title" : "unit min",
                "field" : "unit_min",
                "align" : "center",
            },
            {
                "title" : "price per unit",
                "field" : "price_per_unit",
                "align" : "center",
            },
            {
                "title" : "unit use total",
                "field" : "unit_use_total",
                "align" : "center",
            },
            {
                "title" : "manage",
                "field" : "manage",
                "align" : "center",
            }
        ]
    }
}

export { 
    column_query_delivery,
 }