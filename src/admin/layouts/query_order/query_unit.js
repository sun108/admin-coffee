function column_query_unit(){
    return {
        "th" : [ 
            {
                "title" : "ชื่อสินค้า",
                "field" : "prodname",
                "align" : "center",
            },
            {
                "title" : "ภาพ",
                "field" : "picture",
                "align" : "center",
            },
            {
                "title" : "collection",
                "field" : "columnCollection",
                "align" : "center",
            },
            {
                "title" : "ราคาที่แสดง",
                "field" : "showPrice",
                "align" : "center",
            },
            {
                "title" : "แสดง",
                "field" : "cv",
                "align" : "center",
            },
            {
                "title" : "สินค้าหมด",
                "field" : "oos",
                "align" : "center",
            },
            {
                "title" : "ตั้งค่า",
                "field" : "manage",
                "align" : "center",
            }
        ],
        "en" : [ 
            {
                "title" : "product name",
                "field" : "prodname",
                "align" : "center",
            },
            {
                "title" : "picture",
                "field" : "picture",
                "align" : "center",
            },
            {
                "title" : "collection",
                "field" : "columnCollection",
                "align" : "center",
            },
            {
                "title" : "price",
                "field" : "showPrice",
                "align" : "center",
            },
            {
                "title" : "show",
                "field" : "cv",
                "align" : "center",
            },
            {
                "title" : "out of stock",
                "field" : "oos",
                "align" : "center",
            },
            {
                "title" : "manage",
                "field" : "manage",
                "align" : "center",
            }
        ]
    }
}

export { 
    column_query_unit,
 }