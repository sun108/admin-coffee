function column_query_promotion(){
    return {
        "th" : [ 
            {
                "title" : "ชื่อโปรโมชั่น",
                "field" : "promotionname",
                "align" : "center",
            },
            {
                "title" : "เงื่อนไข",
                "field" : "tcondition",
                "align" : "center",
            },
            {
                "title" : "ลูกค้าได้รับ",
                "field" : "benefit",
                "align" : "center",
            },
            {
                "title" : "การใช้งาน",
                "field" : "activec",
                "align" : "center",
            },
            {
                "title" : "ตั้งค่า",
                "field" : "manage",
                "align" : "center",
            }
        ],
        "en" : [ 
            {
                "title" : "promotion name",
                "field" : "promotionname",
                "align" : "center",
            },
            {
                "title" : "condition",
                "field" : "tcondition",
                "align" : "center",
            },
            {
                "title" : "benefit",
                "field" : "benefit",
                "align" : "center",
            },
            {
                "title" : "active",
                "field" : "activec",
                "align" : "center",
            },
            {
                "title" : "manage",
                "field" : "manage",
                "align" : "center",
            }
        ]
    }
}

export { 
    column_query_promotion,
 }