function column_query_collection(){
    return {
        "th" : [ 
            {
                "title" : "ชื่อ collection",
                "field" : "collectname",
                "align" : "center",
            },
            {
                "title" : "สินค้ารวม",
                "field" : "unitCount",
                "align" : "center",
            },
            {
                "title" : "URL",
                "field" : "url",
                "align" : "center",
            },
            {
                "title" : "แสดงผล",
                "field" : "cv",
                "align" : "center",
            },
            {
                "title" : "ตั้งค่า",
                "field" : "manage",
                "align" : "center",
            }
        ],
        "en" : [ 
            {
                "title" : "ชื่อ collection",
                "field" : "collectname",
                "align" : "center",
            },
            {
                "title" : "สินค้ารวม",
                "field" : "unitCount",
                "align" : "center",
            },
            {
                "title" : "URL",
                "field" : "url",
                "align" : "center",
            },
            {
                "title" : "visible",
                "field" : "cv",
                "align" : "center",
            },
            {
                "title" : "ตั้งค่า",
                "field" : "manage",
                "align" : "center",
            }
        ]
    }
}

export { 
    column_query_collection,
 }