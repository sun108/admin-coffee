function column_query_admin(){
    return {
        "th" : [ 
            {
                "title" : "employee code",
                "field" : "emp_idcode",
                "align" : "center",
            },
            {
                "title" : "รูปภาพ",
                "field" : "picture",
                "align" : "center",
            },
            {
                "title" : "ชื่อเล่น",
                "field" : "nickname",
                "align" : "center",
            }
        ],
        "en" : [ 
            {
                "title" : "employee code",
                "field" : "emp_idcode",
                "align" : "center",
            },
            {
                "title" : "picture",
                "field" : "picture",
                "align" : "center",
            },
            {
                "title" : "nickname",
                "field" : "nickname",
                "align" : "center",
            },
        ]
    }
}

export { 
    column_query_admin,
 }