let appb = <AppBar position="fixed" className={classes.appBar}>
    <StdSnackBar_control open={this.state.toastOpen} mode={this.state.toastMode} 
    header={this.state.toastHeader} body={this.state.toastBody}/>
    <Toolbar>
        <IconButton color="inherit" aria-label="open drawer" edge="start" 
        onClick={this.handleDrawerToggle} className={classes.menuButton}>
            <MenuIcon />
        </IconButton>
        <Typography variant="h6" noWrap className={classes.title}>{` FHR ${process.env.REACT_APP_VERSION}`}</Typography>

        <StdBtnModal_logout 
        className='modal-trigger'
        variant="contained" 
        color="secondary"
        href="#modal_logout" 
        node="button"
        />

        <FormControl variant="outlined" className={classes.formControl}>
        
        <Select
            labelId="inpLang" id="inpLang" value={lang} defaultValue={'th'}
            onChange={this.func_checngLang} className={classes.select} 
        >
            <MenuItem value={'th'}>THAI</MenuItem>
            <MenuItem value={'en'}>ENG</MenuItem>
        </Select>
        </FormControl>

    </Toolbar>
</AppBar>

export default appb