import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory , withRouter , Redirect ,Router } from 'react-router'
// import Routes from '../../routes'
// import Login from '../login/login'
import {checkCookiePack_existAndNotExpire} from '../../services/authen/authen'

export default function (ComposedComponent) {    
    class Authentication extends Component {      
        constructor(props) {
            super(props);
            this.state = {
                authenticated: undefined
            }
        }
        static contextTypes = {            
            router: PropTypes.object        
        }      
        async componentWillMount() {
            await this.start()
        }
        async componentWillUpdate(props) {
            await this.start()
        }
        start = async()=>{
            var result = await checkCookiePack_existAndNotExpire()
            // console.log(result)
            if (result === false) {             
                this.direct_login()
            }
        }
        direct_login =()=>{
            this.context.router.push('/mnglogin');
            browserHistory.push('/mnglogin')
        }
        render() {
            // console.log(`reqAuthen.js render`)
            return <ComposedComponent {...this.props} />
        }    
    }       
    function mapStateToProps(state) {
        return {
            authenticated: state.authReducers.authenticated        
        };
    }
    // return (withRouter(Authentication));
    return connect(mapStateToProps)(withRouter(Authentication));
}
