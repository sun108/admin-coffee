function column_report_day(){
    return {
        "th" : [ 
            {
                "title" : "idcode",
                "field" : "idcode",
                "align" : "center",
            },
            {
                "title" : "วันที่เวลา",
                "field" : "datetime",
                "align" : "center",
            },
            {
                "title" : "รวมจำนวนที่สั่ง",
                "field" : "amountlist",
                "align" : "center",
            },
            {
                "title" : "จำนวนเงิน",
                "field" : "totalprice",
                "align" : "center",
            },
        ],
        "en" : [ 
            {
                "title" : "idcode",
                "field" : "idcode",
                "align" : "center",
            },
            {
                "title" : "datetime",
                "field" : "datetime",
                "align" : "center",
            },
            {
                "title" : "amount list",
                "field" : "amountlist",
                "align" : "center",
            },
            {
                "title" : "total",
                "field" : "totalprice",
                "align" : "center",
            },
        ]
    }
}

function column_report_day_detail(){
    return {
        "th" : [ 
            {
                "title" : "ชื่อกาแฟ (TH)",
                "field" : "coffee[1].name[0].th",
                "align" : "center",
            },
            {
                "title" : "ชื่อสถานะ (TH)",
                "field" : "state[1].name[0].th",
                "align" : "center",
            },
            {
                "title" : "จำนวนส่วนเสริม (ต่อ1)",
                "field" : "option_amount",
                "align" : "center",
            },
            {
                "title" : "ราคาส่วนเสริม (ต่อ1)",
                "field" : "option_price",
                "align" : "center",
            },
            {
                "title" : "จำนวนที่สั่ง",
                "field" : "amount",
                "align" : "center",
            },
            {
                "title" : "รวมราคา",
                "field" : "price",
                "align" : "center",
            },
        ],
        "en" : [ 
            {
                "title" : "coffee (EN)",
                "field" : "coffee[1].name[1].en",
                "align" : "center",
            },
            {
                "title" : "state (EN)",
                "field" : "state[1].name[1].en",
                "align" : "center",
            },
            {
                "title" : "amount optional",
                "field" : "option_amount",
                "align" : "center",
            },
            {
                "title" : "optional price",
                "field" : "option_price",
                "align" : "center",
            },
            {
                "title" : "amount",
                "field" : "amount",
                "align" : "center",
            },
            {
                "title" : "total price",
                "field" : "price",
                "align" : "center",
            },
        ]
    }
}

function column_report_day_detail_optional(){
    return {
        "th" : [ 
            {
                "title" : "ชื่อส่วนเสริม (TH)",
                "field" : "name[0].th",
                "align" : "center",
            },
            {
                "title" : "จำนวนชิ้น",
                "field" : "amount",
                "align" : "center",
            },
            {
                "title" : "ราคาต่อชิ้น",
                "field" : "add_price",
                "align" : "center",
            },
            {
                "title" : "รวมราคา",
                "field" : "price",
                "align" : "center",
            }
        ],
        "en" : [ 
            {
                "title" : "option (EN)",
                "field" : "name[1].en",
                "align" : "center",
            },
            {
                "title" : "amount",
                "field" : "amount",
                "align" : "center",
            },
            {
                "title" : "price per one",
                "field" : "add_price",
                "align" : "center",
            },
            {
                "title" : "total price",
                "field" : "price",
                "align" : "center",
            }
        ]
    }
}

export { 
    column_report_day, column_report_day_detail ,column_report_day_detail_optional
 }