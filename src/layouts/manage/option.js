function column_query_option(){
    return {
        "th" : [ 
            {
                "title" : "ชื่อส่วนเสริม (TH)",
                "field" : "name_th",
                "align" : "center",
            },
            {
                "title" : "ชื่อส่วนเสริม (EN)",
                "field" : "name_en",
                "align" : "center",
            },
            {
                "title" : "เพิ่มราคา",
                "field" : "price",
                "align" : "center",
            },
            {
                "title" : "จัดการ",
                "field" : "manage",
                "align" : "center",
            },
        ],
        "en" : [ 
            {
                "title" : "name (TH)",
                "field" : "name_th",
                "align" : "center",
            },
            {
                "title" : "name (EN)",
                "field" : "name_en",
                "align" : "center",
            },
            {
                "title" : "price",
                "field" : "price",
                "align" : "center",
            },,
            {
                "title" : "manage",
                "field" : "manage",
                "align" : "center",
            },
        ]
    }
}

export { 
    column_query_option,
 }