function column_option_selected(){
    return {
        "th" : [ 
            {
                "title" : "ชื่อส่วนเสริม",
                "field" : "option_name[0].th",
                "align" : "center",
            },
            {
                "title" : "ราคาต่อชิ้น",
                "field" : "add_price",
                "align" : "center",
            },
            {
                "title" : "จำนวน",
                "field" : "option_amoun",
                "align" : "center",
            },
            {
                "title" : "รวมเป็นราคา",
                "field" : "totalprice",
                "align" : "center",
            },
            {
                "title" : "จัดการ",
                "field" : "manage",
                "align" : "center",
            },
        ],
        "en" : [ 
            {
                "title" : "name",
                "field" : "option_name[1].en",
                "align" : "center",
            },
            {
                "title" : "price per sec",
                "field" : "add_price",
                "align" : "center",
            },
            {
                "title" : "amount",
                "field" : "option_amoun",
                "align" : "center",
            },
            {
                "title" : "รวมเป็นราคา",
                "field" : "totalprice",
                "align" : "center",
            },
            {
                "title" : "จัดการ",
                "field" : "manage",
                "align" : "center",
            },
        ]
    }
}

function column_result_sale(){
    return {
        "th" : [ 
            {
                "title" : "รายการสินค้า",
                "field" : "product_name[0].th",
                "align" : "center",
            },
            {
                "title" : "ราคา",
                "field" : "totalprice_frontend",
                "align" : "center",
            },
            {
                "title" : "จัดการ",
                "field" : "manage",
                "align" : "center",
            },
        ],
        "en" : [ 
            {
                "title" : "name",
                "field" : "product_name[1].en",
                "align" : "center",
            },
            {
                "title" : "price",
                "field" : "totalprice_frontend",
                "align" : "center",
            },
            {
                "title" : "manage",
                "field" : "manage",
                "align" : "center",
            },
        ]
    }
}
export { 
    column_option_selected ,column_result_sale
 }