import React , { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';

import MenuIcon from '@material-ui/icons/Menu';

class Nav_HeadBar extends Component {
    render () {
        const title = this.props.class_imgShowDisplay
        const formControl = this.props.class_formControl;
        const select= this.props.class_select;
        const appBar = this.props.class_appBar;
        const menuButton = this.props.class_menuButton;

        return (
            <AppBar position="fixed" className={appBar}>
                <Toolbar>
                    <IconButton color="inherit" aria-label="open drawer" edge="start" className={menuButton}>
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap className={title}> FHR </Typography>

                    <Button color="inherit">Login</Button>

                    <FormControl variant="outlined" className={formControl}>
                    <InputLabel id="demo-simple-select-outlined-label">LANG</InputLabel>
                    <Select
                        labelId="demo-simple-select-outlined-label" id="demo-simple-select-outlined"
                        label="LANG" className={select} value={this.props.lang}
                    >
                        <MenuItem value={10}>THAI</MenuItem>
                        <MenuItem value={20}>ENG</MenuItem>
                    </Select>
                    </FormControl>

                </Toolbar>
            </AppBar>
    )
    };
};

export default Nav_HeadBar;