import React , { Component } from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { Link } from "react-router";
import LinearProgress from '@material-ui/core/LinearProgress';
import { connect } from 'react-redux'
import {std_header} from '../../services/std_service/header'

//style
import styleCode from '../../style/menu'

//service
import FetchData from '../../services/fetch_data/fetch'
import ErrorBoundary from '../../services/error/errorboundary'

//icon
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

//FontAwesomeIcon
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import {faDiceD20,faUserTie,faUserInjured,faUserClock,faUser,faCity,faWarehouse,faUsers,faChessKnight,faChessPawn,faGem,faFeather,faPlane,faUsersCog,faCogs} from "@fortawesome/free-solid-svg-icons";

//materialize
import 'materialize-css';
import { Collapsible, CollapsibleItem } from 'react-materialize';

class menu_humanResource extends Component {
    constructor(props) {
        super(props);
    }
    state = {
        hasError: false,
        lang: null,
        load: true ,
        titleMain: [],
        titleSub: []
    }
    componentDidCatch(error, info) {
        this.setState({ hasError: true })
    }
    componentWillMount() {
        this.start_page();
    }
    start_page= async()=>{
        this.setState({load:true})
        var url =null , data= []

        url = `${process.env.REACT_APP_URL_API}query?page_appMainMenu=all&page_appSubMenu=all`
        data.push ({"page_mainSubMenu":await FetchData(url,'page_mainSubMenu',0)})

        this.setState({
            titleMain : data.find(x=>x['page_mainSubMenu']).page_mainSubMenu[0],
            titleSub : data.find(x=>x['page_mainSubMenu']).page_mainSubMenu[1]
        })
        this.setState({load:false})
    }
    render () {
        // console.log('menu render : ' + getTime())
        const root = this.props.class_root
        const iconFASideMenu = this.props.class_iconFASideMenu
        var lang = null;
        library.add(fab,faUserInjured,faUserTie,faUserClock,faUser,faCity,faWarehouse,faUsers,faChessKnight,faChessPawn,faGem,faFeather,faPlane,faUsersCog,faCogs,faDiceD20)
        if(this.state.load){
            return (
                <ErrorBoundary>
                <div>
                    <LinearProgress color="secondary" />
                </div>
                </ErrorBoundary>
            )
        }else{
            lang = this.props.lang
                return (
                    <ErrorBoundary>
                        <div>
                        <style>{ styleCode }</style>
                        <Collapsible accordion={false} >
                            {loop_titleMain(this.state.titleMain.page_appMainMenu,this.state.titleSub.page_appSubMenu,lang)}
                        </Collapsible>
                        </div>
                    </ErrorBoundary>
                )
        }
    };
};
function loop_titleMain(valMain,valSub,lang){
    var result = [],valResult = null
    for(var i in valMain) {
        try{
            valResult = valMain.find(x => x.order === i)
            if(valResult.show){
                result.push(
                    <CollapsibleItem expanded={true}
                        header={valResult[lang]} style={{width: '100%' }}
                        node="div"  icon={<ExpandMoreIcon/>}
                    >
                        <li>{loop_titleSub(valSub,valResult.idcode,lang)}</li>
                    </CollapsibleItem>
                )
            }
        }catch(e){ }
    }
    return result
}
function loop_titleSub(val,findMainIdcode,lang){
    var result = []
    {
        val.map((row) =>
            {
                if(row.show){
                    if (row.main_idcode == findMainIdcode){
                        result.push(
                            <ListItem button component={Link} to={row.link}>
                                {/* <ListItemIcon>
                                    <FontAwesomeIcon icon={faUserAlt} />
                                </ListItemIcon> */}
                                <ListItemText primary={row[lang]} />
                                <FontAwesomeIcon icon={row.icon} />
                            </ListItem>
                        )
                    }
                }
            }
        )
    }
    return result //className={iconFASideMenu}
}
function getStringTitleMain(dataState , number , lang) {
    return dataState.find(x => x.order === number)[lang]
}
function getTime(){
    var d = new Date();
    return d.toLocaleString() + ' ' + d.getMilliseconds();
}
function mapStateToProps(state) {
    return {
        lang: state.page_getLangToState
    }
}

export default connect(mapStateToProps)(menu_humanResource)