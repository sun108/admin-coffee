import React , { Component } from 'react';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { Link } from "react-router";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faCity , faWarehouse , faChessKnight , faChessPawn , faGem , faFeather , faPlane , faCalendarAlt } from "@fortawesome/free-solid-svg-icons";

class menu_humanResource extends Component {
    render () {
        const root = this.props.class_root
        const iconFASideMenu = this.props.class_iconFASideMenu
        return (
            <div>
            <List component="nav" aria-labelledby="nested-list-subheader" style={{ backgroundColor: "#3949ab" }}
                subheader={
                    <ListSubheader component="div" id="nested-list-subheader" style={{ color: "white" }}>MASTER DATA</ListSubheader>
                } className={root}
            >
            </List>

            <ListItem button component={Link} to="/company">
                <ListItemIcon>
                    <FontAwesomeIcon icon={faCity} className={iconFASideMenu} />
                </ListItemIcon>
                <ListItemText primary="จัดการข้อมูลบริษัท" />
            </ListItem>
            
            <ListItem button component={Link} to="/dashboard">
                <ListItemIcon>
                    <FontAwesomeIcon icon={faWarehouse} className={iconFASideMenu} />
                </ListItemIcon>
                <ListItemText primary="จัดการข้อมูลสาขา" />
            </ListItem>

            <ListItem button>
                <ListItemIcon>
                    <FontAwesomeIcon icon={faChessKnight} className={iconFASideMenu} />
                </ListItemIcon>
                <ListItemText primary="จัดการข้อมูลฝ่ายงาน" />
            </ListItem>

            <ListItem button>
                <ListItemIcon>
                    <FontAwesomeIcon icon={faChessPawn} className={iconFASideMenu} />
                </ListItemIcon>
                <ListItemText primary="จัดการข้อมูลหน่วยงาน" />
            </ListItem>

            <ListItem button>
                <ListItemIcon>
                    <FontAwesomeIcon icon={faGem} className={iconFASideMenu} />
                </ListItemIcon>
                <ListItemText primary="จัดการข้อมูลระดับงาน" />
            </ListItem>

            <ListItem button>
                <ListItemIcon>
                    <FontAwesomeIcon icon={faFeather} className={iconFASideMenu} />
                </ListItemIcon>
                <ListItemText primary="จัดการข้อมูลคำนำงาน" />
            </ListItem>

            <ListItem button>
                <ListItemIcon>
                    <FontAwesomeIcon icon={faPlane} className={iconFASideMenu} />
                </ListItemIcon>
                <ListItemText primary="จัดการข้อมูลวันลา" />
            </ListItem>

            <ListItem button>
                <ListItemIcon>
                    <FontAwesomeIcon icon={faCalendarAlt} className={iconFASideMenu} />
                </ListItemIcon>
                <ListItemText primary="จัดการข้อมูลวันหยุด" />
            </ListItem>
            </div>
        )
    };
};

export default menu_humanResource;