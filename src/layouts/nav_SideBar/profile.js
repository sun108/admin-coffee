import React , { Component } from 'react';
import ExampleComponent from "react-rounded-image";
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import {StdImg_profileImage ,StdImg_personImg100} from '../../component/image_profile/std_imageProfile'
import {std_reqImgPerson_req} from '../../services/std_service/req_image'

// import img_person from '../../storage/person_image/2SsHtESU3ZbFXS6thJxG.jpg'

class nav_sideBar extends Component {
    render () {
        const imgShowDisplay = this.props.class_imgShowDisplay
        return (
            <div >
                {/* <StdImg_profileImage 
                    image={std_reqImgPerson_req(this.props.idcode)} 
                    className={imgShowDisplay} 
                    imageWidth="150" imageHeight="150" 
                    roundedColor="#321124" 
                    roundedSize="13" style={{}} 
                /> */}
                <center>
                <StdImg_personImg100 
                    image={this.props.idcode} 
                    className={this.props.class_imgShowDisplay} 
                />
                </center>
                <TextField className={this.props.class_textBox} id="outlined-basic" label="ชื่อของคุณ" variant="outlined" defaultValue={this.props.fullname} InputProps={{readOnly: true,}} style={{marginTop: '20px' , padding: '10px'}} />
                {/* <TextField className={imgShowDisplay} id="outlined-basic" label="ตำแหน่งคุณ" variant="outlined" defaultValue="อวุโส เจ้าหน้าที่ นักพัฒนาโปรแกรม เทคโนโลยีสารสนเทศ" InputProps={{readOnly: true,}} /> */}
                <Divider />
            </div>
        )
    };
};
function readTextFile(file)
{
    console.log(file)
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                var allText = rawFile.responseText;
                console.log(allText)
            }
        }
    }
    // rawFile.send(null);
}
function checkFile(){
    const fs = require('fs')
    const path = '/usr/src/app/storage/person_image/1101100070858.jpg'
    try {
        if (fs.existsSync(path)) {
          //file exists
        }
      } catch(err) {
        console.error(err)
      }
}

export default nav_sideBar;