const styleCode = `
      .carousel{height:unset}
      .example1 { 
        border: 0px solid black; 
        padding: 10px; 
        margin-bottom: 10px;
        border-radius: 10px; 
        width: 95%;
      }
      .box_selected { 
        padding: 10px; 
        margin-bottom: 10px;
        border-radius: 10px; 
        width: 100%;
        height: 250px;
      }
      .box_upload_slip_selected { 
        padding: 10px; 
        margin-bottom: 10px;
        border-radius: 10px; 
        width: 100%;
        height: 90px;
      }
      .MuiInputBase-input{
        border-bottom: unset !important;
        padding-left: 10px !important;
        padding-right: 10px !important;
      }
      .fieldInput {
        text-align: start;
        width: 100%;
      }`

export {styleCode}