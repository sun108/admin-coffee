const UseStyles = theme => ({
    root_width: {
        width: '100%',
    },
    table: {
      minWidth: 650,
    },
    title: {
        justifyContent: "center",
    },
    div_center: {
        display: "flex",
        textAlignVertical: "center",
        textAlign: 'center',
        alignItems: 'center',
        display: 'flex', 
        justifyContent: 'center'
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        border: '2px solid #000'
    },
});

export default UseStyles