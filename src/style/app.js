import { makeStyles, useTheme } from '@material-ui/core/styles';

const drawerWidth = 240;
const useStyles = theme => ({
  root: {
    display: 'flex',zIndex: '1',padding: `20px`,
    "& .MuiPaper-root": {
      zIndex: '1'
    },
    flexGrow: 1,
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
      zIndex: '2'
    },
  },
  drawerPersistent: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  hide: {
    display: 'none',
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
      zIndex: '1'
    },
  },
  appBarPersistent: {
    transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
        zIndex: '1'
    }),
  },
  appBarShiftPersistent: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
      transition: theme.transitions.create(['margin', 'width'], {
          easing: theme.transitions.easing.easeOut,
          duration: theme.transitions.duration.enteringScreen,
          zIndex: '1'
      }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  menuButtonPersistent: {
    marginLeft: 12,
    marginRight: 20,
  },
  menuButton_right: {
    marginLeft: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  title: {
    flexGrow: 1,
    fontSize: 'xx-large',
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth + 5,
    zIndex: '1'
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    width: '100%',
    paddingTop:'70px'
  },
  inpShowDisplay: {
    padding : 0,
    width: drawerWidth - 20
  },
  imgShowDisplay: {
      boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)'
  },
  textbox: {
    marginTop: '-25px !important'
  },
  iconFASideMenu: {
    fontSize: "x-large"
  },
  select: {
      backgroundColor: 'white',
      '&:before': {
          borderColor: 'white',
      },
      '&:after': {
          borderColor: 'white',
      }
  },
  icon: {
      fill: 'white',
  },
  xsDownPersistent:{
    width: '100%',
    paddingLeft: '240px'
  },
  drawerHeaderPersistent: {
      display: 'flex',
      alignItems: 'center',
      padding: '0 8px',
      ...theme.mixins.toolbar,
      justifyContent: 'flex-end',
  },
  input: { borderBottom:'unset' },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
    // backgroundImage: `url(${`assets/3.jpg`})`,
    // backgroundSize:  `100% 100%`
  }
});

export default useStyles;