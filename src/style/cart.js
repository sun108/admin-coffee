var style_cart = `
    table {
        border: 0px solid #CCC;
        border-collapse: unset !important;
    }
    .MuiTableCell-root{
        padding: 5px;
    }
    td {
        border: none;
    }
    .modal .modal-content{
        padding-left: 24px;
        padding-right: 24px;
        padding-top: 0px;
    }
    .myTextfield_number{
        border-bottom: unset !important;
        padding-left: 10px !important;
        padding-right: 10px !important;
        text-align: end !important;
    }
    div.price div input{
        border-bottom: unset !important;
        padding-left: 10px !important;
        padding-right: 10px !important;
        text-align: end !important;
    }`
export default  style_cart