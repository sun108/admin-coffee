let styleCode = `.collapsible-body{
    padding:unset; 
} .collection-item {
    width:100%; 
} .collapsible-header {
    font-size: large;font-weight: bold;background-color: #434dae;color: white;
} li{width: 100%;}

button:focus {background-color: unset}

input {
    border-bottom: unset !important;
    padding-left: 3px !important;
    padding-right: 3px !important;
}

input:focus {
    border-bottom: unset !important;
    box-shadow: unset !important;
}

.myfont_orderall_textfield{
    font-family: 'DB HeaventRounded Med', cursive;
    background-color: white;
    width: 100%;
}
.myfont_orderall_head{
    font-size: 28px;
    font-weight: bold;
    color: black;
    font-family: 'DB HeaventRounded Med', cursive;
}
.myfont_orderall_under_head{
    font-size: 23px;
    color: black;
    font-family: 'DB HeaventRounded Med', cursive;
}
.myfont_orderall_table_head{
    font-size: 16px;
    font-weight: bold;
    font-family: 'DB HeaventRounded Med', cursive;
}

.myCard_style{
    font-family: 'DB HeaventRounded Med', cursive;
    padding: 10px;
    margin-top: 10px;
    border-radius: 20px;
}
.myCard_style_title{
    font-family: 'DB HeaventRounded Med', cursive;
}
.myfont_cart_table_summary{
    color: green;
    font-size: 20px;
    font-family: 'DB HeaventRounded Med', cursive;
}
.myFontDashBoard{
    color: black;
    font-family: 'DB HeaventRounded Med', cursive;
}
`

export default styleCode