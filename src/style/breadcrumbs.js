let breadcrumbStyle = `
nav{
  box-shadow: unset;
  background-color: unset;
}
.MuiBreadcrumbs-ol{ flex-wrap: unset; }
li{ width: unset !important; }
nav a{ color: #717500}
.myChip{ margin-left: 5px }
`

export default breadcrumbStyle