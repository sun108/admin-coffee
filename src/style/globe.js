import { bounce , fadeInDown,  fadeInUp ,fadeOutDown} from 'react-animations';

import Radium, {StyleRoot} from 'radium';
import '../style/font.css'
let styleCode = `.collapsible-body{
    padding:unset; 
} .collection-item {
    width:100%; 
} 

body{
    font-family: 'DB HeaventRounded Med';
    font-size: 12px
}

.ponf_title_head{
    font-size: 3em;
    font-family: 'DB HeaventRounded Med';
}

.collapsible-header {
    font-size: large;
    font-weight: bold;
    color: white;
    background: 
    radial-gradient(
        ellipse farthest-corner at right bottom, 
        #FEDB37 0%, 
        #FDB931 8%, 
        #9f7928 30%, 
        #8A6E2F 40%, 
        transparent 80%
    ),radial-gradient(
        ellipse farthest-corner at left top, 
        #ffffcc 0%, 
        #FFFFAC 15%, 
        #D1B464 40%, 
        #5d4a1f 62.5%, 
        #5d4a1f 100%
    );
} 
li{width: 100%;}
button:focus {background-color: unset}

input {
    border-bottom: unset !important;
}

input:focus {
    border-bottom: unset !important;
    box-shadow: unset !important;
}

.myfont_cart_modal_head{
    font-size: 25px;
    font-family: 'DB HeaventRounded Med';
}
.myfont_cart_modal_list_unit{
    text-align: start;
    font-weight: bold;
    font-size: 18px;
    font-family: 'DB HeaventRounded Med';
    color: maroon;
}
.myfont_content_result{
    font-size: 19px;
    text-align: start;
    font-family: 'DB HeaventRounded Med';
}

.myfont_content_payment{
    font-size: 18px;
    font-family: 'DB HeaventRounded Med';
}

.myfont_content_std{
    font-size: 19px;
    font-family: 'DB HeaventRounded Med';
}
.myfont_content_detail{
    font-size: 12px;
    font-family: 'DB HeaventRounded Med';
}
.myfont_content_detail_price{
    font-size: 20px;
    font-family: 'DB HeaventRounded Med';
}
.myfont_content_headtitle{
    margin-top: 10px !important;
    font-family: 'DB HeaventRounded Med';
    color: black;
}

.myfont_idcode_thankyou{
    font-size: 23px;
    font-family: 'DB HeaventRounded Med';
    color: black;
}
.myfont_content_thnakyou_xs{
    font-size: 15px;
    font-family: 'DB HeaventRounded Med';
    color: black;
}
.myfont_content_thnakyou_md{
    font-size: 20px;
    font-family: 'DB HeaventRounded Med';
    color: black;
}

.myFont_title_tracking{
    font-size: 27px;
    font-family: 'DB HeaventRounded Med';
}

.myfont_cart_table_head{
    font-size: 15px;
    font-weight: bold;
    font-family: 'DB HeaventRounded Med';
    color: darkred;
}
.myfont_cart_table_body{
    font-size: 17px;
    font-family: 'DB HeaventRounded Med';
}
.myfont_cart_table_summary{
    color: green;
    font-size: 20px;
    font-family: 'DB HeaventRounded Med';
}
.myfont_cart_content{
    font-size: 13px;
    font-family: 'DB HeaventRounded Med';
}

.mybottom_shoping_navigative{
    background-color: #005d92;
    color: white;
    border-radius: 25px;
}
.mybottom_contactus_navigative{
    background-color:  #739200 ;
    color: white;
    border-radius: 25px;
}
.mybottom_std{
    padding: 5px;
    height: 50px;
    position: fixed;
    width: 100%;
    bottom: 0px;
    box-shadow: 0px 2px 5px;
}
.myModal_90{
    height: 85% !important;
}

.pCircleCart {
    text-align: center;
    background-color: red;
    border-radius: 100%;
    height: 20px;
    width: 20px;
}
.pCartWarningBottomTotalPrice{
    margin: 0 0 0px;
    font-size: 18px;
    font-weight: bold;
    font-family: 'DB HeaventRounded Med';
}
.pCartWarningBottomPromotion{
    margin: 0 0 0px;
    font-size: 18px;
    font-family: 'DB HeaventRounded Med';
}

.pLimitLine_orderCard{
    font-size: 15px;
    font-family: 'DB HeaventRounded Med';
    text-overflow: ellipsis;
    word-wrap: break-word;
    display: -webkit-box;
    max-height: 3.6em;
    line-height: 1.8em;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    -moz-box-orient: vertical;
    overflow: hidden;
    text-align: start;
}

.pPricePromotionStyle{
    font-size: 23px;
    font-weight: bold;
    color: #b57420;
    font-family: 'DB HeaventRounded Med';
    text-align: start;
    margin: 0px;
}
.pAnotherDetailStyle{
    font-size: 13px;
    font-family: 'DB HeaventRounded Med';
    text-align: start;
    margin: 0px;
}
.pContentDetailStyle{
    font-size: 15px;
    font-family: 'DB HeaventRounded Med';
    text-align: start;
    margin: 0px;
    white-space: pre-line;
}
.pPriceUnderPromotion{
    font-size: 15px;
    text-decoration: line-through;
    font-family: 'DB HeaventRounded Med';
    text-align: start;
    margin: 0px;
}

.cardUnitProduct { 
    border: 0px solid black; 
    padding: 10px; 
    margin-bottom: 10px;
    border-radius: 10px; 
    width: 95%;
}

.pProducTitleStyle{
    font-size: 20px;
    font-weight: bold;
    font-family: 'DB HeaventRounded Med';
    text-align: start;
}
.pProductPricePromotionStyle{
    font-size: 25px;
    font-weight: bold;
    color: red;
    font-family: 'DB HeaventRounded Med';
    text-align: end;
    margin: 0px;
}
.pProductPriceUnderPromotion{
    font-size: 16px;
    text-decoration: line-through;
    font-family: 'DB HeaventRounded Med';
    text-align: end;
    margin: 0px;
}

.pOverAllSideMenu{
    font-size: 20px;
    font-family: 'DB HeaventRounded Med';
    text-align: start;
    padding-left: 5px;
    width: 100%;
    color: black;
}

.pLineOverAll{
    font-size: 25px;
    font-weight: bold;
    font-family: 'DB HeaventRounded Med';
    text-align: center;
    border-bottom: 1px solid #000;
    line-height: 0.1em ;
    margin: 10px 0 20px;
    width: 100%;
}
.pProducTitleStyle_md{
    font-size: 28px;
    font-weight: bold;
    font-family: 'DB HeaventRounded Med';
    text-align: start;
}
.pContentDetailStyle_md{
    font-size: 20px;
    font-family: 'DB HeaventRounded Med';
    text-align: start;
    margin: 0px;
    white-space: pre-line;
}
.pProductPricePromotionStyle_md{
    font-size: 38px;
    font-weight: bold;
    color: red;
    font-family: 'DB HeaventRounded Med';
    margin: 0px;
}
.pProductPriceUnderPromotion_md{
    font-size: 26px;
    text-decoration: line-through;
    font-family: 'DB HeaventRounded Med';
    margin: 0px;
}
.pAnotherDetailStyle_md{
    font-size: 23px;
    font-family: 'DB HeaventRounded Med';
    text-align: start;
    margin: 0px;
}

.myfont_orderall_textfield {
    margin-top: 5px;
}
.myfont_orderall_textfield div input{
    background-color: white;
    width: 100%;
    padding-left: 10px !important;
    padding-right: 10px !important;
    font-size: 5em;
}

.myBoxCard {
    background-color: white;
    border: 0px solid black; 
    padding: 10px; 
    margin-bottom: 10px;
    border-radius: 10px; 
    width: 95%;
  }

.myfont_title_refundPolicy {
    font-size: 23px;
    font-weight: bold;
    font-family: 'DB HeaventRounded Med';
    margin: 0px;
}
.myfont_content_refundPolicy {
    font-size: 20px;
    font-family: 'DB HeaventRounded Med';
    margin: 0px;
}

.myCard_style{
    font-family: 'DB HeaventRounded Med';
    padding: 10px;
    margin-top: 10px;
    border-radius: 20px;
}
.myCard_style_title{
    font-family: 'DB HeaventRounded Med';
}
`
var styleFx = {
    title: {
      animation: 'x 3s',
      animationName: Radium.keyframes(fadeInDown, `fadeOutDown`)
    },
    lineUnder: {
        animation: 'x 5s',
        animationName: Radium.keyframes(fadeInUp, `fadeOutDown`)
    }
  }

export {styleCode , styleFx}