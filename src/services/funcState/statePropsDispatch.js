function mapStateToProps(state) {
    return {
        lang: state.page_getLangToState,
        arrOrder : state.func_actionCart,
        wording: state.getWordingStringThis
    }
  }
function mapDispatchToProps(dispatch) {
    return {
        actionReducer: (type,payload) => {
        dispatch({type: type,payload:payload})
        }
    }  
}

export {
    mapStateToProps ,
    mapDispatchToProps
}