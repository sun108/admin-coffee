import jwt , { sign, verify } from 'jsonwebtoken'
var password = process.env.REACT_APP_HASH_JWT;

async function jwt_encode(payload){
    return await jwt.sign({ payload }, password);
}

async function jwt_decode(token,hash){
    return jwt.verify(token, hash, function(err, decoded) {
        if (err) {
            // console.log(err)
            return err      
        }else{
            // console.log(decoded)
            return decoded
        }
    })
}

export {
    jwt_encode , jwt_decode
};