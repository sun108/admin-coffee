import React , { Component } from 'react';
import {std_header} from '../../services/std_service/header'

async function authorize(body){
    const url = `${process.env.REACT_APP_URL_SERVER_API}lineliff/authorize/suncoffee`
    var result = await Promise.all([ 
         fetch(url ,std_header('POST',body)) 
    ])
    .then(([x]) => Promise.all([x.json()]))
    .then(async([datax]) => {
        return datax
    })
    return result
}

export { authorize }