var jwt = require('jsonwebtoken');

class auth_jwt {
    constructor(){
        this.payload = ['emp_code','pwd','lang']
    }
    met_jwtSetSecretKey(emp_code , pwd , lang){
        this.payload['emp_code'] = emp_code
        this.payload['pwd'] = pwd
        this.payload['lang'] = lang
    }
    met_GenerateStringJWT(){
        var token = jwt.sign({ 
            emp_code: this.payload['emp_code'],
            pwd: this.payload['pwd'],
            lang: this.payload['lang'],
            iat: Math.floor(Date.now() / 1000) - 30
           }, 'secret', { expiresIn: '1h' }
        );
        return token
    }
}

export default auth_jwt;