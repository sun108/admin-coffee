import {std_header} from '../std_service/header'
import {jwt_decode} from '../jwt/jwt_encode'
import {ponEncodeToNode} from '../encryption/ponEncode'

async function FetchData_menu(url,mode,time,body){
    var arrResult = []
        
    var fData_title = new Promise(async(resolve, reject) => { 
        var myBody = JSON.stringify([await ponEncodeToNode(
            JSON.stringify([body])
        )])
        setTimeout(() => resolve(
            fetch(url ,std_header('POST',myBody))
        ), time)
    }); 

    await Promise.all([ fData_title.catch(error => { return error }) ])
    .then(([data_title]) => Promise.all([ data_title.json() ]))
    .then(async([datav]) => {
        var datab = await convertEncode(datav)
        var datax = datab.payload
        if(datax['success']){
            switch(mode){
                case 'menu_dashboard':
                    arrResult.push( datax['data'].find(x => x['customer_order']) )
                    arrResult.push( datax['data'].find(x => x['order_status']) )
                break;
            }
        }else{
            arrResult = returnNotSuccess()
        }
    })
    return arrResult
}
function returnNotSuccess(){
    return []
}
async function convertEncode(datax){
    var replaceString = datax.substring(0,10)
    var convertToJWT = datax.replaceAll(replaceString,'.')
    var cutFirstString = convertToJWT.substring(1);
    var ress = await jwt_decode(cutFirstString,process.env.REACT_APP_HASH_JWT)
    return ress
}

export default FetchData_menu