import {std_header , std_header_fhr} from '../std_service/header'
import {jwt_decode} from '../jwt/jwt_encode'

async function FetchData_fhr(url,mode,time){
    var arrResult = []
        
    var fData_title = new Promise((resolve, reject) => { 
        setTimeout(() => resolve(
            fetch(url ,std_header_fhr('GET',null)) 
        ), time)
    }); 

    await Promise.all([ fData_title.catch(error => { return error }) ])
    .then(([data_title]) => Promise.all([ data_title.json() ]))
    .then(async([datax]) => {
        if(datax['success']){
            switch(mode){
                //admin
                case 'dataSource_admin':
                    arrResult.push( datax['data'].find(x => x['data_admin']).data_admin )
                break;
                case 'dataSource_person':
                    arrResult.push( datax['data'].find(x => x['person']).person )
                break;
            }
        }else{
            arrResult = returnNotSuccess()
        }
    })
    return arrResult
}
function returnNotSuccess(){
    return []
}

export default FetchData_fhr