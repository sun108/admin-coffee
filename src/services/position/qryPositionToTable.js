import React , { Component , useState, useEffect } from 'react';
import {checkStringInNotNullOrEmpty_bool , stringNull} from '../std_service/getString'


function qryPositionToTable(rowOrgc , dataSource_workMaster , lang , dpmArr){
    var row = [], result = null,value = null,fullPositionName = null , ret = null

    if(checkStringInNotNullOrEmpty_bool(rowOrgc.idcode_chiefLevel) || checkStringInNotNullOrEmpty_bool(rowOrgc.idcode_chiefExusiveLevel)){
        if(checkStringInNotNullOrEmpty_bool(rowOrgc.idcode_chiefLevel)){
            result = getLevelAndSectionAndDepartment( dataSource_workMaster.find(x => x['work_chiefLevel'])['work_chiefLevel'] , rowOrgc.idcode_chiefLevel , lang )
            row.levelWork = result[lang]
            fullPositionName = `${result[lang]}`
            value = value + result['value']
        }else if(checkStringInNotNullOrEmpty_bool(rowOrgc.idcode_chiefExusiveLevel)){
            result = getLevelAndSectionAndDepartment( dataSource_workMaster.find(x => x['work_excusiveLevel'])['work_excusiveLevel'] , rowOrgc.idcode_chiefExusiveLevel , lang )
            row.levelWork = result[lang]
            fullPositionName = `${result[lang]}`
            value = value + result['value']
        }else{
            row.levelWork = stringNull
        }
    }else{
        if(checkStringInNotNullOrEmpty_bool(rowOrgc.idcode_levelwork)){
            result = getLevelAndSectionAndDepartment( dataSource_workMaster.find(x => x['work_level'])['work_level'] , rowOrgc.idcode_levelwork , lang )
            row.levelWork = result[lang]
            fullPositionName = `${result[lang]}`
            value = value + result['value']
        }else{
            row.levelWork = stringNull
        }
    
        if(checkStringInNotNullOrEmpty_bool(rowOrgc.idcode_section)){
            result = getLevelAndSectionAndDepartment( dataSource_workMaster.find(x => x['work_section'])['work_section'] , rowOrgc.idcode_section , lang )
            row.sectionWork = result[lang]
            fullPositionName = `${fullPositionName} ${result[lang]}`
            value = value + result['value']
        }else{
            row.sectionWork = stringNull
        }
    
        if(checkStringInNotNullOrEmpty_bool(rowOrgc.idcode_department)){
            result = getLevelAndSectionAndDepartment( dataSource_workMaster.find(x => x['work_department'])['work_department'] , rowOrgc.idcode_department , lang )
            row.departmentWork = result[lang]
            fullPositionName = `${fullPositionName} ${result[lang]}`
            if(!dpmArr.includes(result['idcode'])){
                dpmArr.push(result['idcode'])
            }
        }else{
            row.departmentWork = stringNull
        }
    }
    
    if(checkStringInNotNullOrEmpty_bool(rowOrgc.idcode_leadwork)){
        result = getLevelAndSectionAndDepartment( dataSource_workMaster.find(x => x['work_lead'])['work_lead'] , rowOrgc.idcode_leadwork , lang )
        row.leadWork = result[lang]
        if(result[lang] !== stringNull){
            fullPositionName = `${result[lang]} ${fullPositionName}`
        }
        if(result['operation'] === 'add'){
            value = value + result['value']
        }else if(result['operation'] === 'del'){
            value = value - result['value']
        }
    }else{
        row.leadWork = stringNull
    }
    ret  ={ "row":row , "result":result , "value":value , "fullPositionName":fullPositionName ,"dpmArr":dpmArr}
    // console.log(ret)
    return ret
}
function getLevelAndSectionAndDepartment(wmaster , idcodeFind){
    var result = wmaster.find(x => x['idcode'] === idcodeFind)
    return result
}

export default qryPositionToTable
