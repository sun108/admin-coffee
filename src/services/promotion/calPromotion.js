function calpromotion_giftFree(listData , price ,amount ,toal ,listUnit ,idcode_gen){
    var res = [] , newList = {}
    if(listData.idcode_promotionType === `promotion_002`){
        //same unit
        var priceTrue = 0, findUnit = listUnit.filter(x=>x[`idcode`] === listData.idcode_unit)
        if(amount >= parseInt(listData.condition) && findUnit.length > 0){
            priceTrue = parseFloat(findUnit[0].price)
            if(findUnit[0].price !== findUnit[0].promotion){
                priceTrue = parseFloat(findUnit[0].promotion)
            }
            var getValue = amount / parseInt(listData.condition)
            var gift = Math.floor(getValue)
            newList.idcode_promotion = idcode_gen
            newList.amount = gift * parseInt(listData.amount)
            newList.inItem = findUnit[0]
            newList.total = (parseFloat(priceTrue) * gift)
            newList.priceUse = parseFloat(priceTrue)
        }
    }
    if(listData.idcode_promotionType === `promotion_003`){
        //another unit
        var priceTrue = 0, findUnit = listUnit.filter(x=>x[`idcode`] === listData.idcode_unit_add)
        if(amount >= parseInt(listData.condition) && findUnit.length > 0){
            priceTrue = parseFloat(findUnit[0].price)
            if(findUnit[0].price !== findUnit[0].promotion){
                priceTrue = parseFloat(findUnit[0].promotion)
            }
            var getValue = amount / parseInt(listData.condition)
            // console.log(getValue)
            // console.log(Math.floor(getValue))//donw
            // console.log(Math.ceil(getValue))//up
            var gift = Math.floor(getValue)
            newList.idcode_promotion = idcode_gen
            newList.amount = gift * parseInt(listData.amount)
            newList.inItem = findUnit[0]
            newList.total = (parseFloat(priceTrue) * gift)
            newList.priceUse = parseFloat(priceTrue)
        }
    }
    // res.push(newList)
    return newList
}
function calPromotion_shipping(listPromotion ,listSysDelivery ,listRelativeDelivery ,listItemCustomerBuyer ,totalPrice ,listShippingCustomer){
    var res = [];
    listPromotion.filter(v=>v[`active`]).map(x=>{
        var newList = {} ,priceTotal = 0 ,discount = 0
        if(x.idcode_promotionType === `promotion_001`){
            //if math condition is free
            var findDeliveryIndex = listSysDelivery.filter(b=>b[`idcode`] === x.idcode_delivery)
            if(findDeliveryIndex.length > 0){
                var getListUnitRelativeDelivery = listRelativeDelivery.filter(f=>f[`idcode_delivery`] === x.idcode_delivery)
                if(getListUnitRelativeDelivery.length > 0){
                    getListUnitRelativeDelivery.map(s=>{
                        var findUnitInCondition = listItemCustomerBuyer.filter(g=>g[`idcode_unit`] === s.idcode_unit)
                        if(findUnitInCondition.length > 0){
                            priceTotal = parseFloat(priceTotal) + parseFloat(findUnitInCondition[0].total)
                        }
                    })
                }
            }
            if(priceTotal >= parseFloat(x.condition)){
                // discount = 
                var findDeliveryCost = listShippingCustomer.filter(k=>k[`idcode_delivery`] === x.idcode_delivery)
                if(findDeliveryCost.length > 0){
                    newList.idcode_delivery = findDeliveryCost[0].idcode_delivery
                    newList.total = findDeliveryCost[0].total
                    newList.inItem = x
                }
            }
        }
        if(priceTotal !== 0){
            res.push(newList)
        }
    })
    return res
}
function calCustomerBuy(listItemCustomerBuyer){
    var res = 0    
    listItemCustomerBuyer.map(x=>{
        res = res + x.total
    })
    return res
}
function calShipping_toPrice(listPromotion ,listSysDelivery ,listRelativeDelivery ,listItemCustomerBuyer ,extraCashDestination ,wording){
    const extraCashDestinationPercent = 3
    var res = [] , totalPrice = 0
    listSysDelivery.map(x=>{
        var newList = {} , amount = 0 ,price = 0;
        var findInRelation = listRelativeDelivery.filter(g=>g[`idcode_delivery`] === x.idcode)
        if(findInRelation.length > 0){
            listItemCustomerBuyer.map(f=>{
                var findUnitCal = findInRelation.filter(d=>d[`idcode_unit`] === f.idcode_unit)
                if(findUnitCal.length > 0){
                    amount = amount + parseInt(f.amount)
                }
            })
            price = parseFloat(x.price_start)
            if(amount > parseInt(x.unit_min)){
                price = price + ( parseInt(x.price_per_unit) * ( amount - parseInt(x.unit_min)) )
            }
            if(amount > 0){
                newList.idcode_delivery = x.idcode
                newList.inItem = x
                newList.amount = amount
                newList.total = price
                totalPrice = parseInt(totalPrice) + parseInt(price)
                res.push(newList)
            }
        }
    })
    if(extraCashDestination){
        var newList = {}
        newList.idcode_delivery = `extra_cash_destination`
        newList.inItem = {
            "en":wording[`en`].summary_page.extraCash_destination,
            "th":wording[`th`].summary_page.extraCash_destination,
        }
        newList.amount = 0
        newList.total = Math.floor((parseInt(totalPrice) * extraCashDestinationPercent) / 100)
        //ceil = to up , floor = to down
        res.push(newList)
    }
    return res
}
function calTotalPrice(listUnitCustomerBuy ,listShippingCustomer ,listDiscountCustomer){
    var res = 0;
    listUnitCustomerBuy.filter(x=>x[`total`] !== null && x[`total`] !== undefined ).map(x=>{
        res = parseFloat(res) + parseFloat(x.total)
    })
    listShippingCustomer.filter(x=>x[`total`] !== null && x[`total`] !== undefined ).map(x=>{
        res = parseFloat(res) + parseFloat(x.total)
    })
    listDiscountCustomer.filter(x=>x[`total`] !== null && x[`total`] !== undefined ).map(x=>{
        res = parseFloat(res) - parseFloat(x.total)
    })
    return res
}

export {
    calpromotion_giftFree ,calPromotion_shipping ,calCustomerBuy ,calShipping_toPrice ,calTotalPrice
}