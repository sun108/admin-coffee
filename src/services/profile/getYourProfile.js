import {jwt_decode_payload} from '../../services/authen/authen'
import {setCookie , getCookie , removeCookie} from '../../services/authen/cookieService'
async function getProfile(){
    try{
        let token = getCookie('authen')
        if(token!== null && token!==undefined){
            var result = await jwt_decode_payload(token)
            // console.log(result)
            return result
        }else{
            return false
        }
    }catch(e){
        return false
    }
}

export default getProfile;