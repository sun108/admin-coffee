import React , { Component , useState, useEffect , Fragment} from 'react';

const Cryptr = require('cryptr');
const cryptr = new Cryptr(process.env.REACT_APP_ENCRYPT_FHR);

//======= example ============

//const encryptedString = cryptr.encrypt('bacon');
//const decryptedString = cryptr.decrypt(encryptedString);

//======= example ============

function encrypt_passw(string){
    return cryptr.encrypt(string);
}
function decrypt_passw(string){
    return cryptr.decrypt(string);
}

export {encrypt_passw,decrypt_passw}