import { encode, decode } from 'js-base64';
import {Base64} from 'js-base64';
import {jwt_encode , jwt_decode} from '../jwt/jwt_encode'

function ponEncode(str){
    return Base64.encode(str)
}

function ponDecode(str){
    return Base64.decode(str)
}

async function ponEncodeToNode(payload){
    var rnd = require("randomstring");
    var replaceSign = rnd.generate(10)
    var result = await jwt_encode(payload)
    result = await result.split('.').join(replaceSign)
    result = await replaceSign+result
    return result
}

async function convertEncode(datax){
    var replaceString = datax.substring(0,10)
    var convertToJWT = datax.replaceAll(replaceString,'.')
    var cutFirstString = convertToJWT.substring(1);
    var ress = await jwt_decode(cutFirstString,process.env.REACT_APP_HASH_JWT)
    return ress
}

export{
    ponEncode , ponDecode , ponEncodeToNode ,convertEncode
}