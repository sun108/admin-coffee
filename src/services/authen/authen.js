import {base64Encode_string , base64Decode_string} from '../../services/authen/base64Service'
import {decodeStringVol1_string ,
    decodeStringVol2_string ,
    encodeStringVol1_string ,
    encodeStringVol2_string ,
    randomString_string
} from '../../services/authen/ponEncryption'
import {setCookie , getCookie , removeCookie} from '../../services/authen/cookieService'

var jwt = require('jsonwebtoken');
const secretCode = 'fhr_QSjzhwfJiWpQRztvOJGh'

function packJWT_encode(data){
    var result = base64Encode_string(data)
    result = encodeStringVol1_string(result)
    result = encodeStringVol2_string(result)
    return result
}
function packJWT_decode(data){
    var result = decodeStringVol2_string(data)
    result = decodeStringVol1_string(result)
    result = base64Decode_string(result)
    return result
}
function jwt_encode( data ){
    var token = jwt.sign({
        emp_code: data.emp_code,
        idcode: data.idcode_employee,
        fullname_th: data.fullname_th,
        fullname_en: data.fullname_en,
        nickname_th: data.nickname_th,
        nickname_en: data.nickname_en,
        lang: data.lang,
        theme: data.theme,
        iat: Math.floor(Date.now() / 1000) - 30
       },secretCode, { expiresIn: "6h" , algorithm: 'HS384' }
    );
    return packJWT_encode(token)
}
async function jwt_verify(token){
    var resToken = packJWT_decode(token)
    var result = false
    result = await jwt.verify(resToken, secretCode, function(err, decoded) {
        if(err){
            return false
        }
        if(decoded){
            return true
        }
    })
    return result
}
function jwt_setCookie(token){
    setCookie('authen',token)
}
function delete_oldCookie(){
    removeCookie(`authen`,`/`)
}
async function jwt_decode(token){
    var decoded = await packJWT_decode(token)
    decoded = jwt.decode(decoded, {complete: true});
    console.log(decoded.header)
    console.log(decoded.payload)
}
async function jwt_decode_payload(token){
    var decoded = await packJWT_decode(token)
    var decoded = await jwt.decode(decoded, {complete: true});
    return decoded.payload
}
async function jwt_decode_payloadObj(token,obj){
    var decoded = await packJWT_decode(token)
    var decoded = await jwt.decode(token, {complete: true});
    return decoded.payload[obj]
}

async function checkCookiePack_existAndNotExpire(){
    try{
        let token = getCookie('authen')
        if(token!== null && token!==undefined){
            var result = await jwt_verify(token)
            return result
        }else{
            return false
        }
    }catch(e){
        return false
    }
}

export {
    jwt_encode ,jwt_decode ,jwt_decode_payload ,jwt_decode_payloadObj ,jwt_verify,jwt_setCookie ,checkCookiePack_existAndNotExpire ,delete_oldCookie
}