const numbStr_f = 90
const numbStr_b = 130

function decodeStringVol1_string(str){
    return str.replace("@", "=");
}
function decodeStringVol2_string(str){
    var result = str.substring(numbStr_f)
    result = result.slice(0, -numbStr_b)
    return result
}
function encodeStringVol1_string(str){
    return str.replace("=", "@");
}
function encodeStringVol2_string(str){
    return randomString_string(numbStr_f) + str + randomString_string(numbStr_b)
}
function randomString_string(length){
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

export {
    decodeStringVol1_string ,
    decodeStringVol2_string ,
    encodeStringVol1_string ,
    encodeStringVol2_string ,
    randomString_string ,
}