function base64Encode_string(str){
    return Buffer.from(str).toString('base64')
}
function base64Decode_string(str){
    return Buffer.from(str, 'base64').toString('ascii')
}

export {base64Encode_string , base64Decode_string}