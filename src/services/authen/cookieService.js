import React , { Component , useState, useEffect } from 'react';
import Cookies from 'universal-cookie';

var cookies = null;
cookies = new Cookies();
// class ServiceCookie extends Component{
//     constructor(){
//         super()
//         cookies = new Cookies();
//     }
//     setCookie(varCookie , valueCookie , setPath){ //setPath : '/'
//         cookies.set(varCookie, valueCookie, { 
//             path: setPath , 
//             expires: new Date(Date.now()+1800000)
//         });
//     }
//     getCookie(varCookie){
//         try{
//             return cookies.get(varCookie)
//         }catch(e){
//             return null
//         }
//     }
//     removeCookie(varCookie , setPath){
//         cookies.remove(varCookie, { path: setPath });
//     }
// }
// export {ServiceCookie};

function setCookie(varCookie , valueCookie , setPath){ //setPath : '/'
    cookies.set(varCookie, valueCookie, { 
        path: setPath , 
        expires: new Date(Date.now()+3600000)
    });
}
function getCookie(varCookie){
    try{
        return cookies.get(varCookie)
    }catch(e){
        return null
    }
}
function removeCookie(varCookie , setPath){
    cookies.remove(varCookie, { path: setPath });
}

export {setCookie , getCookie , removeCookie}