import React , { Component } from 'react';
import {std_header} from '../../services/std_service/header'

async function service_insert(body) {
    const url = process.env.REACT_APP_URL_API+'/data_company';
    var result = await Promise.all([ 
         fetch(url ,std_header('POST',body)) 
    ])
    .then(([x]) => Promise.all([x.json()]))
    .then(async([datax]) => {
        if(datax['success']){
            return true
        }else{
            return false
        }
    })
    return result
}

async function service_update(body) {
    const url = process.env.REACT_APP_URL_API+'/data_company';
    var result = await Promise.all([ 
         fetch(url ,std_header('PUT',body)) 
    ])
    .then(([x]) => Promise.all([x.json()]))
    .then(async([datax]) => {
        if(datax['success']){
            return true
        }else{
            return false
        }
    })
    return result
}

async function service_delete(body) {
    const url = process.env.REACT_APP_URL_API+'/data_company';
    var result = await Promise.all([ 
         fetch(url ,std_header('DELETE',body)) 
    ])
    .then(([x]) => Promise.all([x.json()]))
    .then(async([datax]) => {
        if(datax['success']){
            return true
        }else{
            return false
        }
    })
    return result
}
export {service_insert , service_update , service_delete}