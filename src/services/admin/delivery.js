import React , { Component } from 'react';
import {std_header_upload , std_header} from '../../services/std_service/header'
import {generateString_string} from '../../services/std_service/getString'
import {myFormatDate , myFormatTime} from '../../services/std_service/getDate'
import {funcClickLink} from '../../services/link/service_link'
import {ponEncodeToNode} from '../../services/encryption/ponEncode'

async function adminSendToServer_delivery(objectBody , submitLoad , idcode_unit , modeUpdate){
    var url = ``,bodyEncode = `` ,header = [] ,request = ``
    // console.log(modeCreate)
    if(!modeUpdate){
        url = `${process.env.REACT_APP_URL_API}admin/create_delivery/status`
        bodyEncode = await ponEncodeToNode(JSON.stringify([objectBody]))
        request = `POST`
        header = await std_header(request,JSON.stringify([bodyEncode]))
    }else{
        url = `${process.env.REACT_APP_URL_API}admin/update_delivery/status`
        bodyEncode = await ponEncodeToNode(JSON.stringify([{"idcode":idcode_unit},objectBody]))
        request = `POST`
        // console.log(bodyEncode)
        header = await std_header(request,JSON.stringify([bodyEncode]))
    }
    await fetch(url, header).then(async function (res) {
        submitLoad(true)
        setTimeout(async function() {
            if(res.status === 200){
                funcClickLink(`/mng/master_delivery`)
            }else{
                submitLoad(false)
            }
        }.bind(this), 700)
    })
}

async function adminSendToServer_deliveryDelete(objectBody , submitLoad){
    var url = ``,bodyEncode = `` ,header = [] ,request = ``
    url = `${process.env.REACT_APP_URL_API}admin/delete_delivery/status`
    bodyEncode = await ponEncodeToNode(JSON.stringify([objectBody]))
    request = `POST`
    header = await std_header(request,JSON.stringify([bodyEncode]))
    await fetch(url, header).then(async function (res) {
        submitLoad(true)
        funcClickLink(`/mng/master_delivery`)
    })
}

async function adminSendToServer_selectUnitDelete(arrayIdForDelete){
    var url = ``,bodyEncode = `` ,header = [] ,request = ``
    url = `${process.env.REACT_APP_URL_API}admin/del_delivery/status`
    bodyEncode = await ponEncodeToNode(JSON.stringify([arrayIdForDelete]))
    request = `POST`
    header = await std_header(request,JSON.stringify([bodyEncode]))
    await fetch(url, header).then(async function (res) {
        // submitLoad(true)
        setTimeout(async function() {
            if(res.status === 200){
                // funcClickLink(`/mng/master_unit`)
            }else{
                // submitLoad(false)
            }
        }.bind(this), 2000)
    })
}

async function adminSendToServer_selectUnitAdd(objectArrayData){
    var url = ``,bodyEncode = `` ,header = [] ,request = ``
    url = `${process.env.REACT_APP_URL_API}admin/add_delivery/status`
    bodyEncode = await ponEncodeToNode(JSON.stringify([objectArrayData]))
    request = `POST`
    header = await std_header(request,JSON.stringify([bodyEncode]))
    await fetch(url, header).then(async function (res) {
        // submitLoad(true)
        setTimeout(async function() {
            if(res.status === 200){
                // funcClickLink(`/mng/master_unit`)
            }else{
                // submitLoad(false)
            }
        }.bind(this), 2000)
    })
}

export {
    adminSendToServer_delivery ,adminSendToServer_deliveryDelete ,
    adminSendToServer_selectUnitDelete ,adminSendToServer_selectUnitAdd
}