import React , { Component } from 'react';
import {std_header_upload , std_header} from '../../services/std_service/header'
import {generateString_string} from '../../services/std_service/getString'
import {myFormatDate , myFormatTime} from '../../services/std_service/getDate'
import {funcClickLink} from '../../services/link/service_link'
import {ponEncodeToNode} from '../../services/encryption/ponEncode'

async function adminSendToServer_promotion(objectBody , submitLoad , idcode_unit , modeUpdate){
    var url = ``,bodyEncode = `` ,header = [] ,request = ``
    // console.log(modeCreate)
    if(!modeUpdate){
        url = `${process.env.REACT_APP_URL_API}admin/create_promotion/status`
        bodyEncode = await ponEncodeToNode(JSON.stringify([objectBody]))
        request = `POST`
        header = await std_header(request,JSON.stringify([bodyEncode]))
    }else{
        url = `${process.env.REACT_APP_URL_API}admin/update_promotion/status`
        bodyEncode = await ponEncodeToNode(JSON.stringify([{"idcode":idcode_unit},objectBody]))
        request = `POST`
        // console.log(bodyEncode)
        header = await std_header(request,JSON.stringify([bodyEncode]))
    }
    await fetch(url, header).then(async function (res) {
        submitLoad(true)
        setTimeout(async function() {
            if(res.status === 200){
                funcClickLink(`/mng/master_promotion`)
            }else{
                submitLoad(false)
            }
        }.bind(this), 700)
    })
}

async function adminSendToServer_promotionDelete(objectBody , submitLoad){
    var url = ``,bodyEncode = `` ,header = [] ,request = ``
    url = `${process.env.REACT_APP_URL_API}admin/delete_promotion/status`
    bodyEncode = await ponEncodeToNode(JSON.stringify([objectBody]))
    request = `POST`
    header = await std_header(request,JSON.stringify([bodyEncode]))
    await fetch(url, header).then(async function (res) {
        submitLoad(true)
        funcClickLink(`/mng/master_promotion`)
    })
}

export {
    adminSendToServer_promotion ,adminSendToServer_promotionDelete
}