import React , { Component } from 'react';
import {std_header_upload , std_header} from '../../services/std_service/header'
import {generateString_string} from '../../services/std_service/getString'
import {myFormatDate , myFormatTime} from '../../services/std_service/getDate'
import {funcClickLink} from '../../services/link/service_link'
import {ponEncodeToNode} from '../../services/encryption/ponEncode'

async function adminSendToServer_state(objectBody , submitLoad , idcode , modeCreate){
    var url = ``,bodyEncode = `` ,header = [] ,request = ``
    if(modeCreate){
        url = `${process.env.REACT_APP_URL_API}action/state`
        bodyEncode = await JSON.stringify([objectBody])
        request = `POST`
        header = await std_header(request,bodyEncode)
    }else{
        url = `${process.env.REACT_APP_URL_API}action/state`
        bodyEncode = await JSON.stringify([{"state_idcode":idcode},objectBody])
        request = `PUT`
        header = await std_header(request,bodyEncode)
    }
    await fetch(url, header).then(async function (res) {
        submitLoad(true)
        setTimeout(async function() {
            console.log(res)
            if(res.status === 200){
                funcClickLink(`/manage_state`)
            }else{
                submitLoad(false)
            }
        }.bind(this), 2000)
    })
}

async function adminSendToServer_stateDelete(objectBody , submitLoad){
    var url = ``,bodyEncode = `` ,header = [] ,request = ``
    url = `${process.env.REACT_APP_URL_API}action/state`
    bodyEncode = await JSON.stringify(objectBody)
    request = `DELETE`
    header = await std_header(request,bodyEncode)
    await fetch(url, header).then(async function (res) {
        submitLoad(true)
        funcClickLink(`/manage_state`)
    })
}

export {
    adminSendToServer_state ,adminSendToServer_stateDelete
}