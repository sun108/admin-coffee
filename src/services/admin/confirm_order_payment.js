import React , { Component } from 'react';
import {std_header_upload , std_header} from '../std_service/header'
import {generateString_string} from '../std_service/getString'
import {myFormatDate , myFormatTime} from '../std_service/getDate'
import {funcClickLink} from '../link/service_link'
import {ponEncodeToNode} from '../encryption/ponEncode'

async function adminSendToServer_confirmOrderPayment(orderIdcode ,transactionId){
    var url = ``,bodyEncode = `` ,header = [] ,request = `` ,result = false
    url = `${process.env.REACT_APP_URL_SERVER_API}linepay/confirm_payment_transection`
    bodyEncode = await JSON.stringify({'orderIdcode':orderIdcode , 'transactionId':transactionId})
    request = `POST`
    header = await std_header(request,bodyEncode)

    var fData = new Promise((resolve, reject) => { 
        setTimeout(() => resolve(
            fetch(url, header) 
        ), 1500)
    }); 

    await Promise.all([ fData.catch(error => { return error }) ])
    .then(([data_title]) => Promise.all([ data_title.json() ]))
    .then(async([datax]) => {
        if(datax['connect_code'] === 200 ){
            //&& datax[`data`].returnCode === '0000'
            result = datax[`data`]
        }else{
            console.log(`error`)
        }
    });

    return result
}

export {
    adminSendToServer_confirmOrderPayment
}