import React , { Component } from 'react';
import {std_header_upload , std_header} from '../../services/std_service/header'
import {generateString_string} from '../../services/std_service/getString'
import {myFormatDate , myFormatTime} from '../../services/std_service/getDate'
import {funcClickLink} from '../../services/link/service_link'
import {ponEncodeToNode} from '../../services/encryption/ponEncode'

async function adminSendToServer_tagDelete(arrayIdForDelete){
    var url = ``,bodyEncode = `` ,header = [] ,request = ``
    url = `${process.env.REACT_APP_URL_API}admin/del_tag/status`
    bodyEncode = await ponEncodeToNode(JSON.stringify([arrayIdForDelete]))
    request = `POST`
    header = await std_header(request,JSON.stringify([bodyEncode]))
    await fetch(url, header).then(async function (res) {
        // submitLoad(true)
        setTimeout(async function() {
            if(res.status === 200){
                // funcClickLink(`/mng/master_unit`)
            }else{
                // submitLoad(false)
            }
        }.bind(this), 2000)
    })
}

async function adminSendToServer_tagAddition(objectArrayData){
    var url = ``,bodyEncode = `` ,header = [] ,request = ``
    url = `${process.env.REACT_APP_URL_API}admin/add_tag/status`
    bodyEncode = await ponEncodeToNode(JSON.stringify([objectArrayData]))
    request = `POST`
    header = await std_header(request,JSON.stringify([bodyEncode]))
    await fetch(url, header).then(async function (res) {
        // submitLoad(true)
        setTimeout(async function() {
            if(res.status === 200){
                // funcClickLink(`/mng/master_unit`)
            }else{
                // submitLoad(false)
            }
        }.bind(this), 2000)
    })
}

export {adminSendToServer_tagDelete , adminSendToServer_tagAddition}