import React , { Component } from 'react';
import {std_header_upload , std_header} from '../../services/std_service/header'
import {generateString_string} from '../../services/std_service/getString'
import {myFormatDate , myFormatTime} from '../../services/std_service/getDate'
import {funcClickLink} from '../../services/link/service_link'
import {ponEncodeToNode} from '../../services/encryption/ponEncode'

async function adminSendToServer_sale(objectBody , submitLoad){
    var url = ``,bodyEncode = `` ,header = [] ,request = ``
    url = `${process.env.REACT_APP_URL_API}sale/product`
    bodyEncode = await JSON.stringify(objectBody)
    request = `POST`
    header = await std_header(request,bodyEncode)
    submitLoad(true)

    var fData = new Promise((resolve, reject) => { 
        setTimeout(() => resolve(
            fetch(url, header) 
        ), 1500)
    }); 

    await Promise.all([ fData.catch(error => { return error }) ])
    .then(([data_title]) => Promise.all([ data_title.json() ]))
    .then(async([datax]) => {
        if(datax['connect_code'] === 200 && datax[`data`].returnCode === '0000'){
            console.log(datax)
            var urlDirect = datax[`data`].info.paymentUrl.web
            window.location.replace(urlDirect);
        }else{
            submitLoad(false)
        }
    });
}

export {
    adminSendToServer_sale
}