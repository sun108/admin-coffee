import React , { Component } from 'react';
import {std_header_upload , std_header} from '../../services/std_service/header'
import {generateString_string} from '../../services/std_service/getString'
import {myFormatDate , myFormatTime} from '../../services/std_service/getDate'
import {funcClickLink} from '../../services/link/service_link'
import {ponEncodeToNode} from '../../services/encryption/ponEncode'

async function adminSendToServer(objectBody , submitLoad , orderIdcode){
    var url = `${process.env.REACT_APP_URL_API}admin/set/status`
    var bodyEncode = await ponEncodeToNode(JSON.stringify([objectBody]))
    var header = std_header('POST',JSON.stringify([bodyEncode]))
    fetch(url, header).then(async function (res) {
        submitLoad(true)
        setTimeout(async function() {
            if(res.status === 200){
                funcClickLink(`/mng`)
            }else{
                submitLoad(false)
            }
        }.bind(this), 2000)
    })
}

export {adminSendToServer}