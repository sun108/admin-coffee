import {std_header_upload} from '../../services/std_service/header'

function checkUploadPicture(formDataImg,idcode){
    if(formDataImg !== null){
        formDataImg.append('filename',idcode);
        await fetch(`${process.env.REACT_APP_URL_API}upload/picture`, std_header_upload('POST',formDataImg))
        .then(function (res) {
            if (res.ok) {
                //
            }
        })
    }
    return true
}

export{
    checkUploadPicture
}