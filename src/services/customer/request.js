function req_body(name , tel , email ,line, address , paymentType,attach){
    return (
        {
          "orderid":"20210124-0001",
          "onDate":"2021-01-24 13:38",
          "cust_name":name,
          "cust_tel":tel,
          "cust_email":email,
          "cust_line":line,
          "cust_sendToAddress":address,
          "payment_idcode":paymentType,
          "payment_wording":"โอนเงิน",
          "payment_attact":attach,
          "order_list":[
              {
                  "idcode":"xxxx_001",
                  "product":"สินค้าที่ 1",
                  "price":500,
                  "amount":5
              },
              {
                  "idcode":"xxxx_002",
                  "product":"สินค้าที่ 2",
                  "price":800,
                  "amount":2
              }
          ]
      }
    )
}

export {
    req_body
}