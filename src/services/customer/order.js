import React , { Component } from 'react';
import {std_header_upload , std_header} from '../../services/std_service/header'
import {generateString_string} from '../../services/std_service/getString'
import {myFormatDate , myFormatTime} from '../../services/std_service/getDate'
import {funcClickLink} from '../../services/link/service_link'
import {ponEncodeToNode} from '../../services/encryption/ponEncode'
import {customer_upload_slip} from '../../services/upload_picture/upload_picture'

async function customer_start_order(objectData , submitLoad , funcClearItem){
    var idcode_img = generateString_string(90)
    var id = generateString_string(16)
    var uploadImg = false;//cartList

    if(objectData.select_payment === `tranfer`){
        if(objectData.fileImage !== null){
            await customer_upload_slip(objectData.fileImage , idcode_img);
            uploadImg = true;
        }
    }
    
    await sendToLineNotification(objectData , id , idcode_img , uploadImg , submitLoad , funcClearItem)
}

async function sendToLineNotification(objectData , id , idcodeImg , uploadImg , submitLoad , funcClearItem){
    let date = myFormatDate() ; let time = myFormatTime()
    var urlImg = `https://storage.bigidea.co.th/shop_bigidea/slip/${idcodeImg}.jpg`
    var body ={
        "orderid":id,
        "onDate":`${date} ${time}`,
        "dateWithZone":new Date().toString(),
        "datePure":new Date(),
        "cust_name":objectData.name,
        "cust_tel":objectData.tel,
        "cust_email":objectData.email,
        "cust_line":objectData.line,
        "cust_sendToAddress":objectData.address,
        "payment_idcode":objectData.select_payment,
        "payment_wording":"",
        "payment_attact":uploadImg ? urlImg : "",
        "order_list":objectData.cartList,
        "shipping_list":objectData.shippingList,
        "giftFree_list":objectData.gistFreeList,
        "discount_list":objectData.discountList,
        "total_price":objectData.total_price,
        "ip":objectData.ip,
        "browser":objectData.browser,
        "colo":objectData.colo
    }
    // var url = `${process.env.REACT_APP_URL_API}line/notify/customer_order`
    // var url = `${process.env.REACT_APP_URL_API}action/customer_order`
    var url = `${process.env.REACT_APP_URL_API}customer/add/order`
    var bodyEncode = await ponEncodeToNode(JSON.stringify([body]))
    var header = std_header('POST',JSON.stringify([bodyEncode]))
    fetch(url, header).then(async function (res) {
        submitLoad(true)
        setTimeout(async function() {
            if(res.status === 200){
                await funcClearItem()
                funcClickLink(`/thankyou/${id}`)
            }else{
                submitLoad(false)
            }
        }.bind(this), 2000)
    })
}

export {customer_start_order}