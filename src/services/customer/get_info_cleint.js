import React , { Component } from 'react';
import FetchData from '../fetch_data/fetch'
import {getConsoleError} from '../std_service/getString'
// import { usePosition } from 'use-position';

async function get_ip_client(){
    var url =null ; var data= []
    url = `https://www.cloudflare.com/cdn-cgi/trace`
    data = await fetch(url ,{method: `GET`,mode: 'cors'}) 
    let text = await data.text();
    let t2 = text.split("\n")
    let ipaddress = t2.filter(el => el.startsWith("ip")).join('\n').replace(`ip=`,``)
    let browser = t2.filter(el => el.startsWith("uag")).join('\n').replace(`uag=`,``)
    let colo = t2.filter(el => el.startsWith("colo")).join('\n').replace(`colo=`,``)
    // console.log(ipaddress)
    // console.log(browser)
    return [ipaddress,browser,colo]
}

async function get_position_client(){
    // const watch = true;
    // const {
    //     latitude,
    //     longitude,
    //     speed,
    //     timestamp,
    //     accuracy,
    //     error,
    // } = usePosition(watch);
    // console.log(latitude)
}

async function get_location_client(){
    // var ipLocation = require('ip-location')
    // ipLocation('google.co.th', function (err, data) {
    //     console.log(data)
    // })
}

export {
    get_ip_client , get_position_client , get_location_client
}