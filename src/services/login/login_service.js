import React , { Component } from 'react';
import {std_header,std_header_fhr} from '../../services/std_service/header'

async function service_login(body) {
    const url = `${process.env.REACT_APP_URL_API_FHR}login/login`
    var result = await Promise.all([ 
         fetch(url ,std_header_fhr('POST',body)) 
    ])
    .then(([x]) => Promise.all([x.json()]))
    .then(async([datax]) => {
        // console.log(datax)
        if(datax['success']){
            // console.log(datax['data'])
            try{
                var result = datax['data'].slice().reverse().find(x=>x['login']).login ,
                access = false , default_password = false
                try{ access = result.find(x=>x['access']).access }catch(e){}
                try{ default_password = result.find(x=>x['default_password']).default_password }catch(e){}
                return [access , default_password]
            }catch(e){
                return [false,false]
            }
        }else{
            return false
        }
    })
    return result
}
async function service_changePassword(body) {
    const url = `${process.env.REACT_APP_URL_API_FHR}login/changePassword`
    var result = await Promise.all([ 
         fetch(url ,std_header_fhr('POST',body)) 
    ])
    .then(([x]) => Promise.all([x.json()]))
    .then(async([datax]) => {
        if(datax['success']){
            console.log(datax['data'])
            try{
                var result = datax['data'].slice().reverse().find(x=>x['login']).login ,
                changePassword_success = false ,access = false
                try{ access = result.find(x=>x['access']).access }catch(e){}
                try{ changePassword_success = result.find(x=>x['changePassword_success']).changePassword_success }catch(e){}
                return [access,changePassword_success]
            }catch(e){
                return [false,false]
            }
        }else{
            return false
        }
    })
    return result
}
export {service_login,service_changePassword}