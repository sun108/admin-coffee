import React , { Component } from 'react';
import {std_header_upload , std_header} from '../std_service/header'
import {generateString_string} from '../std_service/getString'
import {myFormatDate , myFormatTime} from '../std_service/getDate'
import {funcClickLink} from '../link/service_link'
import {ponEncodeToNode} from '../encryption/ponEncode'

async function linePay_sendRequest(objectBody , submitLoad){
    var url = ``,bodyEncode = `` ,header = [] ,request = ``
    url = `${process.env.REACT_APP_URL_API}sale/product`
    bodyEncode = await JSON.stringify(objectBody)
    request = `POST`
    header = await std_header(request,bodyEncode)
    await fetch(url, header).then(async function (res) {
        submitLoad(true)
        setTimeout(async function() {
            if(res.status === 200){
                submitLoad(false)
            }else{
                submitLoad(false)
            }
        }.bind(this), 2000)
    })
}

export {
    adminSendToServer_sale
}