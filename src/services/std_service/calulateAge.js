const dateFormat = require('dateformat');

function calculateAge(birthDate) { // birthday is a date
    birthDate = new Date(birthDate);
    var otherDate = new Date(dateFormat(new Date(), "isoDate"));

    var years = (otherDate.getFullYear() - birthDate.getFullYear());

    if (otherDate.getMonth() < birthDate.getMonth() || 
        otherDate.getMonth() == birthDate.getMonth() && otherDate.getDate() < birthDate.getDate()) {
        years--;
    }

    return years;
}

export {calculateAge}