import React , { Component } from 'react';

function std_header(method,body) {
    return(
        {
            method: method,
            mode: 'cors',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json;charset=UTF-8',
                'SHOP-API':'eVUqAzgr7gwHZn6Ug8sIjbwzRghIxZme'
            },
            body: body
        }
    )
}

function std_header_fhr(method,body) {
    return(
        {
            method: method,
            mode: 'cors',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json;charset=UTF-8',
                'X-API':'emkIkGDHGqHD4xP9qXwyfhPQHBFDL7yQ'
            },
            body: body
        }
    )
}

function std_header_upload(method,body){
    return(
        {
            method: method,
            mode: 'cors',
            headers: {
                'SHOP-API':'eVUqAzgr7gwHZn6Ug8sIjbwzRghIxZme'
            },
            body: body
        }
    )
}

export {std_header , std_header_upload ,std_header_fhr} 