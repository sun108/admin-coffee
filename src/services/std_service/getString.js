import React , { Component , useState, useEffect } from 'react';
import { browserHistory } from 'react-router';
import {StdToastMz_error,StdToastMz_success,StdToastMz_info,StdToastMz_warning} from '../../component/toast/std_toast.js'


function getStringTitle(dataState , objFind , lang){
    return dataState.find(x => x.idcode === objFind)[lang]
}
function get_comLang(lang){
    switch(lang){
        case 'th':
            return 'name_th';
        break;
        case 'en':
            return 'name_en'
        break;
    }
}
function checkIsHasSpace_bool(str){
    return /\s/g.test(str);
}
function checkString_EnglishAndNumberOnly_bool(str){
    var index;
    var ENGLISH = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    for (index = str.length - 1; index >= 0; --index) {
        if (ENGLISH.indexOf(str.substring(index, index + 1)) < 0) {
            return true;
        }
    }
    return false;
}
function checkStringInNotNullOrEmpty_bool(str){
    try{
        if(str.trim().length <= 0 ){
            return false
        }
        return true
    }catch(e){
        return false
    }
}
function checkArrayIsNotNullOrUndefined(data){
    if(data !== undefined && data !== []){
        return true
    }
    return false
}
function generateString_string(length){
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
function getConsoleError(e){
    console.log(e)
    // browserHistory.push('/login');
}
function stringNull(){
    return '-'
}
function route_isNotAuthen(){
    return '/login'
}
function checkChangePassword(globeStr , lang , oldPass , newPass , newPassAgain){
    var result = true
    if(!checkStringInNotNullOrEmpty_bool(oldPass) || !checkStringInNotNullOrEmpty_bool(newPass) || !checkStringInNotNullOrEmpty_bool(newPassAgain)){
        result = false
        StdToastMz_error(
            getStringTitle(globeStr,'fail_passwordIsNull',lang)
        )
    }
    if(newPass === oldPass){
        result = false
        StdToastMz_error(
            getStringTitle(globeStr,'fail_yourPasswordCannotReplace',lang)
        )
    }
    if(newPass !== newPassAgain){
        result = false
        StdToastMz_error(
            getStringTitle(globeStr,'fail_passwordNotMath',lang)
        )
    }
    if(!checkPasswordIsStrong(newPass)){
        result = false
        StdToastMz_error(
            getStringTitle(globeStr,'fail_yourPasswordNotStrong',lang)
        )
    }
    return result
}
function checkPasswordIsStrong(newPass){
    if(newPass.length < 8){
        return false
    }
    return true
}
function convertIntToMoneyFormat(int){
    return int.toLocaleString("th-TH", {style: "decimal", minimumFractionDigits: 0})
}

export {
    checkChangePassword ,
    route_isNotAuthen,
    checkArrayIsNotNullOrUndefined,
    stringNull,
    get_comLang , 
    getStringTitle , 
    checkIsHasSpace_bool , 
    checkString_EnglishAndNumberOnly_bool , 
    checkStringInNotNullOrEmpty_bool , 
    generateString_string ,
    getConsoleError ,
    convertIntToMoneyFormat
};