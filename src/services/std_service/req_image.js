function std_reqImgPerson_req(picture){
    try{
        return require(`../../storage/person_image/${picture}.jpg`);
    }catch(e){
        return require(`../../assets/profile_picture/who.jpg`)
    }
}
function std_reqImgฺBase64_req(picture){
    return picture
}
function std_getPathImage(picture){
    return `../../storage/person_image/${picture}.jpg`
}
function std_reqPicturePublic(picture){
    return (`/images/${picture}.jpg`)
}

export {std_reqImgPerson_req , std_getPathImage , std_reqPicturePublic}