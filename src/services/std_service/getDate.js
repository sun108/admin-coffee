import moment from 'moment';
var now = new Date();

function myFormatDate(){
    return moment().format('YYYY-MM-DD');
}

function mySubtractFormatDate(day){
    return moment().subtract(day,'d').format('YYYY-MM-DD');
}

function myAddFormatDate(day){
    return moment().add(day,'d').format('YYYY-MM-DD');
}

function myFormatTime(){
    return moment().format('HH:mm');
}

export {
    myFormatDate , myFormatTime , mySubtractFormatDate ,myAddFormatDate
}