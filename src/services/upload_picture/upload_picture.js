import {std_header_upload , std_header} from '../std_service/header'
import {convertEncode} from '../encryption/ponEncode'

async function customer_upload_slip(fileImg , idcode){
    if(fileImg !== null){
        const formData = new FormData();
        formData.append('attachFile',fileImg);
        formData.append('filename',idcode);
        var url = `${process.env.REACT_APP_URL_API}upload/slip`
        var header = std_header_upload('POST',formData)
        await fetch(url, header)
        .then(function (res) {
            if (res.ok) {
                
            }
        })
    }
    return true
}

async function admin_upload_unit_img(fileImg , idcode, idcode_unit){
    var result = false
    if(fileImg !== null){
        const formData = new FormData();
        formData.append('attachFile',fileImg);
        formData.append('filename',idcode);
        formData.append('idcodeunit',idcode_unit);
        var url = `${process.env.REACT_APP_URL_API}upload/unitimg`//unitimg //unitimgmultiple
        var header = std_header_upload('POST',formData)
        result = await Promise.all([ fetch(url, header) ]).then(([x]) => Promise.all([x.json()]))
        .then(async([datav]) => {
            var datab = await convertEncode(datav)
            var datax = datab.payload
            if(datax['success']){
                return [true,datax.data.img_name]
            }else{
                return [false,null]
            }
        })
    }
    
    return {"res":result[0] , "img":result[1]}
}

async function admin_upload_unit_img_multiple(files){
    var result = false
    var formData = new FormData();
    for(var i = 0; i < files.length; i++){
        formData.append('attachFile', files[i]);
    }
    var url = `${process.env.REACT_APP_URL_API}upload/unitimgmultiple`//unitimg //unitimgmultiple
    var header = std_header_upload('POST',formData)
    result = await Promise.all([ fetch(url, header) ]).then(([x]) => Promise.all([x.json()]))
    .then(async([datav]) => {
        var datab = await convertEncode(datav)
        var datax = datab.payload
        if(datax['success']){
            return [true,datax.data.img_name]
        }else{
            return [false,null]
        }
    })
    
    return {"res":result[0] , "img":result[1]}
}

export {
    customer_upload_slip ,
    admin_upload_unit_img ,
    admin_upload_unit_img_multiple
}