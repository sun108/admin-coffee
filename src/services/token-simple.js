const jwt = require('jwt-simple')
const config = require('../config')

function tokenForUser(user) {    
    const timestamp = new Date().getTime();    
    return jwt.encode(        
        {            
            sub: user.id,            
            user_type: user.user_type,            
            name: user.name,            
            username: user.username,            
            iat: timestamp        
        },        
        config.secret    
    )
}

exports.signin = (req, res, next) => {    
    res.send({ token: tokenForUser(req.user) })
}