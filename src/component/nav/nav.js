import React, { Component } from 'react';

import List_menu_nav from './list_menu_nav'
import List_menu_sidenav from './list_menu_sidenav'
import Logo from './logo'
import Profile from './profile'
import './css/nav.css'

class Navbar extends Component{
    componentDidMount(){
        const M = window.M;
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.sidenav');
            var instances = M.Sidenav.init(elems, {});
        });
    }
    render(){
        return(
        <div>   
            <nav class="nmain z-depth-4">
                <a href="#" data-target="slide-out" class="sidenav-trigger">
                    <i class="material-icons">menu</i>
                </a>
                <div className="nav-wrapper">
                    <Logo />
                    <ul id="nav-mobile" className="right hide-on-med-and-down">
                        <List_menu_nav />
                    </ul>
                </div>
            </nav>
            <ul id='slide-out' className='sidenav'>
                <li>
                    <Profile />
                </li>
                <List_menu_sidenav />
            </ul>
        </div>
        )
    }
}
export default Navbar