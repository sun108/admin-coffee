import React  from 'react';

const List_menu_sidenav =(props)=>{
    return(
        <div>
            <li><a href='#!'><i className='material-icons'>cloud</i>First Link With Icon</a></li>
            <li><a href='#!'>Second Link</a></li>
            <li><div className='divider'></div></li>
            <li><a className='subheader'>Subheader</a></li>
            <li><a className='waves-effect' href='#!'>Third Link With Waves</a></li>
        </div>
    );
}
export default List_menu_sidenav