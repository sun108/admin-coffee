import React  from 'react';

const Nav_profile =(props)=>{
    return(
    <div className='user-view'>
        <div className='background'>
            <img src='' />
        </div>
        <a href='#user'><img className='circle' src='' /></a>
        <a href='#name'><span className='white-text name'>John Doe</span></a>
        <a href='#email'><span className='white-text email'>jdandturk@gmail.com</span></a>
    </div>
    );
}
export default Nav_profile