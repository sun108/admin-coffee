import React, { Component }  from 'react';
import { connect } from 'react-redux'

class List_menu_nav extends Component{
    
    render(){
        return(
            <div>
                <li>
                    <a className='testBtn' onClick={ ()=>this.props.SassClick() }>Sass</a>
                </li>
                <li><a href="badges.html">Components</a></li>
                <li><a href="collapsible.html">JavaScript</a></li>
            </div>
        );
    }
}

const mapDisToProps=(dispatch)=>{
    return{
        SassClick:()=>{
            dispatch({type:'ALERT'})
        }
    }
}
export default connect(null,mapDisToProps)(List_menu_nav)