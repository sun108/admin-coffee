import React  from 'react';
import './css/logo.css'

const Nav_logo =(props)=>{
    //onClick={this.props.sassBtn}
    //{props.nav_btn_home}
    const pic_logo = "https://bigidea.co.th/public/resource/img/logo_bigidea_01.png"
    return(
        <a href="#" class="brand-logo">
            <img id="img_nav_logo" class="responsive-img" src={pic_logo} />
        </a>
    );
}
export default Nav_logo