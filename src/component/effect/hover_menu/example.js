import React , { Component , useState, useEffect } from 'react';
import './hover.scss';

function example_hover(){
    return(<div>
        <h1>Link Hover Effects</h1>
        <h4><a class="effect-1" href="">Link Hover Effect 1</a></h4>
        <h4><a class="effect-2" href="">Link Hover Effect 2</a></h4>
        <h4><a class="effect-3" href="">Link Hover Effect 3</a></h4>
        <h4><a class="effect-4" href="">Link Hover Effect 4</a></h4>
        <h4><a class="effect-5" href=""><span>Link Hover Effect 5</span></a></h4>
        <h4><a class="effect-6" href=""><span>Link Hover Effect 6</span></a></h4>
        <h4><a class="effect-7" href="" data-content="Link Hover Effect 7"><span>Link Hover Effect 7</span></a></h4>
        <h4><a class="effect-8" href="" data-content="Link Hover Effect 8"><span>Link Hover Effect 8</span></a></h4>    
        </div>
    )
}

export default example_hover