import React from "react";
import OrganizationChart from "@dabeng/react-orgchart";

const styleCode =(".orgchart { background: #fff; } \
    .orgchart td.left, .orgchart td.right, .orgchart td.top { border-color: #aaa; } \
    .orgchart td>.down { background-color: #aaa; } \
    .orgchart .middle-level .title { background-color: #006699; } \
    .orgchart .middle-level .content { border-color: #006699; } \
    .orgchart .product-dept .title { background-color: #009933; } \
    .orgchart .product-dept .content { border-color: #009933; } \
    .orgchart .rd-dept .title { background-color: #993366; } \
    .orgchart .rd-dept .content { border-color: #993366; } \
    .orgchart .pipeline1 .title { background-color: #996633; } \
    .orgchart .pipeline1 .content { border-color: #996633; } \
    .orgchart .frontend1 .title { background-color: #cc0066; } \
    .orgchart .frontend1 .content { border-color: #cc0066; } "
)

const DefaultChart = () => {
  const ds = {
    id: "n1",
    name: "Lao Lao",
    title: "general manager",
    children: [
      { id: "n2", name: "Bo Miao", title: "department manager" },
      {
        id: "n3",
        name: "Su Miao",
        title: "department manager",
        children: [
          { id: "n4", name: "Tie Hua", title: "senior engineer" },
          {
            id: "n5",
            name: "Hei Hei",
            title: "senior engineer",
            children: [
              { id: "n6", name: "Dan Dan", title: "engineer" },
              { id: "n7", name: "Xiang Xiang", title: "engineer" }
            ]
          },
          { id: "n8", name: "Pang Pang", title: "senior engineer" }
        ]
      },
      { id: "n9", name: "Hong Miao", title: "department manager" },
      {
        id: "n10",
        name: "Chun Miao",
        title: "department manager",
        children: [{ id: "n11", name: "Yue Yue", title: "senior engineer" }]
      }
    ]
  };

    return (
        <div>
        <style>{ styleCode }</style>
            <OrganizationChart 
              datasource={ds} 
              multipleSelect={true}
            />
        </div>
    )
};

export default DefaultChart;