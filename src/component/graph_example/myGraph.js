import React from "react";
import OrganizationChart from "@dabeng/react-orgchart";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import { Button, Card, Row, Col ,Chip , Icon} from 'react-materialize';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Hidden from '@material-ui/core/Hidden';
import LinearProgress from '@material-ui/core/LinearProgress';

const styleCode =(".orgchart { background: #fff; } \
    .orgchart td.left, .orgchart td.right, .orgchart td.top { border-color: #aaa; } \
    .orgchart td>.down { background-color: #aaa; } \
    .orgchart .middle-level .title { background-color: #006699; } \
    .orgchart .middle-level .content { border-color: #006699; } \
    .orgchart .product-dept .title { background-color: #009933; } \
    .orgchart .product-dept .content { border-color: #009933; } \
    .orgchart .rd-dept .title { background-color: #993366; } \
    .orgchart .rd-dept .content { border-color: #993366; } \
    .orgchart .pipeline1 .title { background-color: #996633; } \
    .orgchart .pipeline1 .content { border-color: #996633; } \
    .orgchart .frontend1 .title { background-color: #cc0066; } \
    .orgchart .frontend1 .content { border-color: #cc0066; } "
)

const DefaultChart = () => {
  const ds = {
    id: "n1",
    name: "Lao Lao",
    title: "general manager",
    children: [
      { id: "n2", name: "Bo Miao", title: "department manager" },
      { id: "n3", name: "Su Miao", title: "department manager",
        children: [
          { id: "n4", name: "Tie Hua", title: "senior engineer" },
          { id: "n5", name: "Hei Hei", title: "senior engineer",
            children: [
              { id: "n6", name: "Dan Dan", title: "engineer" },
              { id: "n7", name: "Xiang Xiang", title: "engineer" }
            ]
          },
          { id: "n8", name: "Pang Pang", title: "senior engineer" }
        ]
      },
      { id: "n9", name: "Hong Miao", title: "department manager" },
      { id: "n10", name: "Chun Miao", title: "department manager",
        children: [
            { id: "n11", name: "Yue Yue", title: "senior engineer" }
        ]
      }
    ]
  };
    const chip = (
        <Chip
            close={false}
            closeIcon={<Icon className="close">close</Icon>}
            options={null}
        >
            <img alt="Contact Person" className="responsive-img"
                src="https://materializecss.com/images/yuna.jpg"
            />
            นาย ทดสอบ ทดสอบ<br/>
            sr. manager IT department
        </Chip>
    )
    const chip_director = (
    <Chip close={false} >
        <img alt="Contact Person" className="responsive-img"
            src="https://materializecss.com/images/yuna.jpg"
        />
        นาย ทดสอบ ทดสอบ<br/>
        sr. director IT department
    </Chip>
    )

    return (
        <div>
            <hr />
            <Table aria-label="simple table">
                <TableBody>
                    <TableCell align="center" colspan="100%">
                        {chip}
                    </TableCell>
                </TableBody>
                <TableBody>
                    <TableCell align="center" colspan="100%">
                        {chip}
                    </TableCell>
                </TableBody>
                <TableBody>
                    <TableRow>
                        {/* <TableCell align="center">management</TableCell>
                        <TableCell align="center">production factory</TableCell>
                        <TableCell align="center">human resource</TableCell>
                        <TableCell align="center">accounting</TableCell>
                        <TableCell align="center">product development</TableCell>
                        <TableCell align="center">puchese</TableCell> */}
                        <TableCell align="center"><Button>information technology</Button></TableCell>
                        <TableCell align="center"><Button>ceative design</Button></TableCell>
                        <TableCell align="center"><Button>marketing</Button></TableCell>
                        <TableCell align="center"><Button>sale</Button></TableCell>
                        {/* <TableCell align="center">Store</TableCell>
                        <TableCell align="center">transport</TableCell>
                        <TableCell align="center">Maintenance</TableCell>
                        <TableCell align="center">marketing</TableCell>
                        <TableCell align="center">marketing</TableCell> */}
                    </TableRow>
                    <TableRow>
                        <TableCell align="center">
                            {chip}
                        </TableCell>
                        <TableCell align="center">
                            {chip}
                        </TableCell>
                        <TableCell align="center">
                            {chip}
                        </TableCell>
                        <TableCell align="center">
                            {chip}
                        </TableCell>
                    </TableRow>
                </TableBody>
            </Table>
            <Table aria-label="simple table">
                
            </Table>
        </div>
    )
};

export default DefaultChart;