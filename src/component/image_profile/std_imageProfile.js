import React , { Component } from 'react';
import ExampleComponent from "react-rounded-image";
import { Button, Card, Row, Col ,Chip , Icon} from 'react-materialize';
import Avatar from '@material-ui/core/Avatar';
// import img_person from '../../storage/person_image/11562.jpg'
import {std_reqPicturePublic} from '../../services/std_service/req_image'


function StdImg_avatar(props){
    return (
        <div>
            <img src={`/images/person_image/${props.image}.jpg`} />{/*../../storage/person_image/11562.jpg */}
        </div>
    )
}
function StdImg_stdPictureUpload(props){
    return ( <div> <img src={std_reqPicturePublic(props.image)} style={{width: '200px' , height: '200px'}} /> </div> )
}
function StdImg_stdPictureBase64(props){
    return ( <div> <img src={props.image} style={{width: '200px' , height: '200px'}} /> </div> )
}
function StdImg_profileImage(props){
    return (
        <div>
        <ExampleComponent 
            image={props.image}
            className={props.className} 
            imageWidth="150" 
            imageHeight="150" 
            roundedColor="#321124" 
            roundedSize="13" style={props.style} 
        />
        </div>
    )
}
function StdImg_profileImage100Percent(props){
    return (
        <div>
        <ExampleComponent 
            image={props.image} 
            className={props.className} 
            roundedColor="#321124" 
            roundedSize="13" style={{width: '100%'}} 
        />
        </div>
    )
}
function StdImg_personImgTable(props){
    return (
        <div style={{display: "inline-block"}}>
        <ExampleComponent 
            image={`/images/${props.image}.jpg`} 
            className={props.className} 
            imageWidth="50" 
            imageHeight="50" 
            roundedColor="#321124" 
            roundedSize="2" style={props.style} 
        />
        </div>
    )
}
function StdImg_personImg100(props){
    return (
        <div style={{display: "inline-block"}}>
        <ExampleComponent 
            image={`/images/${props.image}.jpg`} 
            className={props.className}  
            roundedColor="#321124" 
            roundedSize="10" style={{width: '50%'}} 
        />
        </div>
    )
}
function StdImg_personImgMedium(props){
    return (
        <div style={{display: "inline-block"}}>
        <ExampleComponent 
            image={`/images/${props.image}.jpg`} 
            className={props.className} 
            imageWidth="150" 
            imageHeight="150" 
            roundedColor="#321124" 
            roundedSize="2" style={{width: '100%'}} 
        />
        </div>
    )
}
function StdImg_chipImgWithNamePosition(props){
    return (
        <div>
        <Chip close={false} 
            closeIcon={<Icon className="close">close</Icon>}
            options={null}
        >
            <img alt={props.alt} className="responsive-img"
                src="https://materializecss.com/images/yuna.jpg"            
            />
            {props.upline}<br/>
            {props.downline}
        </Chip>
        </div>
    )
}

export {StdImg_personImg100 ,StdImg_personImgMedium ,StdImg_stdPictureUpload , StdImg_stdPictureBase64, StdImg_avatar , StdImg_profileImage , StdImg_personImgTable , StdImg_profileImage100Percent , StdImg_chipImgWithNamePosition}