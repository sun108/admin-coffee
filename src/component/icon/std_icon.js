import React , { Component } from 'react';

//icon
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import DoneIcon from '@material-ui/icons/Done';
import NotInterestedIcon from '@material-ui/icons/NotInterested';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import VisibilityIcon from '@material-ui/icons/Visibility';
import InfoIcon from '@material-ui/icons/Info';
import Update from '@material-ui/icons/Update';
import Create from '@material-ui/icons/Create';
import DeleteForever from '@material-ui/icons/DeleteForever'

//FontAwesomeIcon
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import {faDiceD20,faUserTie,faUserInjured,faUserClock,
    faUser,faCity,faWarehouse,faUsers,faChessKnight,
    faChessPawn,faGem,faFeather,faPlane,faUsersCog,
    faCogs
} from "@fortawesome/free-solid-svg-icons";
import { faCoffee } from '@fortawesome/free-solid-svg-icons'
//materialize
import 'materialize-css';
import { Collapsible, CollapsibleItem } from 'react-materialize';

// library.add(fab,faUserInjured,faUserTie,faUserClock,faUser,faCity,faWarehouse,faUsers,faChessKnight,faChessPawn,faGem,faFeather,faPlane,faUsersCog,faCogs,faDiceD20)

function StdIcon_setting(icon){
    return (
        <div style={{display: 'inline'}}><FontAwesomeIcon icon={icon} size="x"/></div>
    )
}

function StdIcon_empData(){
    return ( StdIcon_setting(faUserTie) )
}
function StdIcon_orgChart(){
    return ( StdIcon_setting(faUsers) )
}
function StdIcon_branch(){
    return ( StdIcon_setting(faWarehouse) )
}
function StdIcon_assignment(){
    return ( <AssignmentIndIcon/>)
}
function StdIcon_yes(){
    return ( <DoneIcon/>)
}
function StdIcon_no(){
    return ( <NotInterestedIcon/>)
}
function StdIcon_create(){
    return ( <Create/>)
}
function StdIcon_update(){
    return ( <Update/>)
}
function StdIcon_delete(){
    return ( <DeleteForever/>)
}
function StdIcon_info(){
    return ( <InfoIcon/>)
}
function StdIcon_visible(){
    return ( <VisibilityIcon/>)
}
function StdIcon_invisible(){
    return ( <VisibilityOffIcon/>)
}

export {
    StdIcon_empData,
    StdIcon_orgChart,
    StdIcon_branch ,
    StdIcon_yes , StdIcon_no , StdIcon_assignment,
    StdIcon_update,StdIcon_delete,StdIcon_info,StdIcon_create,
    StdIcon_visible,StdIcon_invisible
}