import React , { Component , useState, useEffect } from 'react';
import TextField from '@material-ui/core/TextField';

function myTextField(label,defaultValue,line,multiline,onChange,statename){
    return (
      <TextField
        className={`myfont_orderall_textfield`}
        label={label}
        defaultValue={defaultValue}
        InputProps={{
          readOnly: false,
        }}
        rows={line}
        multiline={multiline}
        onChange={(e)=>onChange(e,statename)}
        variant="outlined"
        style={{width: `95%` , marginTop: `10px`}}
      />
    )
}

export {
    myTextField
}