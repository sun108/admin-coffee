import React from 'react';
import clsx from "clsx";
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Headroom from 'react-headroom'

import useScrollTrigger from "@material-ui/core/useScrollTrigger";
// import '../../component/effect/hover_menu/hover.scss';
import '../../component/effect/hover_menu2/hover.css';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    }
}));
function Appbar(){
    const classes = useStyles();
    const trigger = useScrollTrigger();

    return(
      <Headroom>
        <AppBar position="static" style={{height: `50px` , backgroundColor: `black`}}>
        <Toolbar>
            <div className={classes.root}>
              <Grid container spacing={3}>
                <Grid container item xs={12}>
                  <Grid item xs={1}>
                    <IconButton edge="" className={classes.menuButton} color="inherit" aria-label="menu"
                    onClick={this.handleDrawerToggle} className={classes.menuButton} >
                      <MenuIcon />
                    </IconButton>
                  </Grid>
                  <Grid item xs={10}>
                    <center>
                    <img className={classes.menuButton} src={`/assets/img/logo_voodoo.png`} style={{width: `100px`}} />
                    </center>
                  </Grid>
                  <Grid item xs={1}>
                    <IconButton edge="end" className={classes.menuButton} color="inherit" aria-label="menu">
                      <ShoppingCartIcon />
                    </IconButton>
                  </Grid>
                </Grid>
              </Grid>
            </div>
        </Toolbar>
        </AppBar>
      </Headroom>
    )
}

export {Appbar}