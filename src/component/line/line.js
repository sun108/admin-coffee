import React from 'react';

function line_title (w) {
    return (
        <hr style={{width: `${w - 50}px`,
            marginTop: `-20px`,
            color: `#977c00`,backgroundColor: `#977c00`,height: 5
        }}/>
    );
}

function line_order_title(wording){
    return (
        <p className={`pLineOverAll`}>
            <span style={{background:`#fff` , padding: `0 10px`}}>{wording}</span>
        </p>
    );
}

export {
    line_title , line_order_title
};