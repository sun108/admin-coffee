import React , { Component , useState, useEffect } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import {convertIntToMoneyFormat,getConsoleError,generateString_string} from '../../services/std_service/getString'

function query_tableInCard_unitCustomerDiscount(lang,itemData,wording){
    var res = []
    if(itemData.length > 0){
        var findData = itemData.filter(x=>x[`inItem`] !== null && x[`inItem`] !== undefined).length
        if(findData > 0){
            res.push(
                <div>
                <p className={`myfont_cart_modal_list_unit`}>{wording.summary_page.listDiscount.title}</p>
                <TableContainer>
                    <Table>
                    <TableHead>
                        <TableRow>
                        <TableCell align="center" component="th" scope="row">
                            <p className={`myfont_cart_table_head`}>{wording.summary_page.listDiscount.order}</p>
                        </TableCell>
                        <TableCell align="center"><p className={`myfont_cart_table_head`}>{wording.summary_page.listDiscount.total}</p></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {query_listDiscount(lang,itemData,wording)}
                    </TableBody>
                    </Table>
                </TableContainer>
                </div>
            )
        }
    }
    return res
}
function query_tableInCard_unitCustomerShipping(lang,itemData,wording){
    var res = []
    if(itemData.length > 0){
        res.push(
        <div>
            <p className={`myfont_cart_modal_list_unit`}>{wording.summary_page.listShipping.title}</p>
            <TableContainer>
            <Table>
                <TableHead>
                <TableRow>
                    <TableCell align="center" component="th" scope="row">
                        <p className={`myfont_cart_table_head`}>{wording.summary_page.listShipping.order}</p>
                    </TableCell>
                    <TableCell align="center"><p className={`myfont_cart_table_head`}>{wording.summary_page.listShipping.total}</p></TableCell>
                </TableRow>
                </TableHead>
                <TableBody>
                {query_listShipping(lang,itemData,wording)}
                </TableBody>
            </Table>
            </TableContainer>
        </div>
        )
    }
    return res
}
function query_tableInCard_unitCustomerGiftFree(lang,itemData,wording){
    var res = []
    if(itemData.length > 0){
        var findData = itemData.filter(x=>x[`inItem`] !== null && x[`inItem`] !== undefined).length
        if(findData > 0){
            res.push(
                <div>
                    <p className={`myfont_cart_modal_list_unit`}>{wording.summary_page.listGiftFree.title}</p>
                    <TableContainer>
                        <Table>
                        <TableHead>
                            <TableRow>
                            <TableCell align="center" component="th" scope="row">
                                <p className={`myfont_cart_table_head`}>{wording.summary_page.listGiftFree.order}</p>
                            </TableCell>
                            <TableCell align="center"><p className={`myfont_cart_table_head`}>{wording.summary_page.listGiftFree.total}</p></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {query_listGift(lang,itemData,wording)}
                        </TableBody>
                        </Table>
                    </TableContainer>
                </div>
            )
        }
    }
    return res
}
function query_tableInCard_unitCustomerBuy(lang, itemData,wording){
    return (
        <div>
        <p className={`myfont_cart_modal_list_unit`}>{wording.summary_page.listOrder.title}</p>
        <TableContainer>
            <Table>
            <TableHead>
                <TableRow>
                <TableCell align="center" component="th" scope="row">
                    <p className={`myfont_cart_table_head`}>{wording.summary_page.listOrder.order}</p>
                </TableCell>
                <TableCell align="center"><p className={`myfont_cart_table_head`}>{wording.summary_page.listOrder.price}</p></TableCell>
                <TableCell align="center"><p className={`myfont_cart_table_head`}>{wording.summary_page.listOrder.amount}</p></TableCell>
                <TableCell align="center"><p className={`myfont_cart_table_head`}>{wording.summary_page.listOrder.total}</p></TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {query_listUnit(lang,itemData,wording)}
            </TableBody>
            </Table>
        </TableContainer>
        </div>
    )
}
function query_listGift(lang,itemData,wording){
    var element = []
    itemData.map(x=> {
        if(x.inItem !== undefined && x.inItem !== null){
            element.push(
                <TableRow>
                    <TableCell align="center" component="td" scope="row">
                        <p className={`myfont_cart_table_body`}>{x.inItem[lang][0].title}</p>
                    </TableCell>
                    <TableCell align="center"><p className={`myfont_cart_table_body`}>{convertIntToMoneyFormat(x.amount)}</p></TableCell>
                </TableRow>
            );
        }
    })
    return element
}
function query_listUnit(lang,itemData,wording){
    var element = []
    itemData.map(x=> {
        element.push(
        <TableRow>
            <TableCell align="center" component="td" scope="row">
                <p className={`myfont_cart_table_body`}>{x.inItem[lang][0].title}</p>
            </TableCell>
            <TableCell align="center"><p className={`myfont_cart_table_body`}>{convertIntToMoneyFormat(x.priceUse)}</p></TableCell>
            <TableCell align="center"><p className={`myfont_cart_table_body`}>{convertIntToMoneyFormat(x.amount)}</p></TableCell>
            <TableCell align="center"><p className={`myfont_cart_table_body`}>{convertIntToMoneyFormat(x.total)}</p></TableCell>
        </TableRow>
        );
    })
    if(element.length <= 0){
        element.push(<p>no item</p>)
    }
    return element
}
function query_listShipping(lang,itemData,wording){
    var element = []
    itemData.map(x=> {
        element.push(
        <TableRow>
            <TableCell align="center" component="td" scope="row">
                <p className={`myfont_cart_table_body`}>{x.inItem[lang]}</p>
            </TableCell>
            <TableCell align="center"><p className={`myfont_cart_table_body`}>{convertIntToMoneyFormat(x.total)}</p></TableCell>
        </TableRow>
        );
    })
    if(element.length <= 0){
        element.push(<p>no item</p>)
    }
    return element
}
function query_listDiscount(lang,itemData,wording){
    var element = []
    itemData.map(x=> {
        if(x.inItem !== undefined && x.inItem !== null){
            element.push(
            <TableRow>
                <TableCell align="center" component="td" scope="row">
                    <p className={`myfont_cart_table_body`}>{x.inItem[lang][0].title}</p>
                </TableCell>
                <TableCell align="center"><p className={`myfont_cart_table_body`}>{`-${convertIntToMoneyFormat(x.total)}`}</p></TableCell>
            </TableRow>
            );
        }
    })
    return element
}
function query_tableInCard_unitCustomerTotalPrice(lang ,price , wording){
    var res = []
    res.push(
      <div>
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell align="center" component="th" scope="row">
                  <p className={`myfont_cart_table_summary`}>{wording.summary_page.total_price_all_pay}</p>
                </TableCell>
                <TableCell align="center"><p className={`myfont_cart_table_summary`}>{`${convertIntToMoneyFormat(price)} THB`}</p></TableCell>
              </TableRow>
            </TableHead>
          </Table>
        </TableContainer>
      </div>
    )
    return res
  }

export {
    query_tableInCard_unitCustomerDiscount,
    query_tableInCard_unitCustomerShipping,
    query_tableInCard_unitCustomerGiftFree,
    query_tableInCard_unitCustomerBuy ,
    query_tableInCard_unitCustomerTotalPrice
}