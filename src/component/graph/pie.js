import React , { Component , useState, useEffect } from 'react';
import ReactApexChart from "react-apexcharts";

class PieChart extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
      
        series: props.series,
        options: {
          labels: props.labels,
          chart: {
            width: `10%`,
            type: 'donut',
          },
          responsive: [{
            breakpoint: 480,
            options: {
              chart: {
                width: 50
              },
              legend: {
                position: 'bottom'
              }
            }
          }]
        },
      };
    }
    
    render() {
        return (
          <div id="chart">
            <ReactApexChart options={this.state.options} series={this.state.series} type="donut" height={200} />
        </div>
        );
    }
}


export default PieChart
