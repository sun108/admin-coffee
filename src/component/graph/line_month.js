import React , { Component , useState, useEffect } from 'react';
import ReactApexChart from "react-apexcharts";

class ApexChart extends Component {
    constructor(props) {
      super(props);
        this.state = {
            series: [{
                name: "price income total",
                data: props.data
            }],
            options: {
            chart: {
                height: 350,
                type: 'line',
                zoom: {
                enabled: false
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'straight'
            },
            title: {
                text: 'ราคาขายรวมตลอดปี',
                align: 'left'
            },
            grid: {
                row: {
                colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                opacity: 0.5
                },
            },
            xaxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep' ,'Oct' ,'Nov' ,'Dec'],
            }
            },
        
        
        };
        } 
    
    render() {
        return (
          <div id="chart">
            <ReactApexChart options={this.state.options} series={this.state.series} type="line" height={350} />
        </div>
        );
    }
}


export default ApexChart
