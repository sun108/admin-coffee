import React , { Component } from 'react';
import Button from '@material-ui/core/Button';
import ScrollToTop from "react-scroll-to-top";

import Fab from '@material-ui/core/Fab';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import { Link } from "react-router";
import IconButton from '@material-ui/core/IconButton';
import DirectionsIcon from '@material-ui/icons/Directions';
import LinkIcon from '@material-ui/icons/Link';
import InfoIcon from '@material-ui/icons/Info';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import InsertChartIcon from '@material-ui/icons/InsertChart';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import VisibilityIcon from '@material-ui/icons/Visibility';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import MenuOpenIcon from '@material-ui/icons/MenuOpen';
import Create from '@material-ui/icons/Create';
import DeleteForever from '@material-ui/icons/DeleteForever'
import Update from '@material-ui/icons/Update';
import AddIcon from '@material-ui/icons/Add';
import UpIcon from '@material-ui/icons/KeyboardArrowUp';
import CancelIcon from '@material-ui/icons/Cancel';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import EditIcon from '@material-ui/icons/Edit';

function StdBtnIconOnly_upToTop(props){
    return (
        <div style={{display: 'inline'}}>
            <Tooltip title="Up to top page" placement="top">
                <ScrollToTop smooth component={
                    <IconButton 
                        node={props.node}
                        id={props.id}
                        style={{position: 'fixed' ,zIndex: 1 ,bottom: '80px',right: '10px',backgroundColor: '#aa2e25',color:'white'}}
                    >
                        <ArrowUpwardIcon />
                    </IconButton>
                } />
            </Tooltip>
        </div>
    )
}
function StdBtn_create(props){
    return (
        <div style={{display: 'inline'}}>
        <Tooltip title="Create Data" placement="top">
        <Button
            variant="contained"
            color="primary"
            startIcon={<Create />}
            onClick={props.onClick}
            style={{backgroundColor: 'green'}}
        >
            create
        </Button>
        </Tooltip>
        </div>
    )
}
function StdBtn_update(props){
    return (
        <div style={{display: 'inline'}}>
        <Tooltip title="Update Data" placement="top">
        <Button
            id={props.id}
            variant="contained"
            color="primary"
            startIcon={<Update />}
            onClick={props.onClick}
            style={{backgroundColor: 'blue' , display: 'inline'}}
        >
            update
        </Button>
        </Tooltip>
        </div>
    )
}
function StdBtn_delete(props){
    return (
        <div style={{display: 'inline'}}>
        <Tooltip title="Delete Data" placement="top">
        <Button
            variant="contained"
            color="primary"
            startIcon={<DeleteForever />}
            style={{backgroundColor: 'red' , display: 'inline'}}
        >
            delete
        </Button>
        </Tooltip>
        </div>
    )
}
function StdFabBtn_create(props){
    return (
        <Tooltip title="Create New Data" placement="top">
            <Fab aria-label='Add' color='green'
                style={{position: 'fixed' ,bottom: '15px',right: '10px',backgroundColor: '#ffab00', color:'white'}}
                onClick={props.funcClickLink}
            >
                {<Create />}
            </Fab>
        </Tooltip>
    )
}
function StdFabBtn_done(props){
    return (
        <Tooltip title="Submit" placement="top">
            <Fab aria-label='Add' color='green'
                style={{position: 'fixed' ,bottom: '15px',right: '10px',backgroundColor: '#357a38', color:'white'}}
                onClick={props.funcClickSubmit}
            >
                {<DoneAllIcon />}
            </Fab>
        </Tooltip>
    )
}
function StdFabBtn_menu(props){
    return (
        <Tooltip title="Menu" placement="top">
            <Fab aria-label='Add' color='green'
                style={{position: 'fixed' ,bottom: '15px',right: '10px',backgroundColor: '#357a38', color:'white'}}
                onClick={props.funcClickSubmit}
            >
                {<MenuOpenIcon />}
            </Fab>
        </Tooltip>
    )
}
function StdFabBtn_delete(props){
    return (
        <Tooltip title="Delete" placement="top">
            <Fab aria-label='Add' color='red'
                style={{position: 'fixed' ,bottom: '15px',right: '70px',backgroundColor: 'red', color:'white'}}
                onClick={props.funcClickSubmit}
            >
                {<DeleteForeverIcon />}
            </Fab>
        </Tooltip>
    )
}
function StnBtn_close(props){
    return (
        <Tooltip title="Close" placement="top">
        <Button
            variant="contained"
            color="primary" className={props.className}
            startIcon={<CancelIcon />}
            style={{backgroundColor: 'red' , display: 'inline'}}
        >
            close
        </Button>
        </Tooltip>
    )
}
function StnBtn_done(props){
    return (
        <Tooltip title="confirm" placement="top">
        <Button
            type={props.type}
            form={props.form}
            className={props.className}
            variant="contained"
            color="primary"
            startIcon={<CheckCircleIcon />}
            style={{backgroundColor: 'green' , display: 'inline'}}
        >
            done
        </Button>
        </Tooltip>
    )
}
function StnBtnIcon_plus(props){
    return (
        <Tooltip title="add" placement="top">
        <Button
            type={props.type}
            className={props.className}
            variant="contained"
            color="primary"
            startIcon={<CheckCircleIcon />}
            style={{backgroundColor: 'green' , display: 'inline'}}
        >
            done
        </Button>
        </Tooltip>
    )
}
function StnBtn_customDirect(props){
    const bgColor = props.bgColor
    return (
        <Tooltip title="confirm" placement="top">
        <Button
            type={props.type}
            form={props.form}
            className={props.className}
            variant="contained"
            color="primary"
            startIcon={props.icon}
            style={{backgroundColor: bgColor , display: 'inline'}}
        >
            {props.str}
        </Button>
        </Tooltip>
    )
}

function StdBtnIconOnly_Direct(props){
    return (
        <div style={{display: 'inline'}}>
        <Tooltip title="Direct Link" placement="top">
            <IconButton
                className={props.classname} 
                href={props.href} 
                node={props.node}
                id={props.id}
                component={Link} 
                to={props.link}
                onClick={props.onClick}
                style={{backgroundColor: '#673ab7',color:'white'}}
            >
                {props.icon}
            </IconButton>
        </Tooltip>
        </div>
    )
}
function StdBtnIconOnly_customDirect(props){
    return (
        <div style={{display: 'inline'}}>
        <Tooltip title="Direct Link" placement="top">
            <IconButton
                className={props.classname} 
                href={props.href} 
                node={props.node}
                id={props.id}
                component={Link} 
                to={props.link}
                onClick={props.onClick}
                style={props.style}
            >
                {props.icon}
            </IconButton>
        </Tooltip>
        </div>
    )
}

//====================== MODAL ==============================================

function StdBtnModal_delete(props){
    return (
        <div style={{display: 'inline'}}>
        <Tooltip title="Delete Data" placement="top">
        <Button
            className={props.classname} 
            href={props.href} 
            node={props.node}
            id={props.id}
            onClick={props.onClick}
            
            variant="contained"
            color="primary"
            startIcon={<DeleteForever />}
            style={{backgroundColor: 'red'}}
        >
            delete
        </Button>
        </Tooltip>
        </div>
    )
}
function StdBtnModal_update(props){
    return (
        <div style={{display: 'inline'}}>
        <Tooltip title="๊Update Data" placement="top">
        <Button
            className={props.classname} 
            href={props.href} 
            node={props.node}
            id={props.id}
            onClick={props.onClick}
            
            variant="contained"
            color="primary"
            startIcon={<Update />}
            style={{backgroundColor: 'blue'}}
        >
            update
        </Button>
        </Tooltip>
        </div>
    )
}
function StdBtnModalIconOnly_delete(props){
    return (
        <div style={{display: 'inline'}}>
        <Tooltip title="Delete Data" placement="top">
            <IconButton
                className={props.classname} 
                href={props.href} 
                node={props.node}
                id={props.id}
                onClick={props.onClick}
                style={{backgroundColor: 'red',color:'white'}}
            >
                <DeleteForever />
            </IconButton>
        </Tooltip>
        </div>
    )
}
function StdBtnModalIconOnly_invisible(props){
    return (
        <div style={{display: 'inline'}}>
        <Tooltip title="Invisible Data" placement="top">
            <IconButton
                className={props.classname} 
                href={props.href} 
                node={props.node}
                id={props.id}
                onClick={props.onClick}
                style={{backgroundColor: 'red',color:'white'}}
            >
                <VisibilityOffIcon/>
            </IconButton>
        </Tooltip>
        </div>
    )
}
function StdBtnModalIconOnly_visible(props){
    return (
        <div style={{display: 'inline'}}>
        <Tooltip title="Invisible Data" placement="top">
            <IconButton
                className={props.classname} 
                href={props.href} 
                node={props.node}
                id={props.id}
                onClick={props.onClick}
                style={{backgroundColor: '#6d1b7b',color:'white'}}
            >
                <VisibilityIcon/>
            </IconButton>
        </Tooltip>
        </div>
    )
}

function StdBtnModalIconOnly_update(props){
    return (
        <div style={{display: 'inline'}}>
        <Tooltip title="Update Data" placement="top">
        <IconButton
            className={props.classname} 
            href={props.href} 
            node={props.node}
            id={props.id}
            onClick={props.onClick}
            style={{backgroundColor: 'blue',color:'white'}}
        >
            <Update />
        </IconButton>
        </Tooltip>
        </div>
    )
}
function StdBtnModalIconOnly_info(props){
    return (
        <div style={{display: 'inline'}}>
        <Tooltip title="info Data" placement="top">
        <IconButton
            className={props.classname} 
            href={props.href} 
            node={props.node}
            id={props.id}
            onClick={props.onClick}
            style={{backgroundColor: 'orange',color:'white'}}
        >
            <InfoIcon />
        </IconButton>
        </Tooltip>
        </div>
    )
}
function StdFabModal_createRightBottom(props){
    return (
        <Fab 
            color="primary" 
            aria-label="add"
            className={props.classname} 
            href={props.href} 
            node={props.node}
            id={props.id}
            onClick={props.onClick}
            style={{position: 'fixed' ,bottom: '15px',right: '20px',backgroundColor: 'green'}}
        >
            <Create />
        </Fab>
    )
}
function StdFab_DirectCustomBottom(props){
    return (
        <div style={{display: 'inline'}}>
        <Tooltip title="Direct Link" placement="top">
        <Fab 
            color="primary" 
            aria-label="add"
            className={props.classname} 
            href={props.href} 
            node={props.node}
            id={props.id}
            component={Link}
            to={props.link}
            onClick={props.onClick}
            style={props.style}
        >
            {props.icon}
        </Fab>
        </Tooltip>
        </div>
    )
}
function StdFab_DirectRightBottom(props){
    return (
        <div style={{display: 'inline'}}>
        <Tooltip title="Direct Link" placement="top">
        <Fab 
            color="primary" 
            aria-label="add"
            className={props.classname} 
            href={props.href} 
            node={props.node}
            id={props.id}
            component={Link}
            to={props.link}
            onClick={props.onClick}
            style={{position: 'fixed' ,bottom: '100px',right: '20px',backgroundColor: '#6d1b7b'}}
        >
            {props.icon}
        </Fab>
        </Tooltip>
        </div>
    )
}
function StdBtnModal_logout(props){
    return (
        <div style={{display: 'inline'}}>
        <Tooltip title="LogOut" placement="top">
        <Button
            className={'modal-trigger'} 
            href={props.href} 
            node={props.node}
            id={props.id}
            onClick={props.onClick}
            
            variant="contained"
            color="primary"
            style={{backgroundColor: '#c62828',marginRight: "20px"}}
        >
            LOG OUT
        </Button>
        </Tooltip>
        </div>
    )
}
function StdBtn_hideCloseModal(){
    return (<div style={{display:'none'}}><button id='close_modal' className="modal-close">x</button></div>)
}

export {
    StdBtnModalIconOnly_visible ,
    StdBtnModalIconOnly_invisible ,
    StdFabModal_createRightBottom ,
    StdFab_DirectRightBottom, 
    StdBtnIconOnly_upToTop ,
    StdBtnModal_delete , 
    StdBtn_create ,
    StdBtn_update , 
    StdBtn_delete , 
    StdFabBtn_create ,
    StnBtn_customDirect,
    StdBtn_hideCloseModal,
    StnBtn_close,
    StnBtn_done ,
    StdBtnModal_update,
    StdBtnModalIconOnly_delete,
    StdBtnModalIconOnly_update,
    StdBtnModalIconOnly_info,
    StdBtnIconOnly_Direct,
    StdFab_DirectCustomBottom,
    StdBtnIconOnly_customDirect,
    StnBtnIcon_plus,
    StdBtnModal_logout ,
    StdFabBtn_done ,
    StdFabBtn_delete ,
    StdFabBtn_menu
};