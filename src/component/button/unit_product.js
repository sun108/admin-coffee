import React , { Component , useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';

//icon
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import RemoveShoppingCartIcon from '@material-ui/icons/RemoveShoppingCart';

function buttonAddToCart(mobile , funcAddToCart , dataRow , lang , outOfStock ,wording){
    var res = []
    if(mobile){
      res.push(
        <Button 
          variant="contained" 
          style={{ borderRadius: 50 , color: `white` , backgroundColor: outOfStock ? `rgb(15, 15, 15)` :`red` , border: `0.5px solid #aeaeae`}} 
          startIcon={outOfStock ? <RemoveShoppingCartIcon/> : <AddShoppingCartIcon/>}
          disabled={outOfStock}
          onClick={funcAddToCart.bind(this,dataRow)}
        >
          {outOfStock ? `${wording.out_of_stock}` : `${wording.add_to_cart}`}
        </Button>
      )
    }else{
      res.push(
        <Button 
          variant="contained" 
          style={{ color: `white` , backgroundColor: outOfStock ? `rgb(15, 15, 15)` :`red` , border: `0.5px solid #aeaeae`}} 
          startIcon={outOfStock ? <RemoveShoppingCartIcon/> : <AddShoppingCartIcon/>}
          disabled={outOfStock}
          onClick={funcAddToCart.bind(this,dataRow)}
        >
          {outOfStock ? `${wording.out_of_stock}` : `${wording.add_to_cart}`}
        </Button>
      )
    }
    return res
  }
  export {
    buttonAddToCart
  }