import React , { Component } from 'react';

import Snackbar from '@material-ui/core/Snackbar';
import { Alert, AlertTitle } from '@material-ui/lab';
import { makeStyles } from '@material-ui/core/styles';
import M from 'materialize-css'

const useStyles = makeStyles((theme) => ({
    root: {
      width: '80%',
      '& > * + *': {
        marginTop: theme.spacing(2),
      },
    },
  }));

const disTime = 3000

function StdSnackBar_control(props){
    // open , info , header , body
    const classes = useStyles();
    return (
        <div>
        <Snackbar className={classes.root}
            anchorOrigin={{ vertical: 'top',horizontal: 'right' }} 
            // autoHideDuration={6000}
            open={props.open}
            key={'top','right'}
        >
            <div className={classes.root}>
            <Alert severity={props.mode}> {/* info , warning , success , error*/}
            <AlertTitle>{props.header}</AlertTitle>
                {props.body}
            </Alert>
            </div>
        </Snackbar>
        </div>
    )
}

function StdToastMz_error(body){
  var toastHTML = '<span>'+body+'</span>';
  M.toast({html: toastHTML , classes: 'rounded red' , displayLength: disTime});
}

function StdToastMz_success(body){
  var toastHTML = '<span>success : '+body+'</span>';
  M.toast({html: toastHTML , classes: 'rounded green' , displayLength: disTime});
}

function StdToastMz_info(body){
  var toastHTML = '<span>info : '+body+'</span>';
  M.toast({html: toastHTML , classes: 'rounded teal' , displayLength: disTime});
}

function StdToastMz_warning(body){
  var toastHTML = '<span>warning : '+body+'</span>';
  M.toast({html: toastHTML , classes: 'rounded orange' , displayLength: disTime});
}

export {StdSnackBar_control , StdToastMz_error,StdToastMz_success,StdToastMz_info,StdToastMz_warning };