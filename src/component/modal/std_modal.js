import React , { Component } from 'react';

import Button from '@material-ui/core/Button';
import Create from '@material-ui/icons/Create';
import Update from '@material-ui/icons/Update';
import DeleteForever from '@material-ui/icons/DeleteForever'
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import UpIcon from '@material-ui/icons/KeyboardArrowUp';
import EditIcon from '@material-ui/icons/Edit';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import { Link } from "react-router";

import { Modal} from 'react-materialize';
import 'materialize-css';

// const btn_close = (style={color: `white`, backgroundColor: `red`})

function StdModal_create(props){
    return (
        <div>
        {/* <Modal show={openModal} onHide={handleClose} style={{zIndex: '100000 !important',overlay: '100000 !important'}}>
          <Modal.Header closeButton>
            <Modal.Title>{props.title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>Woohoo, you're reading this text in a modal!</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={handleClose}>
              Save Changes
            </Button>
          </Modal.Footer>
        </Modal> */}
        </div>
    )
}
function StdModal_materialize_create(){
    return (
        <div>
            <Modal
            actions={[
                <Button flat modal="close" node="button" waves="green">Close</Button>
            ]}
            bottomSheet={true}
            fixedFooter={true}
            header="Modal Header"
            id="Modal-0"
            open={false}
            options={{
            dismissible: true,
            endingTop: '10%',
            inDuration: 250,
            onCloseEnd: null,
            onCloseStart: null,
            onOpenEnd: null,
            onOpenStart: null,
            opacity: 0.5,
            outDuration: 250,
            preventScrolling: true,
            startingTop: '4%'
            }}
            trigger={<Button node="button">MODAL WITH FIXED FOOTER</Button>}
            >
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
        </Modal>
        </div>
    )
}
function StdModalFab_materialize_create(props){
  const myStyle = {
    height: "90% !important",
    color: "black",
    maxHeight: '100%', 
    overflow: 'visible'
  }
  return (
      <div>
          <Modal
            actions={[
                <Button className="modal-close" flat modal="Close" node="button" waves="green">Close</Button>
            ]}
            style={myStyle}
            bottomSheet={true}
            fixedFooter={true}
            header={props.header}
            id="Modal-0"
            open={false}
            options={{
            dismissible: true,
            endingTop: '80%',
            inDuration: 250,
            onCloseEnd: null,
            onCloseStart: null,
            onOpenEnd: null,
            onOpenStart: null,
            opacity: 0.5,
            outDuration: 250,
            preventScrolling: true,
            startingTop: '50%'
            }}
            trigger={
              <Tooltip title="Create New Data" placement="top">
                <Fab aria-label='Add' color='green'
                    style={{position: 'fixed' ,bottom: '15px',right: '20px',backgroundColor: '#357a38', color:'white'}}
                >
                    {<Create />}
                </Fab>
              </Tooltip>
            }
            >
            {props.form}
        </Modal>
      </div>
  )
}
function StdModalEmpty_materialize_80FixFooter(props){
  const myStyle = {
    height: "90% !important",
    color: "black",
    maxHeight: '100%', 
    overflow: 'visible'
  }
  return (
      <div>
          <Modal
            actions={[
                <Button className="modal-close" flat modal="Close" node="button" waves="green">Close</Button>
            ]}
            style={myStyle}
            bottomSheet={true}
            fixedFooter={true}
            header={props.header}
            id={props.id} //modal_80FixFooter
            open={false}
            options={{
            dismissible: true,
            endingTop: '80%',
            inDuration: 250,
            onCloseEnd: null,
            onCloseStart: null,
            onOpenEnd: null,
            onOpenStart: null,
            opacity: 0.5,
            outDuration: 250,
            preventScrolling: true,
            startingTop: '50%'
            }}
          >
            {props.form}
        </Modal>
      </div>
  )
}
function StdModalEmpty_materialize_90FixFooter(props){
  const myStyle = {
    height: "100% !important",
    color: "black",
    maxHeight: '100%', 
    overflow: 'visible',
    backgroundColor: `white`
  }
  return (
      <div>
          <Modal
            className={`myfont_content_std myModal_90`}
            actions={[
              <div>
                <Button className="modal-close" style={{backgroundColor: `green` , color: `white` , display: props.visible}} flat modal="Close" node="button" waves="green" button component={Link} to={`/order_summary`}>
                  สรุปการสั่งซื้อ
                </Button>&nbsp;&nbsp;&nbsp;
                <Button className="modal-close" style={{backgroundColor: `red` , color: `white`}} flat modal="Close" node="button" waves="green">Close</Button>
              </div>
            ]}
            style={myStyle}
            bottomSheet={true}
            fixedFooter={true}
            // header={props.header}
            id={props.id} //modal_80FixFooter
            open={false}
            options={{
              dismissible: true,
              endingTop: '100%',
              inDuration: 250,
              onCloseEnd: null,
              onCloseStart: null,
              onOpenEnd: null,
              onOpenStart: null,
              opacity: 0.5,
              outDuration: 250,
              preventScrolling: true,
              startingTop: '100%'
            }}
          >
            {props.form}
        </Modal>
      </div>
  )
}
function StdModalEmpty_materialize_100FixFooter(props){
  const myStyle = {
    height: "100% !important",
    color: "black",
    maxHeight: '100%', 
    overflow: 'visible'
  }
  const styleCode =(".modal100{height: 100% !important}")
  return (
      <div>
          <style>{ styleCode }</style>
          <Modal
            actions={[
                <Button className="modal-close" flat modal="Close" node="button" waves="green">Close</Button>
            ]}
            className='modal100'
            style={myStyle}
            bottomSheet={true}
            fixedFooter={true}
            header={props.header}
            id={props.id} //modal_100FixFooter
            open={false}
            options={{
              dismissible: true,
              endingTop: '100%',
              inDuration: 250,
              onCloseEnd: null,
              onCloseStart: null,
              onOpenEnd: null,
              onOpenStart: null,
              opacity: 0.5,
              outDuration: 250,
              preventScrolling: true,
              startingTop: '100%',
              height:"100%"
            }}
          >
            {props.form}
        </Modal>
      </div>
  )
}

function StdModalEmpty_materialize_sizeNormal(props){
  return(
    <Modal
      actions={[
        <Button className="modal-close" flat modal="close" node="button" waves="green">Close</Button>
      ]}
      bottomSheet={false}
      fixedFooter={false}
      header={props.header}
      id={props.id} //modal_normalSize
      open={false}
      options={{
        dismissible: true,
        endingTop: '10%',
        inDuration: 250,
        onCloseEnd: null,
        onCloseStart: null,
        onOpenEnd: null,
        onOpenStart: null,
        opacity: 0.5,
        outDuration: 250,
        preventScrolling: true,
        startingTop: '4%'
      }}
      // root={[object HTMLBodyElement]}
    >
      {props.form}
    </Modal>
  )
}
export {
  StdModal_create , 
  StdModal_materialize_create , 
  StdModalFab_materialize_create , 
  StdModalEmpty_materialize_sizeNormal , 
  StdModalEmpty_materialize_80FixFooter , 
  StdModalEmpty_materialize_90FixFooter ,
  StdModalEmpty_materialize_100FixFooter
};