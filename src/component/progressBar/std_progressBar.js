import React , { Component } from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';

function StdProgessBar_sizeDefault_loadLoop(props){
    return (
        <LinearProgress size={90} thickness={4} color="secondary" />
    )
}
function StdProgessBar_sizeBig001_loadLoop(props){
    return (
        <LinearProgress size={90} thickness={4} color="secondary" />
    )
}
export {StdProgessBar_sizeDefault_loadLoop , StdProgessBar_sizeBig001_loadLoop};