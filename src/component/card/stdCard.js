import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Radium, {StyleRoot} from 'radium';

import '../../component/effect/imagehover.css-master/css/demo-page.css';
import '../../component/effect/imagehover.css-master/css/imagehover.min.css';

const useStyles = makeStyles({
    root: {
      minWidth: 100,
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
});

function StdCard_App(props){
    const classes = useStyles();
    const bull = <span className={classes.bullet}>•</span>;
    return (
    <div>
        <Card className={classes.root} variant="outlined">
            <CardContent>
            <div class="demo">
                <div class="imghvr-push-up">
                    <img src={`assets/app_sub_01/${props.img}.jpg`} style={{width: `100%`}} />
                </div>
                <figcaption>
                    <h3>Hello World</h3>
                    <p>Life is too important to be taken seriously!</p>
                </figcaption>
            </div>
            </CardContent>
            <CardActions>
                <Button size="small">Learn More</Button>
            </CardActions>
        </Card>
    </div>
    )
}
function StdCard_hoverEffect(props){
    return(
        <div class="demo">
            <div class="imghvr-push-up">
                <img src={`assets/app_sub_01/${props.img}.jpg`} style={{width: `100%`}} />
            </div>
            <figcaption>
                <h3>Hello World</h3>
                <p>Life is too important to be taken seriously!</p>
            </figcaption>
        </div>
    )
}

export {StdCard_App,StdCard_hoverEffect}