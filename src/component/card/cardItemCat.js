import React , { Component , useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import { RibbonContainer,  RightCornerRibbon , LeftCornerRibbon } from "react-ribbons";
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import { Link } from "react-router";

import {line_title ,line_order_title} from '../../component/line/line'

import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import RemoveShoppingCartIcon from '@material-ui/icons/RemoveShoppingCart';
import InfoIcon from '@material-ui/icons/Info';
import {convertIntToMoneyFormat} from '../../services/std_service/getString'

const lg= 3 , md= 3 , sm=4 , xs = 6 , spacing = 2

function findItemInThisCollection(listDelivery,listCollection , listUnit , funcAddItemToCart_byOne , lang,h ,wording){
    var res = []
    listUnit.filter(x=>x[`customer_view`]).map(x=>{
      var findInDelivery = listDelivery.filter(g=>g[`idcode_unit`] === x.idcode)
      if(findInDelivery.length > 0){
        res.push(
          <Grid container item lg={lg} md={md+1} sm={sm} xs={xs} spacing={2} >
              <center>{queryItemCardCollection(x , funcAddItemToCart_byOne , lang,(h / 3)-90 ,wording)}</center>
              <hr/>
          </Grid>
        )
      }
    })
    return res
}

function findItemInThisCat(listDelivery ,listCat , listRelative , listUnit , listRibbon , funcAddItemToCart_byOne , lang, h ,wording){
    var res = [] , numb = 0
    var findCatInRelative = listRelative.filter(v=>v['idcode_cat'] === listCat.idcode)
    if(findCatInRelative.length > 0){
        findCatInRelative.map(x=>{
          var findRelativeDelivery = listDelivery.filter(e=>e[`idcode_unit`] === x['idcode_unit'])
          var findItem = listUnit.filter(i=>i['idcode'] === x['idcode_unit'] && i.customer_view === true);
          if(findItem.length > 0 && findRelativeDelivery.length > 0){
            if(numb === 0){
              res.push(<div id={listCat.idcode} className="element"></div>)
              res.push(line_order_title(listCat[lang]))
            }
            if(listCat.ribbon_index !== "" && listCat.ribbon_index !== null){
              var finItemRibbon = listRibbon.filter(f=>f['index'] === listCat.ribbon_index)
              if(finItemRibbon.length > 0){
                res.push(
                  <Grid container item lg={lg} md={md} sm={sm} xs={xs} spacing={spacing} >
                      <center>
                        <RibbonContainer className={`rb`}>
                          <LeftCornerRibbon backgroundColor={finItemRibbon[0].bg} color={finItemRibbon[0].color} fontFamily="DB HeaventRounded Med" >
                            {finItemRibbon[0][lang][0].wording}
                          </LeftCornerRibbon>
                          {queryItemCard(findItem[0] , funcAddItemToCart_byOne , lang,h ,wording)}
                        </RibbonContainer>
                      </center>
                  </Grid>
                )
              }
            }else{
              res.push(
                <Grid container item lg={lg} md={md} sm={sm} xs={xs} spacing={spacing} >
                  <center>
                    {queryItemCard(findItem[0] , funcAddItemToCart_byOne , lang,h ,wording)}
                  </center>
                  <hr/>
                </Grid>
              )
            }
            numb++
          }
        }
      )
    }
    return res
}

function queryItemCardMoreItem(itemRow , lang ,wording){
  var prodIdcode = itemRow.idcode
  return (
    <Box className='cardUnitProduct' boxShadow={4} style={{border: `black solid 0.2px` , marginTop: `15px` ,paddingBottom: `15px`}}>
      <img src={`${process.env.REACT_APP_URL_ASSETS}unit_product/${prodIdcode}/${itemRow.img}`} style={{width:`100%` , height: `20vh`,objectFit: `cover`}} />
      <hr/>
      <div style={{height: `60px`}}>
        <p className={`pLimitLine_orderCard`}>{itemRow[lang][0].title}</p>
      </div>
      {gridButtonMoreItem(itemRow ,wording.[lang].wording)}
    </Box>
  )
}

function queryItemCard(itemRow , funcAddItemToCart_byOne , lang, height ,wording){
    var prodIdcode = itemRow.idcode
    return (
      <Box className='cardUnitProduct' boxShadow={4} style={{border: `black solid 0.2px` , marginTop: `15px` ,paddingBottom: `15px`}}>
          <img src={`${process.env.REACT_APP_URL_ASSETS}unit_product/${prodIdcode}/${itemRow.img}`} style={{width:`80%` , height: `${height - 190}px`,objectFit: `cover`}} />
          <hr/>
          <div style={{height: `60px`}}>
            <p className={`pLimitLine_orderCard`}>{itemRow[lang][0].title}</p>
          </div>
          {gridButton(funcAddItemToCart_byOne,itemRow ,wording)}
      </Box>
    )
}
function queryItemCardCollection(itemRow , funcAddItemToCart_byOne , lang,height ,wording){
  var prodIdcode = itemRow.idcode
  return (
    <Box className='cardUnitProduct' boxShadow={4} style={{border: `black solid 0.2px` , marginTop: `15px` ,paddingBottom: `15px`}}>
        <img src={`${process.env.REACT_APP_URL_ASSETS}unit_product/${prodIdcode}/${itemRow.img}`} style={{width:`100%` , height: `${height}px` ,objectFit: `cover`}} />
        <hr/>
        <div style={{height: `60px`}}>
          <p className={`pLimitLine_orderCard`}>{itemRow[lang][0].title}</p>
        </div>
        {gridButton(funcAddItemToCart_byOne,itemRow ,wording)}
    </Box>
  )
}

function gridButton(funcAddItemToCart_byOne,itemRow ,wording){
  return (
    <Grid container spacing={1}>
      <Grid item sm={12} xs={12} spacing={1}>
        {showPrice(itemRow.promotion , itemRow.price)}
      </Grid>
      <Grid item sm={12} xs={12} spacing={1} style={{fontSize: `15px`}}>
        <Button startIcon={<InfoIcon/>} 
          style={{backgroundColor: `#00465b` , color: `white` , width: `100%`}}
          component={Link}
          to={`/unit_product/${itemRow.url}`}
        >
          {`${wording.more}`}
        </Button>
        <Button startIcon={itemRow.Out_of_stock ? <RemoveShoppingCartIcon/> : <AddShoppingCartIcon/>} 
          disabled={itemRow.Out_of_stock}
          style={{backgroundColor: itemRow.Out_of_stock ? `rgb(15, 15, 15)` : `#514400` , color: `white`, width: `100%` ,marginTop: `3px`}}
          onClick={funcAddItemToCart_byOne.bind(this,itemRow)}
        >
          {itemRow.Out_of_stock ?  `${wording.out_of_stock}` : `${wording.add_to_cart}`}
        </Button>
      </Grid>
    </Grid>
  )
}

function gridButtonMoreItem(itemRow ,wording){
  return (
    <Grid container spacing={1}>
      <Grid item sm={12} xs={12} spacing={1}>
        {showPrice(itemRow.promotion , itemRow.price)}
      </Grid>
      <Grid item sm={12} xs={12} spacing={1} style={{fontSize: `15px`}}>
        <Button startIcon={<InfoIcon/>} 
          style={{backgroundColor: `#00465b` , color: `white` , width: `100%`}}
          component={Link}
          to={`/unit_product/${itemRow.url}`}
        >
          {`${wording.more}`}
        </Button>
      </Grid>
    </Grid>
  )
}

function showPrice(promo , price){
  var res = []
  if(promo !== price){
    res.push(
      <div>
        <p className={`pPricePromotionStyle`}>{`${convertIntToMoneyFormat(promo)} THB`}</p>
        <p className={`pPriceUnderPromotion`}>{`${convertIntToMoneyFormat(price)} THB`}</p>
      </div>
    )
  }else{
    res.push(
      <div>
        <p className={`pPricePromotionStyle`}>{`${convertIntToMoneyFormat(promo)} THB`}</p>
        <p></p>
      </div>
    )
  }
  return res;
}

function showPriceUnit(promo , price , sizeClass){
  var res = []
  if(promo !== price){
    res.push(
      <div>
        <p className={`pProductPricePromotionStyle${sizeClass}`}>{`${convertIntToMoneyFormat(promo)} THB`}</p>
        <p className={`pProductPriceUnderPromotion${sizeClass}`}>{`${convertIntToMoneyFormat(price)} THB`}</p>
      </div>
    )
  }else{
    res.push(
      <div>
        <p className={`pProductPricePromotionStyle${sizeClass}`}>{`${convertIntToMoneyFormat(promo)} THB`}</p>
        <p></p>
      </div>
    )
  }
  return res;
}

export {
    findItemInThisCat , findItemInThisCollection , showPriceUnit , queryItemCard ,queryItemCardMoreItem
}
