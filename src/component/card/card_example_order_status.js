import React , { Component , useState, useEffect } from 'react';
import Chip from '@material-ui/core/Chip';
import Card from '@material-ui/core/Card';

function card_example_order_status(){
    return (
        <Card boxShadow={3} className={`myCard_style`}>
            <h3 className={`myCard_style_title`}>#Example Status</h3>
            <Chip label={`STATUS: 1 ⌚`} style={{backgroundColor: `#c8e6c9`}} />
            <Chip label={`STATUS: 2 😃`} style={{backgroundColor: `#c8e6c9`}} />
            <Chip label={`STATUS: 3 🚚`} style={{backgroundColor: `#c8e6c9`}} />
            <Chip label={`STATUS: 1100 ❌`} style={{backgroundColor: `#ffccbc`}}/>
        </Card>
    )
}
export {
    card_example_order_status
}