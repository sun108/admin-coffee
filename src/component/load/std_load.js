import React , { Component } from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';

function StdLoad_standard(props){
    return (<div> <LinearProgress color="secondary" /> </div>)
}

export {
    StdLoad_standard
};