import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Radium, {StyleRoot} from 'radium';
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Hidden from '@material-ui/core/Hidden';
import { Link ,Router , browserHistory} from "react-router";
import { Link as ScrollLink , DirectLink, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'

//icon
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';

//materialize
import 'materialize-css';
import { Collapsible, CollapsibleItem } from 'react-materialize';

function collapsible_menu(listArrCollection , listArrCatProduct , listArrRelative , listArrUnitProduct , lang , funcClickLink ,onCloseDrawer){
    return (
      <Collapsible accordion >
        <CollapsibleItem 
          expanded={true}
          header="Category"
          icon={<ExpandMoreIcon/>}
          node="div"
        >
          <MenuList>
            {sideMenu_catagory(listArrCatProduct,listArrRelative,listArrUnitProduct,lang,onCloseDrawer)}
          </MenuList>
        </CollapsibleItem>
        <CollapsibleItem
          expanded={false}
          header="Collection"
          icon={<ExpandMoreIcon/>}
          node="div"
        >
          <MenuList>
            {sideMenu_collection(listArrCollection,listArrUnitProduct,lang,funcClickLink)}
          </MenuList>
        </CollapsibleItem>
      </Collapsible>
    )
}

function sideMenu_collection(listCollection,listUnit,lang,funcClickLink){
    var res = []
    listCollection.map(x=>{
      var idcode = x.idcode
      var checkIsColl = listUnit.filter(v=>v.collection === idcode && v.customer_view && x.customer_view)
      if(checkIsColl.length > 0){
        res.push(
          <MenuItem style={{ borderBottom: `0.5px solid #e0e0e0`}} onClick={funcClickLink.bind(this,`/collection_product/${x['url']}`)} >
            <p className={`pOverAllSideMenu`}>{x[lang]}</p>
            <ListItemIcon>
              <KeyboardArrowRightIcon fontSize="small" />
            </ListItemIcon>
          </MenuItem>
        )
      }
    })
    return res
  }
  
  function sideMenu_catagory(listCatagory,listRelation,listUnit,lang,onCloseDrawer){
    var res = []
    listCatagory.map(x=>{
      var idcode = x.idcode
      var checkIsCat = listRelation.filter(v=>v.idcode_cat === idcode && x.customer_view)
      if(checkIsCat.length > 0){
        res.push(
          <ScrollLink activeClass="active" to={x.idcode} spy={true} smooth={true} duration={700} offset={-100} onClick={onCloseDrawer}>
            <MenuItem style={{ borderBottom: `0.5px solid #e0e0e0`}}>
                <p className={`pOverAllSideMenu`}>{x[lang]}</p>
                <ListItemIcon>
                  <KeyboardArrowRightIcon fontSize="small" />
                </ListItemIcon>
            </MenuItem>
          </ScrollLink>
        )
      }
    })
    return res
  }

export {
    collapsible_menu
}