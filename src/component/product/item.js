import React , { Component , useState, useEffect } from 'react';
import VisibilitySensor from "react-visibility-sensor";
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Slide from '@material-ui/core/Slide';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import { shadows } from '@material-ui/system';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

function item_order_product(arrData , func_addItem , func_amountChange){
    return (
        <Box className='example1' boxShadow={3}>
            <p className={`myfont_content_std`} style={{textAlign: `left`}}>{arrData['item_name']}</p>
            <Grid container spacing={2}>
                <Grid item sm={4} xs={4}>
                    <img src={arrData.item_pic_assets} style={{width:`100%`}} />
                </Grid>
                <Grid item sm={8} xs={8}>
                    <p className={`myfont_content_detail`} style={{textAlign: `left`}}>
                    {arrData['item_detail']}
                    </p>
                </Grid>
                <Grid item sm={4} xs={4}>
                    <p className={`myfont_content_detail_price`} style={{textAlign: `left`,fontWeight:`bold`}}>
                    {`ราคา ${arrData['item_price']}`}
                    </p>
                    <p className={`myfont_content_detail`} style={{textAlign: `left`}}>
                    {`ปริมาตรสุทธิ ${arrData['item_quantity']} กรัม`}
                    </p>
                </Grid>
                <Grid item sm={3} xs={3}>
                    <p className={`myfont_content_detail`} style={{textAlign: `left`}}>
                    <TextField 
                        className='myTextfield_number' type="number" placeholder="0"
                        label="จำนวน" InputLabelProps={{ shrink: true,}}
                        ref={`amount_${arrData['idcode']}`}
                        key={`key_${arrData['idcode']}`}
                        variant="outlined" 
                        defaultValue={1}
                        onChange={(e) => func_amountChange(e,arrData['idcode'])}
                    />
                    </p>
                </Grid>
                <Grid item sm={3} xs={3}>
                    <IconButton
                        variant="contained"
                        color="primary"
                        style={{
                            backgroundColor: 'green' ,
                            display: 'inline',
                            borderRadius: `100%`,
                            alignItems: `center`,
                            justifyContent: `center`
                        }}
                        onClick={func_addItem.bind(this,arrData['idcode'])}
                    >
                    <AddShoppingCartIcon style={{color: `white`}}/>
                    </IconButton>
                </Grid>
            </Grid>
        </Box>
    )
}

function item_order_cart(){
    return (
    <TableRow>
        <TableCell component="th" scope="row">
            วูดู
        </TableCell>
        <TableCell align="right">กแหกแหกแ</TableCell>
        <TableCell align="right">กแหกแหกแ</TableCell>
        <TableCell align="right">กแหกแหกแ</TableCell>
        <TableCell align="right">กแหกแหกแ</TableCell>
    </TableRow>
    );
}

export {
    item_order_product,
    item_order_cart
}