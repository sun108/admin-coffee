import React , { Component } from 'react';

import Button from '@material-ui/core/Button';
import Create from '@material-ui/icons/Create';
import Update from '@material-ui/icons/Update';
import DeleteForever from '@material-ui/icons/DeleteForever'
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import UpIcon from '@material-ui/icons/KeyboardArrowUp';
import CancelIcon from '@material-ui/icons/Cancel';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import EditIcon from '@material-ui/icons/Edit';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import { Link } from "react-router";
import IconButton from '@material-ui/core/IconButton';
import LinkIcon from '@material-ui/icons/Link';

import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

function StdChkReadOnly_normal(props){
    return (
        <div>
        <FormControlLabel 
            disabled 
            control={<Checkbox checked={props.checked} 
            name="checkedE" />} label={props.label}/>
        </div>
    )
}

export {
    StdChkReadOnly_normal
};