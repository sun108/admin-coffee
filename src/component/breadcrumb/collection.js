import React , { Component , useState, useEffect } from 'react';
import { Link ,Router , browserHistory} from "react-router";
import Hidden from '@material-ui/core/Hidden';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Typography from '@material-ui/core/Typography';
import {funcClickLink} from '../../services/link/service_link'

function collection_bc(path1 ,path2){
  return (<Breadcrumbs aria-label="breadcrumb">
    <Link color="inherit" onClick={funcClickLink.bind(this,`/order`)} >
      {path1}
    </Link>
    <Hidden only={['xs']}>
      <Typography color="textPrimary">{path2}</Typography>
    </Hidden>
  </Breadcrumbs>)
}

export {
    collection_bc
}
