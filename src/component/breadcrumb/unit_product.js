import React , { Component , useState, useEffect } from 'react';
import { Link ,Router , browserHistory} from "react-router";
import Hidden from '@material-ui/core/Hidden';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Typography from '@material-ui/core/Typography';
import {funcClickLink} from '../../services/link/service_link'

function unit_product(path1 ,path2 ,link2 ,path3){
  return (<Breadcrumbs aria-label="breadcrumb">
    <Link color="inherit" onClick={funcClickLink.bind(this,`/order`)} >
      {path1}
    </Link>
    <Link color="inherit" onClick={funcClickLink.bind(this,link2)}>
      {path2}
    </Link>
    <Hidden only={['xs']}>
      <Typography color="textPrimary">{path3}</Typography>
    </Hidden>
  </Breadcrumbs>)
}

export {
  unit_product
}
