import React, { Component } from 'react';
import './css/home.css'

class carousel_home extends Component{
    //state = {pathPic: ""}
    render(){
        document.addEventListener('DOMContentLoaded', function() {
            const M = window.M;
            var elems = document.querySelectorAll('.carousel');
            var instances = M.Carousel.init(elems, { fullWidth: true });
        });
        const picture1 = "https://www.mydiscprofile.com/_images/homepage-free-personality-test.png"
        const picture2 = "https://cdn-7.nikon-cdn.com/Images/Learn-Explore/Photography-Techniques/2019/CA-Chris-Ogonek-Picture-Controls/Media/Chris-Ogonek-Picture-Controls-Vivid.low.jpg"
        const picture3 = "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__340.jpg"
        return(
            <div class="carousel carousel-slider">
                <a class="carousel-item" href="#one!"><img src={picture1} /></a>
                <a class="carousel-item" href="#two!"><img src={picture2} /></a>
                <a class="carousel-item" href="#tree!"><img src={picture3} /></a>
            </div>
        );
    }
}

export default carousel_home