import React , { Component , useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import FeedbackIcon from '@material-ui/icons/Feedback';
import Hidden from '@material-ui/core/Hidden';

function card(content){
    return (
        <Box className='myBoxCard' boxShadow={4}>
            {content}
        </Box>
    )
}

function card_std02(content){
    return (
        <Box className='myBoxCard02' boxShadow={4}>
            {content}
        </Box>
    )
}

function card_ourService01(img , title , content){
    return (
        <Box className='myBoxCard02' boxShadow={4}>
            <div class="grid">
                <figure class="effect-sadie">
                    {img}
                    <figcaption>
                    {title}
                    <div>
                        <center><p>{content}</p></center><br/>
                        <center><Button variant="contained" startIcon={<FeedbackIcon />} style={{color: `white` ,backgroundColor: `darkblue`}}>read more..</Button></center>
                    </div>
                    </figcaption>			
                </figure>
                <Hidden only={['sm','md','lg']}>
                    <center><Button variant="contained" startIcon={<FeedbackIcon />} style={{color: `white` ,backgroundColor: `darkblue`}}>read more..</Button></center>
                </Hidden>
            </div>
        </Box>
    )
}

export {
    card ,
    card_std02 ,
    card_ourService01 ,
}