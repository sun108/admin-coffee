import React , { Component , useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';

function payment_choice(selectedPayment , onClickPayment ,wording){
    const numGrid = 4
    const styleSelected = `rgb(29, 170, 0) 2px solid`
    return (
        <Box className='example1' boxShadow={4} style={{border: `black solid 0.5px`}}>
            <center><p className={`myfont_cart_modal_head`}>{`${wording.please_select_payment}`}</p></center>
            <Grid container spacing={2}>
                <Grid item sm={numGrid} xs={numGrid}>
                    <Box className='box_selected' boxShadow={3} 
                        style={{border: selectedPayment === `destination` ? styleSelected : `unset`}} 
                        onClick={onClickPayment.bind(this,`destination`)}
                    >
                        <center><p className={`myfont_content_payment`}>{`${wording.select_payment.destination}`}</p></center>
                        <center>{showIconOnSelected( selectedPayment === `destination` ? true : false)}</center>
                    </Box>
                </Grid>
                {/* <Grid item sm={numGrid} xs={numGrid}>
                    <Box className='box_selected' boxShadow={3} 
                        style={{border: selectedPayment === `123` ? styleSelected : `unset`}}
                        onClick={onClickPayment.bind(this,`123`)}
                    >
                        <center><p className={`myfont_content_payment`}>{`123 service`}</p></center>
                        <center>{showIconOnSelected( selectedPayment === `123` ? true : false)}</center>
                    </Box>
                </Grid> */}
                <Grid item sm={numGrid} xs={numGrid}>
                    <Box className='box_selected' boxShadow={3} 
                        style={{border: selectedPayment === `tranfer` ? styleSelected : `unset`}}
                        onClick={onClickPayment.bind(this,`tranfer`)}
                    >
                        <center><p className={`myfont_content_payment`}>{`${wording.select_payment.tranfer}`}</p></center>
                        <center>{showIconOnSelected( selectedPayment === `tranfer` ? true : false)}</center>
                    </Box>
                </Grid>
            </Grid>
        </Box>
    )
}

function payment_slip(selectedAttact , onClickAttact , funcUpload , imgSrc , canUseThisUpload ,wording){
    const numGrid = 6
    const styleSelected = `rgb(29, 170, 0) 2px solid`
    return (
        <div>
        <Box className='example1' boxShadow={4} style={{border: `black solid 0.5px`}}>
            <center><p className={`myfont_cart_modal_head`}>{`${wording.please_upload_slip}`}</p></center>
            <Grid container spacing={2}>
                <Grid item sm={numGrid} xs={numGrid}>
                    <Box className='box_upload_slip_selected' boxShadow={3} 
                        style={{border: selectedAttact ? styleSelected : `unset`}} 
                        onClick={onClickAttact.bind(this,true)}
                    >
                        <center><p className={`myfont_content_payment`}>{`${wording.upload_slip_now}`}</p></center>
                        <center>{showIconOnSelected( selectedAttact ? true : false)}</center>
                    </Box>
                </Grid>
                {/* <Grid item sm={numGrid} xs={numGrid}>
                    <Box className='box_upload_slip_selected' boxShadow={3} 
                        style={{border: !selectedAttact ? styleSelected : `unset`}}
                        onClick={onClickAttact.bind(this,false)}
                    >
                        <center><p className={`myfont_content_payment`}>แนบภายหลัง</p></center>
                        <center>{showIconOnSelected( !selectedAttact ? true : false)}</center>
                    </Box>
                </Grid> */}
            </Grid>
            { selectedAttact ? showAttactFile(funcUpload , imgSrc ,canUseThisUpload ,wording) : null}
        </Box>
        </div>
    )
}

function showIconOnSelected(selected){
    var element = <center><RadioButtonUncheckedIcon/></center>
    if(selected){
        element = <center><CheckCircleIcon style={{color: `green`}}/></center>
    }
    return element;
}

function showAttactFile(funcUpload , imgSrc , canUseThisUpload ,wording){
    return (
        <div>
            <form onSubmit={funcUpload}>
                <Button
                    variant="contained"
                    component="label"
                >
                {`${wording.attach_proof_money_transfer}`}
                    <input type="file" ref="file" name="attachFile" multiple="false" onChange={funcUpload} />
                </Button>
                {/* <button variant="contained" color="primary" component="span" type="submit">Upload</button> */}
            </form>
            <br/>
            {imgFileSlip(imgSrc , canUseThisUpload)}
            {/* <img src={imgSrc} style={{width: '200px' , height: '250px' , maxWidth: `100%` , border: `black solid 0.5px` , boxShadow: `0px 0px 10px .5px` ,display: imgSrc === null || !canUseThisUpload ? `none` : `unset`}} /> */}
            {<p style={{display: canUseThisUpload ? `none` : `unset` , color: `red`}}>{`${wording.warning_slip}`}</p>}
        </div>
    )
}
function imgFileSlip(imgSrc , canUseThisUpload){
    return (<img src={imgSrc} style={{width: '200px' , height: '250px' , maxWidth: `100%` , border: `black solid 0.5px` , boxShadow: `0px 0px 10px .5px` ,display: imgSrc === null || !canUseThisUpload ? `none` : `unset`}} />)
}

function getStringPayment(type , tranfSlip , wording){
    var res = ``
    switch(type){
        case `destination`:
            res = `${wording.destination}`
        break;
        case `123`:
            res = `จ่ายผ่าน 123 service`
        break;
        case `tranfer`:
            if(tranfSlip){
                res = `${wording.tranfer}`
            }else{
                res = `โอนเงิน (ยังไม่ได้โอนเงิน)`
            }
        break;
    }
    return res
}

export {
    payment_choice, payment_slip , getStringPayment ,imgFileSlip
}