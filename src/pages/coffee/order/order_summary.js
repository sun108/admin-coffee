import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Radium, {StyleRoot} from 'radium';
import { Link } from "react-router";
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux'
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField';
import ReCAPTCHA from "react-google-recaptcha";
import CircularProgress from '@material-ui/core/CircularProgress';
// import MaskedInput from 'react-maskedinput'
// import MaskedInput from 'react-text-mask';
// import InputMask from "react-input-mask";

//animation
import { bounce , fadeInDown,  fadeInUp ,fadeOutDown} from 'react-animations';

//component
import {line_title} from '../../../component/line/line'
import {StdToastMz_warning} from '../../../component/toast/std_toast'
import {payment_choice , payment_slip , getStringPayment ,imgFileSlip} from '../../../component/box/card_payment'
import {
  query_tableInCard_unitCustomerDiscount,
  query_tableInCard_unitCustomerShipping,
  query_tableInCard_unitCustomerGiftFree,
  query_tableInCard_unitCustomerBuy ,
  query_tableInCard_unitCustomerTotalPrice
} from '../../../component/table/order_summary'

//style
import {styleCode} from '../../../style/order_summery'
import useStyles  from '../../../style/app.js'

//materialize
import 'materialize-css';

//react-responsive-carousel
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

//service
import {convertIntToMoneyFormat,getConsoleError,generateString_string} from '../../../services/std_service/getString'
import {add_arrItem} from '../../../services/mockup/arrItem'
import {customer_start_order} from '../../../services/customer/order'
import FetchData from '../../../services/fetch_data/fetch'
import {get_ip_client} from '../../../services/customer/get_info_cleint'
import {funcClickLink} from '../../../services/link/service_link'
import {
  calpromotion_giftFree ,
  calPromotion_shipping ,
  calCustomerBuy ,
  calShipping_toPrice ,
  calTotalPrice
} from '../../../services/promotion/calPromotion'

import {checkStringNotNullAndNotEmpty} from '../../../services/std_service/checkIsEmp'

var defSelectedPayment = 'destination'
//destination ,tranfer ,123

class Order_summary extends Component {
  constructor() {
    super();
    this.state = {
      load: false,arrIMastertem:[],activeStep: 0, setActiveStep: null,
      total_price: 0 , total_order: 0 , summary_loading: false,
      selectedPayment: `destination`, selectedAttactNow: true, imgSrc: null, canUseThisUpload: true,
      waringTextFieldNull: false, waringTelephoneNumber: false, waringTextFieldStrong: ``, formDataUploadImage: null, fileImage: null,
      cus_name:null,cus_tel: null,cus_email:``,cus_line:``, cus_address:null,
      rec_value: null , rec_expire: false ,rec_callback: null , rec_canSubmit: false,
      clientIP: null , clientBrowser: null , clientColo: null , submitShowLoad: false,
      arrData: [], wording: [],
      listArrPromotionType:[], listArrRelativeDelivery:[] ,listArrRelativePrmotion: [], listArrUnit:[] ,listArrSysDelivery: [],
      listUnitCustomerBuy: [],listUnitCustomerGiftFree: [], listShippingCustomer: [], listDiscountCustomer: [],
      afterSubmitToConfirmOTPMode: true,
    }
    this.clickUpload = this.clickUpload.bind(this);
    this._reCaptchaRef = React.createRef();
    this.duration = 1;
  }
  componentDidMount() {
    this.start_page(this.props);
  }
  // componentWillReceiveProps(props) {
  //   this.start_page(props);
  // }
  // componentWillUnmount() {
  //   if (this.vantaEffect) this.vantaEffect.destroy()
  // }
  start_page = async(props)=>{
    var wording = await props.wording
    this.setState({load : true ,wording: wording})

    var url =null, data= [], arr = queryDataCart(props.arrOrder);
    if(arr.toString().trim() !== ``){
      var dataClient = await get_ip_client()
      url = `${process.env.REACT_APP_URL_API}query?relative_promotion=all`
      try{ data.push ({"relative_promotion":await FetchData(url,'relative_promotion',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}

      url = `${process.env.REACT_APP_URL_API}query?promotion_type=all`
      try{ data.push ({"promotion_type":await FetchData(url,'promotion_type',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}
    
      url = `${process.env.REACT_APP_URL_API}query?relative_delivery=all`
      try{ data.push ({"relative_delivery":await FetchData(url,'relative_delivery',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}
      
      url = `${process.env.REACT_APP_URL_API}query?unit_product=all`
      try{ data.push ({"unit_product":await FetchData(url,'unit_product',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}
    
      url = `${process.env.REACT_APP_URL_API}query?system_delivery=all`
      try{ data.push ({"system_delivery":await FetchData(url,'system_delivery',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}
      
      try{
        await this.setState({ 
          arrData: props.arrOrder,
          clientIP: dataClient[0],clientBrowser: dataClient[1],clientColo : dataClient[2],
          // arrIMastertem: data.find(x=>x['unit_product']).unit_product[0],
          listArrPromotionType: data.find(x=>x['promotion_type']).promotion_type[0],
          listArrRelativeDelivery: data.find(x=>x['relative_delivery']).relative_delivery[0],
          listArrRelativePrmotion: data.find(x=>x['relative_promotion']).relative_promotion[0],
          listArrUnit: data.find(x=>x['unit_product']).unit_product[0],
          listArrSysDelivery: data.find(x=>x['system_delivery']).system_delivery[0],
        })
      }catch(e){getConsoleError(e)}

      await this.packData();
      this.setState({load : false})
    }else{
      funcClickLink(`/`)
    }
  }
  selected_payment =async(payment)=>{
    if(payment !== this.state.selectedPayment){
      await this.setState({ selectedPayment : payment , imgSrc : null })
      await this.packData();
      StdToastMz_warning(`${this.state.wording[this.props.lang].wording.waring_total_price_has_change}`)  
    }else{
      await this.setState({ selectedPayment : payment })
    }
  }
  selected_attact =(selected)=>{
    if(!selected){
      this.setState({ selectedAttactNow : false , imgSrc : null })
    }else{
      this.setState({ selectedAttactNow : true })
    }
  }
  packData =async()=>{
    await this.setState({ summary_loading : true })
    var listTable = [] , lang = this.props.lang , data = null , priceTrue = 0 ,numb = 0, total = 0, amount = 0 ,idcode_gen = ``,total_price = 0 ,extraCashPercent = false
    var listUnitCustomerBuy= [],listUnitCustomerGiftFree= [], listShippingCustomer= [], listDiscountCustomer= []
    this.state.arrData.array_cart.map(x=>{
      data = `${x.replace(`${process.env.REACT_APP_PREFIX_ORDER}`,``)}`
      var findUnit = this.state.listArrUnit.filter(c=>c[`idcode`] === data && c[`customer_view`] && !c[`Out_of_stock`])
      if(findUnit.length > 0){
        var newList = {}
        idcode_gen = generateString_string(50)
        priceTrue = parseFloat(findUnit[0].price)
        if(findUnit[0].price !== findUnit[0].promotion){
          priceTrue = parseFloat(findUnit[0].promotion)
        }
        newList.idcode_unit = data
        newList.inItem = findUnit[0]
        newList.priceUse = parseFloat(priceTrue)
        amount = parseInt(this.state.arrData.amount_cart[numb])
        newList.amount = amount
        total = (parseFloat(priceTrue) * parseFloat(this.state.arrData.amount_cart[numb]))
        newList.total = total
        newList.idcode_promo = idcode_gen
        var findInpromotion_giftFree = this.state.listArrRelativePrmotion.filter(k=>k[`idcode_unit`] === data && k[`active`] === true)
        if(findInpromotion_giftFree.length > 0){
          var getPromo = calpromotion_giftFree(findInpromotion_giftFree[0] , priceTrue , amount , total ,this.state.listArrUnit ,idcode_gen)
          listUnitCustomerGiftFree.push( getPromo )
        }
        numb++;
        listUnitCustomerBuy.push(newList)
      }
    });
    var total_customerBuy = calCustomerBuy(listUnitCustomerBuy)
    this.state.selectedPayment === `destination` ? extraCashPercent = true : extraCashPercent = false
    listShippingCustomer = calShipping_toPrice( 
      this.state.listArrRelativePrmotion , 
      this.state.listArrSysDelivery , 
      this.state.listArrRelativeDelivery ,
      listUnitCustomerBuy ,
      extraCashPercent , 
      this.state.wording
    )
    listDiscountCustomer = calPromotion_shipping(
      this.state.listArrRelativePrmotion ,
      this.state.listArrSysDelivery , 
      this.state.listArrRelativeDelivery ,
      listUnitCustomerBuy ,
      total_customerBuy ,
      listShippingCustomer
    )
    total_price = calTotalPrice(listUnitCustomerBuy ,listShippingCustomer ,listDiscountCustomer)
    await this.setState({
      listUnitCustomerBuy: listUnitCustomerBuy,
      listUnitCustomerGiftFree: listUnitCustomerGiftFree,
      listShippingCustomer: listShippingCustomer,
      listDiscountCustomer: listDiscountCustomer,
      total_price: total_price ,
      summary_loading : false
    })
  }

  _onChangeTelephone = (e) => {
    this.setState({[e.target.name]: e.target.value})
  }

  async clickUpload(e){
    e.preventDefault();
    var file = this.refs.file.files[0];
    var reader = new FileReader();

    if(file !== [] && file !== undefined && file !== `undefined`){
      if(file.size <= 3000000 && file.type === "image/jpeg"){
        var url = reader.readAsDataURL(file);

        const formData = new FormData();
        await formData.append('attachFile',file);

        reader.onloadend = function (e) {
          this.setState({
              imgSrc: [reader.result] , canUseThisUpload: true ,
              fileImage: file
          })
        }.bind(this);
      }else{
        this.setState({ canUseThisUpload: false, imgSrc: null, fileImage: null })
      }
    }else{
      this.setState({ canUseThisUpload: false, imgSrc: null, fileImage: null  })
    }
  }

  handleNext = () => {
    if(this.state.activeStep === 0 && this.state.wording.th !== undefined){
      var err = false , wordingErr = `` ,wording = this.state.wording[this.props.lang]
      if(checkStringNotNullAndNotEmpty(this.state.cus_name) && checkStringNotNullAndNotEmpty(this.state.cus_tel) && checkStringNotNullAndNotEmpty(this.state.cus_address) && this.state.cus_tel.toString().length >= 10){
        if(this.state.selectedPayment === `tranfer`){
          if(this.state.selectedAttactNow){
            if(this.state.imgSrc !== null && this.state.canUseThisUpload){
              err = false
            }else{
              err = true
              wordingErr = `${wording.summary_page.please_upload_image_payment}`
            }
          }
        }
        if(!err){
          this.setState({activeStep: this.state.activeStep+1 , waringTextFieldNull: false , waringTextFieldStrong: ``})
          // this.myRef.current.scrollTo(0, 0);
          window.scrollTo(0, 0);
        }else{
          alert(wordingErr)
        }
      }else{
        this.setState({waringTextFieldNull: true , waringTextFieldStrong: `${wording.summary_page.please_fill_all_data}`})
      } 
    }
  };
  handleBack = () => {
      this.setState({activeStep: this.state.activeStep-1})
  };
  handleReset = () => {
      this.setState({activeStep: 0})
  };
  txtFOnChange=(value,statename)=>{
    this.setState({ [statename]:value })
    // if(value.trim() !== "" && value !== null && value.length > 0){
    //   this.setState({ [statename]:value })
    // }
  }
  telephoneOnChnageNumber= async(value,statename)=>{
    if(value.toString().length >= 10){
      await this.setState({ [statename]:value , waringTelephoneNumber: false })
    }else{
      await this.setState({ [statename]:value , waringTelephoneNumber: true })
    }
  }
  checkReCatchaBeforeSubmit =()=>{
    if(this.state.rec_expire || this.state.rec_value == null){
      this.ReCAPTCHA_asyncScriptOnLoad()
    }else{
      if(this.state.selectedPayment === `destination`){
        this.packObjAndSend()
      }else{
        this.packObjAndSend()
      }
    }
  }
  changeButtonSubmitToLoadShow =(set)=>{
    this.setState({ submitShowLoad: set })
  }
  packObjAndSend=async()=>{
    var res = {
      "name":`${this.state.cus_name}`,
      "tel":`${this.state.cus_tel}`,
      "email":`${this.state.cus_email}`,
      "line":`${this.state.cus_line}`,
      "address":`${this.state.cus_address}`,
      "select_payment":`${this.state.selectedPayment}`,
      "img":`${this.state.imgSrc}`,
      "fileImage":this.state.fileImage,
      "cartList":this.state.listUnitCustomerBuy,
      "shippingList": this.state.listShippingCustomer,
      "discountList":this.state.listDiscountCustomer,
      "gistFreeList":this.state.listUnitCustomerGiftFree,
      "ip":this.state.clientIP,
      "browser":this.state.clientBrowser,
      "colo":this.state.clientColo,
      "total_price":this.state.total_price,
    }
    customer_start_order(res , this.changeButtonSubmitToLoadShow , this.clearItemInCart)
  }
  ReCAPTCHA_handleChange =(value)=>{
    if(value !== null){
      this.setState({ rec_value: value , rec_expire: false , rec_canSubmit: true })
    }else{
      this.setState({ rec_expire: true , rec_canSubmit: false })
    }
  }
  ReCAPTCHA_asyncScriptOnLoad = () => {
    this.setState({ rec_callback: "called!" });
  };
  clearItemInCart =()=>{
    this.props.actionReducer('clearItemInCart',{"arrItem":null , "amount":null})
  }
  getSteps() {
    if(this.state.wording.th !== undefined){
      var wording = this.state.wording[this.props.lang]
      return [
          `${wording.summary_page.step01}`,
          `${wording.summary_page.step02}`,
      ];
    }
  }
  genResultCash(){
    if(this.state.wording.th !== undefined){
      if(this.state.summary_loading){
        return (<LinearProgress color="secondary" />)
      }else{
        var lang = this.props.lang
        var wording = this.state.wording
        return (
          <div>
            <center>
              <Box className='example1' boxShadow={4} style={{border: `black solid 0.5px`}}>
                <center><h4><p className={`myfont_cart_modal_head`}>{wording[lang].summary_page.summary_payment}</p></h4></center>
                <hr/>
                {query_tableInCard_unitCustomerBuy(lang,this.state.listUnitCustomerBuy ,wording[lang])}
                {query_tableInCard_unitCustomerGiftFree(lang,this.state.listUnitCustomerGiftFree ,wording[lang])}
                {query_tableInCard_unitCustomerShipping(lang,this.state.listShippingCustomer ,wording[lang])}
                {query_tableInCard_unitCustomerDiscount(lang,this.state.listDiscountCustomer ,wording[lang])}
                {query_tableInCard_unitCustomerTotalPrice(lang,this.state.total_price,wording[lang])}
              </Box>
            </center>
          </div>
        )
      }
    }
  }
  getStepContent(stepIndex) {
    if(this.state.wording.th !== undefined){
      var wording = this.state.wording[this.props.lang]
      switch (stepIndex) {
        case 0:
          return (<div>
            <Grid container space={2}>
              <Grid item xs={12} md={6}>
                {this.genResultCash()}
              </Grid>
              <Grid container item xs={12} md={6}>
                <Grid item xs={12}>
                  <center>{payment_choice(this.state.selectedPayment , this.selected_payment ,wording.summary_page)}</center>
                  <center>{
                    this.state.selectedPayment === `tranfer` ? payment_slip(
                      this.state.selectedAttactNow , 
                      this.selected_attact , 
                      this.clickUpload , 
                      this.state.imgSrc ,
                      this.state.canUseThisUpload ,
                      wording.summary_page
                    ) : null}</center>
                </Grid>
                <Grid item xs={12}>
                  <center>
                    <Box className='example1' boxShadow={4} style={{border: `black solid 0.5px`}}>
                      <center><p className={`myfont_cart_modal_head`}>{wording.summary_page.please_enter_your_recieve}</p></center>
                      <Grid container spacing={1}>
                        <Grid item sm={12} xs={12} spacing={2}>
                          <TextField className={`fieldInput`}
                              placeholder={wording.summary_page.hint_consignee_name} required error={this.state.waringTextFieldNull}
                              label={wording.summary_page.consignee_name} InputLabelProps={{ shrink: true,}} value={this.state.cus_name}
                              variant="outlined" onChange={(e)=>this.txtFOnChange(e.target.value,`cus_name`)}
                          />
                        </Grid>
                        <Grid item sm={6} xs={6} spacing={2}>
                          {/* <label>
                            {`${wording.summary_page.tel} *`}:{' '}
                            <MaskedInput required mask="111-111-1111" name="tel" size="10" variant="outlined"
                            onChange={(e)=>this.txtFOnChange(e.target.value,`cus_tel`)} placeholder="089-999-xxxx"/>
                          </label> */}
                          <TextField className={`fieldInput`}
                              placeholder={wording.summary_page.hint_tel} required error={this.state.waringTextFieldNull || this.state.waringTelephoneNumber ? true : false}
                              label={wording.summary_page.tel} InputLabelProps={{ shrink: true,}} value={this.state.cus_tel}
                              variant="outlined" onChange={(e)=>this.telephoneOnChnageNumber(e.target.value,`cus_tel`)}
                              type='number'
                          >
                            {/* <InputMask mask="(0)999 999 99 99" maskChar=" " /> */}
                          </TextField>
                          {/* <InputMask mask="(+66)99-999-9999" maskChar=" " /> */}
                        </Grid>
                        <Grid item sm={6} xs={6} spacing={2}>
                          <TextField className={`fieldInput`}
                              placeholder={wording.summary_page.hint_id_line}  value={this.state.cus_line}
                              label={wording.summary_page.id_line} InputLabelProps={{ shrink: true,}}
                              variant="outlined" onChange={(e)=>this.txtFOnChange(e.target.value,`cus_line`)}
                          />
                        </Grid>
                        <Grid item sm={12} xs={12} spacing={2}>
                          <TextField className={`fieldInput`}
                              placeholder={wording.summary_page.hint_email} value={this.state.cus_email}
                              label={wording.summary_page.email} InputLabelProps={{ shrink: true,}}
                              variant="outlined" onChange={(e)=>this.txtFOnChange(e.target.value,`cus_email`)}
                          />
                        </Grid>
                        <Grid item sm={12} xs={12} spacing={2}>
                          <TextField className={`fieldInput`} 
                            required error={this.state.waringTextFieldNull} helperText={this.state.waringTextFieldStrong}
                            placeholder={wording.summary_page.hint_address} multiline rows={4}
                            label={wording.summary_page.address} InputLabelProps={{ shrink: true,}} value={this.state.cus_address}
                            variant="outlined" onChange={(e)=>this.txtFOnChange(e.target.value,`cus_address`)}
                          />
                        </Grid>
                      </Grid>
                    </Box>
                  </center>
                </Grid>
              </Grid>
            </Grid>
          </div>)
          break;
        case 1:
          return (
            <div ref={this.myRef}>
              <Grid container space={2}>
                <Grid item xs={12} md={6}>
                  <center>
                    <Box className='example1' boxShadow={4} style={{border: `black solid 0.5px`}}>
                      <center><h4><p className={`myfont_cart_modal_head`}>{`${wording.summary_page.delivery_and_payment_summary}`}</p></h4></center>
                      <p className={`myfont_content_result`}>{`${wording.summary_page.consignee_name}: ${this.state.cus_name}`}</p>
                      <p className={`myfont_content_result`}>{`${wording.summary_page.tel}: ${this.state.cus_tel}`}</p>
                      <p className={`myfont_content_result`}>{`${wording.summary_page.address}: ${this.state.cus_address}`}</p>
                      <p className={`myfont_content_result`}>{`${wording.summary_page.email}: ${this.state.cus_email}`}</p>
                      <p className={`myfont_content_result`}>{`${wording.summary_page.id_line}: ${this.state.cus_line}`}</p>
                      <p className={`myfont_content_result`}>{`${wording.summary_page.payment_method}: ${getStringPayment(this.state.selectedPayment , this.state.canUseThisUpload , wording.summary_page.select_payment)}`}</p>
                      <center>
                        {imgFileSlip(this.state.imgSrc , this.state.canUseThisUpload)}
                      </center>
                    </Box>
                  </center>
                </Grid>
                <Grid item xs={12} md={6}>
                  {this.genResultCash()}
                </Grid>
                <Grid item xs={12} md={12}>
                  <br/>
                  <center>
                    <ReCAPTCHA
                      style={{ display: "inline-block" }}
                      theme="light"
                      ref={this._reCaptchaRef}
                      sitekey={process.env.REACT_APP_RECAPCHA_SITE}
                      onChange={this.ReCAPTCHA_handleChange}
                      asyncScriptOnLoad={this.ReCAPTCHA_asyncScriptOnLoad}
                    />
                  </center>
                  <center>
                    <Button
                      className={`myfont_cart_modal_head`}
                      variant="contained"
                      component="label"
                      style={{backgroundColor: `green` , color: `white` , display: (this.state.rec_canSubmit && !this.state.submitShowLoad) ? `unset` : `none`}}
                      onClick={ this.checkReCatchaBeforeSubmit.bind(this) }
                    >
                        {`${wording.summary_page.submit}`}
                    </Button>
                    <div style={{display: this.state.submitShowLoad ? `unset` : `none` }}>
                      <CircularProgress color="secondary" />
                    </div>
                  </center>
                  <hr/>
                </Grid>
              </Grid>
          </div>
          )
          break;
        default:
          return 'Unknown stepIndex';
          break;
      }
    }
  }
  
  render(){
    const load = <LinearProgress color="secondary" />
    const { classes, theme } = this.props;
    if(!this.state.load && this.state.wording.th !== undefined){
      var wording = this.state.wording[this.props.lang]
        var styleFx = {
            title: {
              animation: 'x 3s',
              animationName: Radium.keyframes(fadeInDown, `fadeOutDown`)
            },
            lineUnder: {
              animation: 'x 5s',
              animationName: Radium.keyframes(fadeInUp, `fadeOutDown`)
            }
        }
        var activeStep = this.state.activeStep
        var steps = this.getSteps()
        const variableA = (
          <div style={{marginBottom: `100px`}}>
            <div>
              {activeStep === steps.length ? (
                <div>
                  {`All steps completed`}
                  <Button onClick={this.handleReset}>Reset</Button>
                </div>
              ) : (
                <div>
                  <Stepper activeStep={activeStep} alternativeLabel>
                    {steps.map((label) => (
                    <Step key={label}>
                        <StepLabel>{label}</StepLabel>
                    </Step>
                    ))}
                  </Stepper>
                  {this.getStepContent(activeStep)}
                  <br/>
                  <Stepper activeStep={activeStep} alternativeLabel>
                    {steps.map((label) => (
                    <Step key={label}>
                        <StepLabel>{label}</StepLabel>
                    </Step>
                    ))}
                  </Stepper>
                  <center>
                    <div style={{ display: this.state.showBtnNext}}>
                      <Button
                        disabled={activeStep === 0}
                        onClick={this.handleBack}
                        className={classes.backButton}
                        style={{backgroundColor: `red` , color: `white` , display: activeStep === 0 ? `none` : `unset`}}
                      >
                        Back
                      </Button>
                      <Button variant="contained" color="primary" onClick={this.handleNext} style={{display: activeStep === steps.length - 1 ? 'none' : 'unset'}}>
                        {activeStep === steps.length - 1 ? '' : 'Next'}
                      </Button>
                      </div>
                  </center>
              </div>
              )}
            </div>
          </div>
        )
        return (
        <React.Fragment>
            <style>{styleCode}</style>
            <StyleRoot style={styleFx.title}>
                <center><h3 className={`myfont_content_headtitle`}>{wording.menu.order_summary}</h3></center>
            </StyleRoot>
            <StyleRoot style={styleFx.lineUnder}>
                {line_title(this.state.w)}
            </StyleRoot>
            <div>{variableA}</div>
        </React.Fragment>)
    }else{
      return (<div>{load}</div>)
    }
  }
}

function queryDataCart(dataArray){
  var res = ``
  dataArray.array_cart.map(x=>{
    res = res + `${x.replace(`${process.env.REACT_APP_PREFIX_ORDER}`,``)},`
  })
  return res
}

Order_summary.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
  window: PropTypes.func,
};
function mapStateToProps(state) {
  return {
    lang: state.page_getLangToState,
    arrOrder : state.func_actionCart,
    wording: state.getWordingStringThis
  }
}
function mapDispatchToProps(dispatch) {
  return {
    actionReducer: (type,payload) => {
      dispatch({type: type,payload:payload})
    }
  }  
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(useStyles, { withTheme: true })(Order_summary))