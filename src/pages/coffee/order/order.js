import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Radium, {StyleRoot} from 'radium';
import { Link } from "react-router";
import { connect } from 'react-redux'
import VisibilitySensor from "react-visibility-sensor";
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Slide from '@material-ui/core/Slide';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import { shadows } from '@material-ui/system';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';

//animation
import { bounce , fadeInDown,  fadeInUp ,fadeOutDown} from 'react-animations';
import ReactVivus from 'react-vivus';

import svg from '../component/svg/resource/hi.svg';

//component
import {line_title ,line_order_title} from '../../../component/line/line'
import {StnBtnIcon_plus} from '../../../component/button/std_button'
import {item_order_product} from '../../../component/product/item'
import {StdToastMz_error} from '../../../component/toast/std_toast'

//style
import styleCode from '../../../style/menu'
import useStyles  from '../../../style/app.js'

//icon
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MenuIcon from '@material-ui/icons/Menu';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';

//materialize
import 'materialize-css';
import { Collapsible, CollapsibleItem } from 'react-materialize';

//layout
import Nav_sideBar from '../../../layouts/nav_SideBar/profile.js'

//react-responsive-carousel
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

//service
import {add_arrItem} from '../../../services/mockup/arrItem'

class Order extends Component {
  constructor() {
    super();
    this.state = {
      open: true,mobileOpen: false,lang: null,load: false,
      arrCart:[],arrItem:[],arrAmount:[],
      library: 'Radium',
      mode: null , w: null , h: null,
      setBackDrop: false
    }
    // this.addItemToCart = this.addItemToCart.bind(this);
    this.duration = 1;
  }
  onChangeNumber(e){
    const re = /^[0-9\b]+$/;
    if (e.target.value === '' || re.test(e.target.value)) {
       this.setState({value: e.target.value})
    }
  }
  componentDidMount() {
    this.start_page();
  }
  componentWillUnmount() {
    if (this.vantaEffect) this.vantaEffect.destroy()
  }
  start_page= async()=>{
    this.setState({load:true})

    var w = window.innerWidth , h = window.innerHeight , mode = null
    w > h ? mode = 'desktop' : mode = 'mobile'
    this.setState({mode: mode ,w: w ,h: h})
    this.setDataPack();
    
    this.setState({load:false})
  }

  setDataPack = async()=>{
    var arrData = await add_arrItem();
    this.setState({ arrItem: arrData })
  }

  addItemToCart =async(idcode)=>{
    // console.log(idcode)
    var itemFind = this.state.arrAmount.filter(x=>x['idcode'] === idcode);
    var findItemState = this.state.arrItem.filter(x=>x['idcode'] === idcode);
    if(itemFind.length > 0 && findItemState.length > 0){
      // console.log(itemFind[0].amount);
      this.props.actionReducer('addCart',{"arrItem":findItemState[0] , "amount":itemFind[0].amount})
    }else{
      StdToastMz_error('กรุณาใส่จำนวนสินค้าด้วยค่ะ')
    }
  }

  onChangeInputAmount =(event,idcode)=>{
    var amount = event.target.value
    var arrAmount = this.state.arrAmount;
    var itemFind = this.state.arrItem.filter(x=>x['idcode'] === idcode);
    if(itemFind.length>0){
      var findStateAmount = this.state.arrAmount.filter(x=>x['idcode'] === idcode);
      if(findStateAmount.length > 0){
        findStateAmount[0].amount = amount
      } else {
        arrAmount.push({"idcode":idcode , amount:amount})
      }
    }
    this.setState({ arrAmount : arrAmount })
    // console.log(this.state.arrAmount)
  }
  
  render(){
    const load = <LinearProgress color="secondary" />
    const { classes, theme } = this.props;
    const styleCode =(".carousel{height:unset}")
    const style = `.txt{ color: white ;  } .pic{ width: 30% }`
    const ex = `.example1 { 
      border: 0px solid black; 
      padding: 10px; 
      margin-bottom: 10px;
      border-radius: 10px; 
      width: 95%;
    }
    .MuiInputBase-input{
      border-bottom: unset !important;
      padding-left: 10px !important;
      padding-right: 10px !important;
      text-align: end !important;
  }`
    
    if(this.state.load){
      return (<div>{load}</div>)
    }else{
        var styleFx = {
            title: {
              animation: 'x 3s',
              animationName: Radium.keyframes(fadeInDown, `fadeOutDown`)
            },
            lineUnder: {
                animation: 'x 5s',
                animationName: Radium.keyframes(fadeInUp, `fadeOutDown`)
            }
        }
        return (<React.Fragment>
            <style>{ex}</style>
            <style>{ styleCode }</style>
            <StyleRoot style={styleFx.title}>
                <center><h3 className={`myfont_content_headtitle`}>สั่งซื้อสินค้า</h3></center>
            </StyleRoot>
            <StyleRoot style={styleFx.lineUnder}>
                {line_title(this.state.w)}
            </StyleRoot>
            <br/>
            <div>
              {line_order_title(
                `ดูแลผิวหน้า`
              )}
              <center>
                {this.state.arrItem.map(x=>
                  item_order_product(x,this.addItemToCart,this.onChangeInputAmount)
                )}
              </center>
              {line_order_title(
                `ทำความสะอาดผิวหน้า`
              )}
              {line_order_title(
                `มาส์กหน้า`
              )}
              {line_order_title(
                `สบู่และเครื่องอาบน้ำ`
              )}
              {line_order_title(
                `หมอนและอื่นๆ`
              )}
            </div>
        </React.Fragment>)
    }
  }
}

Order.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
  window: PropTypes.func,
};
function mapStateToProps(state) {
  return {
    lang: state.page_getLangToState,
    arrCart: state.func_actionCart
  }
}
function mapDispatchToProps(dispatch) {
  return {
      actionReducer: (type,payload) => {
        dispatch({type: type,payload:payload})
      }
  }    
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(useStyles, { withTheme: true })(Order))