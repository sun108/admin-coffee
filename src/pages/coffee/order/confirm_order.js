import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Divider from '@material-ui/core/Divider';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux'
import LinearProgress from '@material-ui/core/LinearProgress';
import Grid from '@material-ui/core/Grid';

import useStyles  from '../../../style/app.js'

//component

//service
import {adminSendToServer_confirmOrderPayment} from '../../../services/admin/confirm_order_payment' 
import {funcClickLink} from '../../../services/link/service_link'

//icon
import CreateIcon from '@material-ui/icons/Create';
import CancelIcon from '@material-ui/icons/Cancel';
import WebIcon from '@material-ui/icons/Web';
import LocalAtmIcon from '@material-ui/icons/LocalAtm';

class Confirmorder extends Component {
    constructor() {
        super();
        this.state = {
            load: true,data_menucoffee:[],wording: [],
            param_orderIdcode:``, param_transactionId:``,
            duplicatePayment:false , successPayment:false
        }
    }
    componentDidMount() {
        this.start_page(this.props);
    }

    start_page = async(props)=>{
        var duplicatePayment = false , successPayment = false;
        var wording = await props.wording
        await this.setState({load : true ,wording: wording , param_orderIdcode: props.location.query.orderIdcode ,param_transactionId: props.location.query.transactionId})
        var resultPayment = await adminSendToServer_confirmOrderPayment(this.state.param_orderIdcode, this.state.param_transactionId)
        if(resultPayment.returnCode === '1172'){
            duplicatePayment = true;
        }else if(resultPayment.returnCode === '0000'){
            successPayment = true;
        }
        await this.setState({load : false ,duplicatePayment: duplicatePayment , successPayment: successPayment})
    }
    onChangeStatus =async(event , state)=>{
        await this.setState({ [state] :event.target.value})
    }

    render(){
        const load = <LinearProgress color="secondary" />
        if(this.state.wording.th !== undefined){
            if(this.state.load){
                return (<div>{load}</div>)
            }else{
                var lang = this.props.lang
                var wording = this.state.wording[this.props.lang].salepage
                var btnOnPage = (
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <center>
                                <Button
                                    startIcon={<WebIcon />}
                                    color="default"
                                    style={{backgroundColor: `blue` ,color: `white`}}
                                    onClick={funcClickLink.bind(this,`/`)}
                                >
                                    กลับไปที่หน้าทดสอบขาย
                                </Button>
                            </center>
                        </Grid>
                        {/* <Grid item xs={6}>
                            <center>
                                <Button
                                    startIcon={<LocalAtmIcon />}
                                    color="default"
                                    style={{backgroundColor: `red` ,color: `white`}}
                                    // onClick={onClose.bind(this)}
                                >
                                    ทดสอบการ refund
                                </Button>
                            </center>
                        </Grid> */}
                    </Grid>
                )
                if(this.state.duplicatePayment){
                    return (
                        <div>
                            <center><h4>รายการนี้ คุณเคยจ่ายเงินไปแล้ว</h4></center>
                            <br/>
                            {btnOnPage}
                        </div>
                    )
                }else{
                    return (
                        <div>
                            <center>
                                {/* <Image src="https://media1.tenor.com/images/c60d560067f1cf8b832f4f653249861e/tenor.gif" /> */}
                                <h4>ทำรายการสำเร็จ</h4>
                            </center>
                            <br/>
                            {btnOnPage}
                        </div>
                    )
                }
            }
        }else{
            return (<div>{load}</div>)
        }
    }
}

Confirmorder.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
};
function mapStateToProps(state) {
  return {
    lang: state.page_getLangToState,
    wording: state.getWordingStringThis
  }
}
function mapDispatchToProps(dispatch) {
  return {
      changLang: (lang) => {
          dispatch({type: 'changeLang',payload:lang})
      }
  }    
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(useStyles, { withTheme: true })(Confirmorder))