import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Divider from '@material-ui/core/Divider';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import { connect } from 'react-redux'
import LinearProgress from '@material-ui/core/LinearProgress';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import MaterialTable from 'material-table';

import useStyles  from '../../style/app.js'

//component
import {StdToastMz_error ,StdToastMz_success} from '../../component/toast/std_toast'

//service
import {convertIntToMoneyFormat,getConsoleError,generateString_string} from '../../services/std_service/getString'
import FetchData from '../../services/fetch_data/fetch'
import {adminSendToServer_sale} from '../../services/admin/sale' 

//icon
import SendIcon from '@material-ui/icons/Send';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import AddCircleIcon from '@material-ui/icons/AddCircle';

//layout column
import {column_option_selected ,column_result_sale} from '../../layouts/sale/column_option_selected'

class ResponsiveDrawer extends Component {
  constructor() {
    super();
    this.state = {
      load: true,data_menucoffee:[],wording: [],
      arrObj_coffee:[],arrObj_state:[],arrObj_option:[],
      listArrSelectOption:[],tableLoad: false, panelSaleLoad:false,tableResultSaleLoad:false,

      selCoffee_idcode:null,selState_idcode:null,selOption_idcode:null,selCoffeeState_amount:1,
      selListArrCoffee:[],selListArrState:[],selListArrOptional:[],selOption_amount:1,

      modeSelectCoffeeState: true, listArrResultSalePage:[],
    }
  }
  componentDidMount() {
    this.start_page(this.props);
  }

  start_page = async(props)=>{
    var wording = await props.wording
    this.setState({load : true ,wording: wording})
    var url =null, data= [];
    // if(arr.toString().trim() !== ``){
    //   var dataClient = await get_ip_client()
      url = `${process.env.REACT_APP_URL_API}query/menucoffee`
      try{ data.push ({"menucoffee":await FetchData(url,'menucoffee',process.env.REACT_APP_TIME_Fetch_primary)}) }catch(e){getConsoleError(e)}

      try{
        await this.setState({ 
          data_menucoffee: data.find(x=>x['menucoffee']).menucoffee[0],
          arrObj_coffee: data.find(x=>x['menucoffee']).menucoffee[0][0].coffee.filter(f=>f['enable']),
          arrObj_state: data.find(x=>x['menucoffee']).menucoffee[0][1].state.filter(f=>f['enable']),
          arrObj_option: data.find(x=>x['menucoffee']).menucoffee[0][2].option.filter(f=>f['enable']),
        })
      }catch(e){getConsoleError(e)}
    //   await this.packData();
      this.setState({load : false})
  }
  onSelectCoffeeState=async()=>{
    var findCoffee = this.state.arrObj_coffee.filter(x=>x[`coffee_idcode`] === this.state.selCoffee_idcode)
    var findState = this.state.arrObj_state.filter(x=>x[`state_idcode`] === this.state.selState_idcode)
    if(findCoffee.length > 0 && findState.length > 0){
      await this.setState({ selListArrCoffee: findCoffee[0] , selListArrState: findState[0] ,modeSelectCoffeeState: false})
    }
  }
  onChangeStatus =async(event , state)=>{
    await this.setState({ [state] :event.target.value})
  }
  onAddOptionals =async()=>{
    await this.setState({tableLoad: true})
    var nowList = this.state.selListArrOptional
    var findOptional = this.state.arrObj_option.filter(x=>x[`option_idcode`] === this.state.selOption_idcode)
    if(findOptional.length >0 && this.state.selOption_amount > 0){
      var findIndex = nowList.findIndex(t=>t[`option_idcode`] === this.state.selOption_idcode)
      if(findIndex > -1){
        nowList[findIndex].option_amoun = this.state.selOption_amount
        nowList[findIndex].totalprice = ( this.state.selOption_amount * findOptional[0].add_price )
      }else{
        findOptional[0].option_amoun = this.state.selOption_amount
        findOptional[0].totalprice = ( this.state.selOption_amount * findOptional[0].add_price )
        findOptional[0].manage = (
          <Button
            id={`btn_delete_${findOptional[0].option_idcode}`}
            startIcon={<DeleteForeverIcon />}
            color="default"
            style={{backgroundColor: `red` ,color: `white`}}
            onClick={evt => this.onDelOptionals(evt , 'delete')} 
          ></Button>
        )
        nowList.push( findOptional[0] )
      }
    }
    await this.setState({ selListArrOptional : nowList ,tableLoad: false})
  }
  onDelOptionals =async(evt,mode)=>{
    var idcodeNow = await this.getIdcode(evt.currentTarget.id , mode)
    await this.setState({tableLoad: true})
    var findIndex = this.state.selListArrOptional.findIndex(t=>t[`option_idcode`] === idcodeNow)
    if(findIndex > -1){
      var nowList = this.state.selListArrOptional
      nowList = nowList.filter(x=>x[`option_idcode`] !== idcodeNow)
      await this.setState({ selListArrOptional : nowList ,tableLoad: false})
    }
  }
  onDelOnResultSale =async(evt,mode)=>{
    var idcodeNow = await this.getIdcode(evt.currentTarget.id , mode)
    await this.setState({tableResultSaleLoad: true})
    var findIndex = this.state.listArrResultSalePage.findIndex(t=>t[`idcode_frontend`] === idcodeNow)
    if(findIndex > -1){
      var nowList = this.state.listArrResultSalePage
      nowList = nowList.filter(x=>x[`idcode_frontend`] !== idcodeNow)
      await this.setState({ listArrResultSalePage : nowList ,tableResultSaleLoad: false})
    }
  }
  getIdcode =(id,mode)=>{
    var result = id.replace(`btn_${mode}_`,'')
    return result
  }
  set_submitLoad =(val)=>{
    this.setState({ load: val })
  }
  onAddToResultSale=async()=>{
    var totalPriceOption = 0
    var nameOptionals_th = `` ,nameOptionals_en = ``
    if(this.state.selListArrOptional.length > 0){
      this.state.selListArrOptional.map(k=>{
        totalPriceOption += (k.add_price * k.option_amoun)
        nameOptionals_th += `\r\n  + ${k.option_name[0].th} (${k.add_price}) x ${k.option_amoun}`
        nameOptionals_en += `\r\n  + ${k.option_name[1].en} (${k.add_price}) x ${k.option_amoun}`
      })
    }
    await this.onSelectCoffeeState()
    var dataRow = this.state.listArrResultSalePage
    var str = generateString_string(20)
    var main_menu_price = this.state.selListArrCoffee.price + this.state.selListArrState.add_price
    var productName = [
      {'th':`${this.state.selListArrCoffee.coffee_name[0].th} ${this.state.selListArrState.state_name[0].th} (${main_menu_price}) x ${this.state.selCoffeeState_amount} ${nameOptionals_th}`},
      {'en':`${this.state.selListArrCoffee.coffee_name[1].en} ${this.state.selListArrState.state_name[1].en} (${main_menu_price}) x ${this.state.selCoffeeState_amount} ${nameOptionals_en}`},
    ]
    var totalPrice = (main_menu_price + totalPriceOption) * this.state.selCoffeeState_amount
    var btnDelete = (
      <Button
        id={`btn_delete_${str}`}
        startIcon={<DeleteForeverIcon />}
        color="default"
        style={{backgroundColor: `red` ,color: `white`}}
        onClick={evt => this.onDelOnResultSale(evt , 'delete')} 
      ></Button>
    )
    var formatObjArr = {
      "coffee_idcode":this.state.selCoffee_idcode,
      // "coffee_dataList":this.state.selListArrCoffee,
      "product_name":productName,
      "state_idcode":this.state.selState_idcode,
      // "state_dataList":this.state.selListArrState,
      "option":this.state.selListArrOptional,
      "amount":this.state.selCoffeeState_amount,
      "idcode_frontend":str,
      "manage":btnDelete,
      "totalprice_frontend":totalPrice
    }
    dataRow.push(formatObjArr)
    await this.setState({ listArrResultSalePage: dataRow })
    await this.resetStateNewalCoffee()
    await this.setState({ tableLoad: false, panelSaleLoad:false, tableResultSaleLoad:false })
  }
  resetStateNewalCoffee=async()=>{
    await this.setState({
      selCoffee_idcode:null,selState_idcode:null,selOption_idcode:null,selCoffeeState_amount:1,
      selListArrCoffee:[],selListArrState:[],selListArrOptional:[],selOption_amount:1,
      tableLoad: true,panelSaleLoad: true, tableResultSaleLoad:true
    })
  }
  onSendToServer=async()=>{
    var res={
      "order_list":this.state.listArrResultSalePage
    }
    adminSendToServer_sale(res , this.set_submitLoad)
    await this.setState({listArrResultSalePage:[]})
    await this.resetStateNewalCoffee()
    await this.setState({ tableLoad: false, panelSaleLoad:false, tableResultSaleLoad:false })
    // StdToastMz_success(`ทำรายการเสร็จแล้ว สามารถดูที่ report ได้เลยครับ`)
  }

  render(){
    const load = <LinearProgress color="secondary" />
    if(this.state.wording.th !== undefined){
      var dataTable = this.state.selListArrOptional
      var lang = this.props.lang
      var wording = this.state.wording[this.props.lang].salepage
      var panel_select_coffeeState = (
      this.state.arrObj_coffee !== [] ?
      !this.state.panelSaleLoad ?
      <div>
        <Grid container spacing={2}>
          <Grid item xs={12} lg={4}>
            <FormControl variant="outlined" style={{width: '100%'}}>
              <InputLabel id="selectCoffee">{`${wording.select_coffee}`}</InputLabel>
              <Select
                labelId="selectCoffee"
                onChange={(e)=>this.onChangeStatus(e ,`selCoffee_idcode`)}
                defaultValue={null}
                label={`${wording.select_coffee}`}
                style={{width: '100%'}}
                InputProps={{ readOnly: true }}
              >
                {this.state.arrObj_coffee.map((x)=>(
                  <MenuItem key={x.coffee_idcode} value={x.coffee_idcode}>{`${x.coffee_name[lang === `th` ? 0 : 1][lang]} (${x.price} THB)`}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} lg={4}>
            <FormControl variant="outlined" style={{width: '100%'}}>
              <InputLabel id="selectState">{`${wording.select_state}`}</InputLabel>
              <Select
                  labelId="selectState"
                  onChange={(e)=>this.onChangeStatus(e ,`selState_idcode`)}
                  // defaultValue={this.state.arrObj_state[0].state_idcode}
                  label={`${wording.select_state}`}
                  style={{width: '100%'}}
                  InputProps={{ readOnly: true }}
              >
                  {this.state.arrObj_state.map((x)=>(
                  <MenuItem key={x.state_idcode} value={x.state_idcode}>{`${x.state_name[lang === `th` ? 0 : 1][lang]} (+${x.add_price} THB)`}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} lg={4}>
            <FormControl variant="outlined" style={{width: '100%'}}>
              <InputLabel id="selectcoffeestate">{`${wording.select_amount_cup}`}</InputLabel>
              <Select
                  labelId="selectcoffeestate"
                  onChange={(e)=>this.onChangeStatus(e ,`selCoffeeState_amount`)}
                  defaultValue={1}
                  label={`${wording.select_amount_cup}`}
                  style={{width: '100%'}}
                  InputProps={{ readOnly: true }}
              >
                {selectNumber()}
              </Select>
            </FormControl>
          </Grid>
        </Grid>
        <br/>
        <Grid container spacing={2}>
          <Grid item xs={6} sm={7}>
            <FormControl variant="outlined" style={{width: '100%'}}>
              <InputLabel id="selectOption">{`${wording.select_optional}`}</InputLabel>
              <Select
                  labelId="selectOption"
                  onChange={(e)=>this.onChangeStatus(e ,`selOption_idcode`)}
                  // defaultValue={this.state.arrObj_option[0].option_idcode}
                  label={`${wording.select_optional}`}
                  style={{width: '100%'}}
                  InputProps={{ readOnly: true }}
              >
                {this.state.arrObj_option.map((x)=>(
                  <MenuItem 
                    key={x.option_idcode} 
                    value={x.option_idcode}>
                    {`${x.option_name[lang === `th` ? 0 : 1][lang]} (+${x.add_price} THB)`}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={3} sm={3}>
            <FormControl variant="outlined" style={{width: '100%'}}>
              <InputLabel id="selectamount">{`${wording.select_amount_add_optional}`}</InputLabel>
              <Select
                  labelId="selectamount"
                  onChange={(e)=>this.onChangeStatus(e ,`selOption_amount`)}
                  label={`${wording.select_amount_add_optional}`}
                  defaultValue={1}
                  style={{width: '100%'}}
                  InputProps={{ readOnly: true }}
              >
                {selectNumber()}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={3} sm={2}>
            <Button
              startIcon={<AddCircleIcon />}
              color="default"
              style={{backgroundColor: `orange` ,color: `white`}}
              onClick={this.onAddOptionals.bind(this)}
            >
                {`${wording.add_optional}`}
            </Button>
          </Grid>
        </Grid>
        <br/>
        {this.state.tableLoad ? (load) :
          <Grid container spacing={2}>
            <MaterialTable
              tableRef={this.tableRef}
              align="center" 
              title={`${wording.title_list_optional}`}
              columns={column_option_selected()[lang]}
              style={{zIndex: 0 , fontSize: `medium` , width: `100%`}}
              data={dataTable}
              options={{
                paging: false,
                search: false,
                pageSize: dataTable.length,
                pageSizeOptions: [10, 100, 500],
              }}
            />
          </Grid>
        }
        <br/>
        {
          this.state.selCoffee_idcode !== null && this.state.selState_idcode !== null ?
          (<center>
            <Button
              startIcon={<AddCircleIcon />}
              color="default"
              style={{backgroundColor: `green` ,color: `white`}}
              onClick={this.onAddToResultSale.bind(this)}
            >
                {`${wording.title_add_this_list_to_result}`}
            </Button>
          </center>) :
          (<div></div>)
        }
          
        </div> :(<div>{load}</div>)
        :
        (<div>{load}</div>)
      )
    }else{
      return (<div>{load}</div>)
    }

    var panel_result_sale=(
      <Card boxShadow={3} className={`myCard_style`}>
        <center><p style={{fontSize: `2em`}}>{`${wording.title_listSaleResult}`}</p></center>
        <center><p style={{fontSize: `1.5em`}}>{`${wording.listSaleResult_calculateOnFrontEnd}`}</p></center>
        {this.state.tableResultSaleLoad ? (load) :
          <div>
            <Grid container spacing={2}>
              <MaterialTable
                tableRef={this.tableRef}
                align="center" 
                columns={column_result_sale()[lang]}
                style={{zIndex: 0 , fontSize: `medium` , width: `100%` ,whiteSpace: `pre-wrap`}}
                data={this.state.listArrResultSalePage}
                options={{
                  pageSize: this.state.listArrResultSalePage.length,
                  pageSizeOptions: [10, 100, 500],
                  showTitle: false,
                  search: false,
                  paging: false,
                }}
              />
            </Grid>
            <br/>
            <div>
              {
                this.state.listArrResultSalePage.length > 0 ?
                <center>
                  <Button
                    startIcon={<SendIcon />}
                    color="default"
                    style={{backgroundColor: `green` ,color: `white`}}
                    onClick={this.onSendToServer.bind(this)}
                  >
                      {`${wording.button_submit_summary}`}
                  </Button>
                </center>
                : (<div></div>)
              }
            </div>
          </div>
        }
      </Card>
    )
  

    if(!this.state.load && this.state.data_menucoffee !== []){
      return (
      <React.Fragment>
        <center><p class={`ponf_title_head`}>{`${wording.title}`}</p></center>
        <Grid container spacing={2}>
          <Grid item xs={12} lg={7}>
            {panel_select_coffeeState}
            <Divider/>
          </Grid>
          <Grid item xs={12} lg={5}>
            {panel_result_sale}
          </Grid>
        </Grid>
        <br/>
      </React.Fragment>)
    }else{
      return (<div>{load}</div>)
    }
  }
}

function selectNumber(){
  var res = []
  for(var i=1; i < 10 ; i++){
    res.push(
      <MenuItem key={i} value={i}>{`${i}`}</MenuItem>
    )
  }
  return res
}

ResponsiveDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
};
function mapStateToProps(state) {
  return {
    lang: state.page_getLangToState,
    wording: state.getWordingStringThis
  }
}
function mapDispatchToProps(dispatch) {
  return {
      changLang: (lang) => {
          dispatch({type: 'changeLang',payload:lang})
      }
  }    
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(useStyles, { withTheme: true })(ResponsiveDrawer))