import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import MenuItem from "@material-ui/core/MenuItem";
import { connect } from 'react-redux'
import Menu from "@material-ui/core/Menu";
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import LinearProgress from '@material-ui/core/LinearProgress';
import Button from '@material-ui/core/Button';

//icon
import FreeBreakfastIcon from '@material-ui/icons/FreeBreakfast';
import HotTubIcon from '@material-ui/icons/HotTub';
import AddBoxIcon from '@material-ui/icons/AddBox';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import AssessmentIcon from '@material-ui/icons/Assessment';
import MenuIcon from "@material-ui/icons/Menu";
import AccountCircle from "@material-ui/icons/AccountCircle";

//style
import {styleCode , styleFx} from '../../style/globe.js'
import useStyles  from '../../style/app.js'
import breadcrumbStyle from '../../style/breadcrumbs'
import '../../style/font.css'

//service
import {funcClickLink} from '../../services/link/service_link'

const drawerWidth = 220;

const styles = theme => ({
  root: {
    display: "flex"
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36
  },
  menuButtonIconClosed: {
    transition: theme.transitions.create(["transform"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    transform: "rotate(0deg)"
  },
  menuButtonIconOpen: {
    transition: theme.transitions.create(["transform"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    transform: "rotate(180deg)"
  },
  hide: {
    display: "none"
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap"
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    overflowX: "hidden",
    width: theme.spacing.unit * 7 + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing.unit * 9 + 1
    }
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    marginTop: theme.spacing.unit,
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3
  },
  grow: {
    flexGrow: 1
  }
});

class MiniDrawer extends React.Component {
  state = {
    open: false,
    anchorEl: null,
    wording:[]
  };

  componentDidMount() {
    this.start_page(this.props);
  }
  componentWillReceiveProps(props) {
    this.start_page(props);
  }

  start_page = async(props)=>{
    var wording = await props.wording
    this.setState({wording: wording})
  }

  handleDrawerOpen = () => {
    this.setState({ open: !this.state.open });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };
  handleClose = () => {
    this.setState({ anchorEl: null });
  };
  func_checngLang = (value) => {
    this.props.actionReducer('changeLang',value)
  };
  

  render() {
    const load = <LinearProgress color="secondary" />
    if(this.state.wording.th !== undefined){
      var childrenProps = this.props.children
      var lang = this.props.lang
      var wording = this.state.wording[lang].menu
      const { classes, theme } = this.props;
      const { anchorEl } = this.state;
      const open = Boolean(anchorEl);
      return (
      <div className={classes.root}>
        <style>{ [styleCode ] }</style>
        <CssBaseline />
        <AppBar
          position="fixed"
          className={classes.appBar}
          fooJon={classNames(classes.appBar, { [classes.appBarShift]: this.state.open })}
        >
          <Toolbar disableGutters={true}>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerOpen}
              className={classes.menuButton}
            >
              <MenuIcon
                classes={{
                  root: this.state.open
                    ? classes.menuButtonIconOpen
                    : classes.menuButtonIconClosed
                }}
              />
            </IconButton>
            <Typography
              variant="h6"
              color="inherit"
              className={classes.grow}
              noWrap
            >
              {/* {`sun coffee ${process.env.REACT_APP_VERSION}`} */}
            </Typography>
            <div style={{ marginRight: `10px`}}>
              <FormControl style={{ width:`100%`,color: `white`,}}>
                <InputLabel style={{ color: `white`}}>{`LANG`}</InputLabel>
                <Select
                  style={{ width:`100%` ,color: `white` , padding: `-5px`}}
                  defaultValue={lang}
                  label={`LANG`}
                  onChange={(e)=>this.func_checngLang(e.target.value)}
                >
                  {createListChoiceLang()}
                </Select>
              </FormControl>
            </div>
            {/* <div>
              <IconButton
                aria-owns={open ? "menu-appbar" : undefined}
                aria-haspopup="true"
                onClick={this.handleMenu}
                color="inherit"
              >
                <AccountCircle />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right"
                }}
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right"
                }}
                open={open}
                onClose={this.handleClose}
              >
                <MenuItem onClick={this.handleClose}>Profile</MenuItem>
                <MenuItem onClick={this.handleClose}>My account</MenuItem>
              </Menu>
            </div> */}
          </Toolbar>
        </AppBar>
        <Drawer
          style={{zIndex: 1}}
          variant="permanent"
          className={classNames(classes.drawer, {
            [classes.drawerOpen]: this.state.open,
            [classes.drawerClose]: !this.state.open
          })}
          classes={{
            paper: classNames({
              [classes.drawerOpen]: this.state.open,
              [classes.drawerClose]: !this.state.open
            })
          }}
          open={this.state.open}
        >
          <div className={classes.toolbar} />
          <List>
            <ListItem button key={`${wording.salepage}`} onClick={funcClickLink.bind(this,`/`)}>
              <ListItemIcon>
                <MonetizationOnIcon/>
              </ListItemIcon>
              <ListItemText primary={`${wording.salepage}`} />
            </ListItem>
          </List>
          <Divider />
          <List>
            <ListItem button key={`${wording.manage_coffee}`} onClick={funcClickLink.bind(this,`/manage_coffee`)}>
              <ListItemIcon>
                <FreeBreakfastIcon/>
              </ListItemIcon>
              <ListItemText primary={`${wording.manage_coffee}`} />
            </ListItem>
            <ListItem button key={`${wording.manage_state}`} onClick={funcClickLink.bind(this,`/manage_state`)}>
              <ListItemIcon>
                <HotTubIcon/>
              </ListItemIcon>
              <ListItemText primary={`${wording.manage_state}`}/>
            </ListItem>
            <ListItem button key={`${wording.manage_optional}`} onClick={funcClickLink.bind(this,`/manage_option`)}>
              <ListItemIcon>
                <AddBoxIcon/>
              </ListItemIcon>
              <ListItemText primary={`${wording.manage_optional}`} />
            </ListItem>
          </List>
          <Divider />
          <List>
            <ListItem button key={`${wording.report}`} onClick={funcClickLink.bind(this,`/report_per_day`)}>
              <ListItemIcon>
                <AssessmentIcon/>
              </ListItemIcon>
              <ListItemText primary={`${wording.report}`} />
            </ListItem>
          </List>
        </Drawer>
        <main className={classes.content}>
          <div className={classes.toolbar} />
            <div style={{width: `100%`}}>{childrenProps}</div>
        </main>
      </div>
    );
    }else{
      return (<div>{load}</div>)
    }
  }
}

function createListChoiceLang(){
  var res= []
  res.push(<MenuItem value='th'>{`🇹🇭 TH`}</MenuItem>)
  res.push(<MenuItem value='en'>{`🇺🇸 EN`}</MenuItem>)
  return res
}

MiniDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    lang: state.page_getLangToState,
    arrOrder : state.func_actionCart,
    wording: state.getWordingStringThis,
    windows: state.getWindows
  }
}
function mapDispatchToProps(dispatch) {
  return {
    actionReducer: (type,payload) => {
      dispatch({type: type,payload:payload})
    }
  }    
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles, { withTheme: true })(MiniDrawer));
