import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Radium, {StyleRoot} from 'radium';
import Button from '@material-ui/core/Button';
import { Link } from "react-router";
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import MaterialTable from 'material-table';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Drawer from '@material-ui/core/Drawer';

//icon
import UpdateIcon from '@material-ui/icons/Update';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import CreateIcon from '@material-ui/icons/Create';

//animation
import { bounce , fadeInDown,  fadeInUp ,fadeOutDown} from 'react-animations';
import ReactVivus from 'react-vivus';

//component
import {line_title} from '../../../component/line/line'
import {card} from '../../../component/box/card'
import {StdToastMz_error ,StdToastMz_success} from '../../../component/toast/std_toast'
import {StdBtnIconOnly_upToTop, StdFabBtn_done, StdFabBtn_delete ,StdFabBtn_create} from '../../../component/button/std_button'
import {
  query_tableInCard_unitCustomerDiscount,
  query_tableInCard_unitCustomerShipping,
  query_tableInCard_unitCustomerGiftFree,
  query_tableInCard_unitCustomerBuy ,
  query_tableInCard_unitCustomerTotalPrice
} from '../../../component/table/order_summary'
import {myTextField} from '../../../component/textfield/txtfield'

//style
import useStyles  from '../../../style/app.js'
import {styleCode} from '../../../style/order_summery'

//FontAwesomeIcon
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import {faDiceD20,faUserTie,faUserInjured,faUserClock,faUser,faCity,faWarehouse,faUsers,faChessKnight,faChessPawn,faGem,faFeather,faPlane,faUsersCog,faCogs} from "@fortawesome/free-solid-svg-icons";

//materialize
import 'materialize-css';

//react-responsive-carousel
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

//service
import FetchData from '../../../services/fetch_data/fetch'
import {getStringTitle , getConsoleError ,convertIntToMoneyFormat,generateString_string} from '../../../services/std_service/getString'
import {funcClickLink} from '../../../services/link/service_link'
import { mapStateToProps , mapDispatchToProps } from '../../../services/funcState/statePropsDispatch'
import {adminSendToServer_state ,adminSendToServer_stateDelete} from '../../../services/admin/state' 
// import { TRUE } from 'node-sass';

//form
import {column_query_state} from '../../../layouts/manage/state'

class ReportPerOption extends Component {
  constructor() {
    super();
    this.state = {
      lang: null,load: true, library: 'Radium',modeIdCode: true ,
      datetime: null , payment: null , wording: [], 
      idcode:null, name_th:``,name_en:``, price: 0 ,enable: false ,disactive_state: [],
      openDrawerDelete: false,open: false,
      
      listArrStateTable:[]
    }
    this.duration = 1;
  }
  componentDidMount() {
    this.start_page(this.props);
  }
  componentWillReceiveProps(props) {
    this.start_page(props);
  }
  start_page= async(props)=>{
    this.setState({ idcode:null, name:[], price: 0 ,enable: false ,disactive_state: [] })
    var idcode = props.params.idcode
    this.setState({load:true})
    var url =null ; var data= []
    if(idcode !== null && idcode !== `` && idcode !== undefined && idcode !== `undefined`){
      await this.setState({modeIdCode: true , idcode:idcode })
    }else{
      await this.setState({modeIdCode: false})
    }
    url = `${process.env.REACT_APP_URL_API}query/state`
    try{ data.push ({"state":await FetchData(url,'state',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}

    try{ 
      await this.setState({
        listArrStateTable: data.find(x=>x[`state`]).state[0]
      })
      await this.packData();
      if(this.state.modeIdCode){
        var find = this.state.listArrStateTable.filter(x=>x[`idcode`] === this.state.idcode)
        if(find.length > 0){
          this.setState({name_th:`${find[0].state_name[0].th}`, name_en:`${find[0].state_name[1].en}`, price: find[0].add_price, enable: find[0].enable})
        }
      }
      this.setState({load:false})
    }catch(e){
        getConsoleError(e)
    }
  }

  packData =()=>{
    var listTable = []
    this.state.listArrStateTable.map(x=>{
      x.idcode = x.state_idcode
      x.name_th = x.state_name[0].th
      x.name_en = x.state_name[1].en
      x.manage = (
        <div>
          <Button
            startIcon={<UpdateIcon />}
            color="default"
            style={{backgroundColor: `blue` ,color: `white` , marginTop: `5px`}}
            onClick={funcClickLink.bind(this,`/manage_state/${x.state_idcode}`)}
          >
            manage
          </Button>
        </div>
      )
      x.price= x.add_price
      listTable.push(x)
    })
    this.setState({
      listArrStateTable: listTable
    })
  }

  onPanelManage=async(val)=>{
    await this.setState({ open: val })
  }
  onChangeStatus =async(event , state)=>{
    await this.setState({ [state] :event.target.value})
  }
  onSubmit= async()=>{
    await this.onPanelManage(false)
    await this.packObjAndSend()
  }
  onDeleteProductPanel =()=>{
    this.setState({ openDrawerDelete: true })
  }
  onDeleteProductClose =()=>{
    this.setState({ openDrawerDelete: false })
  }

  funcConfirm= async()=>{
    var idcode = this.state.idcode
    await adminSendToServer_stateDelete([idcode] , this.set_submitLoad)
  }

  packObjAndSend=async()=>{
    var idcode = `stateidcode_${generateString_string(30)}` , price = 0 ,modeCreate = true
    if(this.state.modeIdCode){
      idcode = this.state.idcode
      modeCreate = false
    }
    price = parseFloat(this.state.price)
    var res = {
      "state_idcode":`${idcode}`,
      "state_name":[
        {"th":`${this.state.name_th}`},{"en":`${this.state.name_en}`}
      ],
      "enable":this.state.enable,
      "add_price":price
    }
    adminSendToServer_state(res , this.set_submitLoad , idcode, modeCreate)
  }
  set_submitLoad =(val)=>{
    this.setState({ load: val })
  }
  render(){
    var load = <LinearProgress color="secondary" />
    var { classes, theme } = this.props;
    var styleCode2 =(".carousel{height:unset}")

    if(!this.state.load){
    // var wording = this.state.wording[this.props.lang]
    var styleFx = {
        title: {
            animation: 'x 3s',
            animationName: Radium.keyframes(fadeInDown, `fadeOutDown`)
        },
        lineUnder: {
            animation: 'x 5s',
            animationName: Radium.keyframes(fadeInUp, `fadeOutDown`)
        }
    }
    const headTitle = (
        <div>
        <style>{ [styleCode , styleCode2] }</style>
        <StyleRoot style={styleFx.title}>
            <center><h3 className={`myfont_content_headtitle`}>{`Report of optional coffee`}</h3></center>
            {
              this.state.modeIdCode ? 
                <center><h4 className={`myfont_content_headtitle`}>{`${this.state.idcode}`}</h4></center> :<></>
            }
        </StyleRoot>
        <StyleRoot style={styleFx.lineUnder}>
            {line_title(this.state.w)}
        </StyleRoot>
        </div>
    )
    // const lang = this.props.lang
    var dataRow = this.state.listArrStateTable
    if(this.state.modeIdCode){
      return (
        <React.Fragment>
          {headTitle}
          <br/>
          <Container>
            <Grid container spacing={2}>
              <Grid item xs={4} >
                {myTextField(`state name (th)`,`${this.state.name_th}`,1,false,this.onChangeStatus,`name_th`)}
              </Grid>
              <Grid item xs={4} >
                {myTextField(`state name (en)`,`${this.state.name_en}`,1,false,this.onChangeStatus,`name_en`)}
              </Grid>
              <Grid item xs={2} >
                {myTextField(`state price`,`${this.state.price}`,1,false,this.onChangeStatus,`price`)}
              </Grid>
              <Grid item xs={2} >
                <FormControl variant="outlined" style={{width: '100%'}}>
                  <InputLabel id="selectEnableMode">{`Select Enable`}</InputLabel>
                    <Select
                      labelId="selectEnableMode"
                      // onChange={evt => this.changeSelect(evt,'selectTime')}
                      onChange={(e)=>this.onChangeStatus(e ,`enable`)}
                      defaultValue={this.state.enable}
                      label={`Select Enable`}
                      style={{width: '100%' , marginTop: `5px`}}
                      InputProps={{ readOnly: true }}
                    >
                      <MenuItem key={true} value={true}>{`enable`}</MenuItem>
                      <MenuItem key={false} value={false}>{`disable`}</MenuItem>
                    </Select>
                </FormControl>
              </Grid>
            </Grid>
          </Container>
          {drawerDeleteProduct(`right` , this.state.openDrawerDelete,this.onDeleteProductClose.bind(this) , this.funcConfirm , this.onDeleteProductClose.bind(this) )}
          <StdBtnIconOnly_upToTop />
          <StdFabBtn_done funcClickSubmit={this.onSubmit.bind(this)}/>
          <StdFabBtn_delete funcClickSubmit={this.onDeleteProductPanel.bind(this)} />
        </React.Fragment>
      )
    }else{
      return (
        <React.Fragment>
          {headTitle}
          <br/>
          <Container>
            <Grid container spacing={2}>
              <MaterialTable
                tableRef={this.tableRef}
                align="center" 
                title=""
                columns={column_query_state()[`th`]}
                style={{zIndex: 0 , fontSize: `medium` , width: `100%`}}
                data={dataRow}
                options={{
                  pageSize: 10,
                  pageSizeOptions: [10, 100, 500]
                }}
              />
            </Grid>
          </Container>
          <StdBtnIconOnly_upToTop />
          <StdFabBtn_create funcClickLink={this.onPanelManage.bind(this,true)} />
            {drawerManage(
              `right`,
              this.state.open,
              this.onPanelManage.bind(this,false),
              this.onChangeStatus,
              this.onSubmit.bind(this)
            )}
        </React.Fragment>
      )
    }
    }else{
      return (<div>{load}</div>)
    }
  }
}

function drawerManage(anchor,openStatus,onClose,onChangeStatus,onSubmitCreate){
  var res = []
  res.push(
    <React.Fragment key={anchor}>
      <Drawer anchor={anchor} open={openStatus} onClose={onClose}>
        <div style={{padding: `20px`}}>
          <Grid container space={2}>
            <center><h5>{`Create new state`}</h5></center>
          </Grid>
          <Grid container space={2}>
            <Grid item xs={6}>
              {myTextField(`state name (TH)`,``,1,false,onChangeStatus,`name_th`)}
            </Grid>
            <Grid item xs={6}>
              {myTextField(`state name (EN)`,``,1,false,onChangeStatus,`name_en`)}
            </Grid>
            <Grid item xs={6}>
              {myTextField(`state price`,0,1,false,onChangeStatus,`price`)}
            </Grid>
            <Grid item xs={6}>
              <FormControl variant="outlined" style={{width: '100%'}}>
                <InputLabel id="selectEnableMode">{`Select Enable`}</InputLabel>
                  <Select
                    labelId="selectEnableMode"
                    onChange={(e)=>onChangeStatus(e ,`enable`)}
                    defaultValue={true}
                    label={`Select Enable`}
                    style={{width: '100%' , marginTop: `5px`}}
                    InputProps={{ readOnly: true }}
                  >
                    <MenuItem key={true} value={true}>{`enable`}</MenuItem>
                    <MenuItem key={false} value={false}>{`disable`}</MenuItem>
                  </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <hr/>
              <center>
              <Button
                startIcon={<CreateIcon />}
                color="default"
                style={{backgroundColor: `orange` ,color: `white`}}
                onClick={onSubmitCreate.bind(this)}
              >
                  Create
              </Button>
              </center>
            </Grid>
          </Grid>
        </div>
      </Drawer>
    </React.Fragment>
  )
  return res;
}

function drawerDeleteProduct(anchor,open,onClose , funcConfirm , funcCancel){
  var res = []
  res.push(
    <React.Fragment key={anchor}>
      <Drawer anchor={anchor} open={open} onClose={onClose}>
        <div style={{paddingLeft: `20px` , paddingRight: `20px`}}>
          <Grid container space={2}>
            <Grid item xs={12}>
              <center><h5>{`confirm for delete`}</h5></center>
              <center>
                <Button startIcon={<DoneAllIcon/>} 
                  style={{backgroundColor: `green` ,color: `white` ,marginRight: `5px`}}
                  onClick={funcConfirm}
                >
                  {`confirm`}
                </Button>
                <Button startIcon={<DeleteForeverIcon/>} 
                  style={{backgroundColor: `red` ,color: `white`}}
                  onClick={funcCancel}
                >
                  {`cancel`}
                </Button>
              </center>
            </Grid>
          </Grid>
        </div>
      </Drawer>
    </React.Fragment>
  )
  return res;
}


ReportPerOption.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
  window: PropTypes.func,
};

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(useStyles, { withTheme: true })(ReportPerOption))