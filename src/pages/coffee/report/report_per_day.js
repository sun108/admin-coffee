import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Radium, {StyleRoot} from 'radium';
import Button from '@material-ui/core/Button';
import { Link } from "react-router";
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import MaterialTable from 'material-table';
import FormControl from '@material-ui/core/FormControl';
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';

//animation
import { bounce , fadeInDown,  fadeInUp ,fadeOutDown} from 'react-animations';

//component
import {line_title} from '../../../component/line/line'
import {StdToastMz_error ,StdToastMz_success} from '../../../component/toast/std_toast'
import {StdBtnIconOnly_upToTop, StdFabBtn_done, StdFabBtn_delete ,StdFabBtn_create} from '../../../component/button/std_button'
import PieChart from '../../../component/graph/pie'
//style
import useStyles  from '../../../style/app.js'
import {styleCode} from '../../../style/order_summery'

//FontAwesomeIcon
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas , faFire ,faDonate} from "@fortawesome/free-solid-svg-icons";
import { fab , faLine , faSellcast , faInstalod } from "@fortawesome/free-brands-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";
import { faEnvelope , faPhone } from "@fortawesome/free-solid-svg-icons";

//materialize
import 'materialize-css';

//react-responsive-carousel
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

//service
import FetchData from '../../../services/fetch_data/fetch'
import {getStringTitle , getConsoleError ,convertIntToMoneyFormat,generateString_string} from '../../../services/std_service/getString'
import {myFormatDate} from '../../../services/std_service/getDate'
import {funcClickLink} from '../../../services/link/service_link'
import { mapStateToProps , mapDispatchToProps } from '../../../services/funcState/statePropsDispatch'
import {adminSendToServer_state ,adminSendToServer_stateDelete} from '../../../services/admin/state' 
// import { TRUE } from 'node-sass';

//form
import {column_report_day ,column_report_day_detail ,column_report_day_detail_optional} from '../../../layouts/report/day'

class ReportPerDay extends Component {
  constructor() {
    super();
    this.state = {
      lang: null,load: true, library: 'Radium',modeIdCode: true ,
      datetime: null , payment: null , wording: [], 
      openDrawerDelete: false,open: false,startDate:null,
      
      listArrStateSummery:[],listArrOriginalStateSummery:[],listArrSubTableSymmary:[],
      listArrGetSaleCoffee:[],listArrGetSaleOption:[],
      totalPriceCoffee:0,totalpriceOption:0,
      labelCofee:[],labelOption:[],serieCoffee:[],serieOption:[],
      total_order_count:0, 
    }
    this.duration = 1;
  }
  componentDidMount() {
    this.start_page(this.props);
  }
  componentWillReceiveProps(props) {
    this.start_page(props);
  }
  start_page= async(props)=>{
    var nowDate = myFormatDate()
    if(this.state.startDate === null){
      await this.setState({ startDate: nowDate })
    }
    this.setState({load:true})
    var url =null ; var data= []
    url = `${process.env.REACT_APP_URL_API}query/summary?date=${this.state.startDate}`
    try{ data.push ({"report":await FetchData(url,'report',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}
    url = `${process.env.REACT_APP_URL_API}getitemsale/coffee?sort_amount=desc&date=${this.state.startDate}`
    try{ data.push ({"getitemsale_coffee":await FetchData(url,'getitemsale_coffee',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}
    url = `${process.env.REACT_APP_URL_API}getitemsale/option?sort_amount=desc&date=${this.state.startDate}`
    try{ data.push ({"getitemsale_option":await FetchData(url,'getitemsale_option',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}

    try{ 
      await this.setState({
        listArrOriginalStateSummery: data.find(x=>x[`report`]).report[0][0],
        listArrGetSaleCoffee: data.find(x=>x[`getitemsale_coffee`]).getitemsale_coffee[0],
        listArrGetSaleOption: data.find(x=>x[`getitemsale_option`]).getitemsale_option[0],
      })
      await this.packData();
      this.setState({load:false})
    }catch(e){
        getConsoleError(e)
    }
  }

  packData =()=>{
    var listTable = [],labelCofee=[] ,labelOption=[],serieCoffee=[],serieOption=[],
    priceCoffee=0,priceOption=0
    // console.log(this.state.listArrStateSummery.total_list)
    this.state.listArrOriginalStateSummery.total_list.map(x=>{
      x.idcode = x.order_idcode
      x.datetime = x.order_ondatetime
      x.amountlist = x.order_amount
      x.totalprice = x.order_summary_price
      listTable.push(x)
    })
    this.state.listArrGetSaleCoffee.map(x=>{
      labelCofee.push(x.name[0].th)
      serieCoffee.push(x.amount)
      priceCoffee=x.total_price
    })
    this.state.listArrGetSaleOption.map(x=>{
      labelOption.push(x.name[0].th)
      serieOption.push(x.amount)
      priceOption=x.total_price
    })
    this.setState({
      listArrStateSummery: listTable,
      serieCoffee:serieCoffee ,serieOption:serieOption,
      labelCofee: labelCofee, labelOption:labelOption,
      totalPriceCoffee:priceCoffee ,totalpriceOption: priceOption
    })
  }

  set_submitLoad =(val)=>{
    this.setState({ load: val })
  }
  onSetStateSubTable = async(dataSet)=>{
    await this.setState({ listArrSubTableSymmary: [] })
    console.log(dataSet)
    dataSet.map(g=>{
      console.log(g)
    })
  }
  onChangeStatus =async(event , state)=>{
    await this.setState({ [state] :event.target.value})
  }

  onStartFetch=async()=>{
    this.start_page(null);
  }
  render(){
    var load = <LinearProgress color="secondary" />
    var { classes, theme } = this.props;
    const ex = `.carousel{ height:unset } .rb{ left: 0px; } .radiusCard{ border-radius: 10px; padding: 10px; }`
    var styleCode2 =(".carousel{height:unset}")

    if(!this.state.load){
    // var wording = this.state.wording[this.props.lang]
    var styleFx = {
        title: {
            animation: 'x 3s',
            animationName: Radium.keyframes(fadeInDown, `fadeOutDown`)
        },
        lineUnder: {
            animation: 'x 5s',
            animationName: Radium.keyframes(fadeInUp, `fadeOutDown`)
        }
    }
    const headTitle = (
        <div>
        <style>{ [styleCode , styleCode2] }</style>
        <StyleRoot style={styleFx.title}>
            <center><h3 className={`myfont_content_headtitle`}>{`Report of day`}</h3></center>
        </StyleRoot>
        <StyleRoot style={styleFx.lineUnder}>
            {line_title(this.state.w)}
        </StyleRoot>
        </div>
    )
    // const lang = this.props.lang
    var dataRow = this.state.listArrStateSummery
    var dataRowOri = this.state.listArrOriginalStateSummery
    return (
      <React.Fragment>
        <style>{ex}</style>
        {headTitle}
        <br/>
        <Container>
          <Card boxShadow={3} className={`myCard_style`}>
            <h4 className={`myCard_style_title`}>เลือกวันที่แสดง Report ของวัน</h4>
            <Grid item container space={2}>
              <Grid item xs={10}>
                <FormControl style={{ width:`100%`,padding:`10px`}}>
                  <TextField
                      id="date"
                      label="วันที่เริ่มต้นการแสดงผล"
                      type="date"
                      format={'DD/MM/YYYY'}
                      onChange={(e)=>this.onChangeStatus(e,`startDate`)}
                      defaultValue={this.state.startDate}
                      InputLabelProps={{
                        shrink: true,
                      }}
                  />
                </FormControl>
              </Grid>
              <Grid item xs={2}>
                  <Button style={{backgroundColor: `green`,color:`white`,width:`100%`}} 
                    onClick={this.onStartFetch.bind(this)}>QUERY</Button>
              </Grid>
            </Grid>
            <Grid item container space={2} style={{fontSize: `1.5em`}}>
              <p>{`*** report จะไม่เอาราคาของปัจจุบันมาคำนวน ตอนขายจะบันทึกตามรายการจริง ณ วันที่ขาย`}</p>
            </Grid>
          </Card>
          <br/>
          {queryGraph( dataRowOri.total_order_count , dataRowOri.total_order_amount , dataRowOri.total_summary_price )}
          {queryApexChart(this.state.labelCofee,this.state.serieCoffee,this.state.labelOption,this.state.serieOption)}
          <br/>
          <Grid container spacing={2}>
            <MaterialTable
              tableRef={this.tableRef}
              align="center" 
              title="รายการ order ทั้งหมดของวัน"
              columns={column_report_day()[`th`]}
              style={{zIndex: 0 , fontSize: `medium` , width: `100%`}}
              data={dataRow}
              options={{
                pageSize: dataRow.length,
                pageSizeOptions: [10, 100, 500],
                headerStyle: {
                  backgroundColor: '#01579b',
                  color: '#FFF'
                }
              }}
              detailPanel={rData => {
                // console.log(rData)
                return (
                  <React.Fragment>
                    <MaterialTable
                      tableRef={this.tableRef}
                      align="center" 
                      title="รายการสินค้าใน order นี้"
                      columns={column_report_day_detail()[`th`]}
                      style={{zIndex: 0 , fontSize: `medium` , width: `100%` ,padding: `1em`}}
                      data={rData.order_list}
                      options={{
                        pageSize: rData.order_list.length,
                        pageSizeOptions: [10, 100, 500],
                        rowStyle: {
                          backgroundColor: '#EEE',
                        },
                        headerStyle: {
                          backgroundColor: '#EEE',
                        },
                        search: false
                      }}
                      detailPanel={rDataOption => {
                        console.log(rDataOption)
                        return (
                          <React.Fragment>
                            <MaterialTable
                              tableRef={this.tableRef}
                              align="center" 
                              title="รายละเอียดของ optional"
                              columns={column_report_day_detail_optional()[`th`]}
                              style={{zIndex: 0 , fontSize: `medium` , width: `100%`,padding: `1em`}}
                              data={rDataOption.option[0]}
                              options={{
                                pageSize: rDataOption.option[0].length,
                                pageSizeOptions: [10, 100, 500],
                                rowStyle: {
                                  backgroundColor: '#666633',
                                  color: '#FFF'
                                },
                                headerStyle: {
                                  backgroundColor: '#666633',
                                  color: '#FFF'
                                },
                                search: false
                              }}
                            />
                          </React.Fragment>
                        )
                      }}
                    />
                  </React.Fragment>
                )
              }}
            />
          </Grid>
        </Container>
        <StdBtnIconOnly_upToTop />
      </React.Fragment>
    )
    }else{
      return (<div>{load}</div>)
    }
  }
}

function queryGraph(order , list , price){
  return (
    <div>
      <Grid container space={2} >
        {boxInDashboard( `green` , faSellcast , `จำนวน Order: ` , `${order} Order` )}
        {boxInDashboard( `red` , faFire , `จำนวน List: ` , `${list} List` )}
        {boxInDashboard( `blue` , faInstalod , `รวมราคาทั้งหมด: ` , `${price} THB` )}
      </Grid>
    </div>
  )
}
function queryApexChart(labelCoffee,serieCofee,labelOption,serieoption){
  return (
    <div>
      <Grid container space={2} >
        <Grid item xs={6}>
          <PieChart
            series={serieCofee}
            labels={labelCoffee}
          />
        </Grid>
        <Grid item xs={6}>
          <PieChart
            series={serieoption}
            labels={labelOption}
          />
        </Grid>
      </Grid>
    </div>
  )
}
function boxInDashboard(color , icon , title , value){
  return (
    <Grid item xs={4} style={{padding: `10px`}}>
      <div className={`radiusCard`} style={{ border: `0.5px solid ${color}` ,height: `100px` , width: `100%` , boxShadow: `1px 1px 1px`}}>
        <Grid container>
          <Grid item xs={4}>
            <center>
              <FontAwesomeIcon icon={icon} style={{color: color , fontSize: `xxx-large`}} /> 
              {/* <CancelIcon style={{color: `green` , fontSize: `xxx-large`}} /> */}
            </center>
          </Grid>
          <Grid item xs={8}>
            <p className={`myFontDashBoard`} style={{fontSize: `larger` , textAlign: `end` , margin: `0 0 0px`}}>{`${title}`}</p>
            <p className={`myFontDashBoard`} style={{fontSize: `x-large`, textAlign: `end`, margin: `0 0 0px` , fontWeight: `bold` , color: color}}>{`${value}`}</p>
          </Grid>
        </Grid>
        <Divider/>
      </div>
    </Grid>
  )
}

ReportPerDay.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
  window: PropTypes.func,
};

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(useStyles, { withTheme: true })(ReportPerDay))