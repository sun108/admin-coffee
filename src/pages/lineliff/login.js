import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Divider from '@material-ui/core/Divider';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import { connect } from 'react-redux'
import LinearProgress from '@material-ui/core/LinearProgress';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import MaterialTable from 'material-table';
import liff from '@line/liff'
// import { LineLogin } from 'reactjs-line-login';
import ExampleComponent from "react-rounded-image";

import useStyles  from '../../style/app.js'

//component
import {StdToastMz_error ,StdToastMz_success} from '../../component/toast/std_toast'

//service
import {convertIntToMoneyFormat,getConsoleError,generateString_string} from '../../services/std_service/getString'
import FetchData from '../../services/fetch_data/fetch'
import {adminSendToServer_sale} from '../../services/admin/sale' 

//icon
import SendIcon from '@material-ui/icons/Send';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import DeleteForever from '@material-ui/icons/DeleteForever'
import CancelIcon from '@material-ui/icons/Cancel';
import ShareIcon from '@material-ui/icons/Share';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import AddCircleIcon from '@material-ui/icons/AddCircle';

//css
// import 'reactjs-line-login/dist/index.css';

//layout column
import {column_option_selected ,column_result_sale} from '../../layouts/sale/column_option_selected'

class PageLineLiff extends Component {
  constructor() {
    super();
    this.state = {
      load: true, isLineApp:false, isLogin:false,
      os:null,idToken:'',
      
      profile:[],dataDecode:[],
      profileWeb:[],dataDecodeWeb:[],

      location_lat:0 ,location_long:0,
    }
  }
  componentDidMount() {
    this.start_page();
  }

  start_page = async(props)=>{
    var idToken='',os=null ,profile=[] ,dataDecode=[];
    await this.getLocation()
    await liff.init({ liffId: '1655994242-3VeEP72Q' }, async() => {
      var isLineApp = await liff.isInClient()
      console.log(isLineApp)
      await liff.ready.then(async()=>{
        if(isLineApp){
          console.log('mobile app')
          os = await liff.getOS()          
          // idToken = await liff.getIDToken()
          profile = await liff.getProfile()
          dataDecode = await liff.getDecodedIDToken()
          await this.setState({os :os , profile:profile ,dataDecode:dataDecode ,isLineApp: true ,idToken:idToken ,load : false})
        }else{
          console.log('web app')
          // await liff.init({ liffId: '1655994242-3VeEP72Q' }, async() => {
            if (liff.isLoggedIn()) {
              console.log(`now login`)
              await this.runAppLogInWeb();
            } else {
              console.log(`login please`)
              liff.login();
            }
          // }, err => console.error(err));
        }
      })
    }, err => console.error(err));
  }

  runAppLogInWeb = async()=> {
    var idToken='',os=null ,profile=[] ,dataDecode=[];
    // idToken = await liff.getIDToken()
    os = await liff.getOS()   
    profile = await liff.getProfile()
    dataDecode = await liff.getDecodedIDToken()
    console.log(dataDecode)
    console.log(profile)
    await this.setState({os :os ,profileWeb:profile ,dataDecodeWeb:dataDecode ,idToken:idToken ,load : false})
  }

  getLocation = async()=>{
    await navigator.geolocation.getCurrentPosition(
      this.successCallback_location, 
      this.errorCallback_location,  
      {timeout: 10000}
    );
  }

  successCallback_location=async(position)=>{
    console.log(`get location success`)
    await this.setState({
      location_lat: position.coords.latitude,
      location_long: position.coords.longitude
    })
  }
  errorCallback_location=async()=>{
    console.log(`get location fail`)
  }

  getCoordianates =async(position)=>{
    await this.setState({
      location_lat: position.coords.latitude,
      location_long: position.coords.longitude
    })
  }

  onClick_closeLiff =async()=>{
    await liff.closeWindow()
  }

  onClick_logOut =async()=>{
    if (liff.isLoggedIn()) {
      await liff.logout();
      await liff.closeWindow()
    }    
  }

  onClick_shreMSG =async()=>{
    const res = await liff.shareTargetPicker([
      {
        type: "text",
        text: "message shared by liff"
      }
    ])
    if(res){
      await liff.closeWindow()
    }else{
      alert('share was cancelled by user')
    }
  }

  // setPayload =(res)=> { console.log(res); this.setState({payload:res}) }
  // setIdToken =(res)=> { console.log(res); this.setState({idToken:res}) }

  render(){
    const load = <LinearProgress color="secondary" />
    const style_webApp = {fontWeight: `bold`,color: `green`};
    return (
        <div>
            <center><h4>{`line login`}</h4></center>
            {
              this.state.load === false
              ? this.state.isLineApp === true
                ? this.state.profile !== [] && this.state.dataDecode !== []
                  ?  (<div>
                      <center><p style={style_webApp}>{`this page open on line app`}</p></center>
                      {rander_profile_liff(
                        this.state.os,
                        this.state.profile, 
                        this.state.dataDecode ,
                        this.state.location_lat ,
                        this.state.location_long ,
                        this.onClick_closeLiff.bind(),
                        this.onClick_shreMSG.bind(),
                        this.onClick_logOut.bind()
                      )}
                    </div>)
                  : <div>{`please login`}</div>
                : this.state.profileWeb !== [] && this.state.dataDecodeWeb !== []
                  ? (<div>
                      <center><p style={style_webApp}>{`this page open on web browser`}</p></center>
                      {rander_profile_web(
                        this.state.os,
                        this.state.profileWeb, 
                        this.state.dataDecodeWeb ,
                        this.state.location_lat ,
                        this.state.location_long ,
                      )}  
                    </div>)
                  : <div><center>
                      <p style={style_webApp}>{`this page open on web browser`}</p>
                      <p style={style_webApp}>{`... please login ...`}</p>
                      {/* <LineLogin
                        clientID='1655994242'
                        clientSecret='e8f37b9369eb31e5e7cdb960f5d15949'
                        state='GseN02oHvgDMXgRAHiM8VZ0uIQ9YRmHXJz7DUMWG3dJTidt2Vd'
                        nonce='chalermpon'
                        redirectURI='https://suncoffeeadmin.chalermpon.com/lineliff'
                        scope='profile openid email'
                        setPayload={this.setPayload}
                        setIdToken={this.setIdToken}
                      /> */}
                    </center></div>
              : <div><center><p style={style_webApp}>{`... loading ...`}</p></center></div>
            }
        </div>
    )
  }
}

function rander_profile_web(os ,profile ,dataDecode ,lat,long,){
  return (
    <div>
      <center>
        <p>{`OS : ${os}`}</p>
        <p>{`userId : ${profile.userId}`}</p>
        <p>{`disPlay name : ${profile.displayName}`}</p>
        <p>{`status profile : ${profile.statusMessage}`}</p>
        <p>{`email : ${dataDecode.email}`}</p>
        <p>{`lat : ${lat}`}</p>
        <p>{`long : ${long}`}</p>
        <ExampleComponent 
          image={profile.pictureUrl}
          imageWidth="150" 
          imageHeight="150" 
          roundedColor="#321124" 
          roundedSize="13" 
        />
      </center>
    </div>
  )
}

function rander_profile_liff(os ,profile ,dataDecode , lat,long, funCloseLiff , funcShareMSG ,funcLogOut){
  return (
    <div>
      <center>
        <p>{`OS : ${os}`}</p>
        <p>{`userId : ${profile.userId}`}</p>
        <p>{`disPlay name : ${profile.displayName}`}</p>
        <p>{`status profile : ${profile.statusMessage}`}</p>
        <p>{`email : ${dataDecode.email}`}</p>
        <p>{`lat : ${lat}`}</p>
        <p>{`long : ${long}`}</p>
        <ExampleComponent 
          image={profile.pictureUrl}
          imageWidth="150" 
          imageHeight="150" 
          roundedColor="#321124" 
          roundedSize="13" 
        />
      </center>
      <Divider style={{marginTop: 5}}/>
      <Grid container spacing={2} style={{marginTop: 20}}  alignItems="center" justify="center">
        <Grid item sm={6} alignItems="center" justify="center">
          <center>
            <Button
              variant="contained"
              color="primary"
              startIcon={<ShareIcon />}
              style={{backgroundColor: 'blue' , display: 'inline'}}
              onClick={funcShareMSG}
            >
              Share MSG
            </Button>
          </center>
        </Grid>
        <Grid item sm={6} alignItems="center" justify="center">
          <center>
            <Button
              variant="contained"
              color="primary"
              startIcon={<CancelIcon />}
              style={{backgroundColor: 'red' , display: 'inline'}}
              onClick={funCloseLiff}
            >
              Close liff
            </Button>
          </center>
        </Grid>
      </Grid>
      <Grid container spacing={2} style={{marginTop: 10}}  alignItems="center" justify="center">
        <Grid item sm={12} alignItems="center" justify="center">
          <center>
            <Button
              variant="contained"
              color="primary"
              startIcon={<ExitToAppIcon />}
              style={{backgroundColor: 'red' , display: 'inline'}}
              onClick={funcLogOut}
            >
              LogOut
            </Button>
          </center>
        </Grid>
      </Grid>
    </div>
  )
}

function rander_please_login(funcLogin){
  return (
    <div>
      <center>
        <Button
          variant="contained"
          color="primary"
          startIcon={<ShareIcon />}
          style={{backgroundColor: 'blue' , display: 'inline'}}
          onClick={funcLogin}
        >
          Share MSG
        </Button>
      </center>
    </div>
  )
}

PageLineLiff.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
};
function mapStateToProps(state) {
  return {
    lang: state.page_getLangToState
  }
}
function mapDispatchToProps(dispatch) {
  return {
      changLang: (lang) => {
          dispatch({type: 'changeLang',payload:lang})
      }
  }    
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(useStyles, { withTheme: true })(PageLineLiff))