import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux'
import liff from '@line/liff'
import SyncLoader from "react-spinners/SyncLoader";
// import { LineLogin } from 'reactjs-line-login';
import ExampleComponent from "react-rounded-image";

import useStyles  from '../../style/app.js'

//services
import {authorize} from '../../services/line/authorize'

class PageLineLiffInto extends Component {
  constructor() {
    super();
    this.state = {

      load: true, isLineApp:false, isLogin:false,
      os:null,idToken:'',
      
      profile:[],dataDecode:[],
      profileWeb:[],dataDecodeWeb:[],

      location_lat:0 ,location_long:0,

    }
  }
  componentDidMount() {
    this.start_page();
  }

  start_page = async(props)=>{
    var idToken='',os=null ,profile=[] ,dataDecode=[], url =null ;
    this.setState({load:true})
    // await this.getLocation()
    await liff.init({ liffId: `${process.env.REACT_APP_LINE_LIFF_ID}` }, async() => {
      var isLineApp = await liff.isInClient()
      // console.log(isLineApp)
      await liff.ready.then(async()=>{
        if(isLineApp){
          console.log('mobile app')
          os = await liff.getOS()
          // idToken = await liff.getIDToken()
          profile = await liff.getProfile()
          dataDecode = await liff.getDecodedIDToken()
          await this.func_authorize(this.state.profile.userId)
          await this.setState({os :os , profile:profile ,dataDecode:dataDecode ,isLineApp: true ,idToken:idToken ,load : false})
        }else{
          console.log('web app')
            if (liff.isLoggedIn()) {
              console.log(`now login`)
              await this.runAppLogInWeb();
            } else {
              console.log(`login please`)
              liff.login();
            }
        }
      })
    }, err => console.error(err));
  }
  runAppLogInWeb = async()=> {
    var idToken='',os=null ,profile=[] ,dataDecode=[];
    // idToken = await liff.getIDToken()
    os = await liff.getOS()   
    profile = await liff.getProfile()
    dataDecode = await liff.getDecodedIDToken()
    // console.log(dataDecode)
    // console.log(profile)
    await this.func_authorize(profile.userId)
    await this.setState({os :os ,profileWeb:profile ,dataDecodeWeb:dataDecode ,idToken:idToken ,load : false})
  }

  func_authorize= async(userId)=>{
    var resultAuthorize = await authorize(JSON.stringify({
      "lineIdCode":`${userId}`
    }))
    if(resultAuthorize.data.length > 0){
      console.log(resultAuthorize.data[0])
    }
  }

  render(){
    const style_webApp = {fontWeight: `bold`,color: `green`};
    return (
      <div>
          <center><p>{`LINE CHECK AUTHORIZE`}</p></center>
          {
            this.state.load === false
            ? <div></div>
            : <div>
                <center>
                  <p style={style_webApp}>
                    <SyncLoader 
                      color={`red`} 
                      loading={true} 
                      // css={override} 
                      size={10} 
                    />
                  </p>
                </center>
              </div>
          }
      </div>
    )
  }
}

PageLineLiffInto.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
};
function mapStateToProps(state) {
  return {
    lang: state.page_getLangToState
  }
}
function mapDispatchToProps(dispatch) {
  return {
      changLang: (lang) => {
          dispatch({type: 'changeLang',payload:lang})
      }
  }    
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(useStyles, { withTheme: true })(PageLineLiffInto))