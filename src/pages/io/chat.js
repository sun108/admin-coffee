import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Divider from '@material-ui/core/Divider';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux'
import io from "socket.io-client";
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import useStyles  from '../../style/app.js'

let socket;
// const CONNECTION_PORT = "http://192.168.1.94:9930";
const CONNECTION_PORT = 'https://api.chalermpon.com'
// const CONNECTION_PORT = process.env.REACT_APP_URL_SOCKET_IO;

class PageClintChat extends Component {
  constructor() {
    super();
    this.state = {
      load: true,
      loggedIn:false,
      room:`room_01`,userName:``,message:``,messageList:[],
      messageFromServer:``,data_all_room:[],
      mySessionSocket:null, anotherId:``, anotherUsername:``,def_color:`#000000`,
    }
  }
  componentDidMount() {
    console.log(CONNECTION_PORT)
    socket = io(CONNECTION_PORT, {
      'reconnection delay' : 0 ,
      'reopen delay' : 0 ,
      'force new connection' : true ,
      transports: ['websocket']
    });
    socket.on('returnDrawerResponse', function(message) {
      console.log('id', message)
    });
    socket.on('return_pleaseChangeColor',async (data) => {
      console.log(`return_pleaseChangeColor ${data.color}`)
      await this.setState({def_color:data.color})
    });
    socket.on("receive_message", async (data) => {
      var socketData = this.state.messageList;
      socketData.push(data)
      await this.setState({ messageList: socketData})
    });
    socket.on("return_data_client", async (data) => {
      await this.setState({ messageFromServer: data.data })
      console.log(data)
    });
    socket.on("return_disconnect", async (data) => {
      await socket.disconnect()
      await this.setState({ loggedIn: false })
    });
    socket.on("return_getClientAllInRoom", async (data) => {
      var data = this.state.data_all_room
      data.push(data)
      await this.setState({ data_all_room: data })
      console.log(this.state.data_all_room)
    });
    socket.on("return_mySocketId", async (data) => {
      await this.setState({ mySessionSocket : data })
    });
  }
  connectToRoom = async() => {
    await this.setState({loggedIn:true});
    await socket.emit("join_room", {'room':this.state.room,'userName':this.state.userName});
  };
  getMySeesionSocketId = async() => {
    await socket.emit("get_mySocketId" , {'userName':this.state.userName});
  };
  sendRequestControlColor =async()=>{
    await socket.emit("control_socketId_changeColor",{"socketid":this.state.anotherId});
  }
  sendRequestControlColorByUsername =async()=>{
    await socket.emit("control_socketId_changeColor_byUsername",{"socketid":this.state.anotherUsername});
  }
  getClientAllInRoom = async() => {
    let messageContent = packObjData_message(this.state.room , this.state.userName , this.state.message)
    socket.emit("getClientAllInRoom", messageContent);
  };

  funcSetState=async(value,statename)=>{
    await this.setState({ [statename]:value })
  }
  sendMessage = async () => {
    let messageContent = packObjData_message(this.state.room , this.state.userName , this.state.message)
    await socket.emit("send_message", messageContent);
    var socketData = this.state.messageList;
    socketData.push(messageContent.content)
    await this.setState({ messageList: socketData})
    await this.setState({message:''})
  };
  sendRequestToDisconnectFromServer =async()=>{
    
    await socket.emit("disconnect_from_server",``);
  }
  disconnectFromClient =async()=>{
    await socket.disconnect()
    // await socket.emit("forceDisconnect");
    // await socket.emit("disconnect");
  }
  sendRequestToServerForReturnValue =async()=>{
    let messageContent = packObjData_message(this.state.room , this.state.userName , this.state.message)
    await socket.emit("send_data_to_me_please", messageContent);
  }

  render(){
    const styleInput = {width:`100%` ,borderRadius:`10px`,border: `1px solid black`,backgroundColor: this.state.def_color ,color:`white`}
    return (
      <div className="App">
        {!this.state.loggedIn ? (
          <div className="logIn">
            <div className="inputs">
              <input
                type="text"
                placeholder="Name..."
                style={styleInput}
                onChange={(e) => {
                  this.funcSetState(e.target.value, 'userName');
                }}
              />
              <FormControl variant="outlined" style={{width: '100%'}}>
                <InputLabel id="SelectRoom">{`Select Room`}</InputLabel>
                  <Select
                    labelId="SelectRoom"
                    // onChange={evt => this.changeSelect(evt,'selectTime')}
                    onChange={(e)=>this.funcSetState(e.target.value ,`room`)}
                    defaultValue={`room_01`}
                    label={`Select Room`}
                    style={{width: '100%' , marginTop: `5px`}}
                    InputProps={{ readOnly: true }}
                  >
                    <MenuItem key={`room_01`} value={`room_01`}>{`room 01`}</MenuItem>
                    <MenuItem key={`room_02`} value={`room_02`}>{`room 02`}</MenuItem>
                  </Select>
              </FormControl>
              {/* <input
                type="text"
                placeholder="Room..."
                style={{backgroundColor: `brown`}}
                onChange={(e) => {
                  this.funcSetState(e.target.value, 'room');
                }}
              /> */}
            </div>
            <Divider  />
            <div style={{marginTop : `20px`}}>
              <button onClick={this.connectToRoom}>Enter Chat</button>
            </div>
          </div>
        ) : (
          <div className="chatContainer">
            <div className="messages">
              {this.state.messageList.map((val, key) => {
                return divMessageBox(val.author , val.message , this.state.userName)
                // return (
                //   <div
                //     className="messageContainer"
                //     id={val.author == this.state.userName ? "You" : "Other"}
                //   >
                //     <div className="messageIndividual">
                //       {`${val.author} => ${val.message}`}
                //     </div>
                //   </div>
                // );
              })}
            </div>
  
            <div className="messageInputs">
              <Grid container spacing={2}>
                <Grid item xs={9}>
                  <input
                    type="text"
                    placeholder="Message..."
                    defaultValue={`${this.state.message}`}
                    style={styleInput}
                    onChange={(e) => {
                      this.funcSetState(e.target.value, 'message');
                    }}
                  />
                </Grid>
                <Grid item xs={3}>
                  <FormControl variant="outlined" style={{width: '100%'}}>
                    <InputLabel id="sendOption">{`Send Option`}</InputLabel>
                      <Select
                        labelId="sendOption"
                        // onChange={evt => this.changeSelect(evt,'selectTime')}
                        onChange={(e)=>this.funcSetState(e.target.value ,`room`)}
                        defaultValue={`room`}
                        label={`Select Enable`}
                        style={{width: '100%' , marginTop: `5px`}}
                        InputProps={{ readOnly: true }}
                      >
                        <MenuItem key={`room`} value={`room`}>{`send to room`}</MenuItem>
                        <MenuItem key={`client`} value={`client`}>{`send to client`}</MenuItem>
                      </Select>
                  </FormControl>
                </Grid>
              </Grid>
              <Divider  />
              <Grid container spacing={2} justify={`center`}>
                <Grid item xs={12}>
                  <input
                    type="text"
                    placeholder="Another ID.."
                    defaultValue={`${this.state.anotherId}`}
                    style={styleInput}
                    onChange={(e) => {
                      this.funcSetState(e.target.value, 'anotherId');
                    }}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={2} justify={`center`}>
                <Grid item xs={12}>
                  <input
                    type="text"
                    placeholder="Another UserName.."
                    defaultValue={`${this.state.anotherId}`}
                    style={styleInput}
                    onChange={(e) => {
                      this.funcSetState(e.target.value, 'anotherUsername');
                    }}
                  />
                </Grid>
              </Grid>
              <Divider  />
              <Grid container spacing={2}>
                <p>{`message from server : ${this.state.messageFromServer}`}</p>
              </Grid>
              <Grid container spacing={2}>
                <p>{`your socket id : ${this.state.mySessionSocket}`}</p><br/>
              </Grid>
              <Grid container spacing={2}>
                <p>{`your name : ${this.state.userName}`}</p>
              </Grid>
              <Grid container spacing={2}>
                <Grid item md={2}>
                  <button onClick={this.sendMessage}>Send</button>
                </Grid>
                <Grid item md={2}>
                  <button onClick={this.sendRequestToServerForReturnValue}>Send request for return value</button>
                </Grid>
                {/* <Grid item md={2}>
                  <button onClick={this.disconnectFromClient.bind()}>Disconnect form client</button>
                </Grid> */}
                <Grid item md={2}>
                  <button onClick={this.sendRequestControlColor.bind()}>control another ID</button>
                </Grid>
                <Grid item md={2}>
                  <button onClick={this.sendRequestControlColorByUsername.bind()}>control another UserName</button>
                </Grid>
                {/* <Grid item md={2}>
                  <button onClick={this.getClientAllInRoom.bind()}>get client from room serv</button>
                </Grid> */}
                {/* <Grid item md={2}>
                  <button onClick={this.getClientAllInRoom.bind()}>get client from room serv</button>
                </Grid> */}
                {/* <Grid item md={2}>
                  <button onClick={this.sendRequestToDisconnectFromServer}>send request disconnect to server</button>
                </Grid> */}
              </Grid>
            </div>
          </div>
        )}
      </div>
    );
  }
}

function packObjData_message(room , userName , message){
  var messageContent = {
    room: room,
    content: {
      author: `from : ${userName}`,
      message: message,
    }
  }
  return messageContent
}

function divMessageBox(author , message , userName){
  var box = 
    <div
      className="messageContainer"
      id={author == userName ? "You" : "Other"}
    >
      <div className="messageIndividual">
        {`${author} => ${message}`}
      </div>
    </div>

  return box
}

PageClintChat.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
};
function mapStateToProps(state) {
  return {
    lang: state.page_getLangToState
  }
}
function mapDispatchToProps(dispatch) {
  return {
      changLang: (lang) => {
          dispatch({type: 'changeLang',payload:lang})
      }
  }    
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(useStyles, { withTheme: true })(PageClintChat))