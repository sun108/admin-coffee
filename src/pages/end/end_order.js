import React , { Component , useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme , withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Radium, {StyleRoot} from 'radium';
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid';
import QRCode from "qrcode.react";
import Hidden from '@material-ui/core/Hidden';
import Button from '@material-ui/core/Button';

//component
import {line_title ,line_order_title} from '../../component/line/line'
import {StnBtnIcon_plus , StdBtnIconOnly_upToTop} from '../../component/button/std_button'
import { findItemInThisCat , findItemInThisCollection } from '../../component/card/cardItemCat'

//style
import useStyles  from '../../style/app.js'
import {styleFx} from '../../style/globe'

//materialize
import 'materialize-css';

//react-responsive-carousel
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

//service
import {getStringTitle , getConsoleError ,generateString_string} from '../../services/std_service/getString'
import {add_arrItem} from '../../services/mockup/arrItem'
import FetchData from '../../services/fetch_data/fetch'
import {funcClickLink} from '../../services/link/service_link'

class EndOrder extends Component {
  constructor() {
    super();
    this.state = {
      open: true,mobileOpen: false,lang: null,load: true,
      idcodeOrder: null,
      imgCover:null,
      library: 'Radium',
      setBackDrop: false,
      height: 0
    }
    // this.addItemToCart = this.addItemToCart.bind(this);
    this.duration = 1;
  }
  onChangeNumber(e){
    const re = /^[0-9\b]+$/;
    if (e.target.value === '' || re.test(e.target.value)) {
       this.setState({value: e.target.value})
    }
  }
  componentDidMount() {
    this.start_page(this.props);
  }
  componentWillReceiveProps(props) {
    if(this.state.productCode !== props.params.productCode){
      this.start_page(props);
    }
    if (this.vantaEffect) this.vantaEffect.destroy()
  }
  start_page= async(props)=>{
    var idcode = props.params.idcodeOrder
    if(idcode !== null && idcode !== `undefined` && idcode !== undefined){
      this.setState({load:true , idcodeOrder: idcode , height: props.windows.height })
      var url =null ; var data= []
      
      url = `${process.env.REACT_APP_URL_API}query/collection/customer_order?orderid=${idcode}`
      try{ data.push ({"customer_order":await FetchData(url,'customer_order',process.env.REACT_APP_TIME_Fetch_primary)})  }catch(e){getConsoleError(e)}

      if(data.find(x=>x['customer_order']).customer_order[0].length > 0){
        this.props.actionReducer('clearItemInCart',{"arrItem":null , "amount":null})
        this.setState({load:false})
      }else{
        funcClickLink(`/`)
        // console.log(data.find(x=>x['customer_order']).customer_order[0])
      }
    }else{
      funcClickLink(`/`)
    }
  }
  
  render(){
    const load = <LinearProgress color="secondary" />
    if(this.state.load){
      return (<div>{load}</div>)
    }else{
      var lang = this.props.lang
      var url = `${process.env.REACT_APP_URL_THIS_PROD}tracking/${this.state.idcodeOrder}`
        return (
          <React.Fragment>
            <StyleRoot style={styleFx.title}>
              <center><h3 className={`myfont_content_headtitle`}>{`ขอบคุณค่ะ`}</h3></center>
            </StyleRoot>
            <StyleRoot style={styleFx.lineUnder}>
              {line_title(this.state.w)}
            </StyleRoot>
            <br/>
            <center>
              <QRCode 
                // data-qr={this.state.idcodeOrder} 
                value={url} 
                level={`L`}
                size={this.state.height - 500}
              />
            </center>
            <center>
              <p className={`myfont_idcode_thankyou`}>{this.state.idcodeOrder}</p>
            </center>
            <hr/>
            <Hidden only={['lg','xl','sm','md']}>
              <center>
                <p className={`myfont_content_thnakyou_xs`}>ทางเราจะดำเนินการตรวจยอดเงินที่คุณโอนเข้ามา</p>
                <p className={`myfont_content_thnakyou_xs`}>คุณสามารถ capture หน้าจอนี้ไว้เพื่อติดตาม status สินค้าได้ค่ะ</p>
                <p className={`myfont_content_thnakyou_xs`}>หรือเข้าที่ link ด้านล่างเพื่อติดตามสินค้าค่ะ</p>
                <Button className={`myfont_content_thnakyou_xs`} 
                variant="contained" color="primary" style={{color: `white`}}
                onClick={funcClickLink.bind(this,`/tracking/${this.state.idcodeOrder}`)}>
                  {`Link ติดตามสถานะสินค้า`}
                </Button>
              </center>
            </Hidden>
            <Hidden only={['xs']}>
              <center>
                <p className={`myfont_content_thnakyou_md`}>ทางเราจะดำเนินการตรวจยอดเงินที่คุณโอนเข้ามา</p>
                <p className={`myfont_content_thnakyou_md`}>คุณสามารถ capture หน้าจอนี้ไว้เพื่อติดตามสถานะสินค้าได้ค่ะ</p>
                <p className={`myfont_content_thnakyou_md`}>หรือเข้าที่ link ด้านล่างเพื่อติดตามสินค้าค่ะ</p>
                <Button className={`myfont_content_thnakyou_md`} 
                variant="contained" color="primary" style={{color: `white`}}
                onClick={funcClickLink.bind(this,`/tracking/${this.state.idcodeOrder}`)}>
                  {`Link ติดตามสถานะสินค้า`}
                </Button>
              </center>
            </Hidden>
          </React.Fragment>
        )
    }
  }
}

EndOrder.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
  window: PropTypes.func,
};
function mapStateToProps(state) {
  return {
    lang: state.page_getLangToState,
    arrCart: state.func_actionCart,
    windows: state.getWindows
  }
}
function mapDispatchToProps(dispatch) {
  return {
      actionReducer: (type,payload) => {
        dispatch({type: type,payload:payload})
      }
  }    
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(useStyles, { withTheme: true })(EndOrder))