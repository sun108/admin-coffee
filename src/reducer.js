import { combineReducers } from 'redux'
//import { fetch_api } from './event/fetchData'
import {setCookie , getCookie , removeCookie} from './services/authen/cookieService'
import {checkCookiePack_existAndNotExpire} from './services/authen/authen'
import { browserHistory , withRouter , Redirect ,Router } from 'react-router'
import {route_isNotAuthen} from './services/std_service/getString'
import {reactLocalStorage} from 'reactjs-localstorage';
import {get_all_count_value} from './services/localStorage/service_localstorage'
import {StdToastMz_error ,StdToastMz_success} from './component/toast/std_toast'
import {isInt} from './services/check_is_bool/isInt'
import {ponEncode , ponDecode } from './services/encryption/ponEncode'
import {funcClickLink} from './services/link/service_link'
import {jwt_decode_payload} from './services/authen/authen'

import { cloneDeep } from 'lodash';
import data_lang_en from './lang/en/translates.json';
import data_lang_th from './lang/th/translates.json';

const initialState = {isRejected: true, data: null}

async function authReducers(state=undefined ,action){
    var result = false
    // var result = await checkCookiePack_existAndNotExpire()
    // console.log(result)
    // if(result === false){
    //     browserHistory.push('/login')
    // }
    return  {authenticated: result}
}

function func_checkLangFromCookie(state=process.env.REACT_APP_DEFAULT_LANG_COOKIE , action){
    try{
        let getLang = getCookie(process.env.REACT_APP_VAR_LANG_COOKIE)
        if(getLang!== null && getLang!==undefined){
            state = getLang
        }else{
            setCookie(process.env.REACT_APP_VAR_LANG_COOKIE,state,process.env.REACT_APP_PATH_COOKIE)
        }
        return state
    }catch(e){
        return state
    }
}
function func_getLangToState(state=process.env.REACT_APP_DEFAULT_LANG_COOKIE, action){
    state = getCookie(process.env.REACT_APP_VAR_LANG_COOKIE)
    return state
}

function func_setLang(state=null, action){
    state = getCookie(process.env.REACT_APP_VAR_LANG_COOKIE)
    if(action.type == 'changeLang'){
        if(action.payload !== null){
            // console.log('action.payload : ' + action.payload)
            state = action.payload
            setCookie(process.env.REACT_APP_VAR_LANG_COOKIE,action.payload,process.env.REACT_APP_PATH_COOKIE)
            return state
        }else{
            return state
        }
    }else{
        return state
    }
}

function func_actionCart(state=null, action){
    var result_total_cart = 0 ,result_array_cart = [] ,result_amount_cart = [] ,act = null
    if(action.type === 'updateRefreshCart'){
        act = `refresh`
    }
    if(action.type === 'addCart'){
        act = `add`
        if(isInt(action.payload.amount)){
            var checkItemOld = localStorage.getItem(`${process.env.REACT_APP_PREFIX_ORDER}${action.payload.arrItem.idcode}`)
            if(checkItemOld === null || checkItemOld <= 0 || checkItemOld === NaN || checkItemOld === "NaN" || checkItemOld === ""){
                localStorage.setItem(`${process.env.REACT_APP_PREFIX_ORDER}${action.payload.arrItem.idcode}`, parseInt(action.payload.amount));
            }else{
                localStorage.setItem(`${process.env.REACT_APP_PREFIX_ORDER}${action.payload.arrItem.idcode}`, parseInt(checkItemOld) + parseInt(action.payload.amount));
            }
            StdToastMz_success('เพิ่มสินค้าในตะกร้าคุณแล้ว')
        }else{
            StdToastMz_error(`กรุณาใส่ตัวเลขค่ะ`)
        }
    }
    if(action.type === 'deleteItemInCart'){
        act = `del`
        var checkItemOld = localStorage.getItem(`${process.env.REACT_APP_PREFIX_ORDER}${action.payload.idcode}`)
        if(checkItemOld !== null && checkItemOld !== ""){
            localStorage.removeItem(`${process.env.REACT_APP_PREFIX_ORDER}${action.payload.idcode}`); 
            StdToastMz_success('ลบสินค้านี้ในตะกร้าแล้วค่ะ')
        }else{
            StdToastMz_error(`มีบางอย่างผิดพลาดค่ะ`)
        }
    }
    if(action.type === 'updateItemInCart'){
        act = `update`
        if(isInt(action.payload.amount)){
            var checkItemOld = localStorage.getItem(`${process.env.REACT_APP_PREFIX_ORDER}${action.payload.idcode}`)
            if(checkItemOld !== null && checkItemOld !== ""){
                localStorage.setItem(`${process.env.REACT_APP_PREFIX_ORDER}${action.payload.idcode}`, parseInt(action.payload.amount));
            }
            StdToastMz_success('แก้ไขสินค้าในตะกร้าคุณแล้ว')
        }else{
            StdToastMz_error(`มีบางอย่างผิดพลาดค่ะ`)
        }
    }
    if(action.type === 'clearItemInCart'){
        act = `clear`
        for(var i = 0 ; i < localStorage.length ; i++){
            if(localStorage.key(i).indexOf(`${process.env.REACT_APP_PREFIX_ORDER}`) === 0){
                localStorage.removeItem(localStorage.key(i)); 
            }
        }
    }
    for(var i = 0 ; i < localStorage.length ; i++){
        if(localStorage.key(i).indexOf(`${process.env.REACT_APP_PREFIX_ORDER}`) === 0){
            if(parseInt(localStorage.getItem(localStorage.key(i))) > 0 && parseInt(localStorage.getItem(localStorage.key(i))) != null && parseInt(localStorage.getItem(localStorage.key(i))) != ""){
                result_total_cart = parseInt(result_total_cart) + parseInt(localStorage.getItem(localStorage.key(i)))
                result_array_cart.push(localStorage.key(i))
                result_amount_cart.push(localStorage.getItem(localStorage.key(i)))
            }
        }
    }
    return {
        'total_cart':result_total_cart,
        'array_cart':result_array_cart,
        'amount_cart':result_amount_cart,
        'last_act': act
    };
}

async function getProfile(state=null, action){
    var data = ''
    if(action.type === `get_profile`){
        try{
            let token = await getCookie('authen')
            if(token!== null && token!==undefined){
                var result = await jwt_decode_payload(token)
                console.log(result)
                data = result
            }else{
                data = false
            }
        }catch(e){
            data = false
        }
    }
    return data
}

function getWindows(state=null , action){
    var w = window.innerWidth
    var h = window.innerHeight
    return {"width":w , "height":h}
}

function getDataInCookie_order(idcode_locaStorage){
    var res = false;
    var idcode = idcode_locaStorage.replace(`${process.env.REACT_APP_PREFIX_ORDER}`,``)
    var result = getCookie(`${process.env.REACT_APP_PREFIX_DATAROW}${idcode}`)
    if(result !== null && result !== "" && result !== `undefined` && result !== undefined){
        res = true
    }
    return res
}

async function getWordingStringThis(state=null , action){
    var data = {}
    var th = await cloneDeep(data_lang_th);
    var en = await cloneDeep(data_lang_en);
    data.th = th
    data.en = en
    return data
}

const reducers = combineReducers({
    func_checkLangFromCookie , func_setLang ,
    func_actionCart,
    page_getLangToState : func_getLangToState ,
    authReducers,
    getWindows ,
    getProfile ,
    getWordingStringThis
})


export default reducers