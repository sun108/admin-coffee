#!/bin/sh
docker stop test-sun108admin-react \
&& docker rm test-sun108admin-react \
&& docker rmi test_sun108admin:latest \
&& rm -rf /usr/chalermpon/docker-compose/sun108_admin_react/build \
&& rm -rf /usr/chalermpon/docker-compose/deployment_react_to_nginx/build \
&& docker exec -it dev-sun108_react-admin npm run build \
&& docker build -t test_sun108admin . \
&& docker run -it -d --name test-sun108admin-react \
-p 3051:80 \
--privileged \
test_sun108admin:latest && systemctl restart nginx